
clearvars;
close all;
%% VSD part
Network = 'van_zyl_norm_171';
% Network = 'Lecture_example_norm';

CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing
prob.N =12;
prob.approx_number =17;
prob.vmax_min = 5;
prob.M  = 999;
prob.settings.time_limit  = 300;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';
prob.def =  [Network '_VSD'];
prob.settings.price =  strech(EL.price{2},1); 
prob.settings.DR.status = 'Off';
prob.settings.DR.flex = 'Window1';
prob.settings.DR.desired_ratio = NaN;
prob.settings.DR.rest_period = 240; %minutes
prob.settings.DR.reward = 30000;
prob.settings.DR.demand_factor = 0.6;
prob.settings.solver_gap = 0.00;
prob.settings.solver = 'cplex';
prob.settings.time_analysis = 'Percentage';
%prob.settings.DR.recovery = 240;
%prob.settings.mods.Length_factor = 1;
% prob.settings.time_analysis = 'Time';
% 
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
% 
prob.settings.Time_shift =0;
% prob.settings.mod_schedule.status = 'No';
% prob.settings.mod_schedule.sched = [0 0 0];
% prob.settings.mod_schedule.index = 1;

prob.settings.GHG = CO2_kWh_av ;
prob.settings.limit_overfill = 0.1;

% prob.settings.h0 = [2 2];
% prob.settings.h0 = 0.036895372569841;

prob.settings.OF_spec = 'V1';
out_VSD1 = experiment(prob);

prob.settings.OF_spec = 'V2';
out_VSD2 = experiment(prob);

prob.settings.OF_spec = 'V3';
out_VSD3 = experiment(prob);

prob.settings.OF_spec = 'V4';
out_VSD4 = experiment(prob);
%% FSD
prob.approx_number =7;
prob.settings.OF_spec = 'F2';
prob.def =  [ Network '_FSD'];
out_FSD = experiment(prob);

results{1} = out_VSD1;
results{2} = out_VSD2;
results{3} = out_VSD3;
results{4} = out_VSD4;
results{5} = out_FSD;

save(['Test_run_24_2' Network])

if strcmp(prob.settings.time_analysis,'Percentage')
%     for i = 1:length(out_VSD2.time_taken); time(i) = sum(out_VSD2.time_taken(1:i));end
%     [AX,H1,H2] = plotyy(time,out_VSD2.Remaining_gap,time,out_VSD2.fval_sim_tmp);
%     H1.Marker = '*';
%     H2.Marker = '+';
%     H1.LineStyle = ':';
%     H2.LineStyle = ':';
%     AX(1).YLim = [0 1];
%     AX(2).YLim = [0 round(max(out_VSD2.fval_sim_tmp),2-length(out_VSD2.fval_sim_tmp))+500];
%     hold on

%     figure('Position',[100 100 340 340]); clear time
    figure('Position',[100 100 340 200]); clear time
    hold on
    for i = 1:length(out_VSD1.time_taken); time(i) = sum(out_VSD1.time_taken(1:i));end
    plot(time,out_VSD1.Remaining_gap,':+r'); clear time
    for i = 1:length(out_VSD2.time_taken); time(i) = sum(out_VSD2.time_taken(1:i));end
    plot(time,out_VSD2.Remaining_gap,':+b'); clear time
    for i = 1:length(out_VSD3.time_taken); time(i) = sum(out_VSD3.time_taken(1:i));end
    plot(time,out_VSD3.Remaining_gap,':+g'); clear time
    for i = 1:length(out_VSD4.time_taken); time(i) = sum(out_VSD4.time_taken(1:i));end
    plot(time,out_VSD4.Remaining_gap,':+c'); clear time
    for i = 1:length(out_FSD.time_taken); time(i) = sum(out_FSD.time_taken(1:i));end
    plot(time,out_FSD.Remaining_gap,':+k'); clear time
    
    hleg = legend('$f(q,\tau)$','$f(q, \tau, \tau^2 )$','$f(q, q^2, \tau)$','$f(q, q^2,\tau,\tau^2)$','FSD');
    hleg.Interpreter = 'Latex';
    xlabel('Time (s)','Interpreter','Latex')
    ylabel('Relative optimality gap','Interpreter','Latex')
end


    
    
%disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
% out_VSD.fval_sim/out_FSD.fval_sim