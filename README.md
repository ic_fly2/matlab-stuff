#Readme#

## Dependencies from my repo: ##
* place_matrix 
* Network tests - can be commented out if need be, but ensures networks are in the correct size and shape 
* Pump data - loads the pump data for the pumps used, otherwise specify the curve it self as polynomial 
## External dependencies ##
* CPLEX - with academic licence
* OPTI with SCIP - with academic licence

## Networks ##
* Lecture Example
	* small sample network
	* Works in all cases!
* Van Zyl Network
	* Small benchmark network
	* Works in most cases
* Richmond Network
	* not implimented yet
* Purton Network
	* works but not in final form yet


## Features that need including ##
* Epanet calling to get reservoir heads - help from Edo or Vlad required
* All network .inp files - would be very cool to have
* iteration a la Dorceto - Not really sure about implementation of this
* With a fixed starting head! - Not working for some reason
* Open source solver though opti - To reduce dependencies and enable distribution

## Research related work ##
* Permanent operation switch
* Optimal DR
* long term simulations
* moving time horizon

## Steps before world domination ##
* .inp reader
* fully open-source
	* move to python
	* open opt solver
	* Interface with epanet (modify .inp files or produce .pat files)
	



