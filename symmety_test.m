% clear all
%  [n_pumps_SYM,~] = prepPumps(pumps,curvesMap);%,fitting_type);
%  data.pumps_vec = 
 if strcmp(def,'Richmond_skeleton')
    data.pumps_vec = [2 1 1 1 1 1];
 elseif strcmp(def,'Richmond_skeleton_modified')
     data.pumps_vec = [2 1 1 1 1 1];     
 elseif strcmp(def,'van_zyl_no_station')
      data.pumps_vec = [2 1]; 
% elseif strcmp(def,'van_zyl_no_station')
%       data.pumps_vec = [2 1]; 
      
 end
 
 
     
%  N = 12;
%  n_pumps = [2 1 1 1 1 1];
A_sym = [];
for j = 1:length(data.pumps_vec);
    for i= 1:data.pumps_vec(j)-1;
%          disp(i)
         A_sym = [A_sym;zeros(N,sum(data.pumps_vec(1:j-1))*N) zeros(N,(i-1)*N) -eye(N) eye(N) zeros(N,N*(data.pumps_vec(j)-1-i)) zeros(N,sum(data.pumps_vec(j+1:end))*N)];
    end
end

if ~isempty(A_sym)
    A_cell{data.np+1} = [A_sym zeros(size(A_sym,1),(sum(data.lambda_parts_length)+data.nn+data.np)*N)];
    b_cell{data.np+1} = zeros(size(A_sym,1),1);
end

