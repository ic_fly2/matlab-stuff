function [ h_f ] = PipeHeadLossCalc(f,D,L,Q)
%PipeHeadLossCalc Computes the head loss from the Darcy friction factor
%   Uses A formula to compute the head loss in m from flowrate diameter
%   friction factor and length of pipe using an averaging model

h_f = (f*L*(Q^2))/(D*2*9.81*(0.25*D^2)^2);
end

