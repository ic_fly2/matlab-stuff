% solving the energy line for pump head to compute the required pump head
% from a given resevoir height difference for 2 resevoirs only with valves
% along the way
%% Initialisation
% pipe props
L = 3000; %m Length of pipe
D = 0.3; %m Diameter of pipe
e = 0.00005; %m equivalent roughness of pipe
A = pi*D^2 / 4;

% flow props
Q = 0.4; % m3/s desired flow rate
H_R1 = 100; % m height of resevoir water is removed from
H_R2 = 200; % m height of resevoir water is pumped to
I = imread('pump_schematic.jpg');
% fluid props
fluid ;
gamma = rho*9.81;
%% Calcs
[f,K,n,Re,regime ] = DarcyWeisbachFactorCalc(D,Q,e,L); 
h_f = f*(L/D)*(Q^2 /A^2)/19.62; % form h = KQ^n n = 2 but K also dependand on Q
D_H = H_R2 - H_R1;
h_p = h_f + D_H;
% arg = 'Q';
% regime
% pumps
% disp(regime)
% h_p
%find h_p
%% Energy grade line EL = V^2 /2g + p/gamma + z where gamma = rho * g
% assume straight line connection between resevoirs
z = linspace(H_R1,H_R2,L);
length = 1:L;
V = Q/A;
p_loss = f*(length/D)*(Q^2 /A^2)/19.62;
EL = h_p - p_loss + H_R1; %V^2/2*9.81 + P./(9.81*rho) + z;
plot(length,EL,'b',length,z,'r')


%% Ports
% find paj the pressure in the port orrifice
V_j = V;
p_j = 150;
p_aj = p_j*0.99;
k_L = 0;
err = 1;
% while err >= 0.1
%     err = p_aj - gamma* (V_j^2 / 19.62 + p_j/gamma -(k_L/19.62 + 1/19.62) *(1 /(1+k_L))*19.62*((p_j - p_aj)/gamma +V_j^2 /2)) ;
%     p_aj = p_aj - p_aj*0.1;
% end