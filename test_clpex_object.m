% % Initialize the CPLEX object
% cplex = Cplex('lpex1');
% % Now populate the problem with the data
% % Use arrays to populate the model
% cplex.Model.sense = 'maximize';
% cplex.Model.obj = [1; 2; 3];
% cplex.Model.lb = [0; 0; 0];
% cplex.Model.ub = [40; inf; inf];
% cplex.Model.A = [-1 1 1; 1 -3 1];
% cplex.Model.lhs = [-inf; -inf];
% cplex.Model.rhs = [20; 30];
% % Optimize the problem
% cplex.solve();

% clearvars -except i_d disjunctiveset Time_taken Remaining_gap emphasisset;
% load('vanzylN24_setup_complete.mat')

%% call solver alternative:


% [x,fval,exitflag,info]  = cplexmiqp(H,f,A,b,Aeq,beq,[],[],[],lb,ub,xtype,x0,options);
cplex = Cplex('lpex1');
cplex.Model.sense = 'minimize';
cplex.Model.obj = f;
cplex.Model.Q = H;

cplex.Model.lb = lb;
cplex.Model.ub = ub;
cplex.Model.A = [Aeq; A];
cplex.Model.lhs = [beq; -inf*ones(size(b))];
cplex.Model.rhs = [beq; b];
cplex.Model.ctype = xtype;
% cplex.MipStart.x = x0;
cplex.MipStart.x = x_work;

%    % Use addSOSs to add SOS
%    cplex.addSOSs('1', [2 3]', [25 18]', {'sos1(1)'});
   
   % Add Order to model
% %    [[singlefile(flipud(reshape(72:-1:25,[2,24])))] [24:-1:1]]
%    cplex.Order.ind = [1:72]';%; %	Vector containing the indicies of the variables. 
%    cplex.Order.pri = [[singlefile(flipud(reshape(72:-1:25,[2,24])))] [24:-1:1]]'; %Vector containing the priority order values of the corresponding variables. 
%    cplex.Order.dir = 0*ones(72,1); %The preferred branching direction of the corresponding variable. This will cause CPLEX first to explore the branch specified by dir(i) after branching on variable x(i). The possible values are: CPX_BRANCH_GLOBAL, CPX_BRANCH_DOWN, CPX_BRANCH_UP. 
%    cplex.writeOrder('mipex3.ord');
% %% configuration
cplex.Param.mip.tolerances.mipgap.Cur = 0.05; % optimality gap to stop at
cplex.Param.timelimit.Cur = 600; %time limit in seconds
cplex.Param.mip.strategy.variableselect.Cur = 2;%4 %pseudo = 2; strong = 3; info: http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex137.html
cplex.Param.emphasis.mip.Cur = 0; %0 and 4 work best            %emphasisset(i_e); %0:4 http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex64.html
cplex.Param.mip.strategy.nodeselect.Cur = 0;%2; % works best
%  cplex.Param.mip.lpmethod.Cur = 4;
%   cplex.Param.mip.qpmethod.Cur = 4;
%    cplex.Param.mip.strategy.startalgorithm.Cur = 4;
%    cplex.Param.mip.strategy.subalgorithm.Cur = 4;
%
%cplex.Param.mip.strategy.probe.Cur = 0; %Probing 0 or 3 (auto or aggressive)

% cplex.Param.mip.ordertype.Cur = orderset(i_o); %no influence found %0:3 %http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex67.html
 %http://www-01.ibm.com/support/knowledgecenter/SSSA5P_12.3.0/ilog.odms.cplex.help/Content/Optimization/Documentation/Optimization_Studio/_pubskel/ps_refparameterscplex2299.html
%  cplex.Param.mip.cuts.mircut.Cur  = 0; % Rounding -1 none to 2 aggressive http://www-01.ibm.com/support/knowledgecenter/SSSA5P_12.3.0/ilog.odms.cplex.help/Content/Optimization/Documentation/Optimization_Studio/_pubskel/ps_refparameterscplex2288.html
%  cplex.Param.mip.cuts.gomory.Cur = 0; % -1: 2 gomory cuts
% cplex.Param.mip.cuts.flowcovers.Cur = 0;

% cplex.Param.mip.cuts.disjunctive.Cur =0; %-1:3
%% configuration
% cplex.Param.mip.tolerances.mipgap.Cur = 0.05; % optimality gap to stop at
% cplex.Param.timelimit.Cur = 600; %time limit in seconds

% % cplex.Param.mip.ordertype.Cur = 3; Has no effect!
% % cplex.Param.mip.strategy.variableselect.Cur = varselectset(i_v); %pseudo = 2; strong = 3; info: http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex137.html
% % cplex.Param.mip.strategy.nodeselect.Cur = nodeselectset(i_n); %http://www-01.ibm.com/support/knowledgecenter/SSSA5P_12.3.0/ilog.odms.cplex.help/Content/Optimization/Documentation/Optimization_Studio/_pubskel/ps_refparameterscplex2299.html
% % cplex.Param.mip.cuts.mircut.Cur  = mircutset(i_m); % Rounding -1 none to 2 aggressive http://www-01.ibm.com/support/knowledgecenter/SSSA5P_12.3.0/ilog.odms.cplex.help/Content/Optimization/Documentation/Optimization_Studio/_pubskel/ps_refparameterscplex2288.html
% % cplex.Param.mip.cuts.gomory.Cur = gomoryset(i_g); % -1: 2 gomory cuts
% cplex.Param.mip.cuts.flowcovers.Cur = flowcoversset(i_f);
% % cplex.Param.mip.strategy.probe.Cur = 3; %probingset(i_p); % Probing 
% cplex.Param.mip.cuts.disjunctive.Cur = disjunctiveset(i_d); %-1:3

%% solve
cplex.solve();
beep
Time_taken(i_d) = cplex.Solution.time;
Remaining_gap(i_d)= cplex.Solution.miprelgap; % The remaining GAP!!!

% 
% Time_taken(i_v,i_n,i_m,i_g,i_f,i_d) = cplex.Solution.time;
% Remaining_gap(i_v,i_n,i_m,i_g,i_f,i_d)= cplex.Solution.miprelgap; % The remaining GAP!!!


% % Write the solution
% fprintf ('\nSolution status %s \n',cplex.Solution.statusstring);
% fprintf ('Solution value = %f \n',cplex.Solution.objval);
% disp ('Values = ');
% disp (cplex.Solution.x');
