function x_out = pattern_length_adjustment(x,N)
% takes a pattern x and streches or shortens it to fit length N with
% mean(x) constant. if given a matrix it modifies the number of columns
length_x = length(x);
if min(size(x)) == 1 && size(x,2) == 1
    % make column vector into row vector
    x = x';
elseif min(size(x)) > 1
    length_x = size(x,2);
    %error('Something went wrong here')
end
    
    

% check if vector or matrix

    if length_x  >  N
        % vector too long
        if rem(length_x ,N) == 0
            x_out = zeros(size(x,1),N);
            step_size = length_x /N;
            for i = 1:N
                x_out(:,i) = mean(x(:,(step_size*(i-1)+1):step_size*i),2);
            end
        else
            % strech the whole thing to the least common denominator and
            % then shrink again
            x_tmp = pattern_length_adjustment(x,lcm(length_x ,N)); 
            x_out = pattern_length_adjustment(x_tmp,N);
        end
    elseif length_x  <  N
         % strech the vector
         if rem(N,length_x ) == 0
             % just fill up the empties
             step_size = N/length_x ;
             x_out = zeros(size(x,1),N);
             for i = 1:length_x 
                  x_out(:,[(i-1)*step_size+(1:step_size)]) = x(:,i)*ones(1,step_size);
             end                 
         else
            %strech out to least common denominator 
            x_tmp = pattern_length_adjustment(x,lcm(length_x ,N)); 
            x_out = pattern_length_adjustment(x_tmp,N);
         end
             
    elseif length_x  ==  N
        % do nothing as all is ok
        x_out = x;
    else
        error('What patterns are you supplying')
    end
end