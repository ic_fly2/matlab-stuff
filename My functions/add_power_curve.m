function [p1 p2] = add_power_curve(a_quad_pump,b_quad_pump,c_quad_pump,n_pumps)
% adds the power curve to any pump :)
eta = 0.8; %overall max efficiency
for n = 1:length(n_pumps)
%     for n = 1:n_pumps(i)
        q_star = max(roots([3*a_quad_pump(n,1) 2*b_quad_pump(n,1) c_quad_pump(n,1)])); % max hydraulic power flow
        p_star = polyval([a_quad_pump(n,1) b_quad_pump(n,1) c_quad_pump(n,1)],q_star)*q_star*9.81/eta; % power at max efficiency point
        
        p0 = p_star*0.9; % at stall
        p_dash = 1.1*p_star; % at 33%more flow
        q_dash = 1.33*q_star;
        p2{n} = polyfit([0 q_star q_dash],[p0 p_star p_dash],2);
        p1{n} = polyfit([0 q_star q_dash],[p0 p_star p_dash],1);
%     end
end


%% van zyl analysis stuff
% efficiency data from .inp file
q_points=[ 50 107 151 200 ]; %LPS!!!
h_points=[ 78  80  68  60 ]; %m
p_eff = polyfit(q_points,h_points,2);
eff_fit = polyval(p_eff,linspace(0,200))/100;
%plot(q_points,h_points,linspace(0,200),eff_fit)

% flow rate data from .inp file for main pump curve 1
q_points2= [  0 120 150]; %LPS!!!
h_points2= [100 90 83];   %m
p_flow =  polyfit(q_points2,h_points2,2);
h_fit = polyval(p_flow,linspace(0,200));

power_hydro = linspace(0,200).*h_fit*9.81;
power_electro = power_hydro./eff_fit;
p_elec = polyfit(linspace(0,0.2),power_hydro,2); % for m3/s not LPS like the rest!!

plot(linspace(0,0.2),power_electro/1000)
hold on;
plot(linspace(0,0.2),h_fit)
