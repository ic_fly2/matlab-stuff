function [n_pumps,pumps] = prepPumps(pumps,curvesMap,fitting_type)
% define the set of active pumps in terms off connections and
% specifications. Select if a polynomial or power type fitting is wished:
% poly: H = aq^2 + bq + c
% power: H = c - bq^a
% poly is default and currently only option
% use pumpsOn for GGA and pumps and n_pumps for MIP implimentation

% Written by Ruben Menke
% Created: 22.01.2015 v0.1


%% nargin:

if nargin <3
    fitting_type = 'poly';
end

%% define pumps:
a_org = [];
b_org = [];
c_org = [];
q_lim = [];
pump_curve_data = [];
Power_nameplate = [];
index_of_dublicates = zeros(length(pumps));
% i = 1;

set = 1:length(pumps);
for i = 1:length(pumps);
    if ismember(i,set)
        for j = 1:length(pumps)
            if strcmp(pumps{i}.startNodeId,pumps{j}.startNodeId) && ...
                    strcmp(pumps{i}.endNodeId,pumps{j}.endNodeId) && ...
                    strcmp(pumps{i}.headCurveId,pumps{j}.headCurveId) &&...
                    index_of_dublicates(i,j) ~=1
                % we have a dublicate
                index_of_dublicates(i,j) = 1;
                %             dublicates(1,i) = j;
                %             dublicates(2,j) =i;
                set(set == j)= [];
                
            end
            pumps{i}.Unique = 'Yes';
        end
    else
        pumps{i}.Unique = 'Yes'; %'no'
    end
    %     i = i+1;
end
% index_of_dublicates = index_of_dublicates + eye(length(pumps));
n_pumps = sum(index_of_dublicates,2)';



cur_dir = cd;
cd([cur_dir(1) ':\Phd\MATLAB\Phd\Networks\'])

for i = 1:length(pumps)
    name = pumps{i}.headCurveId;
    
    %remove forbidden Characters:
    name(name == '-') = '_'; name(name == '.') = '_'; name(name == '+') = '_';
    backup = pumps{i};
    if exist([name '.mat'],'file') == 2
        % file alread exists
        pumps{i} = load(name);
        pumps{i}.Id = backup.Id;
        pumps{i}.startNodeId = backup.startNodeId;
        pumps{i}.endNodeId = backup.endNodeId;
        pumps{i}.headCurveId = backup.headCurveId;
        % unused
        pumps{i}.speed = backup.speed;
        pumps{i}.patternId = backup.patternId;
        pumps{i}.name = name;
        % Unique: 'Yes'
        
        if strcmp(pumps{i}.type,'VSD')
            coeffs = fields_rm(pumps{i}.characteristic_curves,'p3');
            pumps{i}.c = max(coeffs);
            pumps{i}.b = pumps{i}.characteristic_curves{find(coeffs== pumps{i}.c)}.p2;
            pumps{i}.a = pumps{i}.characteristic_curves{find(coeffs== pumps{i}.c)}.p1;
           
            % proxy max power
            pumps{i}.power.nameplate = max(pumps{i}.power.surf_data.P);
        end
    else
        % generate proxy data and save the file
%         switch fitting_type %inneficient but eh ok for now.
%             case 'poly'
                % polynomials:
                x = curvesMap(pumps{i}.headCurveId)';
                p = polyfit(x(1,:)/1000,x(2,:),2); %only LPS!!!!
                if min(roots(p)) > 0
                    p(1) = -p(1);
                end
                a =  p(1);
                b =  p(2);
                c =  p(3);
                q_lim =  max(roots(p));
                
                [K,Nexp,~]= PowerLawConverter(p);
                %K =-K; Nexp = Nexp;
                
                q = linspace(0,max(roots(p)));
                h = polyval(p,q);
                [power.nameplate index] = max(q.*h*9.81); % Total efficiency goes in OF
                
                % LIN POWER STUFFs
                q_series = linspace(0,q(index),5);
                q_series2 = [q(index):q_series(5)-q_series(4):4*q(index)];
                q_series2 =  q_series2(2:5); %take  only the bits you need, give nothing back
                q_series = [q_series q_series2];
                
                Pmin =  max(q.*h*9.81)*0.8;
                r = (power.nameplate/Pmin)^0.25;
                
                for ii = 1:9
                    P_series(ii) = Pmin*r^(ii-1);
                end
                
                
                %fit all this data
                power.lin_approx  = polyfit(q_series(4:6),P_series(4:6),1);
                power.quad_approx = polyfit(q_series(4:6),P_series(4:6),2);
                power.curve_data.P = P_series;
                power.curve_data.q = q_series;
                
                type ='FSD';
                
                % generate new file
                save(name,'K','Nexp','a','b','c','power','q_lim','type')
                
                % load and reassign file
                backup = pumps{i};
                pumps{i} = load(name);
                pumps{i}.Id = backup.Id;
                pumps{i}.startNodeId = backup.startNodeId;
                pumps{i}.endNodeId = backup.endNodeId;
                pumps{i}.headCurveId = backup.headCurveId;
                % unused
                pumps{i}.speed = backup.speed;
                pumps{i}.patternId = backup.patternId;
                pumps{i}.name = name;
                
%             case 'power'  %fit
%                 error('Power fitting not implimented yet')
                
        end
        
end
   
cd(cur_dir)





%
%
%
% for i = 1:length(pumps)
% %     if strcmp(pumps{i}.Unique,'Yes')  % sum(index_of_dublicates(i,:)) ~= 0 % Unique pump or first in station
% %         factor = 1;
% %     elseif strcmp(pumps{i}.Unique,'No')
% %         factor = factor +1;
% %     else
% %         error('Something went wrong here, not sure if the pump is Unique or what')
% %     end
% %
%     switch fitting_type %inneficient but eh ok for now.
%         case 'poly'
%             % polynomials:
%             x = curvesMap(pumps{i}.headCurveId)';
%             p = polyfit(x(1,:)/1000*factor,x(2,:),2); %only LPS!!!!
%             if min(roots(p)) > 0
%                 p(1) = -p(1)
%             end
%             pumps{i}.a =  p(1);
%             pumps{i}.b =  p(2);
%             pumps{i}.c =  p(3);
%             pumps{i}.q_lim =  max(roots(p));
%
%
%             [K,Nexp,~]= PowerLawConverter(p);
%             pumps{i}.K =-K;
%             pumps{i}.Nexp = Nexp;
%
%             if isempty(pumps{i}.power)
%                 q = linspace(0,max(roots(p)));
%                 h = polyval(p,q);
%                 [pumps{i}.power index] = max(q.*h*9.81); % Total efficiency goes in OF
%
%                 % LIN POWER STUFFs
%                 q_series = linspace(0,q(index),5);
%                 q_series2 = [q(index):q_series(5)-q_series(4):4*q(index)];
%                 q_series2 =  q_series2(2:5); %take  only the bits you need, give nothing back
%                 q_series = [q_series q_series2];
%
%                 Pmin =  max(q.*h*9.81)*0.8;
%                 r = (pumps{i}.power/Pmin)^0.25;
%
%                 for ii = 1:9
%                     P_series(ii) = Pmin*r^(ii-1);
%                 end
%
%
%                 %fit all this data
%                 [pumps{i}.power_lin pumps{i}.power_lin_quality] =...
%                                     polyfit(q_series(4:6),P_series(4:6),1);
%                 pumps{i}.power_quad  = polyfit(q_series(4:6),P_series(4:6),2);
%
%                 pumps{i}.power_curve_series =  polyfit(q_series,P_series,6);
%
%
%             else
%                 pumps{i}.power = pumps{i}.power*factor;
%             end
%
%         case 'power'  %fit
%             error('Power fitting not implimented yet')
%     end
% end
% %  needs to be here:
% n_pumps(n_pumps == 0) = [];
%
%









