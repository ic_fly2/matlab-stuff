function [cost,schedule,flows,pressure] = optimisePumpSchedule(Network,speed)
% Computes the optimal pump schedule for a given network and considers the
% PC processing power. 
% 
%
% [schedule,flows,pressure] = optimisePumpSchedule(Network) returns a
% schedule for a given network
%
% [schedule,flows,pressure] = optimisePumpSchedule(Network) returns a
% schedule for a given network taking into account the PC speed and
% patience of the user. Setting speed to fast gives the problem 2 minutes 
% while normal gives it 5 minutes and slow allows the solver 10 minutes. 
% Tortoise sets a limit of 1 hour.
%
%
% Example:
% [schedule,flows,pressure] = optimisePumpSchedule('Lecture_example','Fast')



if nargin == 1
    speed = 'Normal';
end

if strcmp(speed,'Fast')
    prob.N =24;
    prob.approx_number =7;
    prob.settings.OF_spec = 'F3';
    prob.settings.time_limit  = 100;
elseif strcmp(speed,'Normal')
    prob.N =24;
    prob.approx_number =7;
    prob.settings.OF_spec = 'F3';
    prob.settings.time_limit  = 300;
elseif strcmp(speed,'Slow')
    prob.N =24;
    prob.approx_number =4;
    prob.settings.OF_spec = 'F3';
    prob.settings.time_limit  = 600;
elseif strcmp(speed,'Tortoise')
    prob.N =48;
    prob.approx_number =4;
    prob.settings.OF_spec = 'F3';
    prob.settings.time_limit  = 1000;
end

CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing
prob.vmax_min = 7;
prob.M  = 9999;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';
prob.def = Network;
prob.settings.price =   strech(EL.price{2},1); 
prob.settings.DR.status = 'Off';
prob.settings.DR.flex = 'Window1';
prob.settings.DR.desired_ratio = NaN;
prob.settings.DR.rest_period = 240; 
prob.settings.DR.reward = 30000;
prob.settings.DR.demand_factor = 1;
prob.settings.solver_gap = 0.00;
prob.settings.solver = 'intlinprog';%'cplex';%
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
prob.settings.Time_shift =0;
prob.settings.GHG = CO2_kWh_av ;
prob.settings.limit_overfill = 0.1;
prob.settings.Robust.status = 'No';

out = experiment(prob);

cost = out.fval;
flows = out.flow_display;
pressure = out.head_display;
schedule = out.schedule_tmp;