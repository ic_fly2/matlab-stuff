% function[cost,outputmi,P_sced] = optimisation_fun_v9_lin_standard(demand_multiplier,switch_penatly,no_pumps,maxtime,K_P_orig,K_S_orig)
%% set pump schedule and outflow demands
clear all;close all
% Create new or run old network definition
% network_definition;

demand_multiplier = 200;
switch_penatly = 0.01;
L_p = 100; % penalty for qB (use to normalise the solution)
Qmin = 200; % Qmin^2 > Pe*P*L_p
Qmax = 100000; % larger than any possible flow

no_pumps = 2;
maxtime = 20;
% K_P_orig = [ -200 0 500]; % both modified for number of pumps and time of day
K_P_orig = [ 0 -260.7 305.77];
K_S_orig = [0 220 100];
%new stuff from system curve solver

%% Initialisation
% pump and system features
plot_on = 2;% turn ploting for all on with 1 for only final =0
% solver type
solver_type = 'QP_cplex'; % Options include 'QP_quadprog' 'MILP_matlab' and  'MIQP_cplex' use matlab solvers with the optimisation toolkit cplex requires cplex from IBM to work
% maxtime = 20;
% controller
controller = 'off'; %options are on or off in on then provide h_initial
h_initial =1;

D = 5000; %diameter of the resevoir
N = 48;
Delta_T = 24*60*60/N;
h_old(1,1:N*no_pumps) = h_initial;

%% Pump and sys curves
K_S_orig(1) = K_S_orig(1)/(1000*1000);
K_S_orig(2) = K_S_orig(2)/(1000);
for i = 1:N
    K_S(i,1) = K_S_orig(1);
    K_S(i,2) = K_S_orig(2);
    K_S(i,3) = K_S_orig(3)+h_old(i);
end

K_P_orig(1) = K_P_orig(1)/(1000*1000);
K_P_orig(2) = K_P_orig(2)/(1000);
for j = 1:no_pumps
    K_P(j,1) = K_P_orig(1,1)/(j^2);
    K_P(j,2) = K_P_orig(1,2)/j;
    K_P(j,3) = K_P_orig(1,3);
end

%% call epanet for initial settings:
for i = 1:N
    for j = 1:no_pumps
        q_old(j,i) = max(roots(K_P(j,:) - K_S(i,:)))+2.2;
    end
end


%% pump
% pump_data =  importdata('E:\Phd\Hydraulics\EPAnet\Etaline_125-160.txt');%  [40.0000   33.5000;   90.0000   33.0000;  150.0000   31.0000;  190.0000   28.9000;  250.0000   24.4000;  300.0000   19.0000];
% pump_data = pump_data.data ;
% X = pump_data(:,1); %m3/0.5h !!!
% Y = pump_data(:,2);
% K_P = polyfit(X,Y,2);

% values
% K_Pa = -0.0002;
% K_Pb = 0.0214;
% K_Pc = 32.9480;

%% system
% replaced by passing K_S directly to the function

% only one pipe (link 1) is relevant
% need to find the compunents of the system curve static and dynamic head
% loss: produce points for a polynomial and then fit tha data through

% static
% elevation =  K_S(3); %m from model


%% end of pump sys curve


%electricity pricing
% Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28;
% Pe(1:24) = Electricity_Price(1:2:48); % test case
% from Pache:
Electricity_Price(1:11) = 28.59; Electricity_Price(12:19) = 85.76; Electricity_Price(20:33) = 48.49; Electricity_Price(34:40) = 85.76; Electricity_Price(41:44) = 48.49; Electricity_Price(45:48) = 28.59;
Pe = Electricity_Price;
% demand
% x = 1:N;
% d = sin(x/(pi*1.2))*400 + 800;%d =d*180;
% demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d/mean(d)*demand_multiplier;
d = d';%m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!
% resevoir
Area = pi*(D/2)^2; %m2
% h0 = 15; % removed in new layout
h_max = 2;
h_min = 0;
reservoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping over stuff that doesn't change
% MILP vars
%A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear A
h_star(1,1) = -Area/Delta_T; h_star(1,N) = Area/Delta_T;
q_star = zeros(1,N); q_star(1) = 1;
% initger_new = [-eye(no_pumps*N) zeros(no_pumps*N,N) zeros(no_pumps*N) -eye(no_pumps*N) ]; % new formulation
A = [zeros(1,N*no_pumps) -h_star -repmat(q_star,1,no_pumps) zeros(1, N*no_pumps);...
    repmat(eye(N),1,no_pumps) zeros(N, N*(2*no_pumps + 1));... % ];  % old formulation ends here
    % 	-Qmin*eye(N*no_pumps) zeros(N*no_pumps,N) zeros(N*no_pumps) -eye(N*no_pumps);... % B + Qmin*t >= Qmin
    -Qmin*eye(N*no_pumps)       zeros(N*no_pumps,N) zeros(N*no_pumps) -eye(N*no_pumps);... % B + t >= 1
    -Qmax*eye(N*no_pumps) zeros(N*no_pumps,N) eye(N*no_pumps) zeros(N*no_pumps);...  % q <= t*Qmax
    Qmin*eye(N*no_pumps) zeros(N*no_pumps,N) -eye(N*no_pumps) zeros(N*no_pumps);... % q >= t*Qmin
    Qmax*eye(N*no_pumps) zeros(N*no_pumps,N) zeros(N*no_pumps) eye(N*no_pumps)];   % B <= Qmax(1-t)
%     initger_new]; % new formulation
%b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b = [ -d(1) ones(1,N) -Qmin*ones(1,N*no_pumps) zeros(1,N*no_pumps)  zeros(1,N*no_pumps)  Qmax*ones(1,N*no_pumps) ];

% b(1,1) = d(1)/Area;
% b(1,1) = -d(1);
% b(1,2:N+1) = 1; % max pumps on
% b(N*no_pumps) = -Qmin;
% b(N*no_pumps) = 0;
% b(1,N+2: size(A,1)) = -1; % defintion of T

% c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = ones(1,N)*0 ; %punishment for having water in the resevoir
i = 1;
c = Pe;
while i <= no_pumps-1
    c = [c Pe*(i+1)];
    i = i+1;
end
c = [c h zeros(1,(2*no_pumps)*N) ];

%Q previously H % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Q_T2
Q_1 = diag(ones(1,N-1)*-1,-1)+  diag(ones(1,N-1)*-1,1) + eye(N)*2;

% off diagonal stuff
S = [];
for i = 1:no_pumps-1
    R = [];
    for j  = 1:no_pumps-i
        R = [R Q_1*j];
    end
    S = [S; zeros(N,N*i) R];
end
S = [S; zeros(N,N*no_pumps)];
S = S + S';

% diagonal stuff
for i = 1:no_pumps
    Q_T2((i-1)*N + 1:i*N,(i-1)*N + 1:i*N) = Q_1*i;
end

Q_T2 = Q_T2*switch_penatly; 
% Q_qB
Q_qB = eye(N*no_pumps)*L_p;
% the rest:
Q_h2 = zeros(N);
Q_hB = zeros(N,N*no_pumps);
Q_q2 = zeros(N*no_pumps,N*no_pumps);
Q_qB = eye(N*no_pumps,N*no_pumps)*L_p;
Q_B2 = zeros(N*no_pumps,N*no_pumps);
Q_TB = zeros(N*no_pumps,N*no_pumps);% eye(N*no_pumps,N*no_pumps)*0*L_p;
Q_Tq = zeros(N*no_pumps,N*no_pumps);% eye(N*no_pumps,N*no_pumps)*-L_p*0;


Q = [Q_T2 Q_hB' Q_Tq Q_TB;...
    Q_hB Q_h2  Q_hB Q_hB;...
    Q_Tq Q_hB' Q_q2 Q_qB;...
    Q_TB Q_hB' Q_qB Q_B2 ];


%bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_bounds = [0 1];%[-inf inf];
h_bounds = [h_min h_max];
q_bounds = [0 Qmax] ;
B_bounds = [0 Qmax];
lb = [ repmat(T_bounds(1),1,no_pumps*N) repmat(h_bounds(1),1,N) repmat(q_bounds(1),1,no_pumps*N) repmat(B_bounds(1),1,no_pumps*N)   ];
ub = [ repmat(T_bounds(2),1,no_pumps*N) repmat(h_bounds(2),1,N) repmat(q_bounds(2),1,no_pumps*N) repmat(B_bounds(2),1,no_pumps*N)   ];

%% loop
iter = 1;
max_iter = 20;
err = 21;
while err >= 0.000001 && iter <= max_iter; % NO STOPPING CRITERION DEFINED YET!!
    %% Curve fitting / Hydraulic solver
    
    epanet_caller_v2 % gives q_old h_old  delta_q_old  delta_H_old
    %% make remaining matracies
    %    % c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %     h = ones(1,N)*2 ; %punishment for having water in the resevoir
    %     j = 1;
    %     c = Pe;
    %     while j <= no_pumps-1
    %         c = [c Pe*(j+1)];
    %         j = j+1;
    %     end
    %     c = [zeros(1,(no_pumps)*N) h c./singlefile(q_old) zeros(1,(no_pumps)*N) ];
    %    % Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %    % energy
    
    %     if iter == 0
    %         for i = 1:N
    %             for j = 1:no_pumps
    %                 K_k(j,i) = 1;
    %             end
    %         end
    %         KK = diag(singlefile(K_k));
    %         energy = [zeros(N*no_pumps) repmat(zeros(N),no_pumps,1) KK KK];
    %     else
    K_k = zeros(no_pumps,N);
    for i = 1:N
        for j = 1:no_pumps
            K_k(j,i) = grad_s(j,i)- 2*K_P(j,1)*q_old(j,i) - K_P(j,2);
        end
    end
    KK = diag(singlefile(K_k));
    
    energy = [zeros(N*no_pumps) repmat(eye(N),no_pumps,1) KK KK];
    %     end
    
    % mass
    h_Delta = diag(ones(1,N),0) + diag(ones(1,N-1)*-1,-1) ;
    mass = [ zeros(N,N*no_pumps) -h_Delta*Area/Delta_T repmat(eye(N),1,no_pumps) zeros(N,N*no_pumps)];
    mass(1,:) = 0;
    
    %intiger
    %     intiger = [ eye(N*no_pumps) zeros(N*no_pumps,N) -diag(1./singlefile(q_old)) zeros(N*no_pumps)];
    
    % controller
    contr = [ zeros(1,N*no_pumps) 1 zeros(1,N-1 + 2* N*no_pumps)];
    if strcmpi('on',controller) % H_1 = h_initial
        %         Aeq = [energy;mass;intiger;contr];
        Aeq = [energy;mass;contr];
    else
        %         Aeq = [energy;mass;intiger];
        Aeq = [energy;mass];
    end
    
    % beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    q_old = multifile(q_old,no_pumps,N);
    %     if iter == 0
    %         for i = 1:N
    %             for j = 1:no_pumps
    %                 E(j,i) = q_old(j,i); % Prescribe non linearised solution for first iterations
    %             end
    %         end
    %
    %     else
    E = [];
    for i = 1:N
        for j = 1:no_pumps
            %           E(j,i) = h_old(i) - (2*K_P(j,1)*q_old(j,i) + K_P(j,2))*q_old(j,i);
            %           E(j,i) =  K_k(j,i)*q_old(j,i) - (2*K_P(j,1)*q_old(j,i) + K_P(j,2))*q_old(j,i);
            E(j,i) = K_k(j,i)*q_old(j,i) + h_old(i);
        end
    end
    
    %     end
    E = singlefile(E);
    d(1) = 0;
    
    if strcmpi('on',controller) % H_1 = h_initial
        %         beq= [ E d zeros(1,no_pumps*N) h_initial];
        beq= [ E d h_initial];
    else
        %         beq= [ E d zeros(1,no_pumps*N)];
        beq= [ E d ];
    end
    
    %% solvers
    %% QP
    if strcmpi('QP_quadprog',solver_type)
        % QP
        options = optimoptions(@quadprog,'Algorithm','active-set','MaxIter',500,'Diagnostics','on')
        [Y, fval, exitflag] = quadprog(Q,c,A,b,Aeq,beq,lb,ub,[],options);
        if length(Y) == (3*no_pumps +1)*N ;
            T_old(:,iter) = Y(1:no_pumps*N,1);
            h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
            q_sol_old = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N,1);
            B_old = Y((2*no_pumps+1)*N+1:(3*no_pumps+1)*N,1);
            q_old = q_sol_old + B_old;
            q_hist(:,iter) = q_old;
        elseif  exitflag == -2
            error(['No solution found no feasable sollution'])
        elseif exitflag == 0
            error(['No solution found too many iterations'])
        else
            error('AHHHAHAH')
        end
    elseif strcmpi('QP_cplex',solver_type)
        % QP
        options = cplexoptimset('solutiontarget',2); % first order solution
        [Y, fval, exitflag] =  cplexqp(Q,c',A,b',Aeq,beq',lb',ub',[],options);
        if length(Y) == (3*no_pumps +1)*N ;
            T_old(:,iter) = Y(1:no_pumps*N,1);
            h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
            q_sol_old = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N,1);
            B_old = Y((2*no_pumps+1)*N+1:(3*no_pumps+1)*N,1);
            q_old = q_sol_old + B_old;
            q_hist(:,iter) = q_old;
        elseif  exitflag == -2
            error(['No solution found no feasable sollution'])
        elseif exitflag == 0
            error(['No solution found too many iterations'])
        else
            error('AHHHAHAH')
        end
    elseif strcmpi('MIQP_cplex',solver_type) %% different formulation alltogether (scrap B)
        % MIQP
        options = cplexoptimset('solutiontarget',2); % first order solution
        [Y, fval, exitflag] =  cplexqp(Q,c',A,b',Aeq,beq',lb',ub',[],options);
        if length(Y) == (3*no_pumps +1)*N ;
            T_old(:,iter) = Y(1:no_pumps*N,1);
            h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
            q_sol_old = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N,1);
            B_old = Y((2*no_pumps+1)*N+1:(3*no_pumps+1)*N,1);
            q_old = q_sol_old + B_old;
            q_hist(:,iter) = q_old;
        elseif  exitflag == -2
            error(['No solution found no feasable sollution'])
        elseif exitflag == 0
            error(['No solution found too many iterations'])
        else
            error('AHHHAHAH')
        end
   end
    
    % figure;
    figure;
    if plot_on == 2
        hold on
        subplot(2,1,1);
        for i = 1:no_pumps
            pic = multifile(T_old(:,iter)',no_pumps,N);
            pic(i,:) = pic(i,:)*i;
            bar(pic(i,:),'stack'); hold on
        end
        plot(Pe*no_pumps/max(Pe),'Linewidth',8)
        subplot(2,1,2);
        plotyy(1:N,Y(1+N*no_pumps:N*(1+no_pumps)),1:N,sum(multifile(q_sol_old,no_pumps,N),1))
        %             figure;
        %             plot(1:N,q_old(1:N),1:N,sum(multifile(q_sol_old,no_pumps,N),1))
        
    else
    end
    err = sum(abs(abs(T_old(:,iter)-0.5)-0.5))
    iter = iter + 1
end

%% export to EPAnet
% convert the schedule of several pumps to one pump (Not working yet!!)
% for i = 1:no_pumps
%  pattern = Y(N*(i-1)+1:N*i);
%
%     save('pump_schedule_' num2str(i) '.txt','pattern','-ascii')
% %      status = dos('COPY pump_schedule.txt pump_schedule.pat')
% end
%% convergance checks
if plot_on == 1
    figure
    for i = 1:N
        pump_perf(i,:,1) = Qp_hist(i,1,:);
        hold on; plot(pump_perf(i,:,1))
    end
    
    figure;
    for i = 1:N
        hold on; plot(h_hist(i,:),'r')
    end
else
end
% err
% end
%% cost
P_sced = [pump_schedule(:,1)];
for i = 2:no_pumps
    P_sced = [P_sced + pump_schedule(:,i)*i];
end
cost = Pe*P_sced/2; % half hour prices
%% save schedule
if no_pumps == 1
    schedule = [ 0; 0;  schedule];% add lines epanet ignores
    save('pump_schedule.txt','schedule','-ascii')
    status = dos('COPY pump_schedule.txt pump_schedule.pat');
end
% end