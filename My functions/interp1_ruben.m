function [ power] = interp1_ruben(q,P,q_pump)
%Calls interp1 if pump is within desing specs and extrapolates if not.
%   Linear interpolation of the data if it is within range otherwise cubi
%   extrapolation

if q_pump < max(q)
    power = interp1(q,P,q_pump);
else
    warning('Pump operating outside of desing limits')
    [p,S] = polyfit(q,P,3); %S left for debugging
    power = polyval(p,q_pump);
end

end

