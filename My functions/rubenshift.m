function A = rubenshift(X,v)
% SHifts matrix or vector X vertically by v, works on all rows

A = [X(v+1:length(X),:); X(1:v,:) ];