%% generate

if strcmpi('on',controller) % H_1 = h_initial
    h0(1) = h_initial;
else
    h0(1) = 0;
end
if h_initial >= 0.7*h_max
    disp('initial and max filling are too close for sensible operation, press Ctrl - C and change the input')
   pause;
end

%% create schedule
Y0_schedule(1:N) = 1;
for i = 1:N-1
h0(i+1) = (Qp(i+1,1)- d(i+1))/(2*Area) +  h0(i) ;
while h0(i+1) <= h_initial && k <= no_pumps
    Y0_schedule(i) = k;
    h0(i+1) = (Qp(i+1,k)- d(i+1))/(2*Area) + h0(i);
    k = k+1;
end
% if k == no_pumps && h0(i+1) <= h_min
k = 2;
end

%% split schedule into readable form

Y0 =  repmat(Y0_schedule, 1, no_pumps);
Y0 = reshape(Y0,N,no_pumps);

for i = 1:no_pumps
for k = 1:N
    if Y0(k,i)== i
        Y0(k,i) =1;
    else
        Y0(k,i) = 0;
    end
%  Y0(Y0(:,i) ~= i)
end
end
Y0 =  reshape(Y0,N*no_pumps,1);

% 
% set = 1;
% Y0_schedule(1:N) = set;
% 
% pump_flow = Qp(:,no_pumps)/2;
% 
% for i = 1:N-1
%     h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
%     if h0(i+1) >= h_max
%         set = 0;
%         h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
%     elseif h0(i+1) <= h0(1) 
%         set = 1;
%         h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
%     end
%     Y0_schedule(i+1) = set; 
% end
% Y0((no_pumps-1)*N+1:no_pumps*N,1) =  Y0_schedule;
Y0(no_pumps*N+1:(no_pumps+1)*N,1) = h0;
%% check
if A*Y0 <= b
    disp('inequaltiy OK')
else
    disp('No initial solution found at all');
%    break;
end

if Aeq*Y0 == beq
    disp('Hurray initial solution found')
else
    fail_by = ['No exact initial solution found for equaltiy constraint. Error is max of:' num2str(max(abs(Aeq*Y0 - beq)))];   
    disp(fail_by );
 end