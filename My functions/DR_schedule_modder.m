function schedule = DR_schedule_modder(schedule,duration,network_name,start)
% Lecture example only!! Maybe van zyl too if no booster is used!
% added capacity for van_zyl but ignores booster, my result in infeasible
% networks
if nargin == 3
    start = 1;
end

if  strmatch('Lecture',network_name)
    
    total = sum(schedule,2);
    
    if min(total(start:start+duration-1)) < 0
        error('Schedule not suitable for DR!')
    end
    
    for i = start:start+duration-1
        total(i) = total(i) - 1;
    end
    
    
    
    tmp = zeros(size(schedule));
    
    
    for i = 1: size(tmp,1)
        tmp(i,1:total(i)) = 1;
    end
    
    schedule = tmp;
    
elseif strmatch('van_zyl',network_name)
    schedule_booster = (schedule(:,3));
    schedule = (schedule(:,[1 2]));
    total = sum(schedule,2);
    
%     if min(total(start:start+duration-1)) < 0
%         error('Schedule not suitable for DR!')
%     end
    
    for i = start:start+duration-1
        total(i) = total(i) - 1;
    end
    
    % if event outside of the window then don't offer DR for it
    if min(total(start:start+duration-1)) < 0
        total(find(total(start:start+duration-1) < 0)) =0;
    end
    
    
    tmp = zeros(size(schedule));
    
    
    for i = 1: size(tmp,1)
        tmp(i,1:total(i)) = 1;
    end
    
    schedule = [tmp schedule_booster];
end