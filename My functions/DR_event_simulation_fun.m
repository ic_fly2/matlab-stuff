function [GHG, cost, gap,schedule_event] = DR_event_simulation_fun(schedule,duration,network_name,price,grid_GHG,level)
prob.N =48;
prob.approx_number = 7;
prob.vmax_min = 5;
prob.M  = 999;
prob.settings.time_limit  = 600;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';

prob.def = network_name;

prob.settings.price =  price;
prob.settings.DR.status = 'Off';
prob.settings.DR.flex = 'Window1';

prob.settings.DR.desired_ratio = NaN;
prob.settings.DR.rest_period = 240; %minutes
prob.settings.DR.reward = 60000;
prob.settings.DR.desired_share = 0.3333;
prob.settings.DR.demand_factor = 1;
prob.settings.DR.recovery = 240; %minutes
prob.settings.plot = 'Off';


prob.settings.GHG = grid_GHG;
prob.settings.h0 = level;

% norm GHG
norm_GHG = sum(grid_GHG.*([132.2262  132.2262   66.4128]* schedule'))*0.5;
norm_cost= sum(price'.*([132.2262  132.2262   66.4128]* schedule'))*0.5;

% 27805.0913 % norm costs

% event
prob.settings.mod_schedule.status = 'Yes';
prob.settings.mod_schedule.index = 1;

schedule_modded = DR_schedule_modder(schedule,duration,network_name,1);
prob.settings.mod_schedule.sched =  schedule_modded(1:duration,:);
out_event = experiment(prob);

GHG = norm_GHG - out_event.GHG;
cost = norm_cost - out_event.cost_pump;
gap = out_event.Remaining_gap;
schedule_event = out_event.schedule_tmp;



