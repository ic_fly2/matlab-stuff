function sched_plot(sched,str,angle,time_step)
%plots a schedule given as set of column vectors in a nice way
% str is optional containing pump names and angle is roation of names 0 for
% horizontal and 90 for vertical
if nargin == 2
    angle = 0;
    time_step = 0;
elseif nargin == 3
     time_step = 0;
end

time = linspace(0+time_step,1+time_step,length(sched)+1);
% plot([0 1],[0 0])

%plot time (hour)
hold on 
for i = 1:48
    plot(i*[1 1]/24,[0 4],'Color',[0.9 0.9 0.9],'Linewidth',0.2)
end

for j = 1:size(sched,2)
    for i = 1:length(sched)
        if sched(i,j) == 1
            h = plot([time(i) time(i+1)],[j j],'k','LineWidth',2);
        end
    end
end
set(gca,'YTick',[1:j])
if exist('str','var')
    set(gca,'YTickLabel',str)
    set(gca,'YTickLabelRotation',angle)
end
%xlabel('Time of day','interpreter','latex')
datetick('x','HH:MM')
axis([ min(time) max(time) 0.5 j + 0.5])

