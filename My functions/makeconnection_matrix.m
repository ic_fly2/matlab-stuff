function [connection_matrix,connection_type] = makeconnection_matrix(pipes,nodesIdListMap,pumps,pipe_approx,pump_approx)
% old:
% function [connection_matrix,connection_type,pumpsUnique] = makeconnection_matrix(pipes,nodesIdListMap,pumps,pipe_approx,pump_approx)
% Give the list of all pipe connections, node map and type of connection
% approximations applied and generates the required files: [connection_matrix,connection_type]
% j = 1;
% for i = 1:length(pumps)
%     if strcmp(pumps{i}.Unique,'Yes')
%         pumpsUnique{j} = pumps{i};
%         j = j + 1;
%     end
% end


j = length(pumps);
% pumpsUnique = pumps;
connections = [pipes pumps];
nc = length(connections);
nn = nodesIdListMap.Count;
%% conneciton matrix
connection_matrix = zeros(nc,nn);
for i = 1:nc
    
    loc_start =  nodesIdListMap(connections{i}.startNodeId);
    loc_end   =  nodesIdListMap(connections{i}.endNodeId);

    connection_matrix(i,loc_start) = -1;
    connection_matrix(i,loc_end) =  1;
end

%% type of connection
for i = 1:nc
    if isfield(connections{i},'Length') && strcmp(connections{i}.Status,'Open')
        connection_type{i} =  pipe_approx;
    elseif isfield(connections{i},'Length') && strcmp(connections{i}.Status,'CV')
%         connection_type{i} = 'pipe_quad_CV'
        connection_type{i} =  [pipe_approx '_CV'];
    elseif  isfield(connections{i},'headCurveId');
        connection_type{i} =  pump_approx;
    else
        error('Unusual pipe or closed pipe found, please open or remove pipe from network')
    end
end

end