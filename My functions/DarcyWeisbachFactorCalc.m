function [ f,Re,regime,H,K,n ] = DarcyWeisbachFactorCalc( D,Q,e ,L,formula)
% Estimates the Darcy Weisbach friction factor
%
% Units of input m,m3,m,m, 'DW' or 'HW'
%
% For DW use:
% Aproximate values for e mm
% Riveted Steel:              0.9  -  9.0
% Concrete:                   0.30  -  3.0
% Cast  Iron:                 0.26
% Galvanized Iron:            0.15
% Asphalted Cast Iron:        0.12
% Commercial or Welded Steel: 0.045
% PVC, Drawn Tubing, Glass:   0.0015
%
% For HW use: (from wikipedia)
% Asbestos-cement:              	140 	
% Cast iron new:                    130 	130
% Cast iron 10 years:           	107 	113 
% Cast iron 20 years:           	89 	100 	
% Cast iron 30 years:              	75 	90 	
% Cast iron 40 years:           	64 	83 	
% Cement-Mortar Lined Ductile Iron Pipe: 	140 	140 
% Concrete:                         100 	140 	
% Copper:                           130 	140 	
% Steel:                            90 	110 	
% Galvanized iron:                  120 	120 	
% Polyethylene:                     140 	140 	
% Polyvinyl chloride (PVC):         150 	150 	
% Fibre-reinforced plastic (FRP): 	150 	150 	
%
% full output: [f,Re,regime,H,K,n]



%% initialisations for water
% fluid props
fluid ;
%% determine regime of flow
Area = pi*D^2 /4; % m2 area of pipe
v = Q/Area ;%m/s
Re = D*v/vu;
Q2 = Q+0.1*Q; % for h =KQ^n form
Re2 = D*(Q2/Area)/vu ;
switch formula
    case 'DW'
        % find f
        if Re <= 2100
            regime = 'Laminar';
            f = 64/Re;
            
            % fully rough flow with flat friction factor for practical puropeses flat
            % parts of moody diagram summarised:
        elseif e/D >= 0.04 && Re >= 20000 || e/D >= 0.015 && Re >= 50000 || ...
                e/D >= 0.01 && Re >= 150000 || e/D >= 0.002 && Re >= 1000000 ||...
                e/D >= 0.0006 && Re >= 2000000 || e/D >= 0.05
            regime = 'Rough and fully Turbulent';
            f = 1.14 - 2*log10(e/D);
            f = 1/ f^2;
            %
        else % iterative solution of colebrook equation
            regime = 'Turbulent';
            % % %     err = 1;
            % % %     f = 1.14 - 2*log10(e/D);
            % % %     f = 1/ f^2; % first guess of f (too large)
            % % %     while abs(err) >= 0.01
            % % %     err = 1.14 - 2*log10(e/D + 9.287/(Re * f^0.5)) - f^-0.5;
            % % %     f = f + 0.0001;
            % % %     end
            
            % Serghides's solution from Wikipedia
            A = -2*log10(e/(3.7*D) + 12/Re);
            B = -2*log10(e/(3.7*D) + (2.51*A)/Re);
            C = -2*log10(e/(3.7*D) + (2.51*B)/Re);
            f = (A - ((B - A)^2)/(C -2*B +A))^-2;
        end
        
        % find f2
        if Re2 <= 2100
            regime = 'Laminar';
            f2 = 64/Re2;
            
            % fully rough flow with flat friction factor for practical puropeses flat
            % parts of moody diagram summarised:
        elseif e/D >= 0.04 && Re2 >= 20000 || e/D >= 0.015 && Re2 >= 50000 || ...
                e/D >= 0.01 && Re2 >= 150000 || e/D >= 0.002 && Re2 >= 1000000 ||...
                e/D >= 0.0006 && Re2 >= 2000000 || e/D >= 0.05
            regime = 'Rough and fully Turbulent';
            f2 = 1.14 - 2*log10(e/D);
            f2 = 1/ f2^2;
            %
        else % iterative solution of colebrook equation
            regime = 'Turbulent';
            
            % Serghides's solution from Wikipedia
            A = -2*log10(e/(3.7*D) + 12/Re2);
            B = -2*log10(e/(3.7*D) + (2.51*A)/Re2);
            C = -2*log10(e/(3.7*D) + (2.51*B)/Re2);
            f2 = (A - ((B - A)^2)/(C -2*B +A))^-2;
        end
        if (~exist('L','var'))
            
            n = NaN;
            K = NaN;
            H = NaN;
        else
            b= log(f/f2)/log(Q2/Q);
            a = f*Q^b;
            n = 2-b;
            K = (a*L)/(19.62*D*Area^2);
            H = (f * 8 * L * Q^2)/ (9.81*pi^2 * D^5);
        end
    case 'HW'
        %disp('You are using Hazen-Williams, are you sure?')
        H = (L*10.67*Q^1.85)/(e^1.85 *D^4.87); 
        f = [];
        
            Area = pi*D^2 /4; % m2 area of pipe
            v = Q/Area ;%m/s
        Re = D*v/vu;
        
        regime = 'Wrong';
        K = (L*10.67)/(e^1.85 *D^4.87);
        n = 1.85;
    otherwise
        error('Unknown friction approximation method used selecd DW or HW please')
end
end % function end

