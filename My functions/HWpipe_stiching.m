function [K,D_new,C_new,L_new]  = HWpipe_stiching(D,C,L)
%combining pipes: Add a vector of properties of HW pipes you wish to stich
%together. Diameter, HW coeffictient and length in m.
% ruben Menke 03.03.15 v0.1
%% checks:
if length(D) ~= length(C) || length(D) ~= length(L)
    error('Incorrect input defenition')
end
%% sum
K = sum((10.675*L)./(C.^1.852 .*D.^4.8704 ));
D_new = mean(D);
C_new = mean(C);

L_new = (K/10.675)* D_new^4.8704 * C_new^1.852;



