function [A12,A10] = makeA12A10(pipes,nodesIdListMap,Allreservoirs,pumps)
% Give the list of all pipe connections, node map and All reservoirs that
% have a fixed head at a given time (tanks nad reservoirs). Returns A12 and
% A10 matracies of the corresponding format.
if nargin <4
    pumps = [];   
end
nc = length(pipes);
nn =  nodesIdListMap.Count - length(Allreservoirs);
A12 = zeros(nc,nn);
A10 = [];
H0 = [];

pipes = [pipes pumps]

ListofTanks = [];
for i = 1:length(Allreservoirs);
    blub = nodesIdListMap(Allreservoirs{i}.Id);
    ListofTanks = [ListofTanks; blub];
end


for i = 1:nc
    loc_start =  nodesIdListMap(pipes{i}.startNodeId);
    loc_end =  nodesIdListMap(pipes{i}.endNodeId);
    if sum(ListofTanks==nodesIdListMap(pipes{i}.startNodeId)) == 1;
        A12(i,loc_end) =  1;
        A10_slice = zeros(nc,1); A10_slice(i,1) = 1;
        A10 = [A10 A10_slice];
        for j = 1:length(Allreservoirs)
            if strcmp(Allreservoirs{j}.Id,pipes{i}.startNodeId)
                H0 = [H0 Allreservoirs{j}.Head];
            end
        end
    elseif sum(ListofTanks==nodesIdListMap(pipes{i}.endNodeId)) == 1;
        A12(i,loc_start) =  -1;
        A10_slice = zeros(nc,1); A10_slice(i,1) = -1;
        A10 = [A10 A10_slice];
        for j = 1:length(Allreservoirs)
            if strcmp(Allreservoirs{j}.Id,pipes{i}.endNodeId)
                H0 = [H0 Allreservoirs{j}.Head];
            end
        end
    elseif isfield(pipes{i},'power')
        A12(i,loc_start) = -1;
        A12(i,loc_end) =  1;
        
        A10_slice = zeros(nc,1); A10_slice(i,1) = 1;
        A10 = [A10 A10_slice];
        
        H0 = [H0 c_org(pump_counter)];
        pump_counter = pump_counter +1;
                
    else
    A12(i,loc_start) = -1; 
    A12(i,loc_end) =  1;
%     A10(i,1) = 0;
    end
end