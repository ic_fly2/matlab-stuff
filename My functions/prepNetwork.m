function [pumps, pipes, junctions, reservoirs, tanks, nodes, data, nodesIdListMap ] = prepNetwork(def, settings, N)%%%% PROFILE SOLVERS
%   Function written by:
%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $
%
%   Modified by Ruben Menke $Date: 14/01/2015 
%
%   prepNetwork should not be used on its own, it is a helper function to
%   extract the data from .inp files for EPAnet.
%
%   See WN_INP_reader, makePipes, makeJunctions, makeReservoirs
 
networkData=WN_INP_reader([def '.inp']);

% units

%WN_options = getWNoptions(networkData('[OPTIONS]'));
pipeData=networkData('[PIPES]');
valveData=networkData('[VALVES]');
junctionData=networkData('[JUNCTIONS]');
reservoirData=networkData('[RESERVOIRS]');
tankData=networkData('[TANKS]');
patternData=networkData('[PATTERNS]');
pumpData = networkData('[PUMPS]'); %ruben
curvesData = networkData('[CURVES]');

 
pumps = makePumps(pumpData,[]); %ruben
pipes = makePipes(pipeData,[]);
valves=makeValves(valveData,[]);
if ~isempty(valves)
    error('Complain to Ruben, valves are not implimeted yet')
end
%juncIdListMap=containers.Map();
%resIdListMap=containers.Map();
nodesIdListMap=containers.Map();
patternsMap=containers.Map();
curvesMap = containers.Map(); %ruben

patternsMap=makePatterns(patternData,patternsMap);
curvesMap=makeCurves(curvesData,curvesMap); %ruben
% [reservoirs,resIdListMap]=makeReservois(reservoirData,resIdListMap);

[junctions,nodesIdListMap]=makeJunctions(junctionData,nodesIdListMap,settings);
[reservoirs,nodesIdListMap]= makeReservois(reservoirData,nodesIdListMap);
for i = 1:length(reservoirs)
    if isfield(reservoirs{i},'Pattern_name') && ~isnan(reservoirs{i}.Pattern_name)
        reservoirs{i}.Pattern = patternsMap(num2str(reservoirs{i}.Pattern_name));
        reservoirs{i}.Head =  pattern_length_adjustment(reservoirs{i}.Pattern,N)+reservoirs{i}.Elev;
    end
end
[tanks,nodesIdListMap]=makeTanks(tankData,nodesIdListMap);


%need makeDemands file!
% [demands,multipliers,baseDemands]=makeDemands(junctions,patternsMap,1);

if exist('settings','var')
    if strcmp(settings.mods.status,'Yes')
        Network_modifier
        settings.mods.status = 'Modded';
    end
end
%%% Make Hydraulic  equation matrices

nodes = [reservoirs junctions tanks ];
data.no = length([reservoirs tanks]);
data.nn = length(nodes);
data.h_min = zeros(1,data.nn); % intialisation
data.h_max = zeros(1,data.nn);
data.nj = length(junctions); % for use in solver
data.npi = length(pipes);
% n_pipes = length(pipes);
% data.n_valves = length(valves);
% pipes = [pipes valves];
% reservoirs=[reservoirs tanks];
% nn = length(junctions);
% no = length(reservoirs);
data.nv=length(valves);

%% pumps
% fitting_type = 'poly';
[data.pumps_vec,pumps] = prepPumps(pumps,curvesMap);%,fitting_type);
data.npu = sum(data.pumps_vec); % work around
data.nc =  data.npi + data.nv + data.npu;


% Fancy pump stuff

 

%% reservoirs:
% set up reservoirs with Init of 0
for i = 1:length(reservoirs)
    reservoirs{i}.Init = 0;
end
            
        

Allreservoirs=[reservoirs tanks]; % no differentiation between tanks and reservoirs
ListofTanks = [];
for i = 1:length(Allreservoirs);
    ListofTanks = [ListofTanks;nodesIdListMap(Allreservoirs{i}.Id) ];
    % pipes feeding reservoirs
   
end





%% Nodes
data.Elevation = zeros(data.nn,1);
 for i = 1:data.nn % for all nodes
     data.Elevation(nodesIdListMap(nodes{i}.Id)) = nodes{i}.Elev;
 end
 
 
%% demand
length_of_patterns = zeros(1,data.nj);
for i = 1:data.nj
    if ~isempty(junctions{i}.Pattern)
        try
            length_of_patterns(i) = length(patternsMap(junctions{i}.Pattern));
        catch
            error('Not all patterns are defined, check spelling and defenitions in .inp file')
        end
    end
end
length_of_patterns = max(length_of_patterns); % used in GGA of constraint prep accordingly

q = zeros(data.nj,1);
qall = zeros(data.nn,length_of_patterns);
% demand_multiplier  = []; % is this needed?

if isfield(settings,'demand_vector')
    qall = settings.demand_vector;
else
    for i = 1:data.nn
        if isfield(nodes{i},'Pattern') &&  isstr(nodes{i}.Pattern) && ...
                ~isempty(nodes{i}.Pattern);
            pattern = patternsMap(nodes{i}.Pattern)/ mean(patternsMap(nodes{i}.Pattern));
            qall(nodesIdListMap(nodes{i}.Id),:) = nodes{i}.Demand*pattern'/1000; %LPS to m3/s
        end
    end
end
qall_gga = pattern_length_adjustment(qall,N);
data.qall = qall;
data.qall_mean = mean(qall_gga,2);
% qall_gga = qall_gga*settings.DR.demand_factor;
qall_gga = circshift(qall_gga',settings.Time_shift)';
data.qall_gga = qall_gga;



