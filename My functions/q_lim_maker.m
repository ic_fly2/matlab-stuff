function [ q_lim_pipe , M_pipe ] = q_lim_maker( pipes,Approximation,vmax_min)
%Takes pipe data and assumes Darcy Weisbach method in no approximation
%provided
% Outputs [q_lim_pipe  M_pipe] the required flow limits and sizes of M for
% the corresponding Pipe. 
% input in D,e,L,vmax,pmax format and Approximation is 'DW' or 'HW'
%




q_lim_pipe = zeros(1,length(pipes));
M_pipe = zeros(1,length(pipes));
for i = 1:length(pipes)
    % q_lim is the max flowrate at the max permissable speed for the pipe
    A = ((pipes{i}.Diameter/1000)^2)*pi/4;
    vmax =  (pipes{i}.Diameter/1000)*6;
    if vmax < vmax_min
        vmax = vmax_min;
    end
%         
    q = vmax*A;
    q_lim_pipe(i) = q;
    
    %M limits
    e =  pipes{i}.Roughness;
    L = pipes{i}.Length;
    [~,~,~,H,~,~]=DarcyWeisbachFactorCalc(pipes{i}.Diameter/1000,q,e,L,Approximation);
    M_pipe(i) = 2*H; %M = twice the max head difference (May need to change this)
end

end

