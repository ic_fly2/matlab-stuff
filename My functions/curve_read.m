function [Out] = curve_read(number_of_data_points)
% load a jpeg graph and read in curves interactivly and return them as
% data points. Reads the number of specified data points, default is 10
if nargin == 0
    number_of_data_points = 10;
end
[filename, pathspec]= uigetfile('*.*','Select the Graph');
% filename = 'D:\Phd\My written stuff\Figures\ETAline-100-100-210\Power_curve.jpg';
A = imread([pathspec filename]);
image(A)
title('Click on the origin, the maximum value of the Y axis and the maximum value of the X axis')

[x,y] = ginput(3);
x_origin = x(1);
y_origin = y(1);
ymax =     y(2);
xmax =     x(3);

x_max_val =  input('What is the max value of the X axis?');
y_max_val = input('What is the max value of the Y axis?');

No_curves = input('How many curves are there?');


tag = cell(1,No_curves);
X_in   = cell(1,No_curves);
Y_in   = cell(1,No_curves);

% % input gathering
for i = 1:No_curves
    title(['Click on ' num2str(number_of_data_points ) ' points of curve ' num2str(i)])
    [X_in{i},Y_in{i}] = ginput(number_of_data_points );
    tag{i} = input('Name the curve ','s');
end

x_factor =  (xmax-x_origin) /x_max_val;
y_factor =  (ymax-y_origin) /y_max_val;

% processing
for i = 1:No_curves
    Out{i,2} = tag{i};
    Out{i,1}(:,1) = (X_in{i}-x_origin)/x_factor;
    Out{i,1}(:,2) = (Y_in{i}-y_origin)/y_factor;    
end

    
    


    