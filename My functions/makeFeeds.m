function Feeds =  makeFeeds(pipes,pumps,tanks) 

%% EPS feeds into tanks
connections = [pipes pumps];
Feeds = zeros(length(tanks),length(connections));
for i = 1:length(tanks)
    for j = 1:length(connections)
        if isfield(connections{j},'endNodeId') && strcmp(connections{j}.endNodeId,tanks{i}.Id)
            % feed in
            Feeds(i,j) = 1;
        elseif isfield(connections{j},'endNodeId') && strcmp(connections{j}.startNodeId,tanks{i}.Id)
            % feed out
            Feeds(i,j) = -1;
        end
    end
end