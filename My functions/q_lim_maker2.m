function [vmax, q, M_pipe ] = q_lim_maker2(pipe,vmax_min)
%Takes pipe data and assumes Darcy Weisbach method in no approximation
%provided
% Outputs [q_lim_pipe  M_pipe] the required flow limits and sizes of M for
% the corresponding Pipe. 
% input in pipe and vmax_min
%


% q_lim_pipe = zeros(1,length(pipes));
% M_pipe = zeros(1,length(pipes));
% for i = 1:length(pipes)
    % q_lim is the max flowrate at the max permissable speed for the pipe
    A = ((pipe.Diameter/1000)^2)*pi/4;
    vmax =  (pipe.Diameter/1000)*6;
    if vmax < vmax_min
        vmax = vmax_min;
    end
%         
    q = vmax*A;
%     q_lim_pipe(i) = q;
    
    %M limits
    e =  pipe.Roughness;
    L = pipe.Length;
    [~,~,~,H,~,~]=DarcyWeisbachFactorCalc(pipe.Diameter/1000,q,e,L,pipe.Approximation);
    M_pipe = 2*(H+abs(pipe.change_gz)); %M = twice the max head difference (May need to change this)
% end

end

