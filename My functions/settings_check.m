function [ settings ] = settings_check( settings )
%Modifies and updates the settings
%   Detailed explanation goes here
if  ~isfield(settings,'time_limit')
    settings.time_limit = time_limit;
end

if  ~isfield(settings,'time_analysis')
    settings.time_analysis = 'Off';
end

if  ~isfield(settings,'OF_spec')
    settings.OF_spec = 'Simple';
end

if  ~isfield(settings,'sos')
    settings.sos = 'Off';
end

if  ~isfield(settings,'asymmetry')
    settings.asymmetry = asymmetry;
end

if  ~isfield(settings,'approx_number')
    settings.approx_number = approx_number;
end

if ~isfield(settings,'mods')
     settings.mods.status = 'No';
end
     
if ~isfield(settings,'Time_shift')
    settings.Time_shift = 0;
end

    

% DR settings
% power pumps to demand
if ~isfield(settings.DR,'desired_ratio')
    settings.DR.desired_ratio = NaN;
end

% power of Pumps dedicated to DR
if  ~isfield(settings,'DR')
    settings.DR.status = 'Off';
    settings.DR.desired_power = NaN;
     
elseif strcmp(settings.DR.status,'On') && ~isfield(settings.DR,'desired_ratio')
     settings.DR.desired_ratio = NaN;
end

if ~isfield(settings.DR,'demand_factor')
      settings.DR.demand_factor = 1;
end

if isfield(settings,'mod_schedule')
    if ~isfield(settings.mod_schedule,'status')
        settings.mod_schedule.status = 'No';
    end
else
    settings.mod_schedule.status = 'No';
end
    
% overfill at the end of the period
if ~isfield(settings,'limit_overfill')
    settings.limit_overfill = 'Off';
end


end

