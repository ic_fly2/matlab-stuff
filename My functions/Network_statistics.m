function results = Network_statistics(WDN_name)
% WDN input file of .inp typ. name with or withour extension accepted

del = strfind(WDN_name,'.inp');
if ~isempty(del)
    WDN_name(del:end) = [];
end
def = WDN_name;
prepNetwork;

for i = 1:length(pipes)
    Diameters(i) = pipes{i}.Diameter;
    Roughness(i) = pipes{i}.Roughness;
    Lengths(i) = pipes{i}.Length;
end

for i = 1:nn
    Elevations(i) = nodes{i}.Elev;
    if isfield(nodes{i},'Demand') && ~isempty(nodes{i}.Demand)
        Demand(i) = nodes{i}.Demand;
    else
        Demand(i) = 0;
    end
end


results = struct('Diameters',Diameters,'Roughness', Roughness,'Lengths',Lengths,'Elevations',Elevations,'Demand',Demand);

%% statistical info
results.meanDiameter= mean(Diameters);
results.minDiameter= min(Diameters);
results.maxDiameter= max(Diameters);
results.stdDiameter= std(Diameters);

results.meanRoughnes = mean(Roughness);
results.minRoughnes= min(Roughness);
results.maxRoughnes= max(Roughness);
results.stdRoughnes= std(Roughness);

results.meanLength= mean(Lengths);
results.minLength= min(Lengths);
results.maxLength= max(Lengths);
results.stdLength= std(Lengths);

results.meanElevation= mean(Elevations);
results.minElevation= min(Elevations);
results.maxElevation= max(Elevations);
results.stdElevation= std(Elevations);

results.meanDemand= mean(Demand);
results.minDemand= min(Demand);
results.maxDemand= max(Demand);
results.stdDemand= std(Demand);

