function Y = array2table(x,sep)
% turns x into a latex array or table, with an additional seperator for
% units or similar
%

if nargin == 1
    sep = '';
elseif  sep(1) ~= ' ';
    sep = [' ' sep];
end

if iscell(x)
    x = cell2mat(x);
end

Y = [];
for j = 1:size(x,1)
    for i = 1:size(x,2)-1
        Y = [Y num2str(x(j,i)) sep  ' & '];
    end
      Y = [Y num2str(x(j,i+1)) sep ' \\ '];
end
    