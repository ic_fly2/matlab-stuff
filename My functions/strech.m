function [ V ] = strech(A,f )
%Streches vector A by power f, distancing the peaks by factor f
%maintainging the mean constant
%   Detailed explanation goes here

av_A = mean(A);
if max(A) == Inf 
    error('Max of A is infinite')
elseif min(A) == 0
    error('Min of A is 0')
else
    f_org = max(A)/min(A);
end

c =f;

A_norm = A/mean(A);
A_new =A_norm.^c;
A_new_norm = A_new/mean(A_new);
V = A_new_norm*av_A;
end

