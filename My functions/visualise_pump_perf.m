function visualise_pump_perf(schedule_tmp,head_display,flow_display,connections,pumps,npi,n_pumps,nodesIdListMap,change_Pumps)
% alternativly use: H_save,Q_save

figure;

%% Pressure diferences accros pumps:
j_init = 1;
Dp = zeros(size(schedule_tmp,1),length(n_pumps));
for i=1:length(n_pumps)
    for j = j_init:sum(n_pumps(1:i))
        Dp(:,i) = Dp(:,i) + (head_display(:,nodesIdListMap(pumps{j}.endNodeId))-head_display(:,nodesIdListMap(pumps{j}.startNodeId))).*schedule_tmp(:,j);
    end
    j_init = sum(n_pumps(1:i))+1;
end

q = flow_display(:,npi+1:length(connections));


%% original curves
elev =[];
for i = 1:length(n_pumps)

    round(Q_save,2) == 0    elev = [elev ones(1,n_pumps(i))*change_Pumps(i)];
end

for i = 1:length(pumps)
    h = polyval([pumps{i}.a pumps{i}.b pumps{i}.c],linspace(0,max(roots([pumps{i}.a pumps{i}.b pumps{i}.c]))));
    h_correct = h-elev(i);
    plot(linspace(0,max(roots([pumps{i}.a pumps{i}.b pumps{i}.c]))),h_correct); hold on;
end

%% plot
plot(q,Dp,'r+')
axis([0 Inf 0 Inf])
xlabel('Flowrate in m3/s')
ylabel('Head in m')