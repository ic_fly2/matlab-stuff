function cplex = addSOS2cplex(cplex,N,nn,np,n_pumps,lambda_parts_length)
% adds ordered weighted sos set to a problem
for i = 1:length(lambda_parts_length)
    init = N*(nn+np+sum(n_pumps))+1+sum(lambda_parts_length(1:i-1));
    ends = N*(nn+np+sum(n_pumps))+sum(lambda_parts_length(1:i));
    cplex.addSOSs('1',[init:ends]', [1:lambda_parts_length(i)]',{['sos1(' num2str(i) ')']});
end