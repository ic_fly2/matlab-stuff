function cplex = problem2cplex(problem)
cplex = Cplex('lpex1');
cplex.Model.sense = 'minimize';
cplex.Model.obj = problem.f;
cplex.Model.Q = problem.H;

cplex.Model.lb = problem.lb;
cplex.Model.ub = problem.ub;
cplex.Model.A = [problem.Aeq; problem.Aineq];
cplex.Model.lhs = [problem.beq; -inf*ones(size(problem.bineq))];
cplex.Model.rhs = [problem.beq; problem.bineq];
cplex.Model.ctype = problem.ctype;
if ~isempty(problem.x0)
    cplex.MipStart.x = problem.x0;
end

%% parameter tuning
cplex.Param.mip.tolerances.mipgap.Cur = problem.gap; % optimality gap to stop at
% cplex.Param.timelimit.Cur = 60; %time limit in seconds
cplex.Param.mip.strategy.variableselect.Cur = 2;%4 %pseudo = 2; strong = 3; info: http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex137.html
cplex.Param.emphasis.mip.Cur = 0; %0 and 4 work best            %emphasisset(i_e); %0:4 http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex64.html
cplex.Param.mip.strategy.nodeselect.Cur = 0;%2; % works best


