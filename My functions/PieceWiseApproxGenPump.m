function [pump ] = PieceWiseApproxGenPump(a_org,b_org,c_org,N,pmax,plotting)
% Piecewise Approximation generation of pump curves flow
% Makes a piecewise approximation of the flowproperties of a given pump.
% a b c are the polynomials off the pump curve.
% N is the number of pumps in the station and pmax is optional, default is Inf.
% 
% pump = PieceWiseApproxGenPump(a,b,c,nmax,pmax,plotting)
% pump is a struct array with .ms and .Cs with N fields 
%   m = gradients of pieces for (number of pumps, pieces)
%   C = y intersect of curve (number of pumps, pieces)
%   
%   y = mx + C form
%   
%   plotting is either 'on' or 'off' or blank    
%   Detailed explanation goes here
if nargin == 5
    plotting = 'off';
elseif nargin == 4
    pmax = Inf; %number of steps
    plotting = 'off';
elseif isempty(pmax)
        pmax = 5;
else  
%     a = -2;
%     b = -1;
%     c = 5;
%     N = 3;
%     pmax = []; 
%     plotting = 'off';
end
for n = 1:N
    a = a_org/(n^2);
    b = b_org/n;
    c = c_org;
qmax = max(roots([a b c]));
q = linspace(0,qmax,200);
for i = 1:length(q)
      H = polyval([a b c],q);
end
H = H(H >= 0);
q = q(1:length(H)); 
Delta_H =  0.005*max(H);
H_plus  = H + 0.5*Delta_H;
H_minus = H - 1.5*Delta_H;

%% crude guessing method (bisection method) (optimisation not analytical)
k = 1;
m = 1;
c_temp = H_plus(1);
Q_lim_index(1) = 1; 
C(1) = H_plus(1);
m(1) = b;

% replace with same thing as before!
%     tol = 0.0005;
    Nsec = pmax;
%    for rep = 1:pmax % half of max number of pieces +1
        Nsep = round(length(q)/Nsec);
        breakpoints = Nsep*(1:Nsec)-1;
        breakpoints(end) = length(q); breakpoints = [1 breakpoints];
        
        
        m = (H(breakpoints(2:end))-H(breakpoints(1:end-1)))./(q(breakpoints(2:end))-q(breakpoints(1:end-1)));
        C =  H(breakpoints(1:end-1))-q(breakpoints(1:end-1)).*m ;
        for j = 1:Nsec
            r{j} = H(breakpoints(j):breakpoints(j+1)) - (q(breakpoints(j):breakpoints(j+1))*m(j) + C(j));
            S(j) = sum(r{j}.^2);
        end
        
        
%         if sum(S)/length(q) < tol*max(H);
%             break
%         else
%             Nsec= Nsec+1;
%         end
%     end
    
    
    Q_lim = q(breakpoints);
%     
% while Q_lim_index(k) < length(q)
%     crit = min(polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) - H_minus(Q_lim_index:end));
%     while  crit >= 0
%         if crit > 0.05
%             m(k) = m(k)- 10*crit;
%         else
%             m(k) = m(k)-0.05;
%         end
%         crit = min(polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) - H_minus(Q_lim_index:end));
% %         c_temp(k) = H_plus(Q_lim_index(k)) - q(Q_lim_index(k))*m(k);
%     end
%     Q_lim_index(k+1) = find( H_plus - polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= 0, 1, 'last' );
%     m(k+1) = m(k) + m(k)*0.01;
%     c_temp(k+1) = H_plus(Q_lim_index(k+1));%- q(Q_lim_index(k+1))*m(k+1);
%     k = k+1;
%     
% end 
% 
% %     C(k) = 5;
% %     k = k+1;
% % while q(Q_lim_index(k)) <= qmax
% %     while m(k)*(q-q(Q_lim_index(k)))+c_temp(k) <= H_plus
% %         m(k) = m(k)+0.01;
% %     end
% %     Q_lim_index(k+1) = find( m(k)*(q-q(Q_lim_index(k)))+c_temp(k) - H_minus > eps, 1, 'last' );
% %     m(k+1) = m(k) + m(k)*0.3;
% %     c_temp(k+1) = H_minus(Q_lim_index(k+1));
% %     k = k+1;
% % end
% % C(1) = H_plus(1);
% for i = 1:k-1 
%     Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
%     H_lim(i) = Q_lim(i)*m(i) + C(i);
%     C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
% end
% 
% C(end) = [];
% m(end) = [];




%% plot stuff
if strcmp(plotting,'on')
    figure;
    hold on
    h1 = plot(q,H);
    h2 = plot(q,H_minus,':');
    plot(q,H_plus,':')
    for i = 1:k-2
        x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
        h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'r');
    end
    hleg = legend([h1 h2 h3],{'D-W Solution',['Allowed error ' num2str(Delta_H) ' m'],'Approximation' } );
%     axis([0 qmax 0 H(end)])
    ylabel('Head loss /m');
    xlabel('Volumeflow /m^3')
%     set(hleg,'Location','NorthWest')
end
pump(n).ms = m;
pump(n).Cs = C;


end
%% stuff to add
% make if find the margin that fits the desired max number of pieces.
% Allow different margins top and bottom.
% Improve convergence speed to get a speed improvement
% shift Qlim along


