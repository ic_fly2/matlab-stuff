function [K_pump,n,omega,pumps] = state2rpm(Tau,h,q,pumps)
% Converts a pump state to its characteristic curves from the pump file
% use as:  [K,n,omega] = state2rpm(Tau,[],q,name) or
%          [K,n,omega] = state2rpm([],h,q,name) if both h and Tau are
%          provided h will be given preference and Tau will be ignored

%% pre alocate
names = fields_rm(pumps,'name');
K_pump= zeros(1,length(names));
n =     zeros(1,length(names));
tmp =   zeros(1,length(names));
omega = zeros(1,length(names));




%% do for VSD
cur_dir = cd;
cd([cur_dir(1) ':\Phd\MATLAB\Phd\Networks\']) 
for i = 1:length(pumps) 
    load(names{i})
    if strcmp(pumps{1}.type,'VSD')
        if ~isempty(h) && q(i) ~= 0
            omega(i) = power.Speedfit_h(q(i),h(i));
            omegas = unique(power.surf_data.w,'stable');
            if omega(i) > max(omegas)
                omega(i) = max(omegas);
            end
            x = interp1(omegas,1:length(omegas), omega);
            x_2 = x-floor(x);
            x_1 = 1-x_2;
            p1 = [characteristic_curves{floor(x)}.p1 characteristic_curves{floor(x)}.p2 characteristic_curves{floor(x)}.p3];
            p2 = [characteristic_curves{ceil(x)}.p1  characteristic_curves{ceil(x)}.p2  characteristic_curves{ceil(x)}.p3];
            P = p2*x_2 + p1*x_1;
            [K_pump(i),n(i),tmp] = PowerLawConverter(P);
            K_pump(i) = -K_pump(i);
            pumps{i}.Status = 'Open';
            pumps{i}.c = tmp;
            pumps{i}.speed = omega;
            
        elseif  ~isempty(Tau)&& q(i) ~= 0
            omega(i) = power.Speedfit_Tau(q(i),Tau(i));
            omegas = unique(power.surf_data.w,'stable');
            if omega(i) > max(omegas)
                omega(i) = max(omegas);
            elseif omega(i) < min(omegas)
                omega(i) = min(omegas);                
            end
            x = interp1(omegas,1:length(omegas), omega(i));
            if x ~= 1
                
                x_2 = x-floor(x);
                x_1 = 1-x_2;
                p1 = [characteristic_curves{floor(x)}.p1 characteristic_curves{floor(x)}.p2 characteristic_curves{floor(x)}.p3];
                p2 = [characteristic_curves{ceil(x)}.p1  characteristic_curves{ceil(x)}.p2  characteristic_curves{ceil(x)}.p3];
                P = p2*x_2 + p1*x_1;

            elseif x == 1
                P = [characteristic_curves{1}.p1 characteristic_curves{1}.p2 characteristic_curves{1}.p3];
            end

            [K_pump(i),n(i),tmp] = PowerLawConverter(P);
            K_pump(i) = -K_pump(i);
            pumps{i}.Status = 'Open';
            pumps{i}.c = tmp;
            pumps{i}.speed = omega;
            
        else %pump is closed
            K_pump(i) = 10000000;
            n(i) = 2;
            pumps{i}.Status = 'Closed';
            pumps{i}.speed = 0;
            
        end
    elseif strcmp(pumps{1}.type,'FSD')
        if abs(Tau(i) - 1) < 0.01
            % pump is on
            pumps{i}.Status = 'Open';   
            K_pump(i) = pumps{i}.K;
            n(i) = pumps{i}.Nexp;
        else
            % pump is off
            pumps{i}.Status = 'Closed';
            K_pump(i) = 10000000;
            n(i) = 2;
        end
    else
        error('Unspecified pump type')
    end
    
    
end
cd(cur_dir)
% make sure always positive!!!
K_pump(K_pump < 0) = - K_pump(K_pump < 0) ;

