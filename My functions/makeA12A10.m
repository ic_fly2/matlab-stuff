function [A12,A10,H0] = makeA12A10(pipes,nodesIdListMap,Allreservoirs,pumps)
% Give the list of all pipe connections, node map and All reservoirs that
% have a fixed head at a given time (tanks nad reservoirs). Returns A12 and
% A10, H0 matracies of the corresponding format. Also gives pump data if
% needed.
if nargin <4
    pumps = [];
    curvesMap = [];
    a_org = [];
    b_org = [];
    c_org = [];
    disp('No pumps in system?')
elseif nargin == 4 && ~isempty(pumps)
    pipes = [pipes pumps];
end

nc = length(pipes);
nn =  nodesIdListMap.Count - length(Allreservoirs);
A12 = zeros(nc,nn);
A10 = [];
H0 = [];


ListofTanks = [];
for i = 1:length(Allreservoirs);
    blub = nodesIdListMap(Allreservoirs{i}.Id);
    ListofTanks = [ListofTanks; blub];
end

A10 = zeros(nc,length(ListofTanks));
H0 = zeros(length(ListofTanks),1);

for i = 1:nc
    loc_start =  nodesIdListMap(pipes{i}.startNodeId);
    loc_end =  nodesIdListMap(pipes{i}.endNodeId);
    if strcmp(pipes{i}.Status,'Closed')
        type = 'Closed';
    elseif isfield(pipes{i},'power')
        type = 'Pump';
    elseif strcmp(pipes{i}.Status,'Open')
        type = 'Pipe';
    elseif strcmp(pipes{i}.Status,'CV')
        type = 'Pipe'; % needs to be changed to check valve or Valve and implimented
    end
    
    
    if sum(ListofTanks==nodesIdListMap(pipes{i}.startNodeId)) == 1 && ...
                    sum(ListofTanks==nodesIdListMap(pipes{i}.endNodeId)) == 1; % start and ends in a tank
                 error('Two tanks connected via a pipe are not supported, simplify the network and come back')
            elseif sum(ListofTanks==nodesIdListMap(pipes{i}.startNodeId)) == 1; % starts in a tank
                A12(i,loc_end) =  1;
                [~,idx]=max(ListofTanks==nodesIdListMap(pipes{i}.startNodeId));
                A10(i,idx) = -1;
                for j = 1:length(Allreservoirs)
                    if strcmp(Allreservoirs{j}.Id,pipes{i}.startNodeId)
                        H0(idx) = Allreservoirs{j}.Elev + Allreservoirs{j}.Init ;
                    end
                end
            elseif sum(ListofTanks==nodesIdListMap(pipes{i}.endNodeId)) == 1; %ends in a tank
                A12(i,loc_start) =  -1;
                 [~,idx]=max(ListofTanks==nodesIdListMap(pipes{i}.endNodeId));
                A10(i,idx) = 1;
                for j = 1:length(Allreservoirs)
                    if strcmp(Allreservoirs{j}.Id,pipes{i}.endNodeId)
                        H0(idx) = Allreservoirs{j}.Elev + Allreservoirs{j}.Init;
                    end
                end
            else
                A12(i,loc_start) = -1;
                A12(i,loc_end)   =  1;
    end
            
    
    switch type
        
        case 'Closed' % kick a fuss or try ignore the pipe
%             error('Closed pipes are not supported. Remove the pipe and come back please, or disable this error and try to continue')
            
        case 'Pipe'
            
        case 'Pump'
            A10_slice = zeros(nc,1); A10_slice(i,1) = 1; %not sure about the sign here
            A10 = [A10 A10_slice];   
            H0 = [H0; -pipes{i}.c];
%             if sum(ListofTanks==nodesIdListMap(pipes{i}.startNodeId)) == 1; % starts in a tank
%                 % normal fill stuff
%                 A12(i,loc_end) =  1;
%                 [~,idx]=max(ListofTanks==nodesIdListMap(pipes{i}.startNodeId));
%                 A10(i,idx) = -1;
%             
%             else
%                 A12(i,loc_start) = -1;
%                 A12(i,loc_end) =  1;
%                 
%                 AP_slice = zeros(nc,1); AP_slice(i,1) = 1; %not sure about the sign here
%                 AP = [AP AP_slice];
%                 
%                 HP = [HP; pipes{i}.c];
%                 
%                 
%                 %     A10(i,1) = 0; 
%             end
        case 'Valve'
            Error('Valves not implimented')
    end
end

A12 = sparse(A12);
A10 = sparse(A10);
H0  = sparse(H0);

end