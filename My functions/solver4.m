function [h,q,err1,kk,CONDS,ERRORS] = solver4(A12,A10,H0,d,K,N_exp,tol,CV)
% solve_Nullspace_LU_Precond: This method solves the linear equation at each iteration of
% the Newton method using Eq.\ (18)-(19) of Todini(1988) but
% with a simple form from block elimination followed by null spaces(See report 1, Edo Abraham)

% Inputs:

% See also  LSOLVE_MATEXP; LSOLVE_BIGSYS; solve_Nullspace_Elhay; solve_Nullspace_QR_Precond

% References:
% Elhay, Sylvan, and Angus R. Simpson.
% "Dealing with zero flows in solving the nonlinear equations for water
% distribution systems." Journal of Hydraulic Engineering 137.10 (2011): 1216-1224.

% Todini, E. & Pilati, S.
% A gradient algorithm for the analysis of pipe networks
% Computer Applications in Water Supply , Vol. 1 (Systems analysis and simulation) , pp. 1-20 , 1988

% Elhay, Sylvan, et al. "A Reformulated Co-tree Flows Method competitive with the global Gradient
% Algorithm for solving the water distribution system equations." Journal
% of Water Resources Planning and Management (2014).

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.0.1 $  $Date: 17/04/2014 $

% mods from Ruben:
%kappa = condest(A12'*A12);
kappa = 1e5;
A21 = A12';
nn = size(A12,2);
np = size(A12,1);

hk = 130*ones(nn,1);
%qk = 0.03*ones(np,1);


%% setup_null script:
%Pipe resistance r, Diameters D, lengths L
% data.chw=K;
nc=np-nn;
%%% USE QR TO GET LS SOLUTION OF A12'x=d
x=A12'\d;
qk = x;
% norm(A12'*x-d,2);%1e-18
%%% Factorize the SPD matrix A12'*A12 to use in multiple iterations
% [L]=chol(A12'*A12);%find symbolic or numeric sparse Chol factors

% [L,~,q_L]=lchol(A12'*A12);%[L,p_L,q_L]=lchol(A12'*A12);
[L_A12,~,q_LA12] = chol(A12'*A12,'lower','vector');%Matlab's chol is the same as lchol

%[L,p] = chol(A12'*A12);
%when A is sparse, returns a permutation matrix S. R'*R=S'*A*S. The factor of S'*A*S tends to be sparser than the factor of A.
%Chol is denser than lchol which chooses a column reordering that reduces
%fill-in
P_L=speye(nn);
Pr=P_L(q_LA12,:);%Fill-reducing Column ordering/Permutation Matrix
% P_c=P_L(:,q_L);
%

nulldata.x=x;
nulldata.Pr=Pr;
nulldata.L_A12=L_A12;

%%%

%% %%% Generate the matrix Z whose columns span the nullspace of A12'
numiters = 10;

[L_lu,~,P_lu] = lu(A12);%[L,U,P] = lu(A12); luflops(L,U);

%toc
L1=L_lu(1:nn,:);
L2=L_lu(nn+1:nn+nc,:);
K_lu=-(L1')\L2';
% L1_inv=L1\speye(nn);
% K1=-L1_inv'*L2';
% Z = P2'*P'*[K;speye(nc)];
Z = P_lu'*[K_lu;speye(nc)];

clear L_lu P_lu K_lu L1 L2

nulldata.Z=Z;

%% solver: 
x=nulldata.x;
Z=nulldata.Z;
P_r=nulldata.Pr;
% Pc = Pr';%lchol factorizes S'AS, Pr=Pc';
L_A12=nulldata.L_A12;
% K=data.chw;% % Hazen-Williams loss equation coefficients for the pipes


CONDS=1;ERRORS=1;

G=K.*abs(qk).^(N_exp-1);% G in Elhay is A11 in Todini
err1 = norm( [G.*qk+A12*hk+A10*H0; A21*qk-d],inf);%infinity norm error, 1-norm, 2-norm can also be used

%%% 
nc=size(Z,2);%size of nullspace nc=np-nn

nnz_ZZ = nnz(Z'*speye(np)*Z);%The most number of nonzeros in Z'*F^k*Z
% We reduce unnecessary  work in formulating X again again as it is the
% most costly operation in multiplying (spdiags())
q_old=qk;
ZT=Z';

Fdiag_old=spalloc(np,1,np);
X=spalloc(nc,nc,nnz_ZZ);
ERRORS =  zeros(1,10000); % added by me
for kk = 1:100
    Fdiag = N_exp.*G;%F in Elhay et al
    %%%'Regularize'
    sigma_max = max(Fdiag); %maximum eigenvalue of F
    t_k = max((sigma_max/kappa)- Fdiag,0);
    Fdiag = Fdiag + t_k; %%% Fdiag = max((sigma_max/kappa)*ones(np,1),Fdiag);
    
    if kk==1
        X= Z'*spdiags(Fdiag,0,np,np)*Z;
        Fdiag_old = Fdiag;
        q_old=qk;
    else
        updates= (find((abs(qk-q_old))/tol >=1e-6));n_upd=length(updates);
        q_old=qk;
        
        X= X+ZT(:,updates)*spdiags(Fdiag(updates)-Fdiag_old(updates),0,n_upd,n_upd)*(ZT(:,updates)');
        
        Fdiag_old = Fdiag;

    end
    
    
    %     b= Z'*(spdiags(Fdiag-G,0,np,np)*qk-A10*H0-Fdiag.*x);%
    b= Z'*((Fdiag-G).*qk-A10*H0-Fdiag.*x);%
    
    v = X\b;%alternatively use spqr_solve..Cholesky faster for square X like this one
    %  [v,stats]=cholmod2(X,b);%return stats as well
    
    q = x+Z*v;%q^{k+1}
    
    %%%% Since A12'*A12 is positive definite, use factored version to
    %%%% easily solve for h
    
    %     b = A12'*(spdiags(Fdiag-G,0,np,np)*qk-A10*H0-Fdiag.*q);
    b = A12'*((Fdiag-G).*qk-A10*H0-Fdiag.*q);
    
    y=(L_A12)\(P_r*b);
    h=P_r'*(L_A12'\y);
    
    %    h=(A12'*A12)\b;
    %h=cholmod2(A12'*A12,b);
    %%% This is the source of errors in the solution as the condition number is large
    %%% factorizing this well will perhaps result in better conditioning
    %%   %%update qk and h^k if convergence is not accomplished
        G = (K.*abs(q).^(N_exp-1));%Note that G:=A11(q) in Todini
    
    %     err0=err1;
    err1 = norm( [G.*q+A12*h+A10*H0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    %     phi=norm(q-qk,inf);%./norm(qk,inf)
    %     rho=norm(h-hk,inf);%./norm(qk,inf)
    %
    %     rho_e(kk) = norm( [G.*q+A12*h+A10*H0],inf);%infinity norm error, 1-norm, 2-norm can also be used
    %     rho_c (kk)= norm( [A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    %% CV pipe checks:
    for i = CV
        %
        if q(i) < -0.005 && K(i) < 10000000;
            %     if q(i) < 0 && abs(q(i)) > 0.001;
            K(i) = K(i) + 1000000;   
        elseif kk >= 100 &&  ERRORS(kk-2)- ERRORS(kk-1) <= 0.000001 % increase K for tighter convergance
            if q(i) < -0.001 && K(i) < 50000000;
                %     if q(i) < 0 && abs(q(i)) > 0.001;
                K(i) = K(i) + 500000;
            end
        end
    end
    
 
%     hold on;
%     plot(kk,err1,'*r')
%     pause(0.1)
    
    ERRORS(kk)=err1;
%     RERRORS=err1;
%     ERR_save(kk) = ERRORS;
    %%% If min-res is used, the 2-norm will be an appropriate measure of error,
    %%% perhaps
    %     fprintf('Iteration %1.0f: Nonlinear equation infinity-norm error is %2.10f \n',kk,err1)
    %     fprintf('norm of t_k is %1.0f \n',norm(t_k,inf))
    if err1 < tol
        fprintf('NR iteration has ended with error %2.10f on iteration %1.0f \n',err1,kk)
        break;% exit if tolerance is met
     
    else
        qk =q;
        hk=h;
    end
%     hold on;
%     plot(kk,err1,'kx');
    
end

if err1 > tol
    fprintf('FAIL! NR iteration ended with error %2.10f \n',err1)
end
% pause
% close all

% figure;
% plot(1:kk,ERR_save,'*')
