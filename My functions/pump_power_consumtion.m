function power = pump_power_consumtion2(q_pump,pumps)
% q_pump is the flowrate through the pumps
% pump is a cell which contains the power curve of the pump
%
% takes the flow rates and the pumps of the system to compute the power
% actually consumed by the pumps in the time_step

%% Check if pump curve exsists

for i = 1:length(pumps)
      
    
    
    if isfield(pumpsOn{i},'powerCurve')
        power(i) = polyval(pumpsOn{i}.powerCurve,q_pump(i));
    elseif strcmp(pumpsOn{i}.Status,'Open')
        power(i) = pumpsOn{i}.power;
    end
	
	
end
