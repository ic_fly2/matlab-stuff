function [a_quad_pump,b_quad_pump,c_quad_pump,m_linear,c_linear,Q_pump_min,Q_pump_max,Delta_H, Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow,OP_head,pmax_pump,plotting_switch )
%pump_approx_gen generates the pump approximations needed for the opti
%solver methods from the characteristic curve defention and pump station
%information. 
%   Usage as: 
% [a_quad_pump,b_quad_pump,c_quad_pump,...
% m_linear,c_linear,...
% Q_pump_min,Q_pump_max,Delta_H,...
% Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow,OP_head,plotting_switch);
%
% for:
%[pump_quad]
%[pump_linear]
%[pump_simple]
%[pump_convex] 


%input checks:
% if nargin == 6
%     OP_head = [];
%     OP_flow =[];    
% end
        
   
% Operating points:

% if size(OP_head,1) ==  size(OP_flow,1) && length(n_pumps) ==  size(OP_flow,1)  
% elseif size(OP_head,2) ~=  size(OP_flow,2) || max(n_pumps) ~=  size(OP_flow,2)  
% else 
%     error('Operating point definition and pump station desctiption n_pumps don''t match')
% end 

%Characteristic curve
if size(OP_head,1) ==  size(OP_flow,1) && length(n_pumps) ==  size(OP_flow,1)  
elseif size(OP_head,2) ~=  size(OP_flow,2) || max(n_pumps) ~=  size(OP_flow,2)  
else 
    error('Operating point definition and pump station desctiption n_pumps don''t match')
end 

% pump_quad
a_quad_pump = zeros(length(n_pumps),max(n_pumps)); %preallocation
b_quad_pump = zeros(length(n_pumps),max(n_pumps));
c_quad_pump = zeros(length(n_pumps),max(n_pumps));
for i = 1:length(n_pumps)
    for n = 1:n_pumps(i)
        a_quad_pump(i,n) = a_org(i)/(n^2);
        b_quad_pump(i,n) = b_org(i)/n;
        c_quad_pump(i,n) = c_org(i);
        
        %guess flow rates:
        p = [a_quad_pump(i,n) b_quad_pump(i,n) c_quad_pump(i,n)];
            q = linspace(0,max(roots(p)));
            h = polyval(p,q);
            [~,I] = max(q.*h*9.81);
         OP_flow(i,n) = q(I);
         OP_head(i,n) = h(I);
    end
end

% for pump_convex
for  i = 1:length(n_pumps)
    [pump ] = PieceWiseApproxGenPump(a_org(i),b_org(i),c_org(i),n_pumps(i),pmax_pump,'off');
    Pump_station(i).pump(1).curves =  [-1 0]; % for every pump station
    for n = 1:length(pump) % for all pumps
        Pump_station(i).pump(n+1).curves = [ pump(n).ms' pump(n).Cs' ];
    end
end
    

% Q_pump_min = zeros(size(OP_flow,2),max(n_pumps)); %preallocation
% Q_pump_max = zeros(size(OP_flow,2),max(n_pumps));
% Delta_H    = zeros(size(OP_flow,2),max(n_pumps));
% for i = 1:size(OP_flow,2)
%     for n = 1:n_pumps(i)
%         Q_pump_min(i,n) = OP_flow(i,n)*0.8 ; % New row for each  pump sation fill with zeros for non needed stuff
%         Q_pump_max(i,n) = OP_flow(i,n)*1.2 ;
%         Delta_H(i,n)    = OP_head(i,n) ;
%     end
% end




%  pump_simple
Q_pump_min = OP_flow*0.8 ; % New row for each  pump sation fill with zeros for non needed stuff
Q_pump_max = OP_flow*2 ;
Delta_H    = OP_head ;
% pump_linear (From Epanet op) %wrong OPs!!
m_linear = zeros(length(n_pumps),max(n_pumps)); %preallocation 
c_linear = zeros(length(n_pumps),max(n_pumps));
for i = 1:length(n_pumps) 
    for n = 1:n_pumps(i)
        m_linear(i,n) = 2*(a_org(i)/(n^2))*OP_flow(i,n) + b_org(i)/n ; 
        c_linear(i,n) = OP_head(i,n) -  m_linear(i,n)*OP_flow(i,n); 
    end
end


end

