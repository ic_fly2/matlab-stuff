% laod the file
clear all; close all; 
gridwatch = csvread('gridwatch_2014_12_04-09.csv',1,2);

grid_demand = gridwatch(:,1);
grid_supply = gridwatch(:,2:end);
for i = 1:9
    grid_share(:,i) = grid_supply(:,i)./grid_demand;
end

grid_features{1,1} = 'demand'; grid_features{1,2} =  'coal' ; grid_features{1,3} = 'nuclear' ;  grid_features{1,4} = 'ccgt' ; grid_features{1,5}  = 'wind' ; grid_features{1,6}  ='pumped' ; grid_features{1,7}  ='hydro'; grid_features{1,8}  = 'oil'; grid_features{1,9}  = 'ocgt' ; grid_features{1,10}  ='other';
grid_features{2,1} =  NaN;     grid_features{2,2} =   0.82  ; grid_features{2,3} =       0.12;  grid_features{2,4} =  0.49  ; grid_features{2,5} =0.12;     grid_features{2,6} =  0.337;	 grid_features{2,7} =  0.24;   grid_features{2,8} = 0.61 ;  grid_features{2,9} =  0.48 	 ; grid_features{2,10} =  0.3;
intensity =  [   0.82  ;    0.12;   0.49  ;0.12;     0.337;	  0.24;  0.61 ;  0.48 	 ;  0.3];
% LCOE =  

% 5 minute intervals so 288 intervalls per day!
for i =1:floor(size(gridwatch,1)/288)
    grid_demand_cell(:,:,i) = grid_demand((i-1)*288+(1:288),:);
    grid_supply_cell(:,:,i) = grid_supply((i-1)*288+(1:288),:);
    grid_share_cell(:,:,i)=  grid_share((i-1)*288+(1:288),:);
    
    
    
end
     
mean_grid_share= mean(grid_share_cell,3);
for i = 1:288
    intensity_all(i,1:9) = intensity; 
end
C02_kg_kwh = mean_grid_share.*intensity_all;
% pound_kwh

for i = 1:9
    hold on
%     h(i) =     plot(1:288,C02_kg_kwh(:,i));
    h(i) =     plot(1:288,mean_grid_share(:,i));
%     legend(h(i), grid_features{1,1+i});
    Legend{i} = grid_features{1,1+i};
end

 legend(Legend)
 axis([0 inf 0 Inf])