%.inp file importer from .inp file to readable file for my optimisation
% v0.1
%% todo:

demand_multiplier %for each demand node check van_zyl2 or similar
switch_penalty %tick
Electricity_Price %tick
demand %(Also location)  %tick

d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
h_min
h_max

Qmax
q_lim

pipedata %tick

Power_nameplate %tick
a_org ... %tick
n_pumps %(pump stations) %tick

connection_type %done

connection_matrix = A12;

Diameter = tanks{:}.Diameter %tick
location_res = nodesIdListMap(tanks{:}.Id)%tick
Initial_fill = nodesIdListMap(tanks{:}.Init)%tick


fixed_val = reservoirs{:}.Elev %tick
location_fix = nodesIdListMap(reservoirs{:}.Id)%tick
Elevation = nodes{:}.Elev; %tick

M 

%% done:
if ~exist('live','var')
    break
end

% import from old :)
Electricity_Price(1:11) = 28.59;
Electricity_Price(12:19) = 85.76;
Electricity_Price(13:33) = 48.49;
Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49; 
Electricity_Price(45:48) =  28.59;

Pe = zeros(1,N);
for ii = 1:N;
    Pe(ii) = sum(Electricity_Price((ii-1)*48/N+(1:48/N)));
end
Pe = Pe/ (48/N);

Reservoir_area = pi*Diameter.^2/4;

% bounds
T_bounds = [0 1];
lambda_bounds = [0 1];

Network_tests