function [pumpsOn,T] = selectPumps(pumps,n_pumps,T)
% given a prepared set of pumps this configures the pumps selected as on in
% the network.

if nargin < 3 || length(T) ~= length(pumps)% set all pump stations to full on!
    T_sec = [];
    T = [];
    for i = 1:length(n_pumps)
        T_sec = zeros(1,n_pumps(i));
        T_sec(n_pumps(i)) = 1;
        T = [T T_sec ];
    end
    %     T = ones(1,length(pumps)); %schedule for this time intervall, all on
else
    T  = ceil(T);
end

% pumpsOn = cell(1,length(n_pumps));
% j = 1;
% for i = 1:length(T)
%     if T(i) == 1
%         pumpsOn{j} = pumps{i};
%         j = j + 1;
%     end
% end

for i = 1:length(n_pumps)
%     for j = 1:n_pumps(i)
        [val,idx] = max(T(sum(n_pumps(1:i-1))+1:sum(n_pumps(1:i))));
        if val == 1
            pumpsOn{i} =  pumps{idx+sum(n_pumps(1:i-1))};
            pumpsOn{i}.Status = 'Open';
        elseif val == 0
            pumpsOn{i}.Status = 'Closed'; % once closed pipes are implimented that could work too
            pumpsOn{i}.endNodeId =  pumps{idx+sum(n_pumps(1:i-1))}.endNodeId;
            pumpsOn{i}.startNodeId =  pumps{idx+sum(n_pumps(1:i-1))}.startNodeId;
            pumpsOn{i}.power = 0;
%             pumpsOn{i}.a = -90000;
%             pumpsOn{i}.b = 1;
%             pumpsOn{i}.c = 0;
%         else
%             error('Ahhh Guns of Brixton')
        end
%     end
end
            