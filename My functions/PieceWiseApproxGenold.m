function [m , C ,Q_lim, P1, P2 , P3 ] = PieceWiseApproxGenold(D,e,L,vmax,OP,pmax,plotting)
% Piecewise Approximation generation of pipe flow
% Makes a piecewise approximation of the flowproperties of a given pipe. vamx and pmax are
% optional. Defaults are 2 m/s and Inf respectivly. Calls the DaryWeisbach
% funktion to solve the flow. Ensures zero flow at zero head
%
% [m , C ,Q_lim,P1 ,P2,P3 ] = PieceWiseApproxGen(D,e,L,vmax,pmax,plotting)
%   m = gradients of pieces
%   C = y intersect of curve
%   Q_lim = active region limit 
%
%   y = mx + C form
%
%   last values of m and C apply up to the allowed max flow
%   P1 is the linear approximation at point(s) OP
%   P2 is the polynomial vector for a quadratic
%   P3 is the polynomial vector for a cubic approximation 
%   Detailed explanation goes here
if nargin == 6 
    plotting = 'off';
elseif nargin == 5
    pmax = Inf; %number of steps
    plotting = 'off';
elseif nargin == 4
    OP = [];
    pmax = Inf;
    plotting = 'off';    
elseif nargin == 3
    OP = [];
    vmax = 2; %m/s
    pmax = Inf;
    plotting = 'off';
else
    if isempty(vmax)
        vmax = 2;
    end
    if isempty(pmax)
        pmax = Inf;
    end
    if isempty(OP)
        OP = [];
    end
end

qmax = vmax*pi*(D^2)/4; % max flow

q = linspace(0,qmax*2,200); % changed from 2

for i = 1:length(q)
     [ ~,~,~,H(i),K1(i),n(i) ] = DarcyWeisbachFactorCalc( D,q(i),e,L,'DW') ;
end
H(1) = 0;
if pmax == 5
tol = 0.004;
elseif pmax == 3
tol = 0.01;
else
    tol = 0.01;
end
Delta_H = tol*max(H);
H_plus  = H + Delta_H;
H_minus = H - Delta_H;
H_minus(H_minus <= 0) = 0;
% max(H)
%% crude guessing method (bisection method) (optimisation not analytical)
k = 1;
m = 1;
c_temp = 0;
Q_lim_index(1) = 1; 

while q(Q_lim_index(k)) <= qmax
    while m(k)*(q-q(Q_lim_index(k)))+c_temp(k) <= H_plus
        m(k) = m(k)+0.01;
    end
    Q_lim_index(k+1) = find( m(k)*(q-q(Q_lim_index(k)))+c_temp(k) - H_minus > eps, 1, 'last' );
    m(k+1) = m(k) + m(k)*0.3;
    c_temp(k+1) = H_minus(Q_lim_index(k+1));
    k = k+1;
end
C(1) = 0;
for i = 1:k-1 
    Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
    H_lim(i) = Q_lim(i)*m(i) + C(i);
    C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
end

% shit qlim along
Q_lim(length(Q_lim)+1) = 0;
Q_lim =  circshift(Q_lim',1)'; % first value
% Q_lim(length(Q_lim)+1) = ; % last value (max)
if pmax < length(m)
    error('Too many segments, increase the tolerarance')
end

% polynomial approximations

H_short = H;
q_short = q;
H_short(q > qmax) = [];
q_short(q > qmax) = [];

P2 = polyfit(q_short,H_short,2)    ;
q_all = [-fliplr(q_short) q_short(2:end)] ;
H_all = [-fliplr(H_short) H_short(2:end)] ;
P3 = polyfit(q_all,H_all,3);


% Linear approximation at operating points OP
P1 = zeros(length(OP),2); 
for i = 1:length(OP)
x = linspace(OP(i)*.8,OP(i)*1.2); % make a flow rate vector 20% to the left and right of OP
[ ~,~,~,~,K,n ] = DarcyWeisbachFactorCalc( D,OP(i),e,L,'DW'); % find the fitting
y = K*x.^n; 
P1(i,:) = polyfit(x,y,1); %compute the linear fitting. 
end

if max(abs(imag(P1))) >= 0.0001
    error('Imaginary component in pipe flow equation')
else
    P1 = real(P1);
end


%% plot stuff
if strcmp(plotting, 'on')
    figure;
    hold on
    h1 = plot(q,H);
    h2 = plot(q,H_minus,':');
    plot(q,H_plus,':')
    for i = 1:k-1
        x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
        h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'r');
    end
    hleg = legend([h1 h2 h3],{'D-W Solution',['Allowed error ' num2str(Delta_H) ' m'],'Approximation' } );
    axis([0 pi*0.25*vmax*D^2 0 H(100)])
    ylabel('Head loss /m');
    xlabel('Volumeflow /m^3')
    set(hleg,'Location','NorthWest')
end

%% stuff to add
% make if find the margin that fits the desired max number of pieces.
% Allow different margins top and bottom.
% Improve convergence speed to get a speed improvement
% shift Qlim along


