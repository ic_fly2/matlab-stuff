function [x0,u,u_H,u_q] = genHydraulicSolution2(problem,T,N,nn)
% generates a hydraulic solution u and the full solution vertoc x0 for a
% WDS defined by A*x <= b , Aeq*x = beq and lb <= x0 <= ub.
% T is the pump schedule to be tested.
%
% uses a least squares approximaiton to fit the solution and then check if
% the solution is within the bounds of which bounds cause it to fail.
if nargin == 8
    u_H = NaN;
    u_q = NaN;
end

%% Make C and d vector
% min |Cx - d|^2
% check T
xtype = problem.ctype;
if min(strfind(xtype,'C'))-1 == length(T)  &&  length(strfind(xtype,'I')) < 2000
    
    C = blkdiag(eye(length(T)),zeros(length(xtype)-length(T)));
    
    if size(T,2) ~= 1
        T = T';
    end
    d = [T;zeros(length(xtype)-length(T),1)];
    
    %      options = cplexoptimset('MaxTime','120');
    
    problem.C = C;
    problem.d = d;
    
    if isfield(problem,'Q')
        [x0 ] = cplexlsqmiqcp(problem);
    else
        [x0 ] = cplexlsqmilp(problem);
    end
    %% recondition and make feasible:
    if isempty(x0)
        
        
        if isfield(problem,'Q')
            [x0 ] = cplexlsqmiqcp(problem);
        else
            [x0 ] = cplexlsqmilp(problem);
        end
        
        %         if ~isempty(x0)
        %             index = 1:length(lb);
        %             index_of_fail_lb = index(x0 < lb);
        %             index_of_fail_ub = index(x0 > ub);
        %            disp(['Problem infeasible with current bounds'...
        %                 num2str( index_of_fail_lb) ' and ' num2str(index_of_fail_ub)...
        %                 ' fail lower and upper bound in unconstrained case!)']);
        if isempty(x0)
            disp('Provided schedule is infeasable')
            u_H = NaN;
            u_q = NaN;
            
        end
    else
        disp('Intial solution generated')
    end
    u = x0(length(T)+1:end); % just the hydraulic solution part
    
    if ~exist('u_H','var')
        u_H = x0(length(T)+(1:nn*N));
        u_H = reshape(u_H,N,nn);
        u_q = x0(length(T)+nn*N+1:max(strfind(xtype,'C')));
        u_q = reshape(u_q,N,[]);
    end
else
    x0 = NaN;
    u = NaN;
    u_H = NaN;
    u_q = NaN;
    disp('T incorrectly defined, skipping x0 generation')
end
