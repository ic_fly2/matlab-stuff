function [ C ] = place_matrix(A,b,dim )
%places the matrix A in a larger matrix C at locations b, like a selective repmat
% example place_matrix(eye(2),[1 0 0 2 0],2) yields [1 0 0 0 0 0 2 0 0 0;
%                                                  0 1 0 0 0 0 0 2 0 0]
%
% A is the matric you want replicated
% b is the driving vector
% dim the dimentions along witch it is driven: 1 for adding rows, 2 for
% columns, default is 2;
%   Detailed explanation goes here
[dim1,dim2] = size(A);
if b(1) ~= 0
    C = A*b(1);
else
    C = zeros(dim1,dim2);
end

for i = 2:length(b)
    if b(i) ~= 0
        add = b(i)*A;
    else
        add = zeros(dim1,dim2);
    end
    
    switch dim
        case 1
            C = [C; add];
        case 2
            C = [C add];
        otherwise
            C = [C add];
    end 
end
end

