function [ Y ] = singlefile( X )
%reshapes a matrix from a bunch of rows adn columns to just one r

[a ~] = size(X);
Y = X(1,:);
for i = 2:a
    Y = [Y X(i,:)];
end
end

