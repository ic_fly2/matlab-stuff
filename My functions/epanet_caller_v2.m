%% fake epanet
% this function provides what i need of epanet.

% for testing
% no_pumps = 5;
% clear all;
% q_old = rand(1,no_pumps);
%% intputs
% pump info
small_delta= 0.00001;

% K_P = [-200 25 200];
K_P_mod = K_P;  K_P_mod(:,2) = K_P(:,2)+ small_delta;  

% systeminfo
for i = 1:N
    K_S(i,1) = K_S_orig(1);
    K_S(i,2) = K_S_orig(2);
    K_S(i,3) = K_S_orig(3)+h_old(i);
end

q_old = multifile(q_old,no_pumps,N);

% call epanet and solve for K_P and K_P_mod
% in the absence solve for it like so:
% for i = 1:N 
%     for j = 1:no_pumps
%         q_dash(j,i) = max(roots(K_P_mod(j,:) - K_S(i,:)));
%         
%         H_old(j,i) = polyval(K_P(j,:),q_old(j,i));
%         H_dash(j,i) = polyval(K_P_mod(j,:),q_dash(j,i));
%         
%         delta_H_old(j,i) = H_dash(j,i) - H_old(j,i);
%         delta_q_old(j,i) = q_dash(j,i) - q_old(j,i);
%         
%         grad_s(j,i) = delta_H_old(j,i)/delta_q_old(j,i);
%     end
% end

for i = 1:N 
    for j = 1:no_pumps
        grad_s(j,i) =  K_S(i,2);
    end
end

for i = 1:N
    for j = 1:no_pumps
        q_old(j,i) = max(roots(K_P(j,:) - K_S(i,:)));
    end
end