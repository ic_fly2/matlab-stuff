function[cost,outputmi,schedules,flag] = optimisation_fun_v11_lin_MIQP_again(demand_multiplier,K_P_orig,K_S_orig,res_time)
%% set pump schedule and outflow demands
% clear all;close all
% Create new or run old network definition
% network_definition;

%changelog: scrapped B and made the enrgy equation compliant in two
%inequaleti4es and thus have a symetric and convex problem again -M(1-T) <=
%K_k q + h -C <= M(1-T) really got to get the git going :S 

% demand_multiplier = 1.5; % factor of pump laod
% res_time = 8;
switch_penatly = 0.1;
L_p = 100; % penalty for qB (use to normalise the solution)
M = 10000; % big M costraints
Qmin = 200; % Qmin^2 > Pe*P*L_p
Qmax = 100000; % larger than any possible flow

no_pumps = 3;
maxtime = 20;
% K_P_orig = [ -200 0 500]; % both modified for number of pumps and time of day
% K_P_orig = [ 0 -260.7 305.77];
% K_S_orig = [39.70	 	0	 	99.25];
%new stuff from system curve solver

%% Initialisation
% pump and system features
plot_on = 3;% turn ploting for all on with 1 for only final =0
% solver type
solver_type = 'MIQP_cplex'; % Options include 'QP_quadprog' 'MILP_matlab' and  'MIQP_cplex' use matlab solvers with the optimisation toolkit cplex requires cplex from IBM to work
% maxtime = 20;
% controller
controller = 'off'; %options are on or off in on then provide h_initial
h_initial =1;

h_max = 2;
h_min = 0;

N = 48;
Delta_T = 24*60*60/N;
h_old(1,1:N*no_pumps) = h_initial;

%% Pump and sys curves
K_S_orig(1) = K_S_orig(1)/(1000*1000); % get flow in l/s so it is O(1000) and not O(1)
K_S_orig(2) = K_S_orig(2)/(1000); 
for i = 1:N
    K_S(i,1) = K_S_orig(1);
    K_S(i,2) = K_S_orig(2);
    K_S(i,3) = K_S_orig(3)+h_old(i);
end

K_P_orig(1) = K_P_orig(1)/(1000*1000);
K_P_orig(2) = K_P_orig(2)/(1000);
for j = 1:no_pumps
    K_P(j,1) = K_P_orig(1,1)/(j^2);
    K_P(j,2) = K_P_orig(1,2)/j;
    K_P(j,3) = K_P_orig(1,3);
end

l2m = 1/1000; % conversion from liters to m3 and back
m2l = 1000;
%% call epanet for initial settings:
% or alternativly cheat ;)
for i = 1:N
    for j = 1:no_pumps
        q_old(j,i) = max(roots(K_P(j,:) - K_S(i,:)));
    end
end
%% pump
% pump_data =  importdata('E:\Phd\Hydraulics\EPAnet\Etaline_125-160.txt');%  [40.0000   33.5000;   90.0000   33.0000;  150.0000   31.0000;  190.0000   28.9000;  250.0000   24.4000;  300.0000   19.0000];
% pump_data = pump_data.data ;
% X = pump_data(:,1); %m3/0.5h !!!
% Y = pump_data(:,2);
% K_P = polyfit(X,Y,2);

%% system
% replaced by passing K_S directly to the function

% only one pipe (link 1) is relevant
% need to find the compunents of the system curve static and dynamic head
% loss: produce points for a polynomial and then fit tha data through

% static
% elevation =  K_S(3); %m from model


%% end of pump sys curve


%electricity pricing
% Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28;
% Pe(1:24) = Electricity_Price(1:2:48); % test case
% from Pache:
Electricity_Price(1:11) = 28.59; Electricity_Price(12:19) = 85.76; Electricity_Price(20:33) = 48.49; Electricity_Price(34:40) = 85.76; Electricity_Price(41:44) = 48.49; Electricity_Price(45:48) = 28.59;
Pe = Electricity_Price;
% demand
% x = 1:N;
% d = sin(x/(pi*1.2))*400 + 800;%d =d*180;
% demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d/mean(d)*q_old(1)*demand_multiplier; %
d = d';%m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!

% resevoir
% D = 500; %diameter of the resevoir
% Area = pi*(D/2)^2; %m2
V_res = max(d)*res_time*60*60*l2m; %resevoir volume in terms of hours of suppliable peak demand. 
Area = V_res/(h_max - h_min); % area required to provide this as function of the resevoir range. 
% reservoir_level(1:N) = 0;
% res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping over stuff that doesn't change
% MILP vars
%A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear A
h_star(1,1) = -Area*m2l/Delta_T; h_star(1,N) = Area*m2l/Delta_T;
q_star = zeros(1,N); q_star(1) = 1;
% initger_new = [-eye(no_pumps*N) zeros(no_pumps*N,N) zeros(no_pumps*N) -eye(no_pumps*N) ]; % new formulation
if strcmpi('MIQP_cplex',solver_type)
else
    A = [zeros(1,N*no_pumps) -h_star        -repmat(q_star,1,no_pumps) zeros(1, N*no_pumps);... %mass balance
        repmat(eye(N),1,no_pumps) zeros(N, N*(2*no_pumps + 1));... % ];  % counter
        % 	-Qmin*eye(N*no_pumps) zeros(N*no_pumps,N) zeros(N*no_pumps) -eye(N*no_pumps);... % B + Qmin*t >= Qmin
       -Qmin*eye(N*no_pumps) zeros(N*no_pumps,N)    zeros(N*no_pumps) -eye(N*no_pumps);... % B + t >= 1
       -Qmax*eye(N*no_pumps) zeros(N*no_pumps,N)    eye(N*no_pumps)    zeros(N*no_pumps);...  % q <= t*Qmax
        Qmin*eye(N*no_pumps) zeros(N*no_pumps,N)    -eye(N*no_pumps)   zeros(N*no_pumps);... % q >= t*Qmin
        Qmax*eye(N*no_pumps) zeros(N*no_pumps,N)    zeros(N*no_pumps)  eye(N*no_pumps)];   % B <= Qmax(1-t)
    %     initger_new]; % new formulation
    %b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b = [ -d(1) ones(1,N) -Qmin*ones(1,N*no_pumps) zeros(1,N*no_pumps)  zeros(1,N*no_pumps)  Qmax*ones(1,N*no_pumps) ];
end

% c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = ones(1,N)*0 ; %punishment for having water in the resevoir
h(N) = 0; % benefit for water in last time interval
i = 1;
c = Pe;
while i <= no_pumps-1
    c = [c Pe*(i+1)];
    i = i+1;
end
if strcmpi('MIQP_cplex',solver_type)
    c = [c h zeros(1,no_pumps*N)];
else
    c = [c h zeros(1,(2*no_pumps)*N) ];
end
%Q previously H % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Q_T2
Q_1 = diag(ones(1,N-1)*-1,-1)+  diag(ones(1,N-1)*-1,1) + eye(N)*2;
% Q_1(1,N) = -1; % signifficantly worse condition number
% Q_1(N,1) = -1;

% off diagonal stuff ( turns it into a NLP problem)
S = [];
for i = 1:no_pumps-1
    R = [];
    for j  = 1:no_pumps-i
        R = [R Q_1*j];
    end
    S = [S; zeros(N,N*i) R];
end
S = [S; zeros(N,N*no_pumps)];
S = S + S';

% diagonal stuff
for i = 1:no_pumps
    Q_T2((i-1)*N + 1:i*N,(i-1)*N + 1:i*N) = Q_1*i;
end

Q_T2 = Q_T2*switch_penatly; 
% Q_qB
Q_qB = eye(N*no_pumps)*L_p;
% the rest:
Q_h2 = zeros(N);
Q_hB = zeros(N,N*no_pumps);
Q_q2 = zeros(N*no_pumps,N*no_pumps);
Q_qB = eye(N*no_pumps,N*no_pumps)*L_p;
Q_B2 = zeros(N*no_pumps,N*no_pumps);
Q_TB = zeros(N*no_pumps,N*no_pumps);% eye(N*no_pumps,N*no_pumps)*0*L_p;
Q_Tq = zeros(N*no_pumps,N*no_pumps);% eye(N*no_pumps,N*no_pumps)*-L_p*0;
if strcmpi('MIQP_cplex',solver_type)
    Q = [Q_T2 Q_hB' Q_Tq;...
         Q_hB Q_h2  Q_hB;...
         Q_Tq Q_hB' Q_q2];
else    
    Q = [Q_T2 Q_hB' Q_Tq Q_TB;...
        Q_hB Q_h2  Q_hB Q_hB;...
        Q_Tq Q_hB' Q_q2 Q_qB;...
        Q_TB Q_hB' Q_qB Q_B2 ];
end

%bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T_bounds = [0 1];%[-inf inf];
h_bounds = [h_min h_max];
q_bounds = [0 Qmax] ;
B_bounds = [0 Qmax];
if strcmpi('MIQP_cplex',solver_type)
    lb = [ repmat(T_bounds(1),1,no_pumps*N) repmat(h_bounds(1),1,N) repmat(q_bounds(1),1,no_pumps*N)];
    ub = [ repmat(T_bounds(2),1,no_pumps*N) repmat(h_bounds(2),1,N) repmat(q_bounds(2),1,no_pumps*N)];
else
    lb = [ repmat(T_bounds(1),1,no_pumps*N) repmat(h_bounds(1),1,N) repmat(q_bounds(1),1,no_pumps*N) repmat(B_bounds(1),1,no_pumps*N)   ];
    ub = [ repmat(T_bounds(2),1,no_pumps*N) repmat(h_bounds(2),1,N) repmat(q_bounds(2),1,no_pumps*N) repmat(B_bounds(2),1,no_pumps*N)   ];
end
%% loop
iter = 1;
max_iter = 5;
err = 21;
max_err = 0.01;
while err >= max_err && iter <= max_iter; % NO STOPPING CRITERION DEFINED YET!!
    %% Curve fitting / Hydraulic solver
        epanet_caller_v2 % gives q_old h_old  delta_q_old  delta_H_old
    %% make remaining matracies
     %    % energy
    K_k = zeros(no_pumps,N);
    for i = 1:N
        for j = 1:no_pumps
            K_k(j,i) = grad_s(j,i)- 2*K_P(j,1)*q_old(j,i) - K_P(j,2);
        end
    end
    KK = diag(singlefile(K_k));
    q_old = multifile(q_old,no_pumps,N);
    E = [];
    for i = 1:N
        for j = 1:no_pumps
            %           E(j,i) = h_old(i) - (2*K_P(j,1)*q_old(j,i) + K_P(j,2))*q_old(j,i);
            %           E(j,i) =  K_k(j,i)*q_old(j,i) - (2*K_P(j,1)*q_old(j,i) + K_P(j,2))*q_old(j,i);
            E(j,i) = K_k(j,i)*q_old(j,i) + h_old(i);
        end
    end
    % A incase of MIQP %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    if strcmpi('MIQP_cplex',solver_type)
        A = [zeros(1,N*no_pumps)        -h_star                     -repmat(q_star,1,no_pumps);... %imbalance for fist intiger value
            repmat(eye(N),1,no_pumps)   zeros(N, N*(no_pumps+1));... % ];  % counter
            -Qmax*eye(N*no_pumps)       zeros(N*no_pumps,N)          eye(N*no_pumps);...  % q <= t*Qmax
            Qmin*eye(N*no_pumps)        zeros(N*no_pumps,N)         -eye(N*no_pumps) ;... % q >= t*Qmin
            M*eye(N*no_pumps)           repmat(-eye(N),no_pumps,1)	-diag(singlefile(K_k)) ; ... Mt  - h  - K_k q<= M-E 
            M*eye(N*no_pumps)           repmat(eye(N),no_pumps,1)    diag(singlefile(K_k)) ];%  Mt + h  + K_k q <= M+E
            %     initger_new]; % new formulation
        %b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        b = [ -d(1) ones(1,N) zeros(1,N*no_pumps)  zeros(1,N*no_pumps) M-singlefile(E) M+singlefile(E)];
    else
    end
    
   % Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
   if strcmpi('MIQP_cplex',solver_type)
        energy = [];
    else
        energy = [zeros(N*no_pumps) repmat(eye(N),no_pumps,1) KK KK];
    end
    %     end
    
    % mass
    h_Delta = diag(ones(1,N),0) + diag(ones(1,N-1)*-1,-1) ;
    if strcmpi('MIQP_cplex',solver_type)
        mass = [zeros(N,N*no_pumps) -h_Delta*Area*m2l/Delta_T repmat(eye(N),1,no_pumps)];
    else
        mass = [ zeros(N,N*no_pumps) -h_Delta*Area*m2l/Delta_T repmat(eye(N),1,no_pumps) zeros(N,N*no_pumps)];
    end
    mass(1,:) = 0;
    
    %intiger
    %     intiger = [ eye(N*no_pumps) zeros(N*no_pumps,N) -diag(1./singlefile(q_old)) zeros(N*no_pumps)];
    
    % controller
    if strcmpi('MIQP_cplex',solver_type)
        contr = [ zeros(1,N*no_pumps) 1 zeros(1,N-1 + N*no_pumps)];
    else
        contr = [ zeros(1,N*no_pumps) 1 zeros(1,N-1 + 2* N*no_pumps)];
    end
    
    
    if strcmpi('on',controller) % H_1 = h_initial
        %         Aeq = [energy;mass;intiger;contr];
        Aeq = [energy;mass;contr];
    else
        %         Aeq = [energy;mass;intiger];
        Aeq = [energy;mass];
    end
    
    % beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    %     end
%     E = singlefile(E);
    d(1) = 0;
    if strcmpi('MIQP_cplex',solver_type)
         if strcmpi('on',controller) % H_1 = h_initial
            %         beq= [ E d zeros(1,no_pumps*N) h_initial];
            beq= [ d h_initial];
        else
            %         beq= [ E d zeros(1,no_pumps*N)];
            beq= [ d ];
        end
    else
        if strcmpi('on',controller) % H_1 = h_initial
            %         beq= [ E d zeros(1,no_pumps*N) h_initial];
            beq= [singlefile(E) d h_initial];
        else
            %         beq= [ E d zeros(1,no_pumps*N)];
            beq= [singlefile(E) d ];
        end
    end
    
    
    
    %% solvers
    %% QP
    if strcmpi('QP_quadprog',solver_type)
        % QP
        options = optimoptions(@quadprog,'Algorithm','active-set','MaxIter',500,'Diagnostics','on')
        [Y, fval, exitflag] = quadprog(Q,c,A,b,Aeq,beq,lb,ub,[],options);
        if length(Y) == (3*no_pumps +1)*N ;
            T_old(:,iter) = Y(1:no_pumps*N,1);
            h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
            q_sol_old = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N,1);
            B_old = Y((2*no_pumps+1)*N+1:(3*no_pumps+1)*N,1);
            q_old = q_sol_old + B_old;
            q_hist(:,iter) = q_old;
        elseif  exitflag == -2
            error(['No solution found no feasable sollution'])
        elseif exitflag == 0
            error(['No solution found too many iterations'])
        else
            error('AHHHAHAH')
        end
    elseif strcmpi('QP_cplex',solver_type)
        % QP
        options = cplexoptimset('solutiontarget',2); % first order solution
        [Y, fval, exitflag] =  cplexqp(Q,c',A,b',Aeq,beq',lb',ub',[],options);
        if length(Y) == (3*no_pumps +1)*N ;
            T_old(:,iter) = Y(1:no_pumps*N,1);
            h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
            q_sol_old = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N,1);
            B_old = Y((2*no_pumps+1)*N+1:(3*no_pumps+1)*N,1);
            q_old = q_sol_old + B_old;
            q_hist(:,iter) = q_old;
        elseif  exitflag == -2
            error(['No solution found no feasable sollution'])
        elseif exitflag == 0
            error(['No solution found too many iterations'])
        else
            error('AHHHAHAH')
        end
    elseif strcmpi('MIQP_cplex',solver_type) %% different formulation alltogether (scrap B)
        %% MIQP
            int(1:N*no_pumps) = 'I';%'I';
            cont(1:N+N*no_pumps) = 'C';
            ctype = strcat([int cont])   ;
            options = cplexoptimset('MaxTime',maxtime,'Display','final');
            options.mip.tolerances.mipgap = 0.005;
%             tic;
            [Y, fvalmi, exitflagmi, outputmi] = cplexmiqp(Q,c',A,b',Aeq,beq',[],[],[],lb',ub',ctype,[],options);
%             toc
            if length(Y) == (2*no_pumps +1)*N ;
                T_old(:,iter) = Y(1:no_pumps*N,1);
                h_old = Y(no_pumps*N+1:(no_pumps+1)*N,1);
                q_flow = Y((no_pumps+1)*N+1:(2*no_pumps+1)*N ,1);
                q_flow_summed = sum(multifile(q_flow,no_pumps,N),1);
                pump_cost(iter) = c(1:no_pumps*N)*T_old(:,iter);
                number_switches(iter) = 0.5*T_old(:,iter)'*Q_T2*T_old(:,iter);
            elseif  exitflagmi == -2
                error(['No solution found no feasable sollution'])
            elseif exitflagmi == 0
                error(['No solution found too many iterations'])
            else
                error('AHHHAHAH')
            end
   end
   
   
    if plot_on == 2
        figure;
        hold on
        subplot(3,1,1);
        for i = 1:no_pumps
            pic = multifile(T_old(:,iter)',no_pumps,N);
            pic(i,:) = pic(i,:)*i;
            bar(pic(i,:),'stack'); hold on
        end
        stairs(Pe*no_pumps/max(Pe),'Linewidth',2)
        axis([1 48 0 no_pumps])
        subplot(3,1,2);
%         plot(1:N,Y(1+N*no_pumps:N*(1+no_pumps)))
         plotyy(1:N,h_old,1:N,q_flow_summed)

        %             figure;
        %             plot(1:N,q_old(1:N),1:N,sum(multifile(q_sol_old,no_pumps,N),1))
        subplot(3,1,3)
        for j = 1:no_pumps 
            plot(q_old(j,:),'r'); hold on;
        end
        axis([1 48 min(q_old(1,:))-5 max(q_old(no_pumps,:))+5])
    else
    end
%     err = sum(abs(abs(T_old(:,iter)-0.5)-0.5))
%% calculate the error
    q_old_deleted = q_old;
    q_old_deleted(multifile(q_flow,no_pumps,N) == 0) = 0;
    err = sum(sum(abs(q_old_deleted-multifile(q_flow,no_pumps,N))))/sum(T_old(:,iter));
    iter = iter + 1;
end
% change iter back incase it finished early
[ grad, min_iter ] = min(abs(gradient(pump_cost))); % find change incase of failure to convervge 
% relying on only finding a bottom point. if a max is found i have a problem. 
% Shouldn't happen though and I'm too lazy to program against that.
if err <= max_err
%     disp(['Converged! Error is ' num2str(err)])
    iter = iter -1;
    cost = pump_cost(iter);
    flag = 1;
    %% save schedule

schedule_list = multifile(T_old(:,iter)',no_pumps,N);
for j = 1:no_pumps
    schedule_list(j,:) = schedule_list(j,:)*j;
end
    pumps_schedule =  sum(schedule_list);
for j = 1:no_pumps
    schedules(j,pumps_schedule >= j) = 1;  
end

%% save to file
% for j = 1:no_pumps
% list{j} = ['pump_schedule_' num2str(j) '.txt'];
% dos_list{j} = ['COPY pump_schedule_' num2str(j) '.txt pump_schedule_' num2str(j) '.pat'];
% end
% 
% for j = 1:no_pumps
%     schedule_var = [0; 0; schedules(j,:)'];
%     save(list{j},'schedule_var','-ascii')
%     status = dos(dos_list{j});
% end
% end
elseif grad == 0
%    	disp(['Pump Costs converged!  Error is ' num2str(err)])
    iter = iter -1; 
    cost = pump_cost(min_iter);
    flag = 2;
    %% save schedule

schedule_list = multifile(T_old(:,iter)',no_pumps,N);
for j = 1:no_pumps
    schedule_list(j,:) = schedule_list(j,:)*j;
end
    pumps_schedule =  sum(schedule_list);
for j = 1:no_pumps
    schedules(j,pumps_schedule >= j) = 1;  
end
else
    flag = 3;
    cost = NaN;
%     error(['Never converged, error is ' num2str(err)])  
    schedules(no_pumps,N) = NaN;
end


%% export to EPAnet
% convert the schedule of several pumps to one pump (Not working yet!!)
% for i = 1:no_pumps
%  pattern = Y(N*(i-1)+1:N*i);
%
%     save('pump_schedule_' num2str(i) '.txt','pattern','-ascii')
% %      status = dos('COPY pump_schedule.txt pump_schedule.pat')
% end
%% convergance checks
% if plot_on == 1
%     figure
%     for i = 1:N
%         pump_perf(i,:,1) = Qp_hist(i,1,:);
%         hold on; plot(pump_perf(i,:,1))
%     end
%     
%     figure;
%     for i = 1:N
%         hold on; plot(h_hist(i,:),'r')
%     end
% else
% end
% err
% end
%% cost
% P_sced = [pump_schedule(:,1)];
% for i = 2:no_pumps
%     P_sced = [P_sced + pump_schedule(:,i)*i];
% end
% cost = Pe*P_sced/2; % half hour prices
%% copied the below into the if loop
% % % % % %% save schedule
% % % % % 
% % % % % 
% % % % % % schedule_list = multifile(T_old(:,iter)',no_pumps,N);
% % % % % % schedule_list= zeros(no_pumps,N) ;
% % % % % schedule_list = multifile(T_old(:,iter)',no_pumps,N);
% % % % % for j = 1:no_pumps
% % % % %     schedule_list(j,:) = schedule_list(j,:)*j;
% % % % % end
% % % % %     pumps_schedule =  sum(schedule_list);
% % % % % for j = 1:no_pumps
% % % % %     schedules(j,pumps_schedule >= j) = 1;  
% % % % % end
% % % % % 
% % % % % %% save to file
% % % % % % for j = 1:no_pumps
% % % % % % list{j} = ['pump_schedule_' num2str(j) '.txt'];
% % % % % % dos_list{j} = ['COPY pump_schedule_' num2str(j) '.txt pump_schedule_' num2str(j) '.pat'];
% % % % % % end
% % % % % % 
% % % % % % for j = 1:no_pumps
% % % % % %     schedule_var = [0; 0; schedules(j,:)'];
% % % % % %     save(list{j},'schedule_var','-ascii')
% % % % % %     status = dos(dos_list{j});
% % % % % % end
% % % % % % end