function Warm_start(problem,time_day,init,demand)
% generates and uses a warm start solution for cplex, from a previously
% solved problem 
% inputs:
% problem is the problem file for cplex
% time_day is the index by which the problem has moved forward, leave at 0
% if not simulating operations
% Init is the tank level initialisation at time step time_day
% demand is the new demand forcast



