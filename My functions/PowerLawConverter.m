function [a,b,c] = PowerLawConverter(p)
% Not true: Based on the formula from wolfram a polynomial is converted to a power
% law function. works for positive and negative curvature polynomials of
% order 2
% Example:
% y = 200 - 3.66*x.^1.85; 
% p = [  -2.2980   -3.2230  201.7960]
% a = -3.66, b = 1.85 c = 200;
% http://mathworld.wolfram.com/LeastSquaresFittingPowerLaw.html
% It isn't very good :(
% Instead using log(x) log(y) fitting, much cruder but better result!!

%% Preliminary checks:
if length(p) ~= 3
    error('Incorrect entry length, must be second order polynomial')
end
% check curvature:
y1 = polyval(p,0);
y2 = polyval(p,1);

if p(1) < 0
    % negative curvature (pump)
    x = linspace(0,max(roots(p)),50);
    y = polyval(p,x);
     inv = 'false'; %never invert
%     inv = 'true'; %Flag to remember to inverse the formula
    c = y(1);
    y = y(1)-y; 
    
    % remove zero entries and maintain length
    y(1) = [];
    x(1) = []; 
elseif p(1) > 0
    % positeve curvature (pipe) % try assume a = 1.85
    x = linspace(0.1,2);
    y = polyval(p,x);
    inv = 'false'; %Flag to remember to inverse the formula
    c = 0; 
end

log_x = log(x);
log_y = log(y);
poslog_x = log_x(log_x >= 0);
poslog_y = log_y(log_x >= 0);
real_x = log_x(imag(log_y) <= 0.01);
real_y = log_y(imag(log_y) <= 0.01);

[p2(1,1:2),S(1)] = polyfit(log_x,log_y,1);
[p2(2,1:2),S(2)] = polyfit(log_x(end/2:end),log_y(end/2:end),1);
[p2(3,1:2),S(3)]  = polyfit(poslog_x,poslog_y,1);
[p2(4,1:2),S(4)]  = polyfit(real_x,real_y,1);


%% test for quality of fit
for i = 1:4
    y_test(i,:) = exp(p2(i,2))*x.^p2(i,1);
    diff(i,:) = y_test(i,:) -y;
end

 [~,idx] = min(sum(abs(diff),2));

a = exp(p2(idx,2));
b = p2(idx,1);



%% make the approximation along least squares
% n = length(x);
% 
% b_top_pos =  n*sum(log(x).*log(y));
% b_top_neg =  sum(log(x))*sum(log(y));
% b_bottom = n*sum(log(x).^2) - (sum(log(x)))^2;
% 
% b = (b_top_pos - b_top_neg)/b_bottom;
% a = exp((sum(log(y)) - b*sum(log(x)))/n);
% a = abs(a);
% b = abs(b);
if strcmp(inv,'true')
    a = -a;
end


    
% plot checking
% plot(x,c-y,'o',x,200 - 3.66*x.^1.85,'r',x,c+a*x.^b,'k')