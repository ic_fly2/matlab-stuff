function EP = Electricty_price_fun(i)
% returns an electricty price vector
switch i
    case 1
        % Pache
        EP(1:11) = 28.59;
        EP(12:19) = 85.76;
        EP(20:33) = 48.49;
        EP(34:40) = 85.76;
        EP(41:44) = 48.49;
        EP(45:48) =  28.59;
        EP = EP/5;
        
    case 2
        % normal Northumbrian
        EP(1:6)   = 6.2;
        EP(7:14)  = 6.0;
        EP(15:22) = 7.9;
        EP(23:30) = 8.5;
        EP(31:38) = 19.2;
        EP(39:46) = 8.5;
        EP(47:48) = 6.2;
        
    case 3
        % weekend Northumbrian
        EP(1:6)   = 6.1;
        EP(7:14)  = 5.9;
        EP(15:28) = 7.5;
        EP(29:36) = 8.5;
        EP(37:45) = 7.2;
        EP(46:48) = 6.1;        
end