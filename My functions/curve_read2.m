% function [Out] = curve_read2(number_of_data_points)
% load a jpeg graph and read in two curves interactivly and return them as
% 3d data points. Reads the number of specified data points, default is 10
% if nargin == 0
%     number_of_data_points = 10;
% end
% [filename, pathspec]= uigetfile('*.*','Select the Graph');
% [filename2, pathspec2]= uigetfile('*.*','Select the Graph');
close all;
clear all;
clc;
disp('REMEMBER FLOW RATES IN M3/H" NOT LPS!!')
number_of_data_points = 8;
% pump = 'ETAline-100-100-210'
% pump = 'Grundfos-NK-100-250';
%pump = 'Grundfos-NB-150-500';
pump = 'Grundfos-DP';

% pump = 'Grundfos-NB-150-500';
filename = ['D:\Phd\My written stuff\Figures\' pump '\Characteristic_curve.jpg'];
filename2 = ['D:\Phd\My written stuff\Figures\' pump '\Power_curve.jpg'];
A = imread([filename]);
B = imread([filename2]);
% A = imread([pathspec filename]);
f1 = figure;
ha = image(A);
f2 = figure;
hb = image(B);
figure(f1)


%% info on image 1
title('Click on the origin, the maximum value of the Y axis and the maximum value of the X axis')
[x,y] = ginput(3);
x_origin = x(1);
y_origin = y(1);
ymax =     y(2);
xmax =     x(3);
x_max_val =  input('What is the max value of the X axis? ');
y_max_val = input('What is the max value of the Y axis? ');

No_curves = input('How many curves are there? ');

tag = cell(1,No_curves);
X_in   = cell(1,No_curves);
Y_in   = cell(1,No_curves);


x_factor1 =  (xmax-x_origin) /x_max_val;
y_factor1 =  (ymax-y_origin) /y_max_val;

%% infor on image 2
    figure(f2)

    title('Click on the origin, the maximum value of the Y axis and the maximum value of the X axis')
    [x2,y2] = ginput(3);
    x_origin2 = x2(1);
    y_origin2 = y2(1);
    ymax2 =     y2(2);
    xmax2 =     x2(3);


    x_max_val2 = input('What is the max value of the X axis? ');
    y_max_val2 = input('What is the max value of the Y axis? ');
    x_factor2  = (xmax2-x_origin2) /x_max_val2;
    y_factor2  = (ymax2-y_origin2) /y_max_val2;



for i = 1:No_curves
    figure(f1)
    title('Click on the minimum and maximum x value of the curve')
    [x_limits{i}, ~] = ginput(2);
    hold on
    x_space1 = (x_limits{i}(2)-x_limits{i}(1))/(number_of_data_points-1);
    flow_measure_vals = zeros(1,number_of_data_points);
    
    for j = 1:number_of_data_points
        hr(j) = plot([x_limits{i}(1) x_limits{i}(1)]+ x_space1*(j-1) ,[ymax y_origin],'r');
        flow_measure_vals(j) =  (x_limits{i}(1)+x_space1*(j-1)-x_origin)/x_factor1;
    end
   
   
    title(['Click on ' num2str(number_of_data_points ) ' points of curve ' num2str(i)])
    [X_in{i},Y_in{i}] = ginput(number_of_data_points );
    tag{i} = input('Name the curve ','s');
    delete(hr);
    
    Out{i,2} = tag{i};
    Out{i,1}(:,1) = (X_in{i}-x_origin)/x_factor1;
    Out{i,1}(:,2) = (Y_in{i}-y_origin)/y_factor1;


%% second Image

    figure(f2)
    measure_points = flow_measure_vals*x_factor2+x_origin2;
    hold on
    for j = 1:number_of_data_points
        hr(j) = plot([measure_points(j) measure_points(j)] ,[ymax2 y_origin2],'r');
        hold on
        
    end
    title(['Click on ' num2str(number_of_data_points ) ' points of curve ' tag{i}])
    [X_in2{i},Y_in2{i}] = ginput(number_of_data_points );
    delete(hr)

    Out{i,1}(:,3) = (X_in2{i}-x_origin2)/x_factor2;
    Out{i,1}(:,4) = (Y_in2{i}-y_origin2)/y_factor2;
end


save(['Raw' pump num2str(randi(100))]) % emergency save

save(pump,'Out')


