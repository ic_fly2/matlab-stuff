function [c, Pe, GHG] = genmultiObjectivePrices(i,lambda_steps,name) 
% given a file name and number of lambda steps generates a set of input
% vectors for a multi objective analysis

load(name); % contains tariff and emissions
sel_t = tariff(i,:);
Pe =  sel_t/mean(sel_t);
sel_e = emissions(i,:);
GHG = sel_e/mean(sel_e);
lambda = linspace(0,1,lambda_steps)';

c = lambda*Pe  + (1-lambda)*GHG;






