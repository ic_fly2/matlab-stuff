% function [op] = hydraulic_solver(def)

% 
% %% nodes
% % Elevation
% Elevation = [0 0 0.5 0.3]; % elevation change for pumps is ignored!
% 
% % Reservoir
% % fixed head
% location_fix = [1];
% fixed_val = [1];% m
% 
% % variable head tanks
% location_res = [4];
% 
% Reservoir_area = 24*60*60/N; % m2
% 
% %% network
% % simple looped network
% connection_matrix = [...
%     1 -1 0 0;... %q1
%     0 1 -1 0;... %q2
%     0 0 1 -1;... %q3
%     0 1 0 -1 ;...  %q4
%     0 0 0 1]; % demand out
% 
% connection_type = {... 
%     'Pump_quad', ...
%     'pipe_piece',...
%     'pipe_piece',...
%     'pipe_piece',...
%     'demand'}; 
% 
% 
% %% hyraulic solver:
% no_s = sum(sum(abs(connection_matrix),1)-1); % number of slack variables needed on total
% S = eye(no_s);
% h_minor = 2;

%S
A(:,181:180+48) = 0;
Aeq(1:48,181:180+48) = eye(48);
Aeq = [Aeq; eye(36) zeros(36, 228-36)];
beq = [beq; zeros(N,1); ones(N,1); zeros(N,1) ];

% bounds
lb = [ repmat(T_bounds(1),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_min,2) repmat(q_bounds(1),1,N*np) repmat(lambda_bounds(1),1,N*length_m) -10000000*ones(1,48)];
ub = [ repmat(T_bounds(2),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_max,2) repmat(q_bounds(2),1,N*np) repmat(lambda_bounds(2),1,N*length_m) 10000000*ones(1,48) ];

% objective funtion
H = zeros(size(H));
H(181:180+48,181:180+48) = eye(48);

f = zeros(1,228);

% Integer Constraints
pumps_int(1:N*sum(n_pumps))  = 'C'; % could be B for binary or I for intiger
lambda_int(1:N*length_m) = 'I';
cont(1:N*nn + N*np) = 'C';
S_constraints(1,1:48) = 'C';
xtype = strcat([pumps_int cont lambda_int S_constraints]);
for i = 1:216
    Q{i}(181:228,181:228) = 0;
    l{i}(181:228,1) = 0; 
end