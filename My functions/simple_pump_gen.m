%simple_pump_gen

% generates: Q_pump_min,Q_pump_max,Delta_H 

    
j = 1;
for i = 1:length(n_pumps)
    for n = 1:n_pumps(i)
        
        T = zeros(sum(n_pumps),1);
    
        T(j) = 1;
        [ K,Nexp,Feeds,pumpsOn,connections] = pump_control(pipes,pumps,n_pumps,tanks,Nexp,K,npi,fitting_type,T);
        % average demands
        
      
        
        [A12,A10,H0] = makeA12A10(pipes,nodesIdListMap,Allreservoirs,pumpsOn);
        tol = 0.000001; % comment out for function use
        [H_gga,Q_gga] = solver4(A12,A10,H0,qall_mean,K,Nexp,tol,CV);
        
        Allreservoirs = [reservoirs tanks];
        for k = 1:no
            H_res2(k,1) =Allreservoirs{k}.Elev+Allreservoirs{k}.Init;
        end
        
        H =  [H_gga; H_res2];%-Elevation;
        
        OP_head(i,n) = H(nodesIdListMap(pumps{j}.endNodeId)) - H(nodesIdListMap(pumps{j}.startNodeId)); 
        OP_flow(i,n) = Q_gga(npi+i);
        
        
        
        
%         a_quad_pump(i,n) = a_org(i)/(n^2);
%         b_quad_pump(i,n)  = b_org(i)/n;
%         c_quad_pump(i,n) = c_org(i);
        
        %guess flow rates:
%         p = [a_quad_pump(i,n) b_quad_pump(i,n) c_quad_pump(i,n)];
%             q = linspace(0,max(roots(p)));
%             h = polyval(p,q);
%             [~,I] = max(q.*h*9.81);
%          OP_flow(i,n) = q(I);
%          OP_head(i,n) = h(I);
    j = j+1;
         
         
    end
end
Q_pump_min = OP_flow*0.8 ; % New row for each  pump sation fill with zeros for non needed stuff
Q_pump_max = OP_flow*2 ;
Delta_H    = OP_head ;



% Q_pump_min,Q_pump_max,Delta_H 






