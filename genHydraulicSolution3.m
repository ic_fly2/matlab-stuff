function [x0,u,u_H,u_q] =  genHydraulicSolution3(prob,def,N,pipe,length_m_part)
% Generates a feasible schedule if a control file exists and generates a
% feasible initial solution for the MIP.

%% generate feasible solution from nullspace algo:


% def = 'van_zyl';
% def = 'Richmond_skeleton';
% def = 'Lecture_example';
% def = 'Purton' % no control yet

prepNetwork;
prepGGA;

%% add pump power curve info
% pump_power_curve_data


%initiallise
H_save = zeros(nj+no,N);
Q_save = zeros(length([pipes])+ length(n_pumps),N);
% H_res = zeros(length(Allreservoirs),1);
T = ones(1,length(n_pumps)); 

% call EPS
for t = 1:N
    
    
    EPS_step;
       %   violation_control;
    if strfind(def,'van')
        van_zyl_control;
    elseif strfind(def,'Richmond')
        Richmond_skeleton_control
    else
        error('Network controlls not implimented')
    end
    T0(t,:) = T;
    
end

H_save = H_save'; Q_save = Q_save';
%%

for i = 1:N
    for j = 1:n_pipes
        mins = find(Q_save(i,j) > pipe(i).q_min);
        maxs = find(Q_save(i,j) < pipe(i).q_max);
        lambda(i,j) = min(intersect(mins,maxs));
        if lambda(i,j) > length_m_part(j)
            lambda(i,j) = length_m_part(j);
        end
        
        lambda_fixer{i,j} = zeros(length_m_part(j),1);
        lambda_fixer{i,j}(lambda(i,j)) = 1;
        
        
    end
end
lambda_fixer = vertcat(lambda_fixer{:});

singlefile(T0')

A_extra_T = [eye(length(singlefile(T0))) zeros(length(singlefile(T0)),size(prob.Aeq,2)-length(singlefile(T0)))];

A_extra_lambda = [zeros(length(singlefile(lambda_fixer)),size(prob.Aeq,2)-length(singlefile(lambda_fixer))) eye(length(singlefile(lambda_fixer))) ];
    
b_extra = [singlefile(T0')';lambda_fixer];


% min norm(C*x-d)^2

d = [singlefile(T0')'; singlefile(H_save')';   singlefile(Q_save')' ;lambda_fixer];
C = diag(d);

% still need the Aineq and Aeq matracies as well as beq and bineq
x =  cplexlsqmiqcp(C,d,Aineq,bineq,Aeq,beq,l,Q,r,[],[],[],lb,ub);
    if isfield(problem,'Q')
        [x0 ] = cplexlsqmiqcp(C,d,Aineq,bineq,Aeq,beq,l,Q,r,[],[],[],lb,ub);
    else
        [x0 ] = cplexlsqmilp(C,d,Aineq,bineq,Aeq,beq,[],[],[],lb,ub);
    end



