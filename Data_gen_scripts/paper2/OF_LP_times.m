
% 
 clear all; close all;
% def = 'Van_zyl';
% file_name = ['Results99' def '_times_and_fval_run1' ];
% N_set = [6 12 24 48];
% for approx_number = 9
%     for i_N = 1:4
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,5,600);
%         save(file_name)
%     end
% end
% 
% def = 'Richmond_skeleton_original';
% file_name = ['Results99' def '_times_and_fval_run1' ];
% N_set = [6 12 24 48];
% for approx_number = 9
%     for i_N = 1:4
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,5,600);
%         save(file_name)
%     end
% end
% 
% emailnotification('rmm08@ic.ac.uk','OF val results', 'success')
% 
% T5 = load('Results5_Richmond_skeleton_original_times_and_fval_run1');
% T9 =  load('Results9_Richmond_skeleton_original_times_and_fval_run1');



