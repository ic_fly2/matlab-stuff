% % run one
clear all; close all;
def = 'loop0';
file_name = ['Results_' def '_times_and_fval_run2' ];
N_set = [6 12 24 48];
for approx_number = [8 7 4 1]
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,2.5,600);
        save(file_name)
    end
end


% % run one
clear all; close all;
def = 'loop1';
file_name = ['Results_' def '_times_and_fval_run2' ];
N_set = [6 12 24 48];
for approx_number = [8 7 4 1]
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,2.5,600);
        save(file_name)
    end
end


% % run one
clear all; close all;
def = 'loop2';
file_name = ['Results_' def '_times_and_fval_run2' ];
N_set = [6 12 24 48];
for approx_number = [8 7 4 1]
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,2.5,600);
        save(file_name)
    end
end
