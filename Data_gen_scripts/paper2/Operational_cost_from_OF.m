% cost for OF for given schedule:
clear all;
load('Results_for_sample_OF_cost_calc')
T0 = reshape(schedule_tmp,[1 numel(schedule_tmp)]);
q = reshape(flow_display,[1 numel(flow_display)]);
list_OF_specs = {'Simple','Sum_only','Linear_power_no_switch','Quad_power'};
for i_OF = 1:length(list_OF_specs)
    OF_spec = list_OF_specs{i_OF};
    make_OF;
    x = [ T0 zeros(1,nn*N) q zeros(1,N*length_lambda)]';
    
    cost_computed(i_OF) =  0.5*x'*H*x + f*x;
end
      