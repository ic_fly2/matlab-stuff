% run one



try
clear all; close all;
def = 'Richmond_skeleton_pump_station1';
file_name = ['Results5' def '_times_and_fval_run1' ];
N_set = [6 12 24 48];
for approx_number = 4:8
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,4,600);
        save(file_name)
    end
end
end
% 
% clear all; close all;
% def = 'Richmond_skeleton_original';
% try
% file_name = ['Results4_' def '_times_and_fval_run1' ];
% N_set = [6 12 24 48];
% for approx_number = 1:8
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,3,600);
%         save(file_name)
%     end
% end
% end




clear all; close all;
def = 'Richmond_skeleton_pump_station1';
% load('Results8_Richmond_skeleton_pump_station1_times_and_fval_run1.mat')
file_name = ['Results9_' def '_times_and_fval_run1' ];
for approx_number =[7 5 8 6 4 ]  	
        N = 48;
        [time_taken{approx_number,1}, fval{approx_number,1}, diff_H{approx_number,1}, diff_Q{approx_number,1},diff_Q_per{approx_number,1} ,diff_H_per{approx_number,1}, Remaining_gap{approx_number,1},  x{approx_number,1} ] = experiment(def,N,approx_number,5,600);
        save(file_name)
end

clear all; close all;
def = 'Richmond_skeleton_original';
file_name = ['Results9_' def '_times_and_fval_run1' ];
for approx_number = [5 6 4 ]     	
        N = 48;
        [time_taken{approx_number,1}, fval{approx_number,1}, diff_H{approx_number,1}, diff_Q{approx_number,1},diff_Q_per{approx_number,1} ,diff_H_per{approx_number,1}, Remaining_gap{approx_number,1},  x{approx_number,1} ] = experiment(def,N,approx_number,5,600);
        save(file_name)
end
clear all; close all;
% 
% try
%     file_name = ['Results6_' def '_times_and_fval_run1' ];
%     N_set = [6 12 24 48];
%     for i_N = 1:4
%         for approx_number = 4:8
%             N = N_set(i_N);
%             [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,1}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,5,600);
%             save(file_name)
%         end
%     end
% end


% 
% 
% clear all; close all;
% def = 'Richmond_skeleton_original';
% file_name = ['Results6_' def '_times_and_fval_run1' ];
% for approx_number = 4:8    	
%         N = 48;
%         [time_taken{approx_number,1}, fval{approx_number,1}, diff_H{approx_number,1}, diff_Q{approx_number,1},diff_Q_per{approx_number,1} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,1},  x{approx_number,1} ] = experiment(def,N,approx_number,5,600);
%         save(file_name)
% end

% 
% %% ultimate solution
% try
% clear all; close all;
% def = 'Richmond_skeleton_pump_station1';
% file_name = ['Results3' def '_ultimate' ];
% for approx_number = 1:8
%     [time_taken{approx_number,1}, fval{approx_number,1}, ~, ~,~ ,~ ,Remaining_gap{approx_number,i_N}, x{approx_number,i_N} ] = experiment(def,48,approx_number,3,6000);
%      save(file_name)
% end
% end
% 
% %% diference in flow limits (bounds):
% try
% clear all; close all;
% def = 'Richmond_skeleton_pump_station1';
% file_name = ['Results3' def '_times_and_fval_run1' ];
% N_set = [6 12 24 48];
% for approx_number = 1:8
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,3,1800);
%         save(file_name)
%     end
% end
% end
% 
% try
% clear all; close all;
% def = 'Richmond_skeleton_pump_station1';
% file_name = ['Results2' def '_times_and_fval_run1' ];
% N_set = [6 12 24 48];
% for approx_number = 1:8
%     for i_N = 1:4	
%         N = N_set(i_N);
% e        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,6,1800);
%         save(file_name)
%     end
% end
% end