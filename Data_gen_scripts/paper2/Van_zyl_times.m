% % run one


clear all; close all;
def = 'van_zyl';
file_name = ['Results3' def '_times_and_fval_run_proper' ];
N_set = [6 12 24 48];
for approx_number = 1:7
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
            diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
            diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
            Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = ...
            experiment(def,N,approx_number,4.5,99,600);
        save(file_name)
    end
end
emailnotification('rmm08@ic.ac.uk','success1',file_name)

clear all; close all;
def = 'van_zyl';
file_name = ['Results3_' def '_times_and_fval_run_proper' ];
N_set = [6 12 24 48];
for approx_number = [1 3 7]
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
            diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
            diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N},...
            Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] =...
            experiment(def,N,approx_number,4.5,99,600*5,'yes');
        save(file_name)
    end
end

 emailnotification('rmm08@ic.ac.uk','success',file_name)

%% effect of larger limits:


clear all; close all;
def = 'van_zyl';
file_name = ['Results2' def '_times_vmax_45' ];
N_set = [6 12 24 48];
for approx_number = 1:8
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,4.5,1800);
        save(file_name)
    end
end

clear all; close all;
def = 'van_zyl';
file_name = ['Results2' def '_times_vmax_6' ];
N_set = [6 12 24 48];
for approx_number = 1:8
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,6,1800);
        save(file_name)
    end
end
