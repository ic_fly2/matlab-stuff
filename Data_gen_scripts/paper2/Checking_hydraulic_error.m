%% Check Hydraulic errors:
clear all;
T5 = load('Results5_Richmond_skeleton_original_times_and_fval_run1');
T10 = load('Results10_original_appox_7.mat');
T10_2 = load('Results10_original_approx5');
T9 = load('Results9_Richmond_skeleton_original_times_and_fval_run1');
T99= load('Results99Richmond_skeleton_original_times_and_fval_run1');

def = 'Richmond_skeleton_original';
T9.x{7} =  T10.x;
T9.x{8} =  NaN;
x_cell = [T5.x T9.x; T99.x(9,1) T99.x(9,2) T99.x(9,3) T99.x(9,4)];

for ii = 4:9 % approximation
    for j = 1:4 % time step size
         x = x_cell{ii,j}; % schedule
         N = 3*2^j;
%          T0 = x(1:N*7);
         %          T0 = reshape(T0,7,N); % not sure yet
         if isnan(x)
             diff_cell{ii,j}.diff_H  = NaN ;
             diff_cell{ii,j}.diff_Q  = NaN ;
             diff_cell{ii,j}.diff_H_per  = NaN ;
             diff_cell{ii,j}.diff_Q_per  = NaN ;
             break;
         end
             
             
             
         data = reshape(x,[N length(x)/N ]); %Make it pretty
         % pump settings
         schedule_tmp = zeros(N,7);
         for i= 1:7 % get the schedule data from the solver
             schedule_tmp(:,i) = data(:,i);
         end
         if exist('live','var') && live == 1;
             schedule_tmp = round(schedule_tmp); %avoid mistakes from near roundings
             for ii = 1:7
                 for i= n_pumps(ii)-1:-1:1; % make schedule data filled with ones at lower pump settings
                     schedule_tmp(:,i) = schedule_tmp(:,i) + schedule_tmp(:,1+i);
                 end
             end
             schedule_tmp(schedule_tmp >= 1) = 1; %Set everything back to 1
         end
         
         % node head:
         
         head_display = data(:,7+(1:48));
         % pipe flow:
         flow_display = data(:,7+48+(1:51));
         
         [H_save Q_save power_con] = EPS_from_T0(schedule_tmp,def,N);%,tank_init)
         
        diff_H = head_display - H_save;
        diff_Q = flow_display - Q_save;
        
        diff_Q_per = (sum(abs(diff_Q))./sum(diff_Q~= 0 ))./(sum(abs(Q_save))./sum(Q_save~= 0 ));
        diff_H_per = (sum(abs(diff_H))./sum(diff_H~= 0 ))./(sum(abs(H_save))./sum(H_save~= 0 ));

        diff_cell{ii,j}.diff_H  = diff_H ;
        diff_cell{ii,j}.diff_Q  = diff_Q ;
        diff_cell{ii,j}.diff_H_per  = diff_H_per ;
        diff_cell{ii,j}.diff_Q_per  = diff_Q_per ;
         

    end
end