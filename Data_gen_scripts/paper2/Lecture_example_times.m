% % run one
clear all; close all;
def = 'Lecture_example';
file_name = ['Results_' def '_times_and_fval_run1' ];
N_set = [6 12 24 48];
for approx_number = 1:12
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, Remaining_gap{approx_number,i_N},  x{approx_number,i_N} ] = experiment(def,N,approx_number,2,600);
        save(file_name)
    end
end

clear all; close all;
def = 'Lecture_example_no_station';
file_name = ['Results' def '_times_and_fval_run1' ];
N_set = [6 12 24 48];
for approx_number = 1:12
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} ,Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = experiment(def,N,approx_number,2,600);
        save(file_name)
    end
end

