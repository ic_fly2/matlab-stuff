clearvars
% def.N = 12;
% def.approx_number = 7;
% def.vmax_min = 4.5;
% def.M  = 999;
% def.settings.time_limit  = 600;
% def.Desired_ratio = NaN;
% def.asymmetry = 'on';
% def.def =  'van_zyl';
% def.settings.price = rand(1,48).^2*100;
% def.settings.DR.status = 'Off';
% def.settings.DR.desired_ratio = 12;
% def.settings.DR.rest_period = 30; %minutes
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing


def.approx_number = 7;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 600;
def.Desired_ratio = NaN;
def.asymmetry = 'on';

% def.def = 'van_zyl_norm'

% def.def =  'Lecture_example_norm';
def.settings.price =   EL.price{1}; 
def.settings.DR.status = 'Off';
def.settings.DR.flex = 'No';

def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 240; %minutes
def.settings.DR.reward = 12000;
def.settings.DR.desired_share = 0.0;%0.0013333;
def.settings.DR.demand_factor = 1;

def.settings.GHG = CO2_kWh_av ;


for i = 1:4
    def.N =3*2^i;
    def.def =  'Richmond_skeleton';
    def.approx_number = 8;
    out{i} = experiment(def);
end
save('three_piece_Richmond')
emailnotification('rmm08@ic.ac.uk','Richmond','done')



for i = 1:4
    def.N =3*2^i;
    def.def =  'van_zyl';
    def.approx_number = 8;
    out2{i} = experiment(def);
end
 save('three_piece_van_zyl')
 emailnotification('rmm08@ic.ac.uk','Richmond','done')
