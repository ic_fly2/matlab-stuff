%% van zyl
cd D:\Phd\MATLAB\Phd\Results
datum = datestr(now,'mm_dd_yy');
if exist(datum,'file') ~=7
    mkdir(datum)
end

try
    clearvars -except datum
% def = 'loop0';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [5 8];
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N},...
%             fval_sim{approx_number,i_N},fval_diff{approx_number,i_N},...
%             fval_diff_per{approx_number,i_N}] = ...
%             experiment(def,N,approx_number,4.5,99,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success',file_name)
% 
% clearvars -except datum
% def = 'loop1';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [5 8];
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N},...
%             fval_sim{approx_number,i_N},fval_diff{approx_number,i_N},...
%             fval_diff_per{approx_number,i_N}] = ...
%             experiment(def,N,approx_number,4.5,99,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success',file_name)
% 
% 
% clearvars -except datum
% def = 'loop2';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [ 5 8];
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N},...
%             fval_sim{approx_number,i_N},fval_diff{approx_number,i_N},...
%             fval_diff_per{approx_number,i_N}] = ...
%             experiment(def,N,approx_number,4.5,99,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success',file_name)

% clearvars -except datum
% def = 'van_zyl';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [1:12];
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N},...
%             fval_sim{approx_number,i_N},fval_diff{approx_number,i_N},...
%             fval_diff_per{approx_number,i_N}] = ...
%             experiment(def,N,approx_number,4.5,99,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success',file_name)

% clearvars -except datum
% def = 'Richmond_skeleton_modified';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [1:12];
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N},...
%             fval_sim{approx_number,i_N},fval_diff{approx_number,i_N},...
%             fval_diff_per{approx_number,i_N}] = ...
%             experiment(def,N,approx_number,4.5,999,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success',file_name)


% 
def = 'van_zyl';
file_name = ['Results_' def '_times_and_fval_run_sym_' datestr(now,'mm_dd_yy_HH_MM')];
N_set = [6 12 24 48];
for approx_number = [7]
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
            diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
            diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N},...
            Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] =...
            experiment(def,N,approx_number,4.5,99,600,'yes');
        save([datum '/' file_name] )
    end
end

 emailnotification('rmm08@ic.ac.uk','success2',file_name)
 
 %% Richmond
% clearvars -except datum
% def = 'Richmond_skeleton_modified';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [9:12]
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = ...
%             experiment(def,N,approx_number,4.5,999,600);
%         save([datum '/' file_name])
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success3',file_name)
% 
%     
% clearvars -except datum     
% def = 'Richmond_skeleton_modified';
% file_name = ['Results_' def '_times_and_fval_run_sym_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = [1]
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N},...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] =...
%             experiment(def,N,approx_number,3.5,999,600*5,'yes');
%         save([datum '/' file_name])
%     end
% end
% 
%  emailnotification('rmm08@ic.ac.uk','success4',file_name)
 
 
%  clear all; close all;
% def = 'Richmond_skeleton_modified';
% file_name = ['Results' def '_times_and_fval_run_proper_' datestr(now,'mm_dd_yy_HH_MM')];
% N_set = [6 12 24 48];
% for approx_number = 1
%     for i_N = 1:4	
%         N = N_set(i_N);
%         [time_taken{approx_number,i_N}, fval{approx_number,i_N},...
%             diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},...
%             diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N}, ...
%             Remaining_gap{approx_number,i_N} , x{approx_number,i_N} ] = ...
%             experiment(def,N,approx_number,3.5,9999,6000);
%         save(file_name)
%     end
% end
% emailnotification('rmm08@ic.ac.uk','success3',file_name)
catch
    emailnotification('rmm08@ic.ac.uk','FAIL',file_name)
end

