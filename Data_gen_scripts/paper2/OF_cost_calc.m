% calculate computed and simulated OF for a given flow configuration
clear all
load('Results2van_zyl_times_and_fval_run1.mat')

N = 48;
x = x{7,4};
T = x(1:3*N);
q_all = x(10*N+(1:8*N));
q_main = x(16*N+(1:N));
q_booster = x(17*N+(1:N));
q_pumps = [q_main; q_booster];
% van_zyl specific:
for i = 1:N
    if T(N+i) == 1
        q_pumps(i) = q_pumps(i)/2; % there is no two flow arangement due to the pump stations
    end
end


% prep make_OF
Delta_T = 24*60*60/N;
filename = ['van_zyl.inp'];
prepNetwork;

Power_nameplate = []; a_org = []; b_org = []; c_org = [];
for i = 1:length(pumps)
    if strcmp(pumps{i}.Unique,'Yes')
        a_org = [a_org pumps{i}.a];
        b_org = [b_org pumps{i}.b];
        c_org = [c_org pumps{i}.c];
        
        Power_nameplate = [Power_nameplate ; pumps{i}.power];
    end
end

% prices
switch_penalty = 0.15;
Electricity_Price(1:11) = 28.59;
Electricity_Price(12:19) = 85.76;
Electricity_Price(13:33) = 48.49;
Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49;
Electricity_Price(45:48) =  28.59;
Pe = zeros(1,N);
for ii = 1:N;
    Pe(ii) = sum(Electricity_Price((ii-1)*floor(48/N)+(1:floor(48/N))));
end
Pe = Pe/ (48/N);

[a_quad_pump,b_quad_pump,c_quad_pump,~,~, ~,~,~,~] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,[],[],5,'off');


%% OF evals:
%% 1a / 7a Standard same as
%OF_spec = 'Simple';
f_cost = []; % pricing for pump operations
for i = 1:length(n_pumps)
    driver = 1:n_pumps(i);
    cost = place_matrix(Power_nameplate(i)*Pe*Delta_T/3600,driver,2);
    f_cost = [f_cost cost];
end

H_switch = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
H_switch = H_switch*switch_penalty;
cost_fval(1) = 0.5*T'*H_switch*T + f_cost*T;
%% 1b / 7d quadratic pump power

%OF_spec = 'Quad_power';
[~, p2] = add_power_curve(a_quad_pump,b_quad_pump,c_quad_pump,n_pumps);

%%% f
% f for T
f_cost = []; % pricing for pump operations
f_flow_cost = [];
for i = 1:length(n_pumps)
    driver = 1:n_pumps(i);
    cost  = place_matrix(p2{i}(3)*Pe*Delta_T/3600,driver,2);
    f_cost = [f_cost cost];
end

%for q
for i = 1:length(n_pumps)
    flow_cost  = p2{i}(2)*Pe*Delta_T/3600;
    f_flow_cost = [f_flow_cost flow_cost];
end
f_flow_cost = [f_flow_cost ];

% make f
f = [f_cost  f_flow_cost];

%%% H
q_flow_cost = [];
H_switch = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
H_switch = H_switch*switch_penalty;


%for q
for i = 1:length(n_pumps)
    flow_cost_quad  = p2{i}(1)*Pe*Delta_T/3600;
    q_flow_cost = [q_flow_cost flow_cost_quad];
end
H_flow = diag(q_flow_cost) ;
H = blkdiag(H_switch,H_flow);


cost_fval(2) = 0.5*[T;q_pumps]'*H*[T;q_pumps] + f*[T;q_pumps];
%% 7b No switch
% OF_spec = 'Sum_only';
f_cost = []; % pricing for pump operations
for i = 1:length(n_pumps)
    driver = 1:n_pumps(i);
    cost = place_matrix(Power_nameplate(i)*Pe*Delta_T/3600,driver,2);
    f_cost = [f_cost cost];
        end
cost_fval(3) =  f_cost*T;

%% 7c linear pump power
[p1, ~] = add_power_curve(a_quad_pump,b_quad_pump,c_quad_pump,n_pumps);
        
% for T
f_cost = []; % pricing for pump operations
f_flow_cost = [];
for i = 1:length(n_pumps)
    driver = 1:n_pumps(i);
    cost  = place_matrix(p1{i}(2)*Pe*Delta_T/3600,driver,2);
    f_cost = [f_cost cost];
end

%for q
for i = 1:length(n_pumps)
    flow_cost  = p1{i}(1)*Pe*Delta_T/3600;
    %             flow_cost  = place_matrix(p1{i}(1)*Pe*Delta_T/3600,driver,2);
    f_flow_cost = [f_flow_cost flow_cost];
end
% make f
f = [f_cost  f_flow_cost ];

cost_fval(4) =  f*[T;q_pumps];      
%% Simulation


