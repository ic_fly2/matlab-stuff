% Result generation for DR paper with new data :(
%% FFR 3D reward, price strech and demand needed for van zyl network
% to be completed later
clearvars;
%% STOR Windowed on both networks looking at the relationship of demand and 
% reward needs as well as pricing
clearvars;
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing

def.N =48;
def.approx_number = 5;
def.vmax_min = 7;
def.M  = 9999;
def.settings.time_limit  = 600;
def.Desired_ratio = NaN;
def.asymmetry = 'on';

% def.def = 'Purton_fake6';
% def.def =  'Richmond_skeleton';
% def.def =  'Lecture_example_norm';
def.def = 'van_zyl_norm';
def.settings.price =   strech(EL.price{2},1); 
def.settings.DR.status = 'Optimal';
def.settings.DR.flex = 'No';

def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 240; 
% def.settings.DR.reward = 30000;
def.settings.DR.desired_share =0.2;

% def.settings.DR.demand_factor = 0.;
%def.settings.DR.recovery = 240;
def.settings.mods.status = 'No';
def.settings.plot = 'Off';
def.settings.Time_shift =0;
def.settings.GHG = CO2_kWh_av ;
def.settings.solver_gap = 0.02;

%% Investigating demand factor and price strech. Reward level was confirmed 
% to be irrelevant. Looking at fully commited FFR service first.
% 
% for i = 1:15
%     for j = 1:14
%         def.settings.DR.reward = 60000;
%         def.settings.DR.demand_factor = 0.6+i*0.4/8;
%         def.settings.price =   strech(EL.price{2},-0.1+j/10); 
%         out = experiment(def);
%         results{i,j}.def = def;
%         results{i,j}.fval = out.fval;
%         results{i,j}.GHG = out.GHG;
%         results{i,j}.schedule_tmp = out.schedule_tmp;
%         results{i,j}.Remaining_gap = out.Remaining_gap;
%         results{i,j}.DR_D  = out.DR_D;
%         results{i,j}.cost_pump  = out.cost_pump;
%         results{i,j}.rev_DR  = out.rev_DR ;
%     end
%  end
% disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
% save('noflex_demand2_1-14_price_strech_0-1_van_zyl_norm_15_09_2015')
% 
% % windowed (both)
% for i = 1:15;
%     for j = 1:14
%         def.settings.DR.reward = 30000;
%         def.settings.DR.flex = 'Windows';
%         def.settings.DR.demand_factor = 0.90+i*0.4/8;
%         def.settings.price =   strech(EL.price{2},-0.1+j/10); 
%         out = experiment(def);
%         results{i,j}.def = def;
%         results{i,j}.fval = out.fval;
%         results{i,j}.GHG = out.GHG;
%         results{i,j}.schedule_tmp = out.schedule_tmp;
%         results{i,j}.Remaining_gap = out.Remaining_gap;
%         results{i,j}.DR_D  = out.DR_D;
%         results{i,j}.cost_pump  = out.cost_pump;
%         results{i,j}.rev_DR  = out.rev_DR ;
%     end
%  end
% disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
% save('windows_demand_1-14_price_strech_0-1_van_zyl_norm_15_09_2015')

% window1
% for i = 1:5;
%     for j = 1:14
%         def.settings.DR.reward = 30000;
%         def.settings.DR.flex = 'Window1';
% %         def.settings.DR.demand_factor = 0.90+i*0.4/8; %original
%         def.settings.DR.demand_factor = 0.45+i*0.4/8; % additional
%         def.settings.price =   strech(EL.price{2},-0.1+j/10); 
%         out = experiment(def);
%         results{i,j}.def = def;
%         results{i,j}.fval = out.fval;
%         results{i,j}.GHG = out.GHG;
%         results{i,j}.schedule_tmp = out.schedule_tmp;
%         results{i,j}.Remaining_gap = out.Remaining_gap;
%         results{i,j}.DR_D  = out.DR_D;
%         results{i,j}.cost_pump  = out.cost_pump;
%         results{i,j}.rev_DR  = out.rev_DR ;
%     end
%  end
% disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
% save('window1_demand_05-07_price_strech_0-1_van_zyl_norm_17_09_2015_additional2')

% no DR
for i = 1:19;
    for j = 1:14
        def.settings.DR.status = 'Off';
        def.settings.DR.reward = 30000;
        def.settings.DR.flex = 'Window1';
%         def.settings.DR.demand_factor = 0.90+i*0.4/8; %original
        def.settings.DR.demand_factor = 0.45+i*0.4/8; % additional
        def.settings.price =   strech(EL.price{2},-0.1+j/10); 
        out = experiment(def);
        results{i,j}.def = def;
        results{i,j}.fval = out.fval;
        results{i,j}.GHG = out.GHG;
        results{i,j}.schedule_tmp = out.schedule_tmp;
        results{i,j}.Remaining_gap = out.Remaining_gap;
        results{i,j}.DR_D  = out.DR_D;
        results{i,j}.cost_pump  = out.cost_pump;
        results{i,j}.rev_DR  = out.rev_DR ;
    end
 end
disp(['fval: ' num2str(out.fval)  '  DR: ' num2str(out.DR_D)  '  GHG: ' num2str(out.GHG)])
save('noDR_demand_05-14_price_strech_0-14_van_zyl_norm_17_09_2015')


% flex (confirmation) Already have the data.
%% 
