% DR_result runs
clearvars

% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

EL_pricing


def.N =24;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 1000;
def.Desired_ratio = NaN;
def.asymmetry = 'on';
def.settings.DR.desired_ratio = 20;
def.settings.DR.rest_period = 1; %minutes
def.settings.DR.desired_share = 0;
def.settings.DR.demand_factor = 1;
def.settings.GHG = CO2_kWh_av;

%% Van Zyl
%VSD
 for r = 1:10
% r = 1;
for i = 1:10

    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'Yes';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 0.6+i/20;
    out{i,r} = experiment(def);
end
 end

    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE1' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
 
    
    
    
for r = 1:10
for i = 1:10
    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'Yes';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 0.6+i/20;
    out{i,r} = experiment(def);
end
end

    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE2' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
    
    
for r = 1:10
for i = 1:10

    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'no';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 0.6+i/20;
    out{i,r} = experiment(def);
end
end

    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE2a' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
    
    
for r = 1:10
for i = 1:10

    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 16;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'no';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 0.6+i/20;
    out{i,r} = experiment(def);
end
end

    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE1' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
    
    
    
    

        
for r = 1:10


    def.def =  'Richmond_skeleton';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'Yes';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 1;
    out{i,r} = experiment(def);
end
    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE3a' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)

    
            
for r = 1:10


    def.def =  'Richmond_skeleton';
    def.settings.price =  EL.price{2};
    def.approx_number = 16;
    def.settings.DR.reward = r*1000;
    def.settings.DR.flex = 'Yes';
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.demand_factor = 1;
    out{i,r} = experiment(def);
end
    name = ['D:\Phd\MATLAB\Phd\Results\DR\real\CASE3' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)


emailnotification('rmm08@ic.ac.uk','Done','Van Zyl VSD')

