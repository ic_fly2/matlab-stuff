% Finding the formula to predict DR performance
clear all
 Lecture_example_questionair_defenitions
% out{length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)} = [];
out_dr(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
out_ghg(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
out_fval(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
out_cost_pump(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
out_rev_dr(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
def.settings.solver_gap = 0.02;
def.settings.time_limit  = 60;
for i1 = 1:length(reward_set)
    for i2 = 1:length(Length_set)
        for i3 = 1:length(Elev_set)
            for i4 = 1:length(Demand_set)
                for i5 = 1:length(storage_set)
                    for i6 = 1:length(price_set)

def.settings.DR.reward = reward_set(i1); %50--100k
def.settings.mods.Length_factor = Length_set(i2); %0.1:6
def.settings.mods.Elev_factor = Elev_set(i3);% 0.1 --1.4
def.settings.mods.Demand_factor = Demand_set(i4);% 1:1.7
def.settings.mods.storage_time = storage_set(i5); %2 :2: 24 
def.settings.price =   strech(EL.price{2},price_set(i6)); %0.1 -- 1
% if isempty(out{i1,i2,i3,i4,i5,i6})
% out{i1,i2,i3,i4,i5,i6} = experiment(def);
% end

out= experiment(def);
out_dr(i1,i2,i3,i4,i5,i6) = out.DR_D;
out_ghg(i1,i2,i3,i4,i5,i6) = out.GHG;
out_fval(i1,i2,i3,i4,i5,i6) = out.fval;
out_cost_pump(i1,i2,i3,i4,i5,i6) = out.cost_pump;
out_rev_dr(i1,i2,i3,i4,i5,i6) = out.rev_DR;

clear out
save('Questionaire6')
                    end
                end
            end
        end
    end
end