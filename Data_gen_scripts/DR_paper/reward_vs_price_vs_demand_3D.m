clear all

% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

EL_pricing


def.N =24;
prob.settings.solver_gap = 0.0;
def.vmax_min = 6.2;
def.M  = 999;
def.settings.time_limit  = 600;
def.Desired_ratio = NaN;
def.asymmetry = 'on';
def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 15; %minutes
def.settings.DR.desired_share = NaN;
def.settings.DR.demand_factor = 1;
def.settings.GHG = CO2_kWh_av;

%% Normed lecture example
% for r = 1:20
%     for i = 1:20
%         def.def =  'Van_zyl_norm';
%         def.settings.price =  EL.price{3};
%         def.approx_number = 16;
%         def.settings.DR.reward = r*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'Optimal';
%         def.settings.DR.demand_factor = 0.7+i/20;
%         out2{r,i} = experiment(def);       
%     end
% end


% 
% for r = 1


% for r = 1:10
%     for i = 1:10
%         for p = 1:10 
%         def.def =  'Richmond_skeleton';
% 
%         def.settings.price =  strech(EL.price{2},p/5);
%         def.approx_number = 7;
%         def.settings.DR.reward = r*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'Optimal';
%         def.settings.DR.demand_factor = 0.7+i/10;
%         out = experiment(def);      
% %         def.settings.x0 = out.x0; 
%         dr_lev(r,i,p) = out.DR_D;
%         GHG(r,i,p) = out.GHG;
%         cost(r,i,p) = out.cost_pump;
%         end
%     end
% end
% save(['D:\Phd\MATLAB\Phd\Results\DR\real_demand_feas_range_Richmond_norm2' datestr(now,30) '.mat'])
% emailnotification('rmm08@ic.ac.uk','Succes','Richmond')
%  
% for r = 1:10

%     for i = 1:10
%          for p = 1:10 
%         def.def =  'van_zyl_norm';
% 
%         def.settings.price =  strech(EL.price{2},p/5);
%         def.approx_number = 7;
%         def.settings.DR.reward = 1*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'On';
%         def.settings.DR.demand_factor = 0.7+i/10;
%         def.settings.DR.desired_share =0;
%         out = experiment(def);      
% %         def.settings.x0 = out.x0; 
% %         dr_lev(r,i,p) = out.DR_D;
%         GHG(i,p) = out.GHG;
%         cost(i,p) = out.cost_pump;
% %         GHG(r,i,p) = out.GHG;
% %         cost(r,i,p) = out.cost_pump;
%         end
%      end
% end
% save(['D:\Phd\MATLAB\Phd\Results\DR\real_demand_feas_range_Richmond_norm2' datestr(now,30) '.mat'])
% emailnotification('rmm08@ic.ac.uk','Succes','Richmond')
%
bad_files = [];
for r = 1:10
    for i = 1:10
        for p = 1:10 
        def.def =  'van_zyl_norm_179';
        def.settings.price =  strech(EL.price{2},p/5-0.2);
        def.approx_number = 7;
        def.settings.DR.reward = r*10000;
        def.settings.DR.rest_period = 15; %minutes
        def.settings.DR.status = 'Optimal';
        def.settings.DR.demand_factor = 0.7+i/10;
        out = experiment(def);      
%         def.settings.x0 = out.x0; 
        dr_lev(r,i,p) = out.DR_D;
        GHG(r,i,p) = out.GHG;
        cost(r,i,p) = out.cost_pump;
        
        if out.Remaining_gap ~= 0
            bad_files = [bad_files; r i p out.Remaining_gap out.time_taken];
        end
            
        end
    end
end
 save(['D:\Phd\MATLAB\Phd\Results\DR\3D_plot_FFR_179_demand.mat'])
% save(['D:\Phd\MATLAB\Phd\Results\DR\real_demand_feas_range_van_zyl' datestr(now,30) '.mat'])
emailnotification('rmm08@ic.ac.uk','Succes','Van_zyl')
% 
% 
% def.N =24;
% 
% for r = 1:10
%     for i = 1:10
%         for p = 1:10 
%         def.def =  'Van_zyl_norm';
% 
%         def.settings.price =  strech(EL.price{2},p/5);
%         def.approx_number = 5;
%         def.settings.DR.reward = r*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'Optimal';
%         def.settings.DR.demand_factor = 0.7+i/10;
%         out = experiment(def);      
% %         def.settings.x0 = out.x0; 
%         dr_lev(r,i,p) = out.DR_D;
%         GHG(r,i,p) = out.GHG;
%         cost(r,i,p) = out.cost_pump;
%         end
%     end
% end
% save(['D:\Phd\MATLAB\Phd\Results\DR\real_demand_feas_range_van_zyl' datestr(now,30) '.mat'])
% emailnotification('rmm08@ic.ac.uk','Succes','Richmond')
% 
% 
% 
% for r = 1:10
%     for i = 1:10
%         for p = 1:10 
%         def.def =  'Richmond_skeleton';
% 
%         def.settings.price =  strech(EL.price{2},p/5);
%         def.approx_number = 5;
%         def.settings.DR.reward = r*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'Optimal';
%         def.settings.DR.demand_factor = 0.7+i/10;
%         out = experiment(def);      
% %         def.settings.x0 = out.x0; 
%         dr_lev(r,i,p) = out.DR_D;
%         GHG(r,i,p) = out.GHG;
%         cost(r,i,p) = out.cost_pump;
%         end
%     end
% end
% save(['D:\Phd\MATLAB\Phd\Results\DR\real_demand_feas_range_Richmond_norm2' datestr(now,30) '.mat'])
% emailnotification('rmm08@ic.ac.uk','Succes','Richmond')
 

