CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing

def.N =48;
def.approx_number = 7;
def.vmax_min = 6.2;
def.M  = 999;
def.settings.time_limit  = 60;
prob.settings.solver_gap = 0.01;
def.Desired_ratio = NaN;
def.asymmetry = 'on';


% def.def = 'van_zyl_norm'
% def.def =  'Richmond_skeleton';
def.def =  'Lecture_example_norm';
def.settings.DR.status = 'Optimal';
def.settings.DR.flex = 'No';

def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 15; %minutes
def.settings.DR.desired_share = 0.3333;
def.settings.DR.demand_factor = 1;
def.settings.mods.status = 'Yes';
def.settings.GHG = CO2_kWh_av ;


reward_set = [20 30 40 50 60 70 80 90 100]*1000;
Length_set = [1];
Elev_set=  0.3497;%1;%[0.2:0.4:1.4];
Demand_set= [0:0.1:0.7]+1;
storage_set= [2:2:24];
price_set= [0:0.2:1.4];% [0.1:0.2:1 1 1.2 1.4];
