% DR_result runs
clear all

% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

EL_pricing

def.N =24;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 1000;
def.Desired_ratio = NaN;
def.asymmetry = 'on';
def.settings.DR.desired_ratio = 20;
def.settings.DR.rest_period = 240; %minutes
def.settings.DR.desired_share = 0;
def.settings.DR.demand_factor = 1;
def.settings.GHG = CO2_kWh_av;




% for s = 1:8
%     def.N =12;
%     def.def =  'Richmond_skeleton';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 16;
%     def.settings.DR.reward = 12500+2500*i;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'Optimal';
%     def.settings.DR.desired_share = 0;
%     def.settings.DR.demand_factor = 1.5+s/10;
%     out{s} = experiment(def);
%     end
% % end
% save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')

% for s = 1:8
%     def.N =12;
%     def.def =  'Richmond_skeleton';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 16;
%     def.settings.DR.reward = 12500+2500*i;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'Optimal';
%     def.settings.DR.flex = 'Yes';
%     def.settings.DR.desired_share = 0;
%     def.settings.DR.demand_factor = 1.5+s/10;
%     out{s} = experiment(def);
%     end
% % end
% save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')

for s = 1:8
    def.N =12;
    def.def =  'Richmond_skeleton';
    def.settings.price =  EL.price{2};
    def.approx_number = 16;
    def.settings.DR.reward = 12500+2500*i;
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.flex = 'Yes';
    def.settings.DR.desired_share = 0;
    def.settings.DR.demand_factor = 1.5+s/10;
    out{s} = experiment(def);
    end
% end
save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')



% Not completed:
% load('C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_van_zyl_norm_20150726T001229.mat')


%% Van Zyl VSD
for i = 1:7
for s = 8:10
    def.N =24;
    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = 12500+2500*i;
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.desired_share = 0;
    def.settings.DR.demand_factor = 0.7+s/10;
    out{s,i} = experiment(def);
    end
end
save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')



for i = 1:7
for s = 1:10
    def.N =6;
    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 16;
    def.settings.DR.reward = 12500+2500*i;
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'Optimal';
    def.settings.DR.desired_share = 0;
    def.settings.DR.demand_factor = 0.7+s/10;
    out{s,i} = experiment(def);
    end
end
save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')


% for i = 1:7


% 
% for i = 1:7
% for s = 1:10
%     def.N =24;
%     def.def =  'Lecture_example_norm';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 7;
%     def.settings.DR.reward = 12500+2500*i;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'Optimal';
%     def.settings.DR.desired_share = 0;
%     def.settings.DR.demand_factor = 0.7+s/10;
%     out{s,i} = experiment(def);
%     end
% end
% save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')
% 
% 
% for i = 1:7
% for s = 1:10
%     def.N =12;
%     def.def =  'Van_zyl_norm';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 16;
%     def.settings.DR.reward = 12500+2500*i;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'Optimal';
%     def.settings.DR.desired_share = 0;
%     def.settings.DR.demand_factor = 0.7+s/10;
%     out{s,i} = experiment(def);
%     end
% end
% save(['C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_' def.def '_' datestr(now,30) '.mat'],'def','out')
