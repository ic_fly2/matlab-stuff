% DR event model
clearvars
% settings:
EL_pricing

def.N =24;
def.approx_number = 7;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 600;
def.Desired_ratio = NaN;
def.asymmetry = 'on';

def.def = 'van_zyl_norm';
% def.def =  'Richmond_skeleton';
% def.def =  'Lecture_example_norm';
def.settings.price =   EL.price{2}; 
def.settings.DR.status = 'On';
def.settings.DR.flex = 'No';

def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 240; %minutes
def.settings.DR.reward = 60000;
def.settings.DR.desired_share = 0.3333;
def.settings.DR.demand_factor = 1.4;
%def.settings.DR.recovery = 240;
%def.settings.mods.Length_factor = 1;
% 
% 
% def.settings.mods.status = 'No';
% 

% def.settings.mod_schedule.status = 'No';
% def.settings.mod_schedule.sched = [0 0 0];
% def.settings.mod_schedule.index = 1;

def.settings.GHG = CO2_kWh_av ;


for i = 1:48 % time shifts:
    def.settings.Time_shift = i;
    % normal DR:
    
    def.settings.mod_schedule.status = 'No';
    out_norm = experiment(def);
    out_norm_c.GHG(i) = out_norm.GHG;
    out_norm_c.cost(i) = out_norm.cost_pump;
    out_norm_c.fval(i) = out_norm.fval;
    out_norm_c.sched{i} = out_norm.schedule_tmp;
    
    def.settings.h0 = out_norm.h0;
    out_norm2 = experiment(def);
    out_norm2_c.GHG(i) = out_norm2.GHG;
    out_norm2_c.cost(i) = out_norm2.cost_pump;
    out_norm2_c.fval(i) = out_norm2.fval;
    out_norm2_c.sched{i} = out_norm2.schedule_tmp;
    
    % event of 150 minutes
    def.settings.mod_schedule.status = 'Yes';
    def.settings.mod_schedule.index = 1;
    
    schedule = DR_schedule_modder(out_norm.schedule_tmp,5,1);
    def.settings.mod_schedule.sched =  schedule(1:5,:);
    out = experiment(def);
    out_150.GHG(i)   = out.GHG;
    out_150.cost(i)  = out.cost_pump;
    out_150.fval(i)  = out.fval;
    out_150.sched{i} = out.schedule_tmp;
    
    % event of 120 minutes
    schedule = DR_schedule_modder(out_norm.schedule_tmp,4,1);
    def.settings.mod_schedule.sched =  schedule(1:4,:);
    out = experiment(def);
    out_120.GHG(i)   = out.GHG;
    out_120.cost(i)  = out.cost_pump;
    out_120.fval(i)  = out.fval;
    out_120.sched{i} = out.schedule_tmp;
    
    % event of 90 mintes
    schedule = DR_schedule_modder(out_norm.schedule_tmp,3,1);
    def.settings.mod_schedule.sched =  schedule(1:3,:);
    out = experiment(def);
    out_90.GHG(i)   = out.GHG;
    out_90.cost(i)  = out.cost_pump;
    out_90.fval(i)  = out.fval;
    out_90.sched{i} = out.schedule_tmp;
    
    % event of 60 minutes
    schedule = DR_schedule_modder(out_norm.schedule_tmp,2,1);
    def.settings.mod_schedule.sched =  schedule(1:2,:);
    out = experiment(def);
    out_60.GHG(i)   = out.GHG;
    out_60.cost(i)  = out.cost_pump;
    out_60.fval(i)  = out.fval;
    out_60.sched{i} = out.schedule_tmp;
    
    % event of 30 minutes
    schedule = DR_schedule_modder(out_norm.schedule_tmp,1,1);
    def.settings.mod_schedule.sched =  schedule(1,:);
    out = experiment(def);
    out_30.GHG(i)   = out.GHG;
    out_30.cost(i)  = out.cost_pump;
    out_30.fval(i)  = out.fval;
    out_30.sched{i} = out.schedule_tmp;
    
    
    
    rmfield(def.settings,'h0');
    
    
    save('DR_event5')
end
   
    
    
    