clear all

% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

EL_pricing


def.N =12;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 1000;
def.Desired_ratio = NaN;
def.asymmetry = 'on';
def.settings.DR.desired_ratio = 20;
def.settings.DR.rest_period = 1; %minutes
def.settings.DR.desired_share = 0;
def.settings.DR.demand_factor = 1;
def.settings.GHG = CO2_kWh_av;

%% Normed lecture example
% for r = 1:20
%     for i = 1:20
%         def.def =  'Van_zyl_norm';
%         def.settings.price =  EL.price{3};
%         def.approx_number = 16;
%         def.settings.DR.reward = r*10000;
%         def.settings.DR.rest_period = 15; %minutes
%         def.settings.DR.status = 'Optimal';
%         def.settings.DR.demand_factor = 0.7+i/20;
%         out2{r,i} = experiment(def);       
%     end
% end

for r = 1:10
    for i = 1:10
        for p = 1:10 
        def.def =  'Van_zyl_norm';
        def.settings.price =  strech(EL.price{3},p/5);
        def.approx_number = 16;
        def.settings.DR.reward = r*10000;
        def.settings.DR.rest_period = 15; %minutes
        def.settings.DR.status = 'Optimal';
        def.settings.DR.demand_factor = 0.7+i/20;
        out2{r,i,p} = experiment(def);       
        end
    end
end


 save(['D:\Phd\MATLAB\Phd\Results\DR\_demand_feas_range_Lecture_norm_EL_4_test' datestr(now,30) '.mat'],'def','out2')
 
 for r = 1:10
    for i = 1:10
        for p = 1:10 
            try
                if   out2{r,i,p}.DR_D > 0;
                    dr_lev(r,i,p) = 1;
                else
                    dr_lev(r,i,p) = NaN;
                end
            catch
                dr_lev(r,i,p) = NaN;
            end
        end
    end
 end
 
for i = 1:10
    x = ones(10,1)*[1:10];
    y = x';
    hold on
    plot3(x,y,dr_lev(:,:,i)*i,'ko')
end
 axis([0 10 0 10 0 10])
 
 
 for r = 1:20
    for i = 1:20
        def.def =  'Richmond_skeleton';
        def.settings.price =  EL.price{2};
        def.approx_number = 16;
        def.settings.DR.reward = r*10000;
        def.settings.DR.rest_period = 15; %minutes
        def.settings.DR.status = 'Optimal';
        def.settings.DR.demand_factor = 0.8+i/20;
        out3{r,i} = experiment(def);       
    end
end
 save(['D:\Phd\MATLAB\Phd\Results\DR\VSD_demand_feas_range_Lecture_norm_EL_2' datestr(now,30) '.mat'],'def','out3')

 for r = 1:20
    for i = 1:20
        def.def =  'van_zyl_norm';
        def.settings.price =  EL.price{3};
        def.approx_number = 16;
        def.settings.DR.reward = r*10000;
        def.settings.DR.rest_period = 15; %minutes
        def.settings.DR.status = 'Optimal';
        def.settings.DR.demand_factor = 0.7+i/20;
        out2{r,i} = experiment(def);       
    end
end
 save(['D:\Phd\MATLAB\Phd\Results\DR\VSD_demand_feas_range_van_zyl_norm_EL_3' datestr(now,30) '.mat'],'def','out2')
 
 for r = 1:20
    for i = 1:20
        def.def =  'van_zyl_norm';
        def.settings.price =  EL.price{2};
        def.approx_number = 16;
        def.settings.DR.reward = r*10000;
        def.settings.DR.rest_period = 15; %minutes
        def.settings.DR.status = 'Optimal';
        def.settings.DR.demand_factor = 0.8+i/20;
        out3{r,i} = experiment(def);       
    end
end
 save(['D:\Phd\MATLAB\Phd\Results\DR\VSD_demand_feas_range_van_zyl_norm_EL_2' datestr(now,30) '.mat'],'def','out3')
