s% DR_result runs
clear all

% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

EL_pricing


def.N =12;
def.vmax_min = 4.5;
def.M  = 999;
def.settings.time_limit  = 1000;
def.Desired_ratio = NaN;
def.asymmetry = 'on';
def.settings.DR.desired_ratio = 20;
def.settings.DR.rest_period = 1; %minutes
def.settings.DR.desired_share = NaN;
def.settings.DR.demand_factor = 1;
def.settings.GHG = CO2_kWh_av;

%% Van Zyl
%VSD
for i = 1:11
    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 7;
    def.settings.DR.reward = 30000;
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'On';
    def.settings.DR.desired_share = i/20 - 1/20;
    def.settings.DR.flex = 'Yes';
    out{i} = experiment(def);
end
    name = ['C:\Phd\MATLAB\Phd\Results\DR\Flex_fixed_DR_case' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
    
    for i = 1:11
    def.def =  'van_zyl_norm';
    def.settings.price =  EL.price{2};
    def.approx_number = 16;
    def.settings.DR.reward = 30000;
    def.settings.DR.rest_period = 240; %minutes
    def.settings.DR.status = 'On';
    def.settings.DR.desired_share = i/20 - 1/20;
    def.settings.DR.flex = 'Yes';
    out{i} = experiment(def);
end
    name = ['C:\Phd\MATLAB\Phd\Results\DR\Flex_fixed_DR_case_VSD' def.def '_' datestr(now,30) '.mat'];
    save(name,'def','out')
    emailnotification('rmm08@ic.ac.uk','Done',name)
% % fixed
% for s = 1:15
%     def.def =  'van_zyl_norm';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 16;
%     def.settings.DR.reward = 30000;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'On';
%     def.settings.DR.desired_share = s/30-1/30;
%     out{s} = experiment(def);
% end
%     name = ['D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case2' def.def '_' datestr(now,30) '.mat'];
%     save(name,'def','out')
%     emailnotification('rmm08@ic.ac.uk','Done',name)
%     
%     
%     
%     %VSD
% for s = 1:15
%     def.def =  'van_zyl_norm';
%     def.settings.DR.flex = 'Yes';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 7;
%     def.settings.DR.reward = 60000;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'On';
%     def.settings.DR.desired_share = s/30-1/30;
%     out{s} = experiment(def);
% end
%     name = ['D:\Phd\MATLAB\Phd\Results\DR\real\TEST_FFR_VSD_price_fixed_DR__case1' def.def '_' datestr(now,30) '.mat'];
%     save(name,'def','out')
%     emailnotification('rmm08@ic.ac.uk','Done',name)
%     
% % fixed
% for s = 1:15
%     def.def =  'van_zyl_norm';
%     def.settings.DR.flex = 'Yes';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 16;
%     def.settings.DR.reward = 30000;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'On';
%     def.settings.DR.desired_share = s/30-1/30;
%     out{s} = experiment(def);
% end
%     name = ['D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case2' def.def '_' datestr(now,30) '.mat'];
%     save(name,'def','out')
%     emailnotification('rmm08@ic.ac.uk','Done',name)
% 
% %VSD
% for s = 1:15
%     def.def =  'Richmond_skeleton';
%     def.settings.price =  EL.price{2};
%     def.settings.DR.flex = 'No';
%     def.approx_number = 7;
%     def.settings.DR.reward = 30000;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'On';
%     def.settings.DR.desired_share = s/30-1/30;
%     out{s} = experiment(def);
% end
%     name = ['D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case3' def.def '_' datestr(now,30) '.mat'];
%     save(name,'def','out')
%     emailnotification('rmm08@ic.ac.uk','Done',name)
% % fixed
% for s = 1:15
%     def.def =  'Richmond_skeleton';
%     def.settings.price =  EL.price{2};
%     def.approx_number = 7;
%     def.settings.DR.flex = 'Yes';
%     def.settings.DR.reward = 30000;
%     def.settings.DR.rest_period = 240; %minutes
%     def.settings.DR.status = 'On';
%     def.settings.DR.desired_share = s/30-1/30;
%     out{s} = experiment(def);
% end
%     name = ['D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case4' def.def '_' datestr(now,30) '.mat'];
%     save(name,'def','out')
%     emailnotification('rmm08@ic.ac.uk','Done',name)


% emailnotification('rmm08@ic.ac.uk','Done','Van Zyl VSD')

