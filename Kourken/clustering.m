% clear all
% close all
% clc
% 
% M = csvread('D:\Phd\MATLAB\Phd\Kourken\Price_CO2_data.csv'); %first 24  is price, second 24 are CO2
% x = 1:40;
% for i = x
%     [idx{i},C{i},sumd{i}]  = kmeans(M,i,'Replicates', 10);
%     av_sum_d(i) = mean(sumd{i});
%     sum_sum_d(i) =  sum(sumd{i});
% end
% 
%save('Clustering')
 load('Clustering')

figure
%plot(x,sum_sum_d , 'k+')
plot(x,av_sum_d , 'k+')

T = linspace(0,1,24);
%plotyy(T,C{10}(:,1:24)',T,C{10}(:,25:48)')

plot(M','Color',[0 0 0 0.05]); hold on
plot(C{10}','r-')

cl = 16;
figure
subplot(2,1,1)
plot(T,M(:,1:24)','Color',[0 0 0 0.05]); hold on
plot(T,C{cl}(:,1:24)','r-')
%title('Cost �/MWh')
ylabel('Cost �/MWh')
xlabel('Time HH:MM')
datetick('x','HH:MM')

subplot(2,1,2)
plot(T,M(:,25:48)','Color',[0 0 0 0.05]); hold on
plot(T,C{cl}(:,25:48)','r-')
%title('Cost �/MWh')
ylabel('CO_2')
xlabel('Time HH:MM')
datetick('x','HH:MM')