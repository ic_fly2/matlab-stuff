clearvars
% prob.N = 12;
% prob.approx_number = 7;
% prob.vmax_min = 4.5;
% prob.M  = 999;
% prob.settings.time_limit  = 600;
% prob.Desired_ratio = NaN;
% prob.asymmetry = 'on';
% prob.prob =  'van_zyl';
% prob.settings.price = rand(1,48).^2*100;
% prob.settings.DR.status = 'Off';
% prob.settings.DR.desired_ratio = 12;
% prob.settings.DR.rest_period = 30; %minutes
% CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
% EL_pricing

prob.N =24;
prob.approx_number = 7;
prob.vmax_min = 7;
prob.M  = 999;
prob.settings.time_limit  = 600;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';

% prob.def = 'Purton_fake';
% prob.def =  'Richmond_skeleton';
% prob.def =  'Lecture_example_norm';
prob.def = 'van_zyl_norm';

prob.settings.DR.status = 'Off';
% prob.settings.DR.flex = 'Window1';
% 
% prob.settings.DR.desired_ratio = NaN;
% prob.settings.DR.rest_period = 240; %minutes
% prob.settings.DR.reward = 60000;
% prob.settings.DR.desired_share =0;
prob.settings.DR.demand_factor = 1; %160l/s base rate

%prob.settings.DR.recovery = 240;
%prob.settings.mods.Length_factor = 1;
% 
% 
prob.settings.solver_gap = 0.02;
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
% 
prob.settings.Time_shift =0;
for i = 1:20
    for j = 1:10
        [c, Pe, GHG] = genmultiObjectivePrices(i,10,'Tariffs_n_emissions1');
        prob.settings.price =   c(j,:); 
        prob.settings.GHG = GHG ;
        out = experiment(prob);
        
        results{i,j}.fval = out.fval;
        results{i,j}.GHG = out.GHG;
        results{i,j}.schedule_tmp = out.schedule_tmp;
        results{i,j}.Remaining_gap = out.Remaining_gap;
    end
end

save('Results_24')

%% second run
prob.N =48;
prob.approx_number = 5;
prob.vmax_min = 7;
prob.M  = 999;
prob.settings.time_limit  = 600;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';

% prob.def = 'Purton_fake';
% prob.def =  'Richmond_skeleton';
% prob.def =  'Lecture_example_norm';
prob.def = 'van_zyl_norm';

prob.settings.DR.status = 'Off';
prob.settings.DR.demand_factor = 1; %160l/s base rate
prob.settings.solver_gap = 0.02;
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
% 

prob.settings.Time_shift =0;
for i = 1:20
    for j = 1:10
        [c, Pe, GHG] = genmultiObjectivePrices(i,10,'Tariffs_n_emissions1');
        prob.settings.price =   c(j,:); 
        prob.settings.GHG = GHG ;
        out = experiment(prob);
        
        results{i,j}.fval = out.fval;
        results{i,j}.GHG = out.GHG;
        results{i,j}.schedule_tmp = out.schedule_tmp;
        results{i,j}.Remaining_gap = out.Remaining_gap;
    end
end

save('Results_48')