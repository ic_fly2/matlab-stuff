% curve_interpretation
% plots a surface correlating pump performance and power consumption
% plots stored in ETAline folder

%Make approximations first 
for i = 1:8;
    hold on;
    plot(Char_curves{i}(:,1),Char_curves{i}(:,2));
    q(i,:)    = linspace(min(Char_curves{i}(:,1)),max(Char_curves{i}(:,1)));
    p_h(i,:)  = polyfit(Char_curves{i}(:,1),Char_curves{i}(:,2),2);
    p_p2(i,:) = polyfit(Power_curves{i}(:,1),Power_curves{i}(:,2),2);
    p_p1(i,:) = polyfit(Power_curves{i}(:,1),Power_curves{i}(:,2),1);
    H(i,:)    = polyval( p_h(i,:),q(i,:)); 
    P1(i,:)   = polyval(p_p1(i,:),q(i,:));
    P2(i,:)   = polyval(p_p2(i,:),q(i,:));
end
plot3(q,H,P1)
figure
plot3(q,H,P2)
