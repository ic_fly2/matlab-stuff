clearvars
iters = 200;
% Price 
% not fiddled with

% date
day = 1:250;
w = [1 0.8 1 1 1.2 1.2];
seasons = [ones(41,1)*w(1); ones(41,1)*w(2); ones(42,1)*w(3); ones(42,1)*w(4); ones(42,1)*w(5); ones(42,1)*w(6)];
event_date = datasample(day,iters,'Weights',seasons);
% hist(event_date,1:251)
dates = [];
for i = 1:356
    if ~rem(i,7)
        dates = [dates i+(1:5)];
    end
end
dates = dates-2;

% day bit for starting time
% window_av = [7.0000   13.5000   16.5000   21.0000];
% window_av = window_av*48/24;
N= 48;
% W = zeros(N,1);
% STOR
% W(window_av(1):window_av(2)) = 1;
% W(window_av(3):window_av(4)) = 1;
data =[1:48];
% FFR
W = ones(N,1);

event_start = datasample(data,iters,'Weights',W);
% plot to check
% hist(event_start,data)
% axis([1 48 0 Inf])

%Event  duration STOR
% Event (min.)&0--30&30--60&60--90&90--120 &120--150&$>$150\\
cdf = [0 100; 15 94.444; 30 87.1; 45 74.7;  60 60; 75 47; 90 36.5; 105 26.7; 120 20; 135 14.1; 150 11; 165 8.8; 180 6.1; 195 4.1; 210 4; 225 2.6; 240 2.5; 255 1; 270 0.6; 285 0.2; 300 0.1; 330 0];
cdf2 = cdf; cdf2(:,2) =cdf2(:,2)/100;
pdf1(1,1:2) =0;
for i = 1:length(cdf)-1
    pdf1(i+1,1) = (cdf2(i+1,1)+cdf2(i,1))/2;
    pdf1(i+1,2) = -(cdf2(i+1,2)-cdf2(i,2))/(cdf2(i+1,1)-cdf2(i,1));
end
pdf_smooth = smooth(pdf1(:,2));
add = [];
for i = [30:30:180]
    add = [add ;i interp1q(pdf1(:,1),pdf1(:,2),i) ];
end
pdf2 = sortrows([ pdf1 ; add]);
% 30
prob(1) = trapz(pdf2(1:4,1),pdf2(1:4,2));
prob(2) = trapz(pdf2(4:7,1),pdf2(4:7,2)); % 60
prob(3) = trapz(pdf2(7:10,1),pdf2(7:10,2)); % 90
prob(4) = trapz(pdf2(10:13,1),pdf2(10:13,2)); % 120
prob(5) = trapz(pdf2(13:16,1),pdf2(13:16,2)); % 150
prob(6) = trapz(pdf2(16:28,1),pdf2(16:28,2)); % <150
% event_duration = datasample( 1:6,iters,'Weights',prob);
% hist(event_duration,1:6)

%Event  duration  FFR
event_duration  = ones(1,iters);

load('D:\Phd\MATLAB\Phd\Kourken\Files\Files\Combo.mat','EI_2014')
load('DR_norm_schedule_window1')
% GHG plot
% figure('Position',[100 100 350 150])
% av = zeros(48,1);
% for i = 1:365
%     hold on
%     lh = plot(linspace(0,1,48),EI_2014{i});
%     lh.Color = [0 0 0 0.1];
%     
%     av = av + EI_2014{i};
% 
% end
% %    CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
% CO2_kWh_av = av/365;   
% plot(linspace(0,1,48),CO2_kWh_av,'r','Linewidth',2);  
% ylabel('$gCO_2e$/kWh','Interpreter','Latex')
% xlabel('Time of Day','Interpreter','Latex')
% datetick('x','HH:MM')
% axis([0 1 -Inf Inf])
network_name = 'van_zyl_norm_171';
duration_record = zeros(iters,1);
date_record = zeros(iters,1);
time_record = zeros(iters,1);

gap = zeros(iters,1);
cost = zeros(iters,1);
GHG = zeros(iters,1);

for i = 1:iters
    try
    duration = event_duration(i); duration_record(i) = duration;
    time = event_start(i); time_record(i) = time;
    date = event_date(i); date_record(i) = date;
    
    
    
    
    grid_GHG_1 = EI_2014{dates(date)};
    grid_GHG_2 = EI_2014{dates(date)+1};
    grid_GHG = [ grid_GHG_1 ; grid_GHG_2];
    grid_GHG = grid_GHG(time+(0:47))';
    
    price = rubenshift(Electricty_price_fun(2)',time);
    
    schedule = rubenshift(schedule_norm,time);
    level = levels(time,:);
   
    [GHG(i) cost(i) gap(i) schedule_event{i}] = DR_event_simulation_fun(schedule,duration,network_name,price,grid_GHG,level);
    save('Numbers_only_FFR_5','GHG','cost','gap','duration_record','time_record','date_record')
    save('Numbers_and_schedule_FFR_5','GHG','cost','gap','schedule_event','duration_record','time_record','date_record')
    catch
        emailnotification('rmm08@ic.ac.uk','Crashed',['Crashed on iteration' num2str(i)])
    end
end



