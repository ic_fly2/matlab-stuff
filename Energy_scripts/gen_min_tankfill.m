% function h_min = gen_min_tankfill(def,tanks,settings,nodesIdListMap,h_min,data,pipes,pumps,reservoirs,d)
function h_min = gen_min_tankfill(tanks,settings,nodesIdListMap,data,pipes,pumps,reservoirs,d)
% generated minimum tank fillings if DR is on or optimal

% [H_24, ~, ~] = EPS_from_T0(zeros(1,data.npu),,1,settings,data,pipes,pumps,tanks,reservoirs,d,nodesIdListMap);
  [H_24, ~, ~] = EPS_from_T0(zeros(1,data.npu),N,data,pipes,pumps,tanks,reservoirs,d,nodesIdListMap,zeros(1,data.npu));
demand_factor = sum(d)/mean(sum(d));
for i = 1:length(tanks)    
    h_min(nodesIdListMap(tanks{i}.Id),:) = -H_24(nodesIdListMap(tanks{i}.Id))...
                                            *settings.DR.rest_period/(24*1440)*demand_factor; 
                                         % change in tank level in one minute
                                         % with average demand out flow. 
end


