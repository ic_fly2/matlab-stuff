function [Power] = Energy_assessment(junctions,pumps,reservoirs,Desired_ratio)
% Evaluates the power out and in of the WDS and can change the demand to
% alter their ratio


Power.demand = zeros(length(junctions),1);
for i = 1:length(junctions)
    Power.demand(i) = junctions{i}.Demand/1000*... % demand rate in m3/s
        (junctions{i}.Elev-reservoirs{1}.Elev)... % Elevation difference
        *9.81*1000/1000; % Constants g,rho W--> kW
end

Power.supply = zeros(length(pumps),1);

for i = 1:length(pumps)
    Power.supply(i) = pumps{i}.power.nameplate;
end
    
% power setup    
Power.total(1) =  sum(Power.supply);
Power.total(2) = -sum(Power.demand);
Power.total(3) =  sum(Power.supply)/sum(Power.demand);

%d = d.*demand_factor;

if  ~isnan(Desired_ratio);   
    %beef demand as required:
    if Power.demand ~= 0
%         d = d.*(Power.total(3)/Desired_ratio);
    else
        disp('Desired_ratio and no demand ratio given not set as demand power is zero')
    end
end

