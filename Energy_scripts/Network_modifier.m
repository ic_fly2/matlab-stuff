% Network Modifier
% Gets the LEcture example and changes all sorts of paramters about its
% parameters: resistance of pipes, storage in terms of demand, demand,
% potential difference in pipes.
pipes{1}.Length = pipes{1}.Length*settings.mods.Length_factor;
tanks{1}.Elev = tanks{1}.Elev*settings.mods.Elev_factor;
junctions{1}.Demand = junctions{1}.Demand*settings.mods.Demand_factor;
tanks{1}.Diameter = sqrt(2.2918*junctions{1}.Demand*settings.mods.storage_time);