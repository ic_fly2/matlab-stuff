    CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
    
    OCGT = 460;
    Hydro = 420; 
    time = linspace(0,1,length(CO2_kWh_av));
    
    plot(time,CO2_kWh_av,'k')
    hold on
    plot(time,ones(1,24)*375,'k:')
    plot(time,ones(1,24)*400,'k:')
    plot(time,ones(1,24)*425,'k:')
    axis([0 1 350 450])
    datetick('x','HH:MM')
    xlabel('')