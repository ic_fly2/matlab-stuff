function Y = fields_rm(X,field)
% reads out X{:}.field as one would expect. Makes a matrix / vector output
% where possible otherwise writes it to a cell object.

for i=1:length(X)
    if ischar(X{i}.(field))
        Y{i} = X{i}.(field);
    elseif isscalar(X{i}.(field))
        Y(i) = X{i}.(field);
    elseif isvector(X{i}.(field))
        Y(i,:) = X{i}.(field);
    else
        Y{i} = X{i}.(field);
    end
end