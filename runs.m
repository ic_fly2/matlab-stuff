% % run one
clear all; close all;
def = 'loop0';

file_name = ['results_from_runs_1_' def];
N_set = [6 12 24 48];
for approx_number = 1:9
    for i_N = 1:4	
        N = N_set(i_N);
        [time_taken{approx_number,i_N}, fval{approx_number,i_N}, diff_H{approx_number,i_N}, diff_Q{approx_number,i_N},diff_Q_per{approx_number,i_N} ,diff_H_per{approx_number,i_N} Remaining_gap{approx_number,i_N}  x{approx_number,i_N} ] = experiment(def,N,approx_number,4.5);
        save(file_name)
    end
end


%% produce graphics
% collect all differences in flow speed
load(file_name)


%% detect gaps and prepare labeling

%% time taken
time_taken = cell2mat(time_taken);
h_time = bar(time_taken);
colormap('gray');
axis([0.4 9.6 0.1 1000]);
% h_time = legend('N = 6','N = 12','N = 24','N = 48');
h_time = legend('N = 6','N = 12','N = 24');
set(gca,'YScale','log','XAxisLocation','Bottom')
xlabel('Approximation Number')
ylabel('Time taken (s)')
 set(gca,'yMinorGrid','on')
 
% print(['D:\Phd\My written stuff\Figures\Barplot_' file_name ],'-dpdf')
%% Fval 
close all;
fval = cell2mat(fval);
h_fval = bar(fval);
colormap('gray');
axis([0.4 9.6 -Inf 21000]);
h_fval = legend('N = 6','N = 12','N = 24','N = 48');
% set(gca,'YScale','log')
xlabel('Approximation Number')
ylabel('Operating cost in �')
set(gca,'yMinorGrid','on')
% % van zyl
% text(1.05,0,'No solution','Rotation',90,'FontSize',9)
% text(1.25,0,'No solution','Rotation',90,'FontSize',9)
% text(0.85,0,'No solution','Rotation',90,'FontSize',9)
% text(2.05,0,'No solution','Rotation',90,'FontSize',9)
% text(2.25,0,'No solution','Rotation',90,'FontSize',9)
%Richmond
text(1,1000,'No solutions','Rotation',90,'FontSize',10)
text(2,1000,'No solutions','Rotation',90,'FontSize',10)
text(3,1000,'No solutions','Rotation',90,'FontSize',10)
 
% set(gca,'YTick',[-2 -1 0 1 2 3 ],'YTickLabel',num2str([10^-2 10^-1 10^0 10^1 10^2 10^3 ]'))
% print(['D:\Phd\My written stuff\Figures\fval_' file_name ],'-dpdf')
close all

%% accuracy
 for approx_number = 1:9
     diff_accum{approx_number} = [];
     for i_N = 1:3
        diff_temp = abs(diff_Q{approx_number,i_N});
        diff_temp = diff_temp(diff_temp ~= 0);
        diff_accum{approx_number} = [diff_accum{approx_number}; diff_temp];
     end
    
 end

 % make uniform
 diff_lengths = cellfun(@(x)length(x(:)), diff_accum);
 for approx_number = 1:9
    diff_accum{approx_number} = [diff_accum{approx_number};  NaN*ones(max(diff_lengths)-diff_lengths(approx_number),1)]; 
 end
 diff_accum = cell2mat(diff_accum);
 

%  boxplot(diff_accum) % no rela gain from this
 diff_accum = diff_accum(:,4:9);
  for approx_number = 1:6
     diff_eval = diff_accum(:,approx_number);
     diff_eval =  diff_eval(~isnan(diff_eval));
     mean_eval(approx_number) = median(diff_eval);
     max_eval(approx_number) = max(diff_eval);
     sorted_diff = sort(diff_eval);
     quartil_diff(approx_number) = sorted_diff(floor(0.75*length(diff_eval)));
  end
  
  % make my own box plot:
  hold on;
  for approx_number = 1:6
      plot([approx_number approx_number]+3,[0 max_eval(approx_number)],'k--');
  end
  %       plot(max_eval,'k+','MarkerSize',12);
  bar([zeros(1,3) quartil_diff],'FaceColor','w','EdgeColor','k','LineWidth',1)
  for approx_number = 1:6
      plot([approx_number-0.4 approx_number+0.4]+3,[mean_eval(approx_number) mean_eval(approx_number)],'k-','LineWidth',1.5)
      plot([approx_number-0.4 approx_number+0.4]+3,[max_eval(approx_number) max_eval(approx_number)],'k-','LineWidth',0.5)
  end
  text(1,0.025,'No solutions','Rotation',90,'FontSize',10)
text(2,0.025,'No solutions','Rotation',90,'FontSize',10)
text(3,0.025,'No solutions','Rotation',90,'FontSize',10)
  axis([0.5 9.5 0 0.45 ])
  xlabel('Approximation Number')
  ylabel('Absolute differences in flow rate (m^3/s)')
%   print(['D:\Phd\My written stuff\Figures\Hydraulicerror_' file_name ],'-dpdf')
