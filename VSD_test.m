% VSD test
clear all; %close all
q = linspace(0,1);
D_H = @(q,m,c) m*q + c;

m = [-1 -4 ];
c = [2.5 4];

for i =1:length(m)
    H(i,:) = D_H(q,m(i),c(i));
    plot(q,H(i,:),'r'); hold on
end

xm = (c(2)-c(1))/(m(1) - m(2));
plot(xm,D_H(xm,m(1),c(1)),'+');


m = [-1 -4];
c = [2.5 4];

for i =1:length(m)
    H(i,:) = D_H(q,m(i),c(i));
    plot(q,H(i,:),'b'); hold on
end

axis([0 1 0 4])

% guidance
plot([0 1], [0 4],'c--')

% figure
% 
% 
% D_Hv = @(q,m,c,w) m*q + c;
% for w = 0:0.2:1;
%     for i =1:length(m)
%         H(i,:) = D_Hv(q,m(i),c(i),w);
%         plot(q,H(i,:),'r'); hold on
%     end
% end
% 
