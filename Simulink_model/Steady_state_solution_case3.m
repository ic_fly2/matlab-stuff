%solving a network in quasi steady state

% explanation of notations
% N = head at nodes first only in meters as D>>Q dynamic head is ignored at
% first

%% Initialisation
% plot image of the network to show naming convention
% I = imread('pump_schematic.jpg');
% imagesc(I)

% parameters
% pipe features
% D = [0.5 0.5 0.5] ;
% L = [5 10 5] ;
% e = [0.005 0.005 0.005] ;
% Delta_Z = zeros(1,3);
% pipe info matrix D e L Delta z
clear all;
info =  [0.5 0.005 25 0; 0.5 0.005 25 0; 0.5 0.005 25 0 ];
%     info =  [0.5 0.005 5 0; 0.5 0.005 50 0 ];
% computational parameters
error = 5;
delta_P = 0.01; % 0.01 is ok to get 0.0001 mass imbalance error

%enter initial guesses: 
% nodes
% N = [N1 --> N10] %pressures in the nodes initially
N = [0.8]; %m Head s
Nold = N;
error_N(1:length(N)) =  1;
% pipe flows
% Q = [Q0 --> Q11]  % Flows in the pipes initially (for friction factor)
Q = [ 1 2 1];

% Tank fill heights
TA = 60;
TB = 5;
TC = 20;

%initial pressure differences
% DP(1) = TA-N(1); %pipe 0
% DP(2) = N(1)-N(2); %Pipe 1
% DP(3) = N(1)-TB;
DP(1) = TA-N(1); %pipe 0
DP(2) = N(1)-TB;
DP(3) = N(1)-TC;
% Compute initial flows
Q(1) = ezPipeFlowCalc2(DP(1),info(1,:),Q(1)); %pipe 0
Q(2) = ezPipeFlowCalc2(DP(2),info(2,:),Q(2)); %Pipe 1
Q(3) = ezPipeFlowCalc2(DP(3),info(3,:),Q(3));
% Q(4) = ezPipeFlowCalc2(N(2),N(4),D(4),e(4),L(4),Delta_Z(4),Q(4));
% Q(5) = ezPipeFlowCalc2(N(3),N(5),D(5),e(5),L(5),Delta_Z(5),Q(5));
% Q(6) = ezPipeFlowCalc2(N(4),N(6),D(6),e(6),L(6),Delta_Z(6),Q(6));
% Q(7) = ezPipeFlowCalc2(N(5),N(6),D(7),e(7),L(7),Delta_Z(7),Q(7));
% Q(8) = ezPipeFlowCalc2(N(6),TB,D(8),e(8),L(8),Delta_Z(8),Q(8));
c = 1 ; % counter
while max(abs(error_N)) >= 0.0001 % allowed mass imbalance (less than matlab short displays)
%find directon of flow +ve is inflow -ve is outflow
DP(1) = TA-N(1); %pipe 0
DP(2) = N(1)-TB;
DP(3) = N(1)-TC;
% DP(2) = N(1)-N(2); %Pipe 1
% DP(3) = N(1)-TB;
% DP(4) = N(2)-N(4);
% DP(5) = N(3)-N(5);
% DP(6) = N(4)-N(6);
% DP(7) = N(5)-N(6);
% DP(8) = N(6)-TB;

% summation at the nodes
% node 1
error_N_old = error_N;
error_N(1) = DP(1)*Q(1)/abs(DP(1)) - DP(2)*Q(2)/abs(DP(2))  - DP(3)*Q(3)/abs(DP(3)); 

% if abs(Nold(1)-N(1)) >= abs(Nold(1)+N(1));
if abs(error_N(1)+error_N_old(1)) <= abs(error_N(1)-error_N_old(1));% find oscilation --> switch to bisection method
    Nold(1) = N(1);
    N(1) = N(1) + error_N(1)*delta_P; 
    N(1) = (Nold(1)+ N(1))/2;
else % if it doesn't oscilate
    N(1) = N(1) + error_N(1)*delta_P; % correct pressure by delta_p 
end
% node 2
% error_N(2) = DP(2)*Q(2)/abs(DP(2)) - DP(3)*Q(3)/abs(DP(3)); 
% N(2) = N(2) + error_N(2)*delta_P; 

% % node 3
% error_N(3) = DP(3)*Q(3)/abs(DP(3)) + DP(4)*Q(4)/abs(DP(5)); 
% N(3) = N(3) + error_N(3)*delta_P;

% % node 4
% error_N(4) = DP(4)*Q(4)/abs(DP(4)) + DP(6)*Q(6)/abs(DP(6)); 
% N(4) = N(4) + error_N(4)*delta_P; 
% 
% % node 5
% error_N(5) = DP(5)*Q(5)/abs(DP(5)) + DP(7)*Q(7)/abs(DP(7)); 
% N(5) = N(5) + error_N(5)*delta_P;
% 
% % node 6
% error_N(6) = DP(6)*Q(6)/abs(DP(6)) + DP(7)*Q(7)/abs(DP(7)) + DP(8)*Q(8)/abs(DP(8)); % + DP4*Q4/abs(DP4); 
% N(6) = N(6) + error_N(6)*delta_P; 


% compute new flows
Q(1) = ezPipeFlowCalc2(DP(1),info(1,:),Q(1)); %pipe 0
Q(2) = ezPipeFlowCalc2(DP(2),info(2,:),Q(2)); %Pipe 1
% Q(3) = ezPipeFlowCalc2(DP(3),info(3,:),Q(3));
% Q(4) = ezPipeFlowCalc2(N(2),N(4),D(4),e(4),L(4),Delta_Z(4),Q(4));
% Q(5) = ezPipeFlowCalc2(N(3),N(5),D(5),e(5),L(5),Delta_Z(5),Q(5));
% Q(6) = ezPipeFlowCalc2(N(4),N(6),D(6),e(6),L(6),Delta_Z(6),Q(6));
% Q(7) = ezPipeFlowCalc2(N(5),N(6),D(7),e(7),L(7),Delta_Z(7),Q(7));
% Q(8) = ezPipeFlowCalc2(N(6),TB,D(8),e(8),L(8),Delta_Z(8),Q(8));

% error = abs(error_N);
% error = sum(error_N)
%% analysis of solver
error_hist(c,1) = error_N(1);
N_hist(c,1) = N(1);
Q_hist(c,:) = Q; 
c = c+1; % operations counter



end

c
Q
clf;
plot(1:c-1,error_hist(:,1))
% plot(1:c-1,N_hist(:,1))
% find pressure differences seperatly for each pipe

% 
% while error >= 0.01 % allowed mass imbalance
% 
%     %find diracton of flow +ve is inflow -ve is outflow
%     error = DP1*Q1/abs(DP1) + DP2*Q2/abs(DP2) + DP3*Q3/abs(DP3) + DP4*Q4/abs(DP4); 
% P = P_init + error*delta_P/abs(error); % correct pressure by delta_p 
% end

