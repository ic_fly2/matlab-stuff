function [ DH ] = ezNodeCalc2(Q_in,Q_out,D)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

A = D^2 *0.25*pi;
V_out = Q_out / A;
V_in = Q_in /A;
DH = (V_out^2 - V_in^2)/2*9.81;
end

