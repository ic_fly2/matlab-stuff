function [Q] = ezPumpCalc4(H,power)
%ezPumpCalc Calculates the head a pump creates given the flowrate water power and  curve factorsSummary of this function goes here
%   A B C are the polynomilas at 100% power a b c are factors how much A B C are influenced by the relative
%   power 

%define factors
A = power - H^2;
if A <= 0 
%     error('power is insufficient')
A = 1;
end

Q = sqrt(A);



end

