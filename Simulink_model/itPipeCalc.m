%pipe iterative formulation
% initial inputs
Q_out_init = 50; % from node or tank or what ever
H_in = 2 ;% actually calculated as time dependant

% parameters
D = 0.5 ;%Diameter
L = 100;%Length
e = 0.002;%roughness
Delta_Z = 0;% elevation change

% initialisation
Q = Q_out_init;
errorQ = 100;

while errorQ >= 0.01
[ f,K,n,Re,regime ] = DarcyWeisbachFactorCalc(D,Q,e,L);
H_out = H_in - K*Q^n;
Q_new = ezPipeFlowCalc(H_in,H_out,D,e,L,Delta_Z,Q);
Q = Q_new;
errorQ = abs(Q-Q_new);
end