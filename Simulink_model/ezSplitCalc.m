function [ output] = ezSplitCalc(Q_out1,Q_out2,H_in,K,A1,A2,A3)
%ezSplitCalc Summary of this function goes here
%   Detailed explanation goes here
Q_in = Q_out1 +Q_out2;
v1 = Q_in/A1;
v2 = Q_out1/A2;
v3 = Q_out2/A3;
B = H_in + v1^2 / (2*9.81);
H2 = B - v2^2 / (2*9.81);
H3 = B - v3^2 / (2*9.81);

%minor losses
H_out1 = H2 - K*Q_out1^2 ;
H_out2 = H3 - K*Q_out2^2 ;

output(1) = H_out1;
output(2) =H_out2;
output(3) =Q_in;
end

