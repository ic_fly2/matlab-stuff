function [ Q_j,H_in,Q_out ] = ezJunctionCalc(Q_in,H_j,K,H_out,Q_out )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


Q_bar = (Q_in + Q_out)/2;
H_in = H_out + K*Q_bar^2;
H_bar = (H_out + H_in)/2;
Q_j = sqrt((H_bar - H_j)/K);
Q_out = Q_in - Q_j;
end

