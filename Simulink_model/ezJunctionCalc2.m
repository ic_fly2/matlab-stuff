function [result] = ezJunctionCalc2(Q_in,Q_j,H_out,K)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
Q_out = Q_in - Q_j; % Ouflow on main pipe is calculated
H_in = H_out + K*((Q_in + Q_out)/2)^2; %inlet head is calculated assuming it behaves like a minor loss
H_j = (H_in + H_out)/2; % The head at the split stream is calcuates as average head in the junction
result = [ Q_out,H_j,H_in ]; % output is bundled
end

