function [Q] = ezPumpCalc2(Delta_H,power,A,B,C)
%ezPumpCalc Calculates the head a pump creates given the flowrate water power and  curve factorsSummary of this function goes here
%   A B C are the polynomilas at 100% power, power is not used  yet

x = [A B (C - Delta_H)];
Q = roots(x);
y = [norm(Q(1)) norm(Q(2))] ;
Q = max(y);

power_water = 999*9.81*Q*Delta_H;
if power_water > power
    warning('Powers do not match')
end

head = 1:200; 
flow = A*head.^2 + B*head +C;
plot(flow,head)

end

