function Q = ezPipeFlowCalc(H_in,H_out,D,e,L,Delta_Z,Q_bar)
%#codegen Calculates the flow though a pipe given head and pipe properties Q = f(Head)
% hazen williams for a start later move to darcy weisbach
if Q_bar <= 0 % to prevent an infinite friction factor 
    Q_bar = 1;
end


[ f,K,n,Re,regime ] = DarcyWeisbachFactorCalc( D,Q_bar,e ,L) ;
% f = e/D; % not ok but for testing only


D_H = H_in -H_out;
if D_H <= 0 % to prevent further errors
    DH = 5;
end
Area = (D/2)^2 * pi;
innersum = (D_H + Delta_Z)*9.81;
Q = 2*innersum * Area^2;
Q = Q/(f*(L/D));
Q = Q^0.5;
end