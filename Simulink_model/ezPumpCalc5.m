function [H_out] = ezPumpCalc5(H_in,Q,power)
%ezPumpCalc Calculates the head a pump creates given the flowrate water power and  curve factorsSummary of this function goes here
%   A B C are the polynomilas at 100% power a b c are factors how much A B C are influenced by the relative
%   power 

%define factors
A = power - Q^2;
if A <= 0 
%     error('power is insufficient')
A = 1;
end

H = sqrt(A);
H_out = H + H_in;


end

