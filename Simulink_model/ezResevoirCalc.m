function H_out = ezResevoirCalc(V,D,elevation,Max_Height)
%#codegen Calculates the filling of a finite storage tank and 
%  the resulting head by assuming a circular cross section

% calc height of water in tank
Area = (D/2)^2 * pi;
Height = V/Area;

% check for overflow / empty (could also be max min limits of tank
if  Height >= Max_Height
    Height =  Max_Height;
%     error('Tank is over full') % Optional
elseif Height <= 0 
    error('Tank is empty')
end

H_out = elevation + Height;
end
