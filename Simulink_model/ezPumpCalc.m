function [delta_H] = ezPumpCalc(Q,power,A,B,C,a,b,c)
%ezPumpCalc Calculates the head a pump creates given the flowrate water power and  curve factorsSummary of this function goes here
%   A B C are the polynomilas at 100% power a b c are factors how much A B C are influenced by the relative
%   power 

%define factors
A_star =  A*power*a;
B_star =  B*power*b;
C_star =  C*power*c;

delta_H = A_star*Q^2 + B_star*Q + C_star;
end

