function Q = ezPipeFlowCalc2(D_H,info,Q_init)
%  Q = ezPipeFlowCalc2(H_in,H_out,D,e,L,Delta_Z,Q_bar)
%#codegen Calculates the flow though a pipe given head and pipe properties Q = f(Head)
% hazen williams for a start later move to darcy weisbach
%initialisation
D = info(1);
e = info(2);
L = info(3);
Delta_Z =  info(4);
Q_init = abs(Q_init);
D_H    = abs(D_H);%- Delta_Z;
error = 1;
dQ = 0.001;
Q_init = abs(Q_init);
if Q_init <= 0 % to prevent an infinite friction factor 
    Q_init = 0.001;
end
Q = Q_init;

while error >= 0.01; % any smaller and it takes forever and a bit to converge
[ f,K,n,Re,regime ] = DarcyWeisbachFactorCalc( D,Q,e ,L) ;
% f = e/D; % not ok but for testing only
Area = (D/2)^2 * pi;
error = D_H - f * (L/D)*(Q^2)/(2*9.81*Area^2); 
%adjust Q
Q = Q + error*dQ;
Q = abs(Q);
error = abs(error);
if abs(Q) <= 1;
    error = error/Q;
end
end




% 
% Area = (D/2)^2 * pi;
% innersum = (D_H + Delta_Z)*9.81;
% Q = 2*innersum * Area^2;
% Q = Q/(f*(L/D));
% Q = Q^0.5;
end