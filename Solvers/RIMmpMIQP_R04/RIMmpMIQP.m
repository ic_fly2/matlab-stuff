function varargout = RIMmpMIQP(varargin)
% RIMMPMIQP MATLAB code for RIMmpMIQP.fig
%      RIMMPMIQP, by itself, creates a new RIMMPMIQP or raises the existing
%      singleton*.
%
%      H = RIMMPMIQP returns the handle to a new RIMMPMIQP or the handle to
%      the existing singleton*.
%
%      RIMMPMIQP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RIMMPMIQP.M with the given input arguments.
%
%      RIMMPMIQP('Property','Value',...) creates a new RIMMPMIQP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RIMmpMIQP_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RIMmpMIQP_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RIMmpMIQP

% Last Modified by GUIDE v2.5 01-Sep-2014 18:15:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RIMmpMIQP_OpeningFcn, ...
                   'gui_OutputFcn',  @RIMmpMIQP_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RIMmpMIQP is made visible.
function RIMmpMIQP_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RIMmpMIQP (see VARARGIN)

% Choose default command line output for RIMmpMIQP
handles.output = hObject;

% Add all the necessary paths
addpath(genpath('./Files'));

% Print logos in the boxes
%imshow('./Logos/logo_imperial.png','parent',handles.logo);

% Check for the current system
v = version;
k = strfind(v,'(');
t = strfind(v,')');
set(handles.System_MATLAB,'String',['MATLAB: ',v(k+1:t-1)]);
if exist('a00ac','var')
    set(handles.System_NAG,'String','NAG: On');
else
    set(handles.System_NAG,'String','NAG: Off');
end
if exist('cplexlp','file')
    set(handles.System_CPLEX,'String','CPLEX: On');
else
    set(handles.System_CPLEX,'String','CPLEX: Off');
end
if exist('wgdx','file')
    set(handles.System_GAMS,'String','GAMS: On');
else
    set(handles.System_GAMS,'String','GAMS: Off');
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RIMmpMIQP wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RIMmpMIQP_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in About_Button.
function About_Button_Callback(hObject, eventdata, handles)
% hObject    handle to About_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

msgbox({'Version: R04','','This software was developed by Richard Oberdieck',...
    'in the group of Prof. Pistikopoulos.',...
    '',...
    'If you would like to be updated about any changes or new versions',...
    'of this software, please send an email to',...
    '',...
    'richard.oberdieck11@imperial.ac.uk'},...
    'About');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in Help_Button.
function Help_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Help_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

helpdlg({'Got any problems, questions, comments or suggestions?',...
    '','Please email me at richard.oberdieck11@imperial.ac.uk!'},...
    'Help');

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in TheGoButton.
function TheGoButton_Callback(hObject, eventdata, handles)
% hObject    handle to TheGoButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Add the options
% Upper Bound
val = get(handles.UpperBound_Set,'Value');
if val == 3
    options.UpperBound_Set = 'On';
    
    vala = get(handles.UpperBound_Style,'Value');
    if vala == 3
        options.UpperBound_Style = 'Integer';
    elseif vala == 4
        options.UpperBound_Style = 'ParameterSpace';
    end
    
    str = get(handles.UpperBound_Tau,'String');
    valb = str2double(str);
    
    if ceil(valb) ~= floor(valb)
        errordlg('Please specify a correct accuracy (has to be integer)!','No integer input!');
    else
        options.UpperBound_Tau = valb;
    end
elseif val == 4
    options.UpperBound_Set = 'Off';
end

% Parallelization
val = get(handles.sigma,'Value');
if val > 2
    options.sigma = (val-2);
end

str = get(handles.rho_limit,'String');
val = str2double(str);
if ~isempty(val)
    if ceil(val) ~= floor(val)
        errordlg('Please specify a correct value (has to be integer)!','No integer input!');
    else
        options.rho_limit = val;
    end
end

% Suboptimality
str = get(handles.epsilon,'String');
val = str2double(str);
if ~isempty(val)
    if val < 0
        errordlg('Please specify a correct value (has to be positive)!','No positive input!');
    else
        options.epsilon = val;
    end
end

% The solver
val = get(handles.solver,'Value');
if val == 3
    options.solver = 'Decomposition';
elseif val == 4
    options.solver = 'BranchAndBound';
elseif val == 5
    options.solver = 'ExhaustiveEnumeration';
end

val = get(handles.Global_Solver,'Value');
if val == 3
    options.Global_Solver = 'ANTIGONE';
elseif val == 4
    options.Global_Solver = 'BARON';
end

% Comparison
val = get(handles.Comparison,'Value');
if val == 3
    options.Comparison = 'None';
elseif val == 4
    options.Comparison = 'MinMax';
elseif val == 5
    options.Comparison = 'Affine';
elseif val == 6
    options.Comparison = 'Exact';
end

% Post-Processing
val = get(handles.save,'Value');
if val == 3
    options.Save = 'On';
elseif val == 4
    options.Save = 'Off';
end

val = get(handles.Display_Solution,'Value');
if val == 3
    options.display_Solution = 'On';
elseif val == 4
    options.display_Solution = 'Off';
end

val = get(handles.Display_Information,'Value');
if val == 3
    options.display_Information = 'On';
elseif val == 4
    options.display_Information = 'Off';
end

val = get(handles.plot_Solution,'Value');
if val == 3
    options.plot_Solution = 'On';
    
    vala = get(handles.plot_Solution_Style,'Value');
    if vala == 3
        options.plot_Solution_Style = 'all';
    elseif vala == 4
        options.plot_Solution_Style = 'CR';
    elseif vala == 5
        options.plot_Solution_Style = 'OBJ';
    end
    
    valb = str2num(get(handles.plot_Solution_Dim,'Value'));
    if ~isempty(valb)
        options.plot_Solution_Dim = valb;
    end
elseif val == 4
    options.plot_Solution = 'Off';
end

val = get(handles.plot_Time,'Value');
if val == 3
    options.plot_Time = 'On';
elseif val == 4
    options.plot_Time = 'Off';
end

val = get(handles.plot_Stat,'Value');
if val == 3
    options.plot_Stat = 'On';
elseif val == 4
    options.plot_Stat = 'Off';
end

% Read the problem out
val = get(handles.ProblemWorkspace,'Value');
if val > 2
    item = get(handles.ProblemWorkspace,'String');
    problem = evalin('base',item{val});
else
    val = get(handles.ProblemLibrary,'Value');
    if val < 3
        errordlg('Please specify a problem!','Problem specification');
        problem = [];
    else
        item = get(handles.ProblemLibrary,'String');
        problem = ProblemLibrary(item{val});
    end
end

if ~isempty(problem)
    [Solution, Information] = RIMmpMIQP_R04(problem, options);
    
    assignin('base','Solution',Solution);
    assignin('base','Information',Information);
    
    msgbox({'The problem was successfully solved and is now available in the workspace under the name ''Solution''.',...
        'Corresponding solution information is avaiable under ''Information''.'},'Problem solved successfully');
    
    val = get(handles.save,'Value');
    if val == 3
        msgbox('The results were successfully saved in the file ''Solution_mp-MIQP.dat''.','Save successful');
    end
end

guidata(hObject, handles);




% --- Executes on selection change in ProblemWorkspace.
function ProblemWorkspace_Callback(hObject, eventdata, handles)
% hObject    handle to ProblemWorkspace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ProblemWorkspace contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ProblemWorkspace

val = get(hObject,'Value');

if val > 2
    set(handles.ProblemLibrary,'Value',1);
    
    items = get(hObject,'String');    
    mpPProblem = evalin('base',items{val});
    
    Size.x = num2str(size(mpPProblem.A,2));
    Size.y = num2str(size(mpPProblem.E,2));
    Size.p = num2str(size(mpPProblem.F,2));
    Size.m = num2str(size(mpPProblem.A,1));
    Size.n = num2str(size(mpPProblem.Aeq,1));
    
    set(handles.CurrentProblem_ParametersText,'String',['Parameters: ',Size.p]);
    set(handles.CurrentProblem_ContinuousText,'String',['Continuous: ',Size.x]);
    set(handles.CurrentProblem_BinaryText,'String',['Binary: ', Size.y]);
    set(handles.CurrentProblem_EqualityText,'String',['Equality: ',Size.n]);
    set(handles.CurrentProblem_InequalityText,'String',['Inequality: ', Size.m]);
    
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ProblemWorkspace_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProblemWorkspace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Get the contents of the workspace
Wspace = evalin('base','who');
Wspace = [{'Problem from Workspace';''}; Wspace];

% Give this content to the menu
set(hObject,'String',Wspace);

guidata(hObject, handles);


% --- Executes on selection change in ProblemLibrary.
function ProblemLibrary_Callback(hObject, eventdata, handles)
% hObject    handle to ProblemLibrary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ProblemLibrary contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ProblemLibrary

val = get(hObject,'Value');

if val > 2
    set(handles.ProblemWorkspace,'Value',1);
    
    items = get(hObject,'String');    
    mpPProblem = ProblemLibrary(items{val});
    
    Size.x = num2str(size(mpPProblem.A,2));
    Size.y = num2str(size(mpPProblem.E,2));
    Size.p = num2str(size(mpPProblem.F,2));
    Size.m = num2str(size(mpPProblem.A,1));
    Size.n = num2str(size(mpPProblem.Aeq,1));
    
    set(handles.CurrentProblem_ParametersText,'String',['Parameters: ',Size.p]);
    set(handles.CurrentProblem_ContinuousText,'String',['Continuous: ',Size.x]);
    set(handles.CurrentProblem_BinaryText,'String',['Binary: ', Size.y]);
    set(handles.CurrentProblem_EqualityText,'String',['Equality: ',Size.n]);
    set(handles.CurrentProblem_InequalityText,'String',['Inequality: ', Size.m]);
    
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ProblemLibrary_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ProblemLibrary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Define contents in library
lib = {'Problem from Library?','','2x2x2_01','2x2x2_02','2x2x2_03','2x1x4_01','2x6x4_01','2x2x3_01'};

% Give this content to the menu
set(hObject,'String',lib);

guidata(hObject, handles);

% --- Executes on selection change in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns save contents as cell array
%        contents{get(hObject,'Value')} returns selected item from save


% --- Executes during object creation, after setting all properties.
function save_CreateFcn(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Save?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in Display_Solution.
function Display_Solution_Callback(hObject, eventdata, handles)
% hObject    handle to Display_Solution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Display_Solution contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Display_Solution


% --- Executes during object creation, after setting all properties.
function Display_Solution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Display_Solution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Display the solution?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in Display_Information.
function Display_Information_Callback(hObject, eventdata, handles)
% hObject    handle to Display_Information (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Display_Information contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Display_Information


% --- Executes during object creation, after setting all properties.
function Display_Information_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Display_Information (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Display information?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in plot_Solution.
function plot_Solution_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Solution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_Solution contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_Solution

val = get(hObject,'Value');

if val == 3
    set(handles.plot_Solution_Style,'Visible','On');
    set(handles.plot_Solution_Style,'Value',1);
    
    set(handles.plot_Solution_Dim,'Visible','On');
    set(handles.plot_Solution_Dim,'String','Fixed Parameters?');
else
    set(handles.plot_Solution_Style,'Visible','Off');
    set(handles.plot_Solution_Dim,'Visible','Off');
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function plot_Solution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_Solution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Plot the solution?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in plot_Solution_Style.
function plot_Solution_Style_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Solution_Style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_Solution_Style contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_Solution_Style


% --- Executes during object creation, after setting all properties.
function plot_Solution_Style_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_Solution_Style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Plotting style?','','all','CR','OBJ'});

guidata(hObject, handles);



function plot_Solution_Dim_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Solution_Dim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of plot_Solution_Dim as text
%        str2double(get(hObject,'String')) returns contents of plot_Solution_Dim as a double


% --- Executes during object creation, after setting all properties.
function plot_Solution_Dim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_Solution_Dim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in plot_Time.
function plot_Time_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_Time contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_Time


% --- Executes during object creation, after setting all properties.
function plot_Time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_Time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Plot timing info?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in plot_Stat.
function plot_Stat_Callback(hObject, eventdata, handles)
% hObject    handle to plot_Stat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns plot_Stat contents as cell array
%        contents{get(hObject,'Value')} returns selected item from plot_Stat


% --- Executes during object creation, after setting all properties.
function plot_Stat_CreateFcn(hObject, eventdata, handles)
% hObject    handle to plot_Stat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Plot stats?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in Comparison.
function Comparison_Callback(hObject, eventdata, handles)
% hObject    handle to Comparison (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Comparison contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Comparison


% --- Executes during object creation, after setting all properties.
function Comparison_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Comparison (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Comparison','','None','MinMax','Affine','Exact'});

guidata(hObject, handles);





% --- Executes on selection change in solver.
function solver_Callback(hObject, eventdata, handles)
% hObject    handle to solver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns solver contents as cell array
%        contents{get(hObject,'Value')} returns selected item from solver

val = get(hObject,'Value');

if val == 3
    set(handles.Global_Solver,'Visible','On');
    set(handles.Global_Solver,'Value',1);
else
    set(handles.Global_Solver,'Visible','Off');
end

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function solver_CreateFcn(hObject, eventdata, handles)
% hObject    handle to solver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'How to handle integers?','','Decomposition (requires GAMS)','Branch And Bound','Exhaustive Enumeration'});

guidata(hObject, handles);



% --- Executes on selection change in Global_Solver.
function Global_Solver_Callback(hObject, eventdata, handles)
% hObject    handle to Global_Solver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Global_Solver contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Global_Solver


% --- Executes during object creation, after setting all properties.
function Global_Solver_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Global_Solver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Solver for global optimization?','','ANTIGONE','BARON'});

guidata(hObject, handles);


% --- Executes on selection change in UpperBound_Set.
function UpperBound_Set_Callback(hObject, eventdata, handles)
% hObject    handle to UpperBound_Set (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns UpperBound_Set contents as cell array
%        contents{get(hObject,'Value')} returns selected item from UpperBound_Set

val = get(hObject,'Value');

if val == 3
    set(handles.UpperBound_Style,'Visible','On');
    set(handles.UpperBound_Style,'Value',1);
    
    set(handles.UpperBound_Tau,'Visible','On');
    set(handles.UpperBound_Tau,'String','How accurate (>= 0)?');
    
    set(handles.Global_Solver,'Visible','On');
    set(handles.Global_Solver,'Value',1);
else
    set(handles.UpperBound_Style,'Visible','Off');
    set(handles.UpperBound_Tau,'Visible','Off');
    val = get(handles.solver,'Value');
    if val ~= 3
        set(handles.Global_Solver,'Visible','Off');
    end
end

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function UpperBound_Set_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpperBound_Set (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'Upper Bound?','','On','Off'});

guidata(hObject, handles);


% --- Executes on selection change in UpperBound_Style.
function UpperBound_Style_Callback(hObject, eventdata, handles)
% hObject    handle to UpperBound_Style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns UpperBound_Style contents as cell array
%        contents{get(hObject,'Value')} returns selected item from UpperBound_Style


% --- Executes during object creation, after setting all properties.
function UpperBound_Style_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpperBound_Style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',{'What style?','','By Integers','By Parameter Space'});

guidata(hObject, handles);



function UpperBound_Tau_Callback(hObject, eventdata, handles)
% hObject    handle to UpperBound_Tau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UpperBound_Tau as text
%        str2double(get(hObject,'String')) returns contents of UpperBound_Tau as a double


% --- Executes during object creation, after setting all properties.
function UpperBound_Tau_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpperBound_Tau (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function sigma_Callback(hObject, eventdata, handles)
% hObject    handle to sigma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of sigma as text
%        str2double(get(hObject,'String')) returns contents of sigma as a double


% --- Executes during object creation, after setting all properties.
function sigma_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sigma (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject,'String',[{'How many threads?',''},num2cell(1:maxNumCompThreads)]);

guidata(hObject, handles);


function rho_limit_Callback(hObject, eventdata, handles)
% hObject    handle to rho_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rho_limit as text
%        str2double(get(hObject,'String')) returns contents of rho_limit as a double


% --- Executes during object creation, after setting all properties.
function rho_limit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rho_limit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function epsilon_Callback(hObject, eventdata, handles)
% hObject    handle to epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of epsilon as text
%        str2double(get(hObject,'String')) returns contents of epsilon as a double


% --- Executes during object creation, after setting all properties.
function epsilon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in TheChoiceMenu.
function TheChoiceMenu_Callback(hObject, eventdata, handles)
% hObject    handle to TheChoiceMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns TheChoiceMenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from TheChoiceMenu

val = get(hObject,'Value');

if val == 1
    set(handles.TheFootnoteTag,'Visible','Off');
    
    % Set the options
        set(handles.UpperBound_Set,'Value',1);
        set(handles.UpperBound_Style,'Value',1);
        set(handles.UpperBound_Tau,'String','How accurate (>= 0)?');
        set(handles.UpperBound_Style,'Visible','Off');
        set(handles.UpperBound_Tau,'Visible','Off');
        
        set(handles.sigma,'Value',1);
        set(handles.rho_limit,'String','How much local?');
        set(handles.epsilon,'String','Absolute suboptimality?');
        
        set(handles.solver,'Value',1);
        set(handles.Global_Solver,'Value',1);
        set(handles.Global_Solver,'Visible','Off');
        
        set(handles.Comparison,'Value',1);
        
        set(handles.save,'Value',1);
        set(handles.Display_Solution,'Value',1);
        set(handles.Display_Information,'Value',1);
        set(handles.plot_Solution,'Value',1);
        set(handles.plot_Solution_Style,'Value',1);
        set(handles.plot_Solution_Style,'Visible','Off');
        set(handles.plot_Solution_Dim,'Visible','Off');
        set(handles.plot_Time,'Value',1);
        set(handles.plot_Stat,'Value',1); 
        set(handles.plot_Solution_Dim,'String','Fixed Parameters?');
else
    set(handles.TheFootnoteTag,'Visible','On');
    if val == 2
        set(handles.TheFootnoteTag,'String','* Dua, V. Bozinis, N.A.; Pistikopoulos, E.N. Computers & Chemical Engineering, 26 (2002), 715 - 733.');
    
        % Set the options
        set(handles.UpperBound_Set,'Value',4);
        set(handles.UpperBound_Style,'Visible','Off');
        set(handles.UpperBound_Tau,'Visible','Off');
        
        set(handles.sigma,'Value',3);
        set(handles.rho_limit,'String','Inf');
        set(handles.epsilon,'String','0');
        
        set(handles.solver,'Value',3);
        set(handles.Global_Solver,'Value',3); %This was not specified in Dua et al.
        set(handles.Global_Solver,'Visible','On');
        
        set(handles.Comparison,'Value',3);
        
        set(handles.save,'Value',3);
        set(handles.Display_Solution,'Value',4);
        set(handles.Display_Information,'Value',4);
        set(handles.plot_Solution,'Value',4);
        set(handles.plot_Solution_Style,'Visible','Off');
        set(handles.plot_Solution_Dim,'Visible','Off');
        
        set(handles.plot_Time,'Value',4);
        set(handles.plot_Stat,'Value',4);    
    
    elseif val == 3
        set(handles.TheFootnoteTag,'String','* Axehill, D.; Besselmann, T.; Raimondo, D.M.; Morari, M. Automatica, 50 (2014), 240 - 246.');
    
        % Set the options
        set(handles.UpperBound_Set,'Value',4);
        set(handles.UpperBound_Style,'Visible','Off');
        set(handles.UpperBound_Tau,'Visible','Off');
        
        set(handles.sigma,'Value',3);
        set(handles.rho_limit,'String','Inf');
        set(handles.epsilon,'String','0');
        
        set(handles.solver,'Value',4);
        set(handles.Global_Solver,'Visible','Off');
        
        set(handles.Comparison,'Value',4);
        
        set(handles.save,'Value',3);
        set(handles.Display_Solution,'Value',4);
        set(handles.Display_Information,'Value',4);
        set(handles.plot_Solution,'Value',4);
        set(handles.plot_Solution_Style,'Visible','Off');
        set(handles.plot_Solution_Dim,'Visible','Off');
        
        set(handles.plot_Time,'Value',4);
        set(handles.plot_Stat,'Value',4); 
        
    elseif val == 4
        set(handles.TheFootnoteTag,'String','* Oberdieck, R; Wittmann-Hohlbein, M.; Pistikopoulos, E.N. Journal of Global Optimization, 59 (2014), 527 - 543.');
    
            % Set the options
        set(handles.UpperBound_Set,'Value',4);
        set(handles.UpperBound_Style,'Visible','Off');
        set(handles.UpperBound_Tau,'Visible','Off');
        
        set(handles.sigma,'Value',3);
        set(handles.rho_limit,'String','Inf');
        set(handles.epsilon,'String','0');
        
        set(handles.solver,'Value',4);
        set(handles.Global_Solver,'Visible','Off');
        
        set(handles.Comparison,'Value',5);
        
        set(handles.save,'Value',3);
        set(handles.Display_Solution,'Value',4);
        set(handles.Display_Information,'Value',4);
        set(handles.plot_Solution,'Value',4);
        set(handles.plot_Solution_Style,'Visible','Off');
        set(handles.plot_Solution_Dim,'Visible','Off');
        
        set(handles.plot_Time,'Value',4);
        set(handles.plot_Stat,'Value',4);  
    end
end

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function TheChoiceMenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TheChoiceMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Define the different options
options = {'None','Dua, 2002*', 'Axehill, 2014*', 'Oberdieck, 2014*'};

% Give this content to the menu
set(hObject,'String',options);

guidata(hObject, handles);


% --- Executes on button press in TheResetButton.
function TheResetButton_Callback(hObject, eventdata, handles)
% hObject    handle to TheResetButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.ProblemWorkspace,'Value',1);
set(handles.ProblemLibrary,'Value',1);
set(handles.CurrentProblem_ParametersText,'String','Parameters:');
set(handles.CurrentProblem_ContinuousText,'String','Continuous:');
set(handles.CurrentProblem_BinaryText,'String','Binary:');
set(handles.CurrentProblem_EqualityText,'String','Equality:');
set(handles.CurrentProblem_InequalityText,'String','Inequality:');

set(handles.TheChoiceMenu,'Value',1);
set(handles.TheFootnoteTag,'Visible','Off');

% Set the options
set(handles.UpperBound_Set,'Value',1);
set(handles.UpperBound_Style,'Value',1);
set(handles.UpperBound_Tau,'String','How accurate (>= 0)?');
set(handles.UpperBound_Style,'Visible','Off');
set(handles.UpperBound_Tau,'Visible','Off');

set(handles.sigma,'Value',1);
set(handles.rho_limit,'String','How much local?');
set(handles.epsilon,'String','Absolute suboptimality?');

set(handles.solver,'Value',1);
set(handles.Global_Solver,'Value',1);
set(handles.Global_Solver,'Visible','Off');

set(handles.Comparison,'Value',1);

set(handles.save,'Value',1);
set(handles.Display_Solution,'Value',1);
set(handles.Display_Information,'Value',1);
set(handles.plot_Solution,'Value',1);
set(handles.plot_Solution_Style,'Value',1);
set(handles.plot_Solution_Style,'Visible','Off');
set(handles.plot_Solution_Dim,'Visible','Off');
set(handles.plot_Time,'Value',1);
set(handles.plot_Stat,'Value',1);
set(handles.plot_Solution_Dim,'String','Fixed Parameters?');

guidata(hObject, handles);



% --- Executes on button press in TheExportButton.
function TheExportButton_Callback(hObject, eventdata, handles)
% hObject    handle to TheExportButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Ask the user for a name
prompt = {'Enter name of .m file:'};
dlg_title = 'Name of .m file';
num_lines = 1;
def = {'Example'};
answer = inputdlg(prompt,dlg_title,num_lines,def);
answer = answer{1};

if ~strcmp(answer(end-1:end),'.m')
    answer = [answer,'.m'];
end

% Open the file
fid = fopen(answer,'w');

% Write the beginning
fprintf(fid,'%%==========================================================================\n');
fprintf(fid,['%% File Name     : <',answer,'>\n']);
fprintf(fid,'%% Description   : This file was automatically created by the software\n');
fprintf(fid,'%% RIMmpMIQP.\n');
fprintf(fid,'%% \n');
fprintf(fid,'%% This function contains the options set when the problem was exported. If\n');
fprintf(fid,'%% the user ould like to change these options, he or she is kindly referred\n');
fprintf(fid,'%% to the user manual, where all the options are listed.\n');
fprintf(fid,'%%--------------------------------------------------------------------------\n');
fprintf(fid,'%% Author        : Richard Oberdieck\n');
fprintf(fid,'%% Office        : C511, Rhoderic Hill, Imperial College London\n');
fprintf(fid,'%% Mail          : richard.oberdieck11@imperial.ac.uk\n');
fprintf(fid,'%%--------------------------------------------------------------------------\n');
fprintf(fid,'%% Generation date | Author  | Description\n');
fprintf(fid,'%%-----------------+---------+----------------------------------------------\n');
fprintf(fid,['%% ',date,'     | RO      | Automatically generated version\n']);
fprintf(fid,'%%==========================================================================\n');
fprintf(fid,'\n');

% Add the correct path
fprintf(fid,'addpath(genpath(''./Files''));\n');
fprintf(fid,'\n');

% Load the problem
val = get(handles.ProblemWorkspace,'Value');
if val > 2
    item = get(handles.ProblemWorkspace,'String');
    name = item{val};
    
    fprintf(fid, ['problem = ',name,';\n']);
else
    val = get(handles.ProblemLibrary,'Value');
    if val > 2
        item = get(handles.ProblemLibrary,'String');
        name = item{val};
        
        fprintf(fid, ['problem = ProblemLibrary(',name,');\n']);
    else
        fprintf(fid,'%% Please specify a problem using\n');
        fprintf(fid,'%% problem = ');
        msgbox('No problem has been specified, please add in the script manually.','Problem specification');
    end
end
fprintf(fid,'\n');

% Add the options
% Upper Bound
val = get(handles.UpperBound_Set,'Value');
if val == 3
    fprintf(fid,'options.UpperBound_Set = ''On'';\n');
    
    vala = get(handles.UpperBound_Style,'Value');
    if vala == 3
        fprintf(fid,'options.UpperBound_Style = ''Integer'';\n');
    elseif vala == 4
        fprintf(fid,'options.UpperBound_Style = ''ParameterSpace'';\n');
    end
    
    str = get(handles.UpperBound_Tau,'String');
    valb = str2double(str);
    
    if ~isnan(valb)
        if ceil(valb) ~= floor(valb)
            errordlg('Please specify a correct accuracy (has to be integer)!','No integer input!');
        else
            fprintf(fid,['options.UpperBound_Tau = ',str,';\n']);
        end
    end
elseif val == 4
    fprintf(fid,'options.UpperBound_Set = ''Off'';\n');
end

% Parallelization
val = get(handles.sigma,'Value');
if val > 2
    str = num2str(val-2);
    fprintf(fid,['options.sigma = ',str,';\n']);
end

str = get(handles.rho_limit,'String');
val = str2double(str);
if ~isnan(val)
    if ceil(val) ~= floor(val)
        errordlg('Please specify a correct value (has to be integer)!','No integer input!');
    else
        fprintf(fid,['options.rho_limit = ',str,';\n']);
    end
end

% Suboptimality
str = get(handles.epsilon,'String');
val = str2double(str);
if ~isnan(val)
    if val < 0
        errordlg('Please specify a correct value (has to be positive)!','No positive input!');
    else
        fprintf(fid,['options.epsilon = ',str,';\n']);
    end
end

% The solver
val = get(handles.solver,'Value');
if val == 3
    fprintf(fid,'options.solver = ''Decomposition'';\n');
elseif val == 4
    fprintf(fid,'options.solver = ''BranchAndBound'';\n');
elseif val == 5
    fprintf(fir,'options.solver = ''ExhaustiveEnumeration'';\n');
end

val = get(handles.Global_Solver,'Value');
if val == 3
    fprintf(fid,'options.Global_Solver = ''ANTIGONE'';\n');
elseif val == 4
    fprintf(fid,'options.Global_Solver = ''BARON'';\n');
end

% Comparison
val = get(handles.Comparison,'Value');
if val == 3
    fprintf(fid,'options.Comparison = ''None'';\n');
elseif val == 4
    fprintf(fid,'options.Comparison = ''MinMax'';\n');
elseif val == 5
    fprintf(fid,'options.Comparison = ''Affine'';\n');
elseif val == 6
    fprintf(fid,'options.Comparison = ''Exact'';\n');
end

% Post-Processing
val = get(handles.save,'Value');
if val == 3
    fprintf(fid,'options.Save = ''On'';\n');
elseif val == 4
    fprintf(fid,'options.Save = ''Off'';\n');
end

val = get(handles.Display_Solution,'Value');
if val == 3
    fprintf(fid,'options.display_Solution = ''On'';\n');
elseif val == 4
    fprintf(fid,'options.display_Solution = ''Off'';\n');
end

val = get(handles.Display_Information,'Value');
if val == 3
    fprintf(fid,'options.display_Information = ''On'';\n');
elseif val == 4
    fprintf(fid,'options.display_Information = ''Off'';\n');
end

val = get(handles.plot_Solution,'Value');
if val == 3
    fprintf(fid,'options.plot_Solution = ''On'';\n');
    
    vala = get(handles.plot_Solution_Style,'Value');
    if vala == 3
        fprintf(fid,'options.plot_Solution_Style = ''all'';\n');
    elseif vala == 4
        fprintf(fid,'options.plot_Solution_Style = ''CR'';\n');
    elseif vala == 5
        fprintf(fid,'options.plot_Solution_Style = ''OBJ'';\n');
    end
    
    str = get(handles.plot_Solution_Dim,'Value');
    valb = str2num(str);
    if ~isempty(valb)
        if isempty(strfind(str,'['))
            str = ['[',str,']'];
        end
        fprintf(fid,['options.plot_Solution_Dim = ',str,';\n']);
    end
elseif val == 4
    fprintf(fid,'options.plot_Solution = ''Off'';\n');
end

val = get(handles.plot_Time,'Value');
if val == 3
    fprintf(fid,'options.plot_Time = ''On'';\n');
elseif val == 4
    fprintf(fid,'options.plot_Time = ''Off'';\n');
end

val = get(handles.plot_Stat,'Value');
if val == 3
    fprintf(fid,'options.plot_Stat = ''On'';\n');
elseif val == 4
    fprintf(fid,'options.plot_Stat = ''Off'';\n');
end
    
% Start the solver
fprintf(fid,'\n');
fprintf(fid,'[Solution, Information] = RIMmpMIQP_R04(problem,options);\n');
fclose(fid);

msgbox('File successfully created!','Export successful');

guidata(hObject, handles);
