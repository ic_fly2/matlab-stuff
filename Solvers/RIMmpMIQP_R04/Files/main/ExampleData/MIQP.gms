$set matout "'matsol.gdx', y, x, OBJ ";

SET      i number of continuous variables /1*1/ ,
         j number of binary variables /1*1/ ,
         k number of inequality constraints /1*1/ ,
         r number of parametric cuts /1*1/ ;

ALIAS    (i1,i),
         (j1,j),
         (k1,k);

$if exist matdata.gms $include matdata.gms

PARAMETER
Q(i,i1) quadratic terms of continuous variables
          / 1 .1 1.0 / ,

Qbin(j,j1) quadratic terms of binary variables
          / 1 .1 1.0 / ,

Qxy(i,j) quadratic terms of binary-continuous bilinear terms
          / 1 .1 1.0 / ,

ccon(i)  linear terms of continuous variables
         / 1 1.0 / ,

dcon(j)  linear terms of binary variables
         / 1 1.0 / ,

A(k,i)   constraint matrix of continuous variables
         / 1 .1 1.0 / ,

E(k,j)   constraint matrix of binary variables
         / 1 .1 1.0 / ,

b(k)     coefficents of constraints
         / 1 1.0 / ,

Px(r,i,i1)   quadratic part of the continuous variables of parametric cut
         / 1.1 .1 1.0 / ,

Pxy(r,i,j)   quadratic part of the continuous_binary variables of parametric cut
         / 1.1 .1 1.0 / ,

Py(r,j,j1)   quadratic part of the binary variables of parametric cut
         / 1.1 .1 1.0 / ,

tx(r,i)   linear part of continuous variables of parametric cut
         / 1 .1 1.0 / ,

ty(r,j)   linear part of binary variables of parametric cut
         / 1 .1 1.0 / ,

f(r)     constant part of parametric cut
         / 1 1.0 / ;

$if exist Parameters.gms $include Parameters.gms

VARIABLE OBJ ;
POSITIVE VARIABLES x(i) ;
BINARY VARIABLES y(j) ;

EQUATION cost, inequality(k), paracut(r) ;

cost.. OBJ =e= sum((i,i1), x(i) * Q(i,i1) * x(i1)) + sum((j,j1), y(j) * Qbin(j,j1) * y(j1)) +
         sum((i,j), x(i) * Qxy(i,j) * y(j)) + sum(i, ccon(i) * x(i)) + sum(j, dcon(j) * y(j)) ;

inequality(k).. sum(i, A(k,i) * x(i)) + sum(j, E(k,j) * y(j)) =l= b(k) ;

paracut(r).. sum((i,i1), x(i) * Px(r,i,i1) * x(i1)) + sum((i,j), x(i) * Pxy(r,i,j) * y(j)) +
         sum((j,j1), y(j) * Py(r,j,j1) * y(j1)) + sum(i, tx(r,i) * x(i)) +
         sum(j, ty(r,j) * y(j)) + f(r) =l= 0 ;



model MIQP /ALL/;
Solve MIQP Using MINLP Minimizing OBJ;

execute_unload %matout%;
