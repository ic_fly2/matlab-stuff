$gdxin Sets.gdx

SET      i number of continuous variables
         j number of binary variables
         k number of inequality constraints
         r number of parametric cuts ;

$load i j k r
$gdxin

ALIAS    (i1,i),
         (j1,j),
         (k1,k);

$gdxin Parameters.gdx

PARAMETER
Q(i,i1) quadratic terms of continuous variables
Qbin(j,j1) quadratic terms of binary variables
Qxy(i,j) quadratic terms of binary-continuous bilinear terms
ccon(i)  linear terms of continuous variables
dcon(j)  linear terms of binary variables
A(k,i)   constraint matrix of continuous variables
E(k,j)   constraint matrix of binary variables
b(k)     coefficents of constraints  ;
$load Q Qbin Qxy ccon dcon A E b
$gdxin

$gdxin Cuts.gdx
PARAMETER
Px(r,i,i1)   quadratic part of the continuous variables of parametric cut
Pxy(r,i,j)   quadratic part of the continuous_binary variables of parametric cut
Py(r,j,j1)   quadratic part of the binary variables of parametric cut
tx(r,i)   linear part of continuous variables of parametric cut
ty(r,j)   linear part of binary variables of parametric cut
f(r)     constant part of parametric cut ;

$load Px Pxy Py tx ty f
$gdxin

VARIABLE OBJ ;
VARIABLES x(i) ;
BINARY VARIABLES y(j) ;

EQUATION cost, inequality(k), paracut(r) ;

cost.. OBJ =e= sum((i,i1), x(i) * Q(i,i1) * x(i1)) + sum((j,j1), y(j) * Qbin(j,j1) * y(j1)) + sum((i,j), x(i) * Qxy(i,j) * y(j)) + sum(i, ccon(i) * x(i)) + sum(j, dcon(j) * y(j)) ;

inequality(k).. sum(i, A(k,i) * x(i)) + sum(j, E(k,j) * y(j)) =l= b(k) ;

paracut(r).. sum((i,i1), x(i) * Px(r,i,i1) * x(i1)) + sum((i,j), x(i) * Pxy(r,i,j) * y(j)) + sum((j,j1), y(j) * Py(r,j,j1) * y(j1)) + sum(i, tx(r,i) * x(i)) + sum(j, ty(r,j) * y(j)) + f(r) =l= 0 ;


model MIQP /ALL/;
Solve MIQP Using MINLP Minimizing OBJ;

DISPLAY x.L, y.L, OBJ.L, MIQP.modelstat

If ((MIQP.modelstat eq 1) or (MIQP.modelstat eq 2) or (MIQP.modelstat eq 8),
execute_unload "gams_data.gdx" y.L ;
);
