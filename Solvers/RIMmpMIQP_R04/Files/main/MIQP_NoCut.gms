$gdxin Sets.gdx

SET      i number of continuous variables
         j number of binary variables
         k number of inequality constraints ;

$load i j k
$gdxin

ALIAS    (i1,i),
         (j1,j),
         (k1,k);

$gdxin Parameters.gdx

PARAMETER
Q(i,i1) quadratic terms of continuous variables
Qbin(j,j1) quadratic terms of binary variables
Qxy(i,j) quadratic terms of binary-continuous bilinear terms
ccon(i)  linear terms of continuous variables
dcon(j)  linear terms of binary variables
A(k,i)   constraint matrix of continuous variables
E(k,j)   constraint matrix of binary variables
b(k)     coefficents of constraints

$load Q Qbin Qxy ccon dcon A E b
$gdxin

VARIABLE OBJ ;
VARIABLES x(i) ;
BINARY VARIABLES y(j) ;

EQUATION cost, inequality(k) ;

cost.. OBJ =e= sum((i,i1), x(i) * Q(i,i1) * x(i1)) + sum((j,j1), y(j) * Qbin(j,j1) * y(j1)) + sum((i,j), x(i) * Qxy(i,j) * y(j)) + sum(i, ccon(i) * x(i)) + sum(j, dcon(j) * y(j)) ;

inequality(k).. sum(i, A(k,i) * x(i)) + sum(j, E(k,j) * y(j)) =l= b(k) ;


model MIQP /ALL/;
Solve MIQP Using MINLP Minimizing OBJ;

DISPLAY x.L, y.L, OBJ.L, MIQP.modelstat

If ((MIQP.modelstat eq 1) or (MIQP.modelstat eq 2) or (MIQP.modelstat eq 8),
execute_unload "gams_data.gdx" y.L ;
);

