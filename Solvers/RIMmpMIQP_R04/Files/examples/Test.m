% Add the necessary paths

% addpath(genpath('/home/ps/ce-fs1/ro611/Code/MATLAB/POP_FULL_x64'));
% addpath(genpath('/home/ps/ce-fs1/ro611/Code/MATLAB/MY_CODES/RIMmpQP_RN02'));
% addpath(genpath('/usr/local/NAG/MBL6A24DNL/help/NAG'));
% addpath('/usr/local/NAG/MBL6A24DNL/mex.a64');
% addpath('/usr/local/GAMS/gams24.2.3_linux_x64_64_sfx/');
% 
% 
% startup
% LPSOLVER = 3;
% QPSOLVER = 1;



%% New example
options.UpperBound_Set = 'Off';
options.UpperBound_Style = 'Integer';
options.UpperBound_Tau = 0;
%options.solver = 'Decomposition';
options.plot_Solution = 'Off';
options.rho_limit = inf;
options.display_Solution = 'Off';
options.Comparison = 'None';
options.plot_Time = 'Off';
options.sigma = 1;
options.solver = 'ExhaustiveEnumeration';
problem = ProblemLibrary('2x2x3_01');

[Solution, Information] = RIMmpMIQP_R04(problem, options);

%list = {'Decomposition','BranchAndBound','ExhaustiveEnumeration'};
%lista = {'2x2x2_01','2x2x2_02','2x2x2_03','2x1x4_01','2x6x4_01'};
%time = zeros(length(list),length(lista));

%for i = 1:length(lista)
% for k = 1:length(list)
%     options.solver = list{k};
%     problem = ProblemLibrary(lista{i});
 %    try
 %[Solution, Information] = RIMmpMIQP_R04(problem, options);
  %   catch
 %        Information.Time.Overall = inf;
 %    end
% time(k,i) = Information.Time.Overall;
% disp('++++++++++++++++++++++++++++++++++++++++++++++++');
% end
%end

save('solu','Solution');

%% Plots for Paper
% hd = findall(gcf,'type', 'axes');
% S1 = 28;
% S2 = 32;
% set(hd,'fontsize',S1);
%     
% set(cell2mat(get(hd,'Xlabel')),'fontsize',S2) %change label font size
% set(cell2mat(get(hd,'Ylabel')),'fontsize',S2)
% set(cell2mat(get(hd,'Zlabel')),'fontsize',S2)
% set(cell2mat(get(hd,'title')),'fontsize',S2)
% 
% j = 0;
% for k = 1:length(sol(1).sol)
%     j = j + length(sol(1).sol(k).Solution);
% end
% fprintf('Average = %2.4g\n',j/k);

%save('Solution','Solution');