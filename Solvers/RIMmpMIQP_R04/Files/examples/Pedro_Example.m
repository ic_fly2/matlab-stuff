% 
% addpath(genpath('/home/ps/ce-fs1/ro611/Code/MATLAB/POP_FULL_x64'));
% addpath(genpath('/home/ps/ce-fs1/ro611/Code/MATLAB/MY_CODES/RIMmpQP_RN02'));
% addpath(genpath('/usr/local/NAG/MBL6A24DNL/help/NAG'));
% addpath('/usr/local/NAG/MBL6A24DNL/mex.a64');
% addpath('/usr/local/GAMS/gams24.2.3_linux_x64_64_sfx/');


startup
LPSOLVER = 3;
QPSOLVER = 1;
%% Implementation Pedro's Examples
% Example 1 from Rivotti and Pistikopoulos "A dynamic programming based
% approach for explicit model predictive control of hybrid systems"

Initial.deltak = [10.1 0;
    0 10.1;
    1 1;
    -1 -1;
    -12.028 0;
    -12.028 0;
    -12.028 0;
    -12.028 0;
    12.028 0;
    12.028 0;
    12.028 0;
    12.028 0;
    0 -12.028;
    0 -12.028;
    0 -12.028;
    0 -12.028;
    0 12.028;
    0 12.028;
    0 12.028;
    0 12.028];

Initial.zk = [zeros(4);
    1 0 0 0;
    0 1 0 0;
    -1 0 0 0;
    0 -1 0 0;
    1 0 0 0;
    0 1 0 0;
    -1 0 0 0;
    0 -1 0 0;
    0 0 1 0;
    0 0 0 1;
    0 0 -1 0;
    0 0 0 -1;
    0 0 1 0;
    0 0 0 1;
    0 0 -1 0;
    0 0 0 -1];

Initial.uk = [zeros(9,1);
    1;
    0;
    -1;
    zeros(5,1);
    1;
    0;
    -1];

Initial.xk = [1 0;
    -1 0;
    zeros(6,2);
    0.4 -0.69282;
    0.69282 0.4;
    -0.4 0.69282;
    -0.69282 -0.4;
    zeros(4,2);
    0.4 0.69282;
    -0.69282 0.4;
    -0.4 -0.69282;
    0.69282 -0.4];

Initial.const = [10.1;
    10.1;
    1;
    -1;
    0;
    0;
    0;
    0;
    12.028;
    12.028;
    12.028;
    12.028;
    0;
    0;
    0;
    0;
    12.028;
    12.028;
    12.028;
    12.028];

zx = [eye(2) eye(2)];

Q = eye(2);
P = eye(2);
R = 1;

xmin = [-10; -10];
xmax = [10; 10];
umin = -1;
umax = 1;

%% Reformulate the problem
N = 2; %Define the horizon
% OBJ: min xN'*P*xN + sum_(k=0)^(N-1) x{k+1}'*Q*x{k+1} + u{k}'*R*u{k}
%
% s.t. [-u{k} (z{k} - Ix{k+1})], k = 1,...,N-2
%      [-u{N-1} z{N-1}] (this is because the k-constraints are only for N-1
%      xmin <= x{k} <= xmax
%      umin <= u{k} <= umax

% Objective function definition
Quad = [];

for k = 0:(N-2)
    Quad = blkdiag(Quad,R,zx'*Q*zx);
end
% Final value
Quad = blkdiag(Quad,R,zx'*P*zx);

% Constraints (not box constraints)
A = [];
E = [];
b = [];

for k = 0:(N-2)
    A = blkdiag(A,[-Initial.uk (Initial.zk - Initial.xk*zx)]);
    E = blkdiag(E, Initial.deltak);
    b = [b; Initial.const];
end

A = blkdiag(A, [-Initial.uk Initial.zk]);
E = blkdiag(E, Initial.deltak);
b = [b; Initial.const];

len = size(A,1) - size(Initial.xk,1);
F = [Initial.xk; zeros(len,size(Initial.xk,2))];

% Add the box constraints
block = [1; -1];
cont_block = [];
cont_b = [];

for k = 0:(N-1)
    cont_block = blkdiag(cont_block, block, [zx; -zx]);
    cont_b = [cont_b; umax; -umin; xmax; -xmin];
end

A = [A; cont_block];
E = [E; zeros(size(cont_block,1),size(E,2))];
b = [b; cont_b];
F = [F; zeros(size(cont_block,1),size(F,2))];

%% Write the problem into RIMmpMIQP_R04 style

problem.Q = 2*Quad;
problem.A = A;
problem.E = E;
problem.F = F;
problem.b = b;

problem.Aeq = zeros(1,size(A,2));
problem.Eeq = zeros(1,size(E,2));
problem.beq = 0;
problem.Feq = zeros(1,size(F,2));

problem.CRA = [1 0; 0 1; -1 0; 0 -1];
problem.CRb = [10; 10; 10; 10];
    
options.UpperBound_Set = 'Off';
options.UpperBound_Style = 'Integer';
options.UpperBound_Tau = 0;
options.solver = 'Decomposition';
options.plot_Solution = 'On';
options.sigma = 1;
options.rho_limit = inf;
options.display_Solution = 'On';
options.Comparison = 'None';
options.plot_Time = 'Off';
[Solution, Time] = RIMmpMIQP_R04(problem, options);
    
    
    
    
    
    