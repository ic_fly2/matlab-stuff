Welcome to RIMmpMIQP!

Version: R04
Developed by Richard Oberdieck

----------------------------------------------------

Start-up:

The library is started by running 'RIMmpMIQP.m' (not 'RIMmpMIQP.fig').

For all further detail, please consult the user manual!


Bugs and Errors:

If you find any problems or bugs in the program, or if you have a suggestion on how
things should be done differently, then please contact me at richard.oberdieck11@imperial.ac.uk.
