% [x,val,status]=MILP(f,A,b,Aeq,beq,lb,ub,M,e)
% this function solves a mixed-integer linear programming problem
% using the branch and bound algorithm
% The code uses MATLAB's linear programming solver "linprog"
% to solve the LP relaxations at each node of the branch and bound tree.
%   min f*x
%  subject to
%        A*x <=b
%        Aeq * x = beq   
%        lb <= x <= ub
%        M is a vector of indeces for the variables that are constrained to be integers
%        e is the integarilty tolerance
% the output variables are :
% x : the solution
% val: value of the objective function at the optimal solution
% status =1 if successful
%        =0 if maximum number of iterations reached in the linprog function
%        =-1 if there is no solution
% Example:
%        maximize 17 x1 + 12 x2 
%        subject to
% 	             10 x1 + 7 x2 <=40
%                   x1 +   x2 <= 5
%                   x1, x2 >=0 and are integers
% f=[-17, -12]; %take the negative for maximization problems
% A=[ 10  7; 1 1];
% B=[40; 5];
% lb=[0 0];
% ub=[inf inf];
% M=[1,2];
% e=2^-24;
% [x v s]= IP(f,A,B,[],[],lb,ub,M,e)
% Original code
% By Sherif A. Tawfik, Faculty of Engineering, Cairo University
% Modified by Kartik with comments for MA 505
% Last updated: 5th April, 2006.

function [x,val,status]=milp(f,A,b,Aeq,beq,lb,ub,M,e)

options = optimset('display','off');
bound=inf; % the initial bound is set to +ve infinity

% Solve the LP relaxation at the root node using MATLAB's linprog function
% Type "help linprog" for help with the linprog routine

[x0,val0]=linprog(f,A,b,Aeq,beq,lb,ub,[],options); 

[x,val,status,b]=rec(f,A,b,Aeq,beq,lb,ub,x0,val0,M,e,bound); % a recursive function that processes the BB tree 

function [xx,val,status,bb]=rec(f,A,b,Aeq,beq,lb,ub,x,v,M,e,bound) 
options = optimset('display','off');
% x is an initial solution and v is the corressponding objective function value

% Solve the LP relaxation at the current node
[x0,val0,status0]=linprog(f,A,b,Aeq,beq,lb,ub,[],options); 

% If the LP relaxation is infeasible then PRUNE THE NODE BY INFEASIBILITY
% If the LP objective value is greater than our current integer bound then PRUNE THE NODE BY BOUNDS

if status0<=0 | val0 > bound  
    xx=x; val=v; status=status0; bb=bound;
    return;
end

% If the solution to the LP relaxation is feasible in the MILP problem, then check the objective value of this 
% against the objective value of the best feasible solution that has been obtained so far for the MILP problem.
% If the new feasible solution has a lower objective value then update the bound
% Else PRUNE THE NODE BY OPTIMALITY

ind=find( abs(x0(M)-round(x0(M)))>e ); 
if isempty(ind)
    status=1;        
    if val0 < bound    % the new feasible solution is better than the current feasible solution hence replace
        x0(M)=round(x0(M));
        xx=x0;        
        val=val0;
        bb=val0;
    else
        xx=x;  % return the input solution
        val=v;
        bb=bound;
    end
    return
end

% if we come here this means that the solution of the LP relaxation is not feasible in the MILP problem.
% However, the objective value of the LP relaxation is lower than the current bound.
% So we branch on this node to create two subproblems.
% We will solve the two subproblems recursively by calling the same branching function.
 
% first LP problem with the added constraint that x_i <= floor(x_i) , i=ind(1)
br_var=M(ind(1));
br_value=x(br_var);
if isempty(A)
    [r c]=size(Aeq);
else
    [r c]=size(A);
end
A1=[A ; zeros(1,c)];
A1(end,br_var)=1;
b1=[b;floor(br_value)];

% second LP problem with the added constraint that x_i >= ceil(x_i) , i=ind(1)
A2=[A ;zeros(1,c)];
A2(end,br_var)=-1;
b2=[b; -ceil(br_value)];


% solve the first LP problem
[x1,val1,status1,bound1]=rec(f,A1,b1,Aeq,beq,lb,ub,x0,val0,M,e,bound);
status=status1;
if status1 >0 & bound1<bound % if the solution was successfull and gives a better bound
   xx=x1;
   val=val1;
   bound=bound1;
   bb=bound1;
else
    xx=x0;
    val=val0;
    bb=bound;
end
    
% solve the second LP problem
[x2,val2,status2,bound2]=rec(f,A2,b2,Aeq,beq,lb,ub,x0,val0,M,e,bound);

if status2 >0 & bound2<bound % if the solution was successful and gives a better bound
    status=status2;
    xx=x2;
    val=val2;
    bb=bound2;
end