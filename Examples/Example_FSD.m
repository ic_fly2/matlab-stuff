% Normal scheduling example
clear all; 
% Tariff options: 

% From Lopez 2009
Electricity_Price(1:11) = 28.59;  Electricity_Price(12:19) = 85.76;
Electricity_Price(20:33) = 48.49; Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49; Electricity_Price(45:48) =  28.59;
Electricity_Price = Electricity_Price/5;

% From UK utility:
North_weekday(1:6)   = 6.2; North_weekday(7:14)  = 6.0; North_weekday(15:22) = 7.9;
North_weekday(23:30) = 8.5; North_weekday(31:38) = 19.2; North_weekday(39:46) = 8.5;
North_weekday(47:48) = 6.2;

North_weekend(1:6)   = 6.1; North_weekend(7:14)  = 5.9; North_weekend(15:28) = 7.5;
North_weekend(29:36) = 8.5; North_weekend(37:45) = 7.2; North_weekend(46:48) = 6.1;

% just a small structure
EL.price{1} = Electricity_Price;
EL.price{2} = North_weekday;
EL.price{3} = North_weekend;

% GHG emissions:
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

% define prob the problem structure:
% Define the network file to be analysed.
prob.def = 'van_zyl_norm_171_FSD' ;%'van_zyl_norm_171_FSD'; % or 'Lecture_example'
prob.settings.demand_factor =0.5; % set the demand factor, if higher demand 
                                  % factor return infeasible, look at vmax


prob.settings.price = EL.price{2}; % pick 1,2,3
prob.settings.GHG = CO2_kWh_av ;
prob.N =12; % Time steps 6,12,24,48,96 (Use 96 only if you have a lot of time)
            % Time steps of 4h, 2h, 1h, 30 mins, 15 mins
   
% Define the objective function             
prob.settings.OF_spec = 'F1';  %  F1, F2,F3,F4 see wiki for details

% Define the pipe and pump approximations:
prob.approx_number =7; % 7 

% Important setting!
prob.settings.time_limit  = 300; % Maximum seconds 
prob.settings.solver_gap = 0.05; % Optimality gap after which to finish

% Some settings
prob.vmax_min = 2; % modify to improve fit or get feasible solutions
prob.M  = 999;
prob.asymmetry = 'on'; % do not deactivate unless you know what you are doing
                       % For new networks, make a description as described
                       % in the assymetry section of the wiki!
                       
prob.settings.solver = 'cplex'; %'intlinprog'; %'cplex' 'gurobi';
prob.settings.plot = 'On'; % Plot results? 'Off'

% Call experiment 
out  = experiment(prob);