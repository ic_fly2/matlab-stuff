function [output] = Calc_T(P, h)
global FP

%call Temperature function
[T,ErrorMsg] = invoke(FP,'Temperature','Ph',P, h);
ErrorCheck(ErrorMsg);

output = T;