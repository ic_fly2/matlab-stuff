function [output] = Calc_v(P, h)
global FP

%call SpecVolume function
[v,ErrorMsg] = invoke(FP,'SpecVolume','Ph',P, h);
ErrorCheck(ErrorMsg);

output = v;