function [output] = CalcH(P,v,P_a)
global g
H = (P -P_a)* 1e5 * v / g; %[m]

output = H;