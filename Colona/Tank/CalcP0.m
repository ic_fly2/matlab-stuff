function output = CalcP0(h,P, H_0, P_atm)
global FP g

%Caculation of the density of the tank: the density depends on the pressure
%also: rho = rho (P,h) so it must be computed iteratively: 
% P - [Pa + rho(P,h) * g * H]=0
%an external solver block must be used.

% get the value of paramters H0 and Pa from Mask input
%H_0 = eval(get_param(gcb,'H_0'));
%P_atm = eval(get_param(gcb,'P_atm'));

%call SpecVolume function
[v,ErrorMsg] = invoke(FP,'SpecVolume','Ph',P, h);
ErrorCheck(ErrorMsg);

rho = 1 / v; %kg/m3

EqnP = P-(P_atm+(rho*g*H_0)/1e5); %bar

%it si better to use a relative error function
output = EqnP  /  P;
