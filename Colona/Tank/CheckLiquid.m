function output=CheckLiquid(P,h)
global FP
%The Tank model is valid only for liquid (it is open)
%a check can be made on the vapor fraction: Vfrac must be equal to zero

%call VaporQual function
[Vfrac,ErrorMsg] = invoke(FP,'VaporQual','Ph',P, h);
ErrorCheck(ErrorMsg);

  if Vfrac>0 
    error('Vfrac>0: The Tank model works only with liquids.');
  else
    output=Vfrac;
  end