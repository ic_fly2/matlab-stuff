function [output] = CalcdP_dt(f_E, f_L, A)
global g

%A = eval(get_param(gcb,'A'));

dP_dt = g / A * ( f_E - f_L) / 1e5; %[bar/sec]

output = dP_dt;