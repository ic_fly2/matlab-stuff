%%%%%%%%%%%%%%%%%%%%%%%%%%
%     ErrorCheck     %
%%%%%%%%%%%%%%%%%%%%%%%%%%
%Temporary work around for bug in IF97:
%returned ErrorMsg is a long string which starts with "No Error"
function ErrorCheck(ErrorMsg)
if strcmp(strtok(ErrorMsg),'No') == 0;
    error(ErrorMsg)
end