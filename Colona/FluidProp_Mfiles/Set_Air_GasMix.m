%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Set_Air_GasMix     %
%%%%%%%%%%%%%%%%%%%%%%%%%%
global FP1

Model = 'GasMix';
nCmp  = 5;
Cmp   = 'AR,CO2,H2O,N2,O2';
Cnc   = [0.0092,0; 0.0003,0; 0.0101,0; 0.7729,0; 0.2075,0];

ErrorMsg = invoke(FP1,'SetFluid_M',Model,nCmp,Cmp,Cnc)
