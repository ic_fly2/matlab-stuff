%%%%%%%%%%%%%%%%%%%%%%%%%%
%     Set_Water_IF97     %
%%%%%%%%%%%%%%%%%%%%%%%%%%
global FP

Model = 'IF97';
nCmp  = 1;         % of no importance in case of pure fluid
Cnc   = [1,0];     % of no importance in case of pure fluid
Cmp   = 'Water';

ErrorMsg = invoke(FP,'SetFluid_M',Model,nCmp,Cmp,Cnc)