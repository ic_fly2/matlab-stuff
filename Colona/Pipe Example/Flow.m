function f2R = Flow(p2C, p1R)
Cv  = 0.00404137; %
R = 286; %J/kg K
T = 293; %K

Delta_p = p1R - p2C;
rho = p2C / (R * T);
f2R = Cv * sqrt(rho * Delta_p);


