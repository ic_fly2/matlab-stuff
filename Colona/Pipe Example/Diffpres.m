function dp2Cdt = DiffPres(f2R, f2C)
R = 286 ; % J/kg K
T = 293 ; % K
V = 8.83125; % m3

dp2Cdt = R * T / V * (f2R - f2C);
