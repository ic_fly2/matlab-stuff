function [output] = CalcValve(Kv,P_E,P_L, h_E)
%Kv [m�] valve friction coefficient
%Thermodynamic properties are in units used by FluidProp

global FP
%check for flow direction
if P_E<P_L;
    ErrorMsg = 'Entering Pressure lower the Leaving Pressure: wrong flow direction'
    error(ErrorMsg)
end

h_L = h_E;

%call SpecVolume function
[v_E,ErrorMsg] = invoke(FP,'SpecVolume','Ph',P_E, h_E);
ErrorCheck(ErrorMsg);
rho_E = 1 / v_E;

[v_L,ErrorMsg] = invoke(FP,'SpecVolume','Ph', P_L, h_L);
ErrorCheck(ErrorMsg);
rho_L = 1 / v_L;
    
rho = (rho_E+rho_L) / 2;
%Kv is in m�, so the pressure must be converted in Pa to obtain kg/sec
f_L = Kv * sqrt(rho * (P_E - P_L) * 1e5);

f_E = f_L;

%%%%%%%%%%
%OUTPUT VARIABLES%
%%%%%%%%%%
output(1) = f_E;
output(2) = f_L;
%Leaving Thermodynamic Properties
output(3) = h_L;



