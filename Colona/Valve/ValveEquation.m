function [output] = ValveEquation(Kv,A,rho_av,PL,PE)
%Kv [m�] valve friction coefficient
%Thermodynamic properties are in units used by ThermoProp

global FP
%check for flow direction
% if PE<PL;
%     ErrorMsg = 'Entering Pressure lower the Leaving Pressure: wrong flow direction'
%     error(ErrorMsg)
% end

f_E = sqrt(2*rho_av*A^2*(PE-PL)*1e5/Kv);

%%%%%%%%%%
%OUTPUT VARIABLES%
%%%%%%%%%%
output(1) = f_E;





