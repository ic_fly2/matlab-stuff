function [data, pipes ] = prepGGA(pipes, data)
% 
% Preps the pipe data for the GGA of nullspace method EPS
%

%% pipe resistance
%pipe data
%DW
if pipes{1}.Roughness < 20 % 20mm is very large for DW or too low for HW
    % we have a case of DW just assume fully turbulent flow and find
    % matching f or K
    
    if pipes{1}.Roughness > 0.01
        disp('Roughness in mm in file')
        for i = 1:data.npi
            pipes{i}.Roughness = pipes{i}.Roughness/1000;
        end
    else
        disp('Roughness in m in file')
    end
    
    K = zeros(data.npi,1);
    for i = 1:data.npi
        %     pipedata(i).data = [pipes{i}.Diameter/1000 130 pipes{i}.Length,2,5]; %D,e,L,vmax,pmax, in m / m /m
        K(i) = 1.1364 - 2*log10(pipes{i}.Roughness/(pipes{i}.Diameter/1000));
        pipes{i}.Approximation = 'DW';
        pipes{i}.K = K(i).^(-2);
        pipes{i}.Nexp = 2;
    end
    %     K = K.^(-2);
    %     Nexp = 2 * ones(data.npi,1);
    
else
    % HW
    K = zeros(data.npi,1);
    for i = 1:data.npi
        %     pipedata(i).data = [pipes{i}.Diameter/1000 130 pipes{i}.Length,2,5]; %D,e,L,vmax,pmax, in m / m /m
        K(i) = 10.675 * pipes{i}.Roughness.^-1.852 .* (pipes{i}.Diameter/1000).^-4.8704 .* (pipes{i}.Length);
        pipes{i}.Approximation = 'HW';
        pipes{i}.K = K(i);
        pipes{i}.Nexp = 1.852;
        
    end
    % Nexp = 1.852 * ones(data.npi,1);
    % [q_lim,M_pipes] = q_lim_maker(pipedata);
end
K = [];
% Write to data
% data.K    = K;
% data.Nexp = Nexp;

%% check valves
data.CV = [];
for i = 1:length(pipes)
    if strcmp(pipes{i}.Status,'CV')
        data.CV = [data.CV i]  ;
    end
end




