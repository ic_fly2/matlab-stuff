function [valves,mapId2List]=makeValves(valveData,mapId2List)


%% This function runs through a VALVES blcok data returned by WN_INP_reader() 
% and returns a vector of valve objects/data structures. The valves are
% approximated by pipes as in ....


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% See WN_INP_reader(),  makePipes(), makeJunctions(), makeReservoirs(), 

%%
% tic
valves=[];
% pipes=containers.Map();%%%Having pipes as a map of data objects costs a
% bit more in computation time, than having a cell array of them...

len=length(valveData);
%read first line of pipesdata
strLine = textscan(valveData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Node1           	Node2           	Length      	Diameter    	Roughness"   	MinorLoss   	Status
valvestr={';ID',    'Node1',    'Node2',     'Diameter',    'Type',   'Setting', 'MinorLoss'};
assert(isempty(setdiff(valvestr,strLine))|isempty(setdiff(valvestr',strLine)), 'Please Check columns names and data for [VALVES] are correct!' );

kk=1;

if len >= 2
    
    for ii=2:len
        strLine = textscan(valveData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)==8 && strLine{8}==';')%check nothing is ammis in the data
            %             C = textscan(valveData{ii},'%s %s %s %f %f %f %f %s');
            valve.Id = strLine{1};
            valve.startNodeId=strLine{2};
            valve.endNodeId=strLine{3};
            %change to relevant data type while copying, if there is an
            valve.Diameter=str2double(strLine{4});
            valve.Type = strLine{5};
            valve.Setting=str2double(strLine{6});
            valve.MinorLoss=str2double(strLine{7});
             
            %%%Add to the valve list only if the valve is connected. 
            if (~isempty(setdiff('Closed',strLine{8})))%if status is not 'Closed'
                valves{kk}=valve;% pipes is cell array of many a valve
                %             pipes(valve.Id)=valve;
                kk=kk+1;
            else
                fprintf('Valve is Closed and so not added!')
            end            
        end
% %         error -- report
        
    end
    
else
    error('','No pipes in the network? :-) ');
end

end