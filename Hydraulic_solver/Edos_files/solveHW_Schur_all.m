function [h,q,err1,kk,CONDS,ERRORS] = solveHW_Schur_all(A12,A21,A10,...
    h0,n_exp,qk,hk,d,np,nn,tol,kappa,data)

%   Edo Abraham
%   Copyright 2014, Imperial College London
%  $Date: 25/07/2014 $



CONDS=1;ERRORS=1;


max_iter = data.max_iter;%
% epsilon=data.epsilon;%1e-9;

% pD=data.Diam;%Pipe Diameters
% pR=data.Rough;%Pipe Rougness
% pL=data.Len;
% fDW=data.fDW;
ResCoeff=data.ResCoeff;

G=ResCoeff.*abs(qk).^(n_exp-1);% G in Elhay is A11 in Todini
err1 = norm( [G.*qk+A12*hk+A10*h0; A21*qk-d],inf);%infinity norm error, 1-norm, 2-norm can also be used

%%%
% nc=size(Z,2);%size of nullspace nc=np-nn
%
% %The most number of nonzeros in A12'*F^k*A12
% We reduce unnecessary  work in formulating X again again as it is the
% most costly operation in multiplying (spdiags())

nnz_A21A12 = nnz(A21*speye(np)*A12);

X=spalloc(nn,nn,nnz_A21A12);


for kk = 1:max_iter
    Fdiag = n_exp.*G;%F in Elhay et al
    %%%`Regularize'
    sigma_max = max(Fdiag); %maximum eigenvalue of F
    t_k = max((sigma_max/kappa)- Fdiag,0);
    Fdiag = Fdiag + t_k; %%% Fdiag = max((sigma_max/kappa)*ones(np,1),Fdiag);
    
    Fdiag_inv=1./Fdiag;%%implementation 2
    X=A21*sparse([1:np]',[1:np]',Fdiag_inv)*A12;%%implementation 2
    b = A21*(Fdiag_inv.* ((Fdiag-G).*qk - A10*h0)) -d ;%%implementation 2
    %Solve for q^{k+1} and h^{k+1}
    h = X\b; %solve for h^{k+1} using another method too
    q = Fdiag_inv.*( (Fdiag-G).*qk  - A10*h0 -A12*h);%%Implementation 2
    
    
    %%   %%update qk and h^k if convergence is not accomplished
    %%%% This line is the most expensive and so do Partial updates
    
    %%% Recompute friction coefficient for all pipes
    
    G = (ResCoeff.*(abs(q).^(n_exp-1)));%Note that G:=A11(q) in Todini
   
    %     err0=err1;
    err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    %     T_con(kk) =(1/tol)* (norm(t_k.*abs(q-q_old),inf));
    
    
    %1/tol may be too conservative at the beginning of the iterations where
    %$|b|is much higher, use 1/||b|| instead
    
    %     phi=norm(q-qk,inf);%./norm(qk,inf)
    %     rho=norm(h-hk,inf);%./norm(qk,inf)
    %
    %     rho_e(kk) = norm( [G.*q+A12*h+A10*h0],inf);%infinity norm error, 1-norm, 2-norm can also be used
    %     rho_c (kk)= norm( [A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    
    
    %     ERRORS_(kk)=err1;
    ERRORS=err1;
    %%% If min-res is used, the 2-norm will be an appropriate measure of error,
    %%% perhaps
    %     fprintf('Iteration %1.0f: Nonlinear equation infinity-norm error is %2.10f \n',kk,err1)
    %     fprintf('norm of t_k is %1.0f \n',norm(t_k,inf))
    if err1 < tol
        
        break;% exit if tolerance is met
    else
        qk =q;
        %         hk=h;
    end
    
end
% kk,n_upd,Gtime
% kk
% set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(UPDATES,'x')
% xlabel('Iteration'),ylabel('Number of updates')
% makefigGood
% % set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(log10(abs(ERRORS_)),'x')
% xlabel('Iteration'),ylabel('ERROR')

%
% figure, plot(log10(Es),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(beta),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(T_con),'x')
% xlabel('Iteration'),ylabel('$\|Ts\|/\|b\|$')