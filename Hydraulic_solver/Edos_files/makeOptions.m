function [options]=makeOptions(optionsData)


%% This function runs through a block of  options data returned by WN_INP_reader()
% and returns a vector of options objects/data structures.


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 21/12/2014 $

% See WN_INP_reader(),  makeJunctions(), makeReservoirs()

%%
% tic
options=[];

options=[];
options.Units = [];
options.Headloss=[];
options.Accuracy=[];
% % %  Units              	LPS, GPM
% % %  Headloss           	D-W, H-W
% % %  SpecificGravity   	1
% % %  Viscosity          	1
% % %  Trials             	40
% % %  Accuracy           	0.001
% % %  Unbalanced         	Continue 10
% % %  Pattern            	1
% % %  Demand Multiplier  	1.0
% % %  Emitter Exponent   	0.5
% % %  Quality            	NONE mg/L
% % %  Diffusivity        	1
% % %  Tolerance

len=length(optionsData);
%read first line of pumpsdata
strLine = textscan(optionsData{1},'%s'); strLine=strLine{:};

assert(len>=1,'Please Check columns names and data for [PUMPS] are correct!' );


%if len >= 1

for ii=1:len
    strLine = textscan(optionsData{ii},'%s'); strLine=strLine{:};
    temp_len=length(strLine);
    
    if (strcmp(strLine{1},';'))%check nothing is ammis in the data or that there is a curve data
        %do nothing
    elseif (length(strLine)>=2 )%check nothing is ammis in the data
        %             C = textscan(optionsData{ii},'%s %s %s %f %f %f %f %s');
        if (strcmp(strLine{1},'Units'))
            options.Units=(strLine{2});
        elseif (strcmp(strLine{1},'Headloss'))
            options.Headloss=strLine{2};
        elseif (strcmp(strLine{1},'Accuracy'))
            options.Accuracy=str2double(strLine{2});
        end
        
    end
    % %         error -- report
    
end

% else
%     error('','No [OPTIONS] in the network? :-) ');
% end

% toc



end