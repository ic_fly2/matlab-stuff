function [reservoirs,mapResId2List]=makeReservois(resrData,mapResId2List)
%% This function runs through a reservoir block data returned by WN_INP_reader() 
% and returns a vector of junction objects/data structures.


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% See WN_INP_reader(),  makePipes(), makeJunctions()

%%

% tic 
reservoirs=[];
% reservoirs=containers.Map();%%%Having reservoirs as a map of data objects costs a
% bit more in computation time, than having a cell array of them...

len=length(resrData);

%read first line of pipesdata
strLine = textscan(resrData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Elev           	Demand           	Pattern
resrvstr={';ID',    'Head',     'Pattern'};
assert(isempty(setdiff(resrvstr,strLine))|isempty(setdiff(resrvstr',strLine)), ...
    'Please Check columns names and data for [RESERVOIRS] are correct!' );


%%% Add to the Junctions List
juncNodenumber=mapResId2List.Count;
kk=1;
if len >= 2
    
    for ii=2:len
        strLine = textscan(resrData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)>=3 && strLine{end}==';')%check if something is ammis in the minimum data needed
            %             C = textscan(resrData{ii},'%s %s %s %f %f %f %f %s');
            reservoir.Id = strLine{1};
            reservoir.Elev=str2double(strLine{2}); %ruben
            reservoir.Head=str2double(strLine{2});
            if ~isnan(str2double(strLine{3}))
                reservoir.Pattern_name =str2double(strLine{3});
            end
            %change to relevant data type while copying, if there is an            
            %reservoir.Pattern=str2double(strLine{4});

%             reservoir,ii,kk
            reservoirs{kk}=reservoir;% reservoirs is cell array of many a reservoir
            
            %%%May be add the nodes to the reservoir map here...since only
            %%%the connected nodes will appear in the hydraulic
            %%%equations. However, this will cost in redundant access to
            %%%JunctionMap elements (upto k*Np times for average k connections, Np reservoirs).
            
            mapResId2List(reservoir.Id)=juncNodenumber+kk;
            
            kk=kk+1;

            
            
        end
        
        %error -- report
        
    end
    
else
    error('','No reservoirs in the network? :-) ');
end

% toc


end