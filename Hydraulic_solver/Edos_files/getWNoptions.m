function options=getWNoptions(optionsData)

% INPUT: The block of data under [OPTIONS] in the INP file 

% OUTPUT:  options -- a map with keys {'Units',    'Headloss', 'Specific Gravity', 'Viscosity', 'Trials',...
%     'Accuracy','CHECKFREQ','MAXCHECK','DAMPLIMIT','Unbalanced','Pattern',...
%     'Demand Multiplier','Emitter
%     Exponent','Quality','Diffusivity','Tolerance'} and corresponding
%     values
% 

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% % See WN_INP_reader, makePipes, makeJunctions, makeReservoirs


optstr={'Units',    'Headloss', 'Specific Gravity', 'Viscosity', 'Trials',...
    'Accuracy','CHECKFREQ','MAXCHECK','DAMPLIMIT','Unbalanced','Pattern',...
    'Demand Multiplier','Emitter Exponent','Quality','Diffusivity','Tolerance'};

optstrfloat = {'Specific Gravity', 'Viscosity', 'Trials',...
    'Accuracy','CHECKFREQ','MAXCHECK','DAMPLIMIT','Pattern',...
    'Demand Multiplier','Emitter Exponent','Diffusivity','Tolerance'};

options=containers.Map();

len=length(optionsData);

if len >= 1
    
    for ii=1:len
        strLine = textscan(optionsData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)>=2)%check nothing is ammis in the data
            %             C = textscan(optionsData{ii},'%s %s %s %f %f %f %f %s');
            
            if (ismember(strLine{2},optstr)&ismember(strLine{1},optstrfloat))
                options(strLine{1})=str2double(strLine{2});
            else
                 options(strLine{1})=strLine{2};
            end
          
        end
% %         error -- report
        
    end
    
else
    error('','No content found in the [OPTIONS] data of the  network! ');
end

end


