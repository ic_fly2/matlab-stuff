function [demands,multipliers,baseDemands] =makeDemands(junctions,patternsMap,timeofDay)
%%% [demands,multipliers,baseDemands] =makeDemands(junctions,patternsMap,timeofDay)

multipliers = ones(length(junctions),1);
demands = ones(length(junctions),1);
baseDemands=ones(length(junctions),1);
for jj=1:length(junctions)
    pattern = junctions{jj}.Pattern;
    baseDemands(jj)=junctions{jj}.Demand;
    if strcmp(pattern,'')
        multipliers(jj)=1;
        demands(jj)=baseDemands(jj);
    else
        patterExists = isKey(patternsMap,pattern);% looks for the specified keys in mapObj, and returns logical true (1)
        if patterExists
            patt_vec = patternsMap(pattern);
            if timeofDay<= length(patt_vec)
                multipliers(jj)=patt_vec(timeofDay);
                demands(jj)=(baseDemands(jj))*multipliers(jj);
            else
                multipliers(jj)=1;
                demands(jj)=baseDemands(jj);
            end
        else
            multipliers(jj)=1;
            demands(jj)=baseDemands(jj);
        end
        
    end
    
    
end

