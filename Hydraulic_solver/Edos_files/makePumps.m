function [pumps,mapId2List]=makePumps(pumpData,mapId2List)


%% This function runs through a block of  pump data returned by WN_INP_reader()
% and returns a vector of pump objects/data structures.

% Each pump  has the data below, if supplied. Else the relevant field is
% left equal to empty (i.e. =[]);
% Id
%  startNodeId
%  endNodeId
%  power ? power value for constant energy pump, hp (kW)
%  head - ID of curve that describes head versus flow for the pump (using this ID, one can retrieve the curves (see
%  makeCurves()
%  speed - relative speed setting (normal speed is 1.0, 0 means pump is off)
%  pattern - ID of time pattern that describes how speed setting varies
%  with time -- using this ID, one can retrieve the patterns (see
%  makePatterns()

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 21/12/2014 $

% See WN_INP_reader(),  makeJunctions(), makeReservoirs()

%%
% tic
pumps=[];

pumpTemplate=[];
pumpTemplate.Id = [];
pumpTemplate.startNodeId=[];
pumpTemplate.endNodeId=[];
pumpTemplate.power=[];
pumpTemplate.headCurveId=[];
pumpTemplate.speed=[];
pumpTemplate.patternId=[];


len=length(pumpData);
%read first line of pumpsdata
strLine = textscan(pumpData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Node1           	Node2           	Length      	Diameter    	Roughness"   	MinorLoss   	Status
pumpstr={';ID',    'Node1',    'Node2',    'Parameters'};
assert(isempty(setdiff(pumpstr,strLine))|isempty(setdiff(pumpstr',strLine)),...
    'Please Check columns names and data for [PUMPS] are correct!' );

kk=1;

if len >= 2
    
    for ii=2:len
        strLine = textscan(pumpData{ii},'%s'); strLine=strLine{:};
        temp_len=length(strLine);
        pump=pumpTemplate;
        
        if (strcmp(strLine{1},';'))%check nothing is ammis in the data or that there is a curve data
            %do nothing
        elseif (length(strLine)>=6 && strLine{temp_len}==';')%check nothing is ammis in the data
            %             C = textscan(pumpData{ii},'%s %s %s %f %f %f %f %s');
            pump.Id = strLine{1};
            pump.startNodeId=strLine{2};
            pump.endNodeId=strLine{3};
            %change to relevant data type while copying, if there is an
            
            for jj=4:2:temp_len-2
                if (strcmp(strLine{jj},'POWER'))
                    pump.power=str2double(strLine{jj+1});
                elseif (strcmp(strLine{jj},'HEAD'))
                    pump.headCurveId=strLine{jj+1};
                elseif (strcmp(strLine{jj},'SPEED'))
                    pump.speed=str2double(strLine{jj+1});
                elseif (strcmp(strLine{jj},'PATTERN'))
                    pump.patternId=strLine{jj+1};
                end
            end
            
            %%%Add to the pump list only if the pump is connected.
            pumps{kk}=pump;% pumps is cell array of many a pump
            %             pumps(pump.Id)=pump;
            kk=kk+1;
        end
        % %         error -- report
        
    end
    
else
    error('','No pumps in the network? :-) ');
end

% toc



% read line and use data
%check important parameters are there
%if pump is closed, DO NOT ADD to list



end