function curveMap=makeCurves(curveData,curveMap)

%% This function runs through a [CURVES] labelled block of data returned by WN_INP_reader()
% and returns a map of curve objects/ whose data element is an [x y] curve data of 2xn matrix

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 21/12/2014 $

% See WN_INP_reader(),  makePatterns(),...

%%


len=length(curveData);
%read first line of data
strLine = textscan(curveData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	X-Value           	Y-value
juncstr={';ID',    'X-Value', 'Y-Value'};
assert(isempty(setdiff(juncstr,strLine))|isempty(setdiff(juncstr',strLine)),...
    'Please Check columns names and data for [CURVES] are correct!' );

if len >= 2
    
    for ii=2:len
        strLine = textscan(curveData{ii},'%s'); strLine=strLine{:};
        temp_len=length(strLine);
        %if it starts with ';' ignore as it is a comment
        if (temp_len>=1 && strcmp(strLine{1}(1),';'))%check nothing is ammis in the data or that there is a curve data
            %do nothing
            
            %if it starts with a str curve Id, get the rest of the data and
            %append/push back to the vector member of the curve in curveMap using the curveId as key
        elseif (temp_len>=2 && ~strcmp(strLine{1}(1),';'))%check nothing is ammis in the data or that there is a curve data
            %             C = textscan(curveData{ii},'%s %s %s %f %f %f %f %s');
            curveId = strLine{1};
            curve_x= str2double(strLine{2});
            curve_y= str2double(strLine{3});
            %ruben:
            
            
            patterExists = isKey(curveMap,curveId);% looks for the specified keys in mapObj, and returns logical true (1)
            if patterExists
                curveMap(curveId)=[curveMap(curveId);[curve_x curve_y] ];%Append new data if it exists
            else
                curveMap(curveId)=[curve_x curve_y];
            end
        end
        
        %error -- report
        
    end
    
else
    error('','\n No Curves in the network data? :( \n');
end



end