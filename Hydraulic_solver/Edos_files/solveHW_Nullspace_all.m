function [h,q,err1,kk,CONDS,ERRORS] = solveHW_Nullspace_all(A12,A21,A10,...
    h0,n_exp,qk,hk,d,np,~,tol,kappa,nulldata,data)
%

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

x=nulldata.x;
Z=nulldata.Z;
P_r=nulldata.Pr;
% Pc = Pr';%lchol factorizes S'AS, Pr=Pc';
L_A12=nulldata.L_A12;

CONDS=1;ERRORS=1;


max_iter = data.max_iter;%50
% epsilon=data.epsilon;%1e-9;

% pD=data.Diam;%Pipe Diameters
% pR=data.Rough;%Pipe Rougness
% pL=data.Len;
ResCoeff=data.ResCoeff;

G=ResCoeff.*abs(qk).^(n_exp-1);% G in Elhay is A11 in Todini
err1 = norm( [G.*qk+A12*hk+A10*h0; A21*qk-d],inf);%infinity norm error, 1-norm, 2-norm can also be used

%%%
nc=size(Z,2);%size of nullspace nc=np-nn

nnz_ZZ = nnz(Z'*speye(np)*Z);%The most number of nonzeros in Z'*F^k*Z
% We reduce unnecessary  work in formulating X again again as it is the
% most costly operation in multiplying (spdiags())
% q_old=qk;
ZT=Z';

Fdiag_old=spalloc(np,1,np);
X=spalloc(nc,nc,nnz_ZZ);
% UPDATES=[];%To plot the number of updates needed with Iteration number
% rannum=max(1,round(rand(1,1)*nn))
% lastNorm=0;
% Gtime=0;

% updates= find(sum(abs(Z),2)>0);%the column space of Z
updates= 1:np;%the column space of Z
n_upd=length(updates);


for kk = 1:max_iter
    Fdiag = n_exp.*G;%F in Elhay et al
    %%%`Regularize'
    sigma_max = max(Fdiag); %maximum eigenvalue of F
    t_k = max((sigma_max/kappa)- Fdiag,0);
    Fdiag = Fdiag + t_k; %%% Fdiag = max((sigma_max/kappa)*ones(np,1),Fdiag);
    
%     X=ZT(:,updates)*sparse([1:n_upd]',[1:n_upd]',Fdiag(updates))*(ZT(:,updates)');
%     X=ZT(:,updates)*sparse([1:n_upd]',[1:n_upd]',Fdiag(updates))*(ZT(:,updates)');
    X=ZT*sparse([1:n_upd]',[1:n_upd]',Fdiag)*(Z);

    Fdiag_old = Fdiag;
   
%     UPDATES(kk)=n_upd;
    
    
    
    
    %     %     b= Z'*(spdiags(Fdiag-G,0,np,np)*qk-A10*h0-Fdiag.*x);%
    b= Z'*((Fdiag-G).*qk-A10*h0-Fdiag.*x);%
    %
    v = X\b;%alternatively use spqr_solve..Cholesky faster for square X like this one
    %     %  [v,stats]=cholmod2(X,b);%return stats as well
    %
    %     [Lm,Dm,Pm] = ldl(X);
    %     v = Pm*(Lm'\(Dm\(Lm\(Pm'*b))));
    % %     v = Lm'\(DA\(LA\b));
    
    
    
    q = x+Z*v;%q^{k+1}
    %     v(2)
    %     lastNorm-norm(Z*v,2)
    %     lastNorm=norm(Z*v,2);
    %%%% Since A12'*A12 is positive definite, use factored version to
    %%%% easily solve for h
    
    %     b = A12'*(spdiags(Fdiag-G,0,np,np)*qk-A10*h0-Fdiag.*q);
    b = A12'*((Fdiag-G).*qk-A10*h0-Fdiag.*q);
    
    y=(L_A12)\(P_r*b);
    h=P_r'*(L_A12'\y);
    
    
    %%   %%update qk and h^k if convergence is not accomplished
    %%%% This line is the most expensive and so do Partial updates

    G(updates) = (ResCoeff(updates).*(abs(q(updates)).^(n_exp(updates)-1)));%Note that G:=A11(q) in Todini
    G(updates) = (ResCoeff(updates).*(abs(q(updates)).^(n_exp(updates)-1)));%Note that G:=A11(q) in Todini
    %         Es(kk) =max(ResCoeff(Noupdates).*abs(abs(q(Noupdates)).^(n_exp(Noupdates)-1)-...
    %             abs(q_old(Noupdates)).^(n_exp(Noupdates)-1)));
    %         beta(kk) =(1/tol)* max(ResCoeff(Noupdates).*abs(q(Noupdates)).*abs(abs(q(Noupdates)).^(n_exp(Noupdates)-1)-...
    %             abs(q_old(Noupdates)).^(n_exp(Noupdates)-1)));
    
    
    %     Gtime=Gtime+toc;
    %     n_upd, Gtime
    
    %     err0=err1;
    err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    
    
    
%     ERRORS_(kk)=err1;
    ERRORS=err1;
    %%% If min-res is used, the 2-norm will be an appropriate measure of error,
    %%% perhaps
    %     fprintf('Iteration %1.0f: Nonlinear equation infinity-norm error is %2.10f \n',kk,err1)
    %     fprintf('norm of t_k is %1.0f \n',norm(t_k,inf))
    if err1 < tol
        %%%%RETURN THE REAL ERROR JUST BEFORE BREAKING
        %%% Recompute friction coefficient for updated set
%         updates= find(sum(abs(Z),2)>0);%the column space of Z
%         G(updates) = (ResCoeff(updates).*(abs(q(updates)).^(n_exp(updates)-1)));%Note that G:=A11(q) in Todini
%         err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
        
        break;% exit if tolerance is met
    else
        qk =q;
        %         hk=h;
    end
    
end
% kk,n_upd,Gtime
% kk
% set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(UPDATES,'x')
% xlabel('Iteration'),ylabel('Number of updates')
% makefigGood
% % set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(log10(abs(ERRORS_)),'x')
% xlabel('Iteration'),ylabel('ERROR')

%
% figure, plot(log10(Es),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(beta),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(T_con),'x')
% xlabel('Iteration'),ylabel('$\|Ts\|/\|b\|$')