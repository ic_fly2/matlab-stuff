function [energyMap, globalData, demandCharge]=makeEnegy(energyData,energyMap)

%% This function runs through a [ENERGY] labelled block of data returned by WN_INP_reader()
% and returns a map of  Pumps whose data element is a data structure with
% efficiency curve and price tariff pattern ID's and the Price value
% (average cost per kW-hour

% globalData returns the global PRICE/PATTERN/EFFIC if they exist, else it
% returns [] in those places

% demandCharge -- returns [] if the demand Charge data is not available


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 21/12/2014 $

% See WN_INP_reader(),  makePatterns(),...

%%
globalData=[];globalData.price=[]; globalData.pattern=[]; globalData.efficiency=[];
demandCharge=[];


len=length(energyData);
%read first line of data
strLine = textscan(energyData{1},'%s'); strLine=strLine{:};
% assert data exists
assert((len >= 2),...
    'Please Check columns names and data for [ENERGY] are correct!' );

if (len >= 2)
    
    for ii=1:len
        strLine = textscan(energyData{ii},'%s'); strLine=strLine{:};
        temp_len=length(strLine);
        %if it starts with ';' ignore as it is a comment
        if (strcmp(strLine{1}(1),';'))%check nothing is ammis in the data or that there is a curve data
            %do nothing
            
            %if it starts with a str Pump Id, or other data get the rest of the data and
            %append/push back to data in energyMap using the PumpId as key
        elseif (temp_len>=2 && strcmp(strLine{1},'Pump'))%
            %             C = textscan(energyData{ii},'%s %s %s %f %f %f %f %s');
            pumpkey=strLine{2};pumpdata=[];
            if (temp_len>=4 && strcmp(strLine{3},'Efficiency'))
                pumpdata.efficiencyID=strLine{4};
            elseif (temp_len>=4 && strcmp(strLine{3},'Pattern'))
                pumpdata.patternID=strLine{4};
            elseif (temp_len>=4 && strcmp(strLine{3},'Price'))
                pumpdata.price=str2double(strLine{4});
            end
            
            patterExists = isKey(energyMap,pumpkey);% looks for the specified keys in mapObj, and returns logical true (1)
            if patterExists
                energyMap(pumpkey)=pumpdata;%Append new data if it exists
            else
                energyMap(pumpkey)=pumpdata;
            end
        elseif (temp_len>=2 && strcmp(strLine{1},'Global'))%
            if (temp_len>=3 && strcmp(strLine{2},'Efficiency'))
                globalData.efficiency=str2double(strLine{3});
            elseif (temp_len>=3 && strcmp(strLine{2},'Price'))
                globalData.price=str2double(strLine{3});
            elseif (temp_len>=3 && strcmp(strLine{2},'Pattern'))
                globalData.pattern=str2double(strLine{3});
            end
        elseif (temp_len>=3 && strcmp(strLine{1},'Demand') && strcmp(strLine{2},'Charge'))%
            demandCharge=str2double(strLine{3});
            
            %error -- report
            
            
        else
            error('','\n No Curves in the network data? :( \n');
        end
    end
end