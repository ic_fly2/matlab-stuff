function [junctions,mapJuncId2List]=makeJunctions(juncData,mapJuncId2List,settings)
%% This function runs through a junction blcok data returned by WN_INP_reader()
% and returns a vector of junction objects/data structures.
%
%
%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.2 $  $Date: 25/07/2014 $
%   Modded Ruben Menke 10.2.2016
%
% See WN_INP_reader(),  makePipes(), makeReservoirs()

%%
if nargin ==2
    settings = [];
end


% tic
junctions=[];
% junctions=containers.Map();%%%Having junctions as a map of data objects costs a
% bit more in computation time, than having a cell array of them...

len=length(juncData);
%read first line of pipesdata
strLine = textscan(juncData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Elev           	Demand           	Pattern
juncstr={';ID',    'Elev',    'Demand',    'Pattern'};
assert(isempty(setdiff(juncstr,strLine))|isempty(setdiff(juncstr',strLine)),...
    'Please Check columns names and data for [JUNCTIONS] are correct!' );

kk=1;
if len >= 2
    for ii=2:len
        strLine = textscan(juncData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)==5 && strcmp(strLine{5},';') )%check nothing is ammis in the data or that there is a pattern data
            %             C = textscan(juncData{ii},'%s %s %s %f %f %f %f %s');
            junction.Id = strLine{1};
            junction.Elev=str2double(strLine{2});
            junction.Demand=str2double(strLine{3});
            if isfield(settings,'demand_factor')
                junction.Demand = junction.Demand*settings.demand_factor;
            elseif isfield(settings.DR, 'demand_factor')
                junction.Demand = junction.Demand*settings.DR.demand_factor;
            end
            junction.Pattern=strLine{4};
            %change to relevant data type while copying, if there is an            junction.Pattern=str2double(strLine{4});
            
            %             junction,ii,kk
            junctions{kk}=junction;% junctions is cell array of many a junction
            
            %%%May be add the nodes to the junction map here...since only
            %%%the connected nodes will appear in the hydraulic
            %%%equations. However, this will cost in redundant access to
            %%%JunctionMap elements (upto k*Np times for average k connections, Np junctions).
            mapJuncId2List(junction.Id)=kk;
            kk=kk+1;
        elseif  (length(strLine)==4 && strcmp(strLine{4},';'))%pattern is left empty
            junction.Id = strLine{1};
            junction.Elev=str2double(strLine{2});
            junction.Demand=str2double(strLine{3});
            junction.Pattern='';%Make pattern member empty
            %change to relevant data type while copying, if there is an            junction.Pattern=str2double(strLine{4});
            
            %             junction,ii,kk
            junctions{kk}=junction;% junctions is cell array of many a junction
            mapJuncId2List(junction.Id)=kk;
            kk=kk+1;
        end
    end
    
else
    error('','No junctions in the network? :-) ');
end
end