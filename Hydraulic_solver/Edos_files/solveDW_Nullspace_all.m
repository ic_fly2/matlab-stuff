function [h,q,err1,kk,CONDS,ERRORS] = solveDW_Nullspace_all(A12,A21,A10,...
    h0,n_exp,qk,hk,d,np,~,tol,kappa,nulldata,data)
%

%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

x=nulldata.x;
Z=nulldata.Z;
P_r=nulldata.Pr;
% Pc = Pr';%lchol factorizes S'AS, Pr=Pc';
L_A12=nulldata.L_A12;

CONDS=1;ERRORS=1;
rho = 998.2; % density of water (kg/m^3) at 20 degrees C
mu = 1.002e-3; % dynamic viscosity (kg/m/s) at 20 degrees C
Viscos = mu/rho; % kinematic viscosity (m^2/s)    %visscosity of water at 20 deg C in m^2/s


max_iter = data.max_iter;%

pD=data.Diam;%Pipe Diameters
pR=data.Rough;%Pipe Rougness
pL=data.Len;
ResCoeff=zeros(np,1);
G=zeros(np,1);
Fdiag=ones(np,1);
[fDw,laminarInd,nonLaminarInd] = DWcoeff_mex(qk, pD,pR);%
% ResCoeff = (fDw./12.1).*pL.*(pD.^-5);
% G=ResCoeff.*abs(qk).^(n_exp-1);
ResCoeff(laminarInd)=(128*Viscos/(pi*9.81)).*pL(laminarInd).*(pD(laminarInd).^-4);
ResCoeff(nonLaminarInd) = (fDw(nonLaminarInd)./12.1).*pL(nonLaminarInd).*(pD(nonLaminarInd).^-5);
G(laminarInd)=ResCoeff(laminarInd);
G(nonLaminarInd)=ResCoeff(nonLaminarInd).*abs(qk(nonLaminarInd)).^(n_exp(nonLaminarInd)-1);
err1 = norm( [G.*qk+A12*hk+A10*h0; A21*qk-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
%%%

nc=size(Z,2);%size of nullspace nc=np-nn
% We reduce unnecessary  work in formulating X again again as it is the
% most costly operation in multiplying (spdiags())
ZT=Z';
nnz_ZZ = nnz(ZT*speye(np)*Z);%The most number of nonzeros in Z'*F^k*Z
X=spalloc(nc,nc,nnz_ZZ);
% updates= find(sum(abs(Z),2)>0);%the column space of Z
updates=[1:np]';
n_upd=length(updates);

for kk = 1:max_iter
    if kk==1
        Fdiag(updates(laminarInd)) = G(updates(laminarInd));
        Fdiag(updates(nonLaminarInd)) = n_exp(updates(nonLaminarInd)).*G(updates(nonLaminarInd));
%         Fdiag = n_exp.*G;    %%%`Regularize'
    else
        Fdiag = Fdiag-t_k;
        %         Fdiag(updates) = n_exp(updates).*G(updates);
        Fdiag(updates(laminarInd)) = G(updates(laminarInd));
        Fdiag(updates(nonLaminarInd)) = n_exp(updates(nonLaminarInd)).*G(updates(nonLaminarInd));
    end%%%`Regularize'
    sigma_max = max(Fdiag); %maximum eigenvalue of F
    t_k = max((sigma_max/kappa)- Fdiag,0);
    Fdiag = Fdiag + t_k; %%% Fdiag = max((sigma_max/kappa)*ones(np,1),Fdiag);
    
    %         Noupdates = find(sum(abs(Z),2)==0);
%     X=ZT(:,updates)*sparse([1:n_upd]',[1:n_upd]',Fdiag(updates))*(ZT(:,updates)');
    X=ZT*sparse([1:n_upd]',[1:n_upd]',Fdiag)*(Z);
    %     %     b= Z'*(spdiags(Fdiag-G,0,np,np)*qk-A10*h0-Fdiag.*x);%
    b= Z'*((Fdiag-G).*qk-A10*h0-Fdiag.*x);%
    v = X\b;%alternatively use spqr_solve..Cholesky faster for square X like this one
    %     %  [v,stats]=cholmod2(X,b);%return stats as well
    %
    %     [Lm,Dm,Pm] = ldl(X);
    %     v = Pm*(Lm'\(Dm\(Lm\(Pm'*b))));
    % %     v = Lm'\(DA\(LA\b));
    
    q = x+Z*v;%q^{k+1}
    %     v(2)
    %     lastNorm-norm(Z*v,2)
    %     lastNorm=norm(Z*v,2);
    %%%% Since A12'*A12 is positive definite, use factored version to
    %%%% easily solve for h
    b = A12'*((Fdiag-G).*qk-A10*h0-Fdiag.*q);
    %         b = A12'*((Fdiag-G-t_k).*qk-A10*h0-(Fdiag-t_k).*q);
    y=(L_A12)\(P_r*b);
    h=P_r'*(L_A12'\y);
    
    
    %%   %%update qk and h^k if convergence is not accomplished
    %%%% This line is the most expensive and so do Partial updates
    %%% Recompute friction coefficient for updated set
    %     [fDw,laminarInd,nonLaminarInd] = DWcoeff(q, pD,pR,1:np);%
    %     ResCoeff = (fDw./12.1).*pL.*(pD.^-5);
    %     G=ResCoeff.*abs(q).^(n_exp-1);
%     [fDw,laminarInd,nonLaminarInd] = DWcoeff_mex(q(updates), pD(updates),pR(updates));%
%     ResCoeff(updates(laminarInd))=(128*Viscos/(pi*9.81)).*pL(updates(laminarInd)).*(pD(updates(laminarInd)).^-4);
%     ResCoeff(updates(nonLaminarInd)) = (fDw((nonLaminarInd))./12.1).*pL(updates(nonLaminarInd)).*(pD(updates(nonLaminarInd)).^-5);
%     G(updates(laminarInd))=ResCoeff(updates(laminarInd));
%     G(updates(nonLaminarInd))=ResCoeff(updates(nonLaminarInd)).*abs(q(updates(nonLaminarInd))).^(n_exp(updates(nonLaminarInd))-1);
%     err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    %%
    [fDw,laminarInd,nonLaminarInd] = DWcoeff(q(updates), pD(updates),pR(updates));%
%     ResCoeff(updates(laminarInd))=(128*Viscos/(pi*9.81)).*pL(updates(laminarInd)).*(pD(updates(laminarInd)).^-4);
%     ResCoeff(updates(nonLaminarInd)) = (fDw((nonLaminarInd))./12.1).*pL(updates(nonLaminarInd)).*(pD(updates(nonLaminarInd)).^-5);
    G(updates(laminarInd))=(128*Viscos/(pi*9.81)).*pL(updates(laminarInd)).*(pD(updates(laminarInd)).^-4);
    G(updates(nonLaminarInd))=(fDw((nonLaminarInd))./12.1).*pL(updates(nonLaminarInd)).*(pD(updates(nonLaminarInd)).^-5).*abs(q(updates(nonLaminarInd))).^(n_exp(updates(nonLaminarInd))-1);
    err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    
    
    %%
    %     T_con(kk) =(1/tol)* (norm(t_k.*abs(q-q_old),inf));
    
    
    %1/tol may be too conservative at the beginning of the iterations where
    %$|b|is much higher, use 1/||b|| instead
    
    %     phi=norm(q-qk,inf);%./norm(qk,inf)
    %     rho=norm(h-hk,inf);%./norm(qk,inf)
    %
    %     rho_e(kk) = norm( [G.*q+A12*h+A10*h0],inf);%infinity norm error, 1-norm, 2-norm can also be used
    %     rho_c (kk)= norm( [A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
    
    
    
    %     ERRORS_(kk)=err1;
    ERRORS=err1;
    %%% If min-res is used, the 2-norm will be an appropriate measure of error,
    %%% perhaps
    %     fprintf('Iteration %1.0f: Nonlinear equation infinity-norm error is %2.10f \n',kk,err1)
    %     fprintf('norm of t_k is %1.0f \n',norm(t_k,inf))
    if err1 < tol
        %%%%RETURN THE REAL ERROR JUST BEFORE BREAKING
        %%% Recompute friction coefficient for updated set
%         updates= find(sum(abs(Z),2)>0);%the column space of Z
%         [fDw,laminarInd,nonLaminarInd] = DWcoeff_mex(q(updates), pD(updates),pR(updates));%
%         ResCoeff(updates(laminarInd))=(128*Viscos/(pi*9.81)).*pL(updates(laminarInd)).*(pD(updates(laminarInd)).^-4);
%         ResCoeff(updates(nonLaminarInd)) = (fDw((nonLaminarInd))./12.1).*pL(updates(nonLaminarInd)).*(pD(updates(nonLaminarInd)).^-5);
%         G(updates(laminarInd))=ResCoeff(updates(laminarInd));
%         G(updates(nonLaminarInd))=ResCoeff(updates(nonLaminarInd)).*abs(q(updates(nonLaminarInd))).^(n_exp(updates(nonLaminarInd))-1);
%         err1 = norm( [G.*q+A12*h+A10*h0; A21*q-d],inf);%infinity norm error, 1-norm, 2-norm can also be used
%         ERRORS=err1;
        
        break;% exit if tolerance is met
    else
        qk =q;
        %         hk=h;
    end
    
end
% kk,n_upd,Gtime
% kk
% set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(UPDATES,'x')
% xlabel('Iteration'),ylabel('Number of updates')
% makefigGood
% % set(0, 'defaultTextInterpreter', 'latex');
% figure, plot(log10(abs(ERRORS_)),'x')
% xlabel('Iteration'),ylabel('ERROR')

%
% figure, plot(log10(Es),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(beta),'x')
% xlabel('Iteration'),ylabel('$\|Es\|/\|b\|$')
%
% figure, plot(log10(T_con),'x')
% xlabel('Iteration'),ylabel('$\|Ts\|/\|b\|$')