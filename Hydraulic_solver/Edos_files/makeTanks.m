function [tanks,mapResId2List]=makeTanks(resrData,mapResId2List)
%% This function runs through a tank block data returned by WN_INP_reader() 
% and returns a vector of junction objects/data structures.


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% See WN_INP_reader(),  makePipes(), makeJunctions()

%%

% tic 
tanks=[];
% tanks=containers.Map();%%%Having tanks as a map of data objects costs a
% bit more in computation time, than having a cell array of them...

len=length(resrData);

%read first line of pipesdata
strLine = textscan(resrData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Elev           	Demand           	Pattern
resrvstr={';ID','Elevation','InitLevel','MinLevel','MaxLevel','Diameter','MinVol','VolCurve'};

assert(isempty(setdiff(resrvstr,strLine))|isempty(setdiff(resrvstr',strLine)), ...
    'Please Check columns names and data for [RESERVOIRS] are correct!' );


%%% Add to the Junctions List
juncNodenumber=mapResId2List.Count;
kk=1;
if len >= 2
    
    for ii=2:len
        strLine = textscan(resrData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)>=3 && strLine{end}==';')%check if something is ammis in the minimum data needed
            %             C = textscan(resrData{ii},'%s %s %s %f %f %f %f %s');
            tank.Id = strLine{1};
            tank.Elev = str2double(strLine{2}); %ruben
            tank.Head=str2double(strLine{2})+str2double(strLine{3});%elevation + initial level
            tank.Init=str2double(strLine{3});
            tank.Diameter=str2double(strLine{6});
            tank.Min = str2double(strLine{4});
            tank.Max = str2double(strLine{5});
            tank.Area = pi*tank.Diameter^2/4;
            %change to relevant data type while copying, if there is an            
            %tank.Pattern=str2double(strLine{4});
            tank.Area = (tank.Diameter^2)*pi/4;
%             tank,ii,kk
            tanks{kk}=tank;% tanks is cell array of many a tank
            
            %%%May be add the nodes to the tank map here...since only
            %%%the connected nodes will appear in the hydraulic
            %%%equations. However, this will cost in redundant access to
            %%%JunctionMap elements (upto k*Np times for average k connections, Np tanks).
            
            mapResId2List(tank.Id)=juncNodenumber+kk;
            
            kk=kk+1;

            
            
        end
        
        %error -- report
        
    end
    
else
    error('','No tanks in the network? :-) ');
end

% toc


end