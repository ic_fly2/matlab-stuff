

%%%% Similations for JEES, Elsevier Paper
%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Date: 05/02/2015 $

% %

tic
clear all,
%%close all

% filename='Purton3';% filenames={'CTnet','Richnet','WCnet','BWFLnet','EXnet','BWKWnet','NYnet'}%, 'BWKWnet'};%'Field_Lab_','kingswoodnet'
% units
% networkData=WN_INP_reader(['networks/',filename,'.inp']);%---works
networkData=WN_INP_reader(['van_zyl.inp'])

SimOptionsMap = getWNoptions(networkData('[OPTIONS]'));
pipeData=networkData('[PIPES]');
valveData=networkData('[VALVES]');
junctionData=networkData('[JUNCTIONS]');
reservoirData=networkData('[RESERVOIRS]');
tankData=networkData('[TANKS]');
patternData=networkData('[PATTERNS]');
optionsData=networkData('[OPTIONS]');
junctionXYData=networkData('[COORDINATES]');%input argument to makejuncXY();
SimOptions=makeOptions(optionsData);


pipes = makePipes(pipeData,[]);
valves=makeValves(valveData,[]);
juncIdListMap=containers.Map();resIdListMap=containers.Map();
nodesIdListMap=containers.Map();

[junctions,nodesIdListMap]=makeJunctions(junctionData,nodesIdListMap);
[reservoirs,nodesIdListMap]=makeReservois(reservoirData,nodesIdListMap);
[tanks,nodesIdListMap]=makeTanks(tankData,nodesIdListMap);


patternsMap=containers.Map();
patternsMap=makePatterns(patternData,patternsMap);
[demands,multipliers,baseDemands]=makeDemands(junctions,patternsMap,10);

DataReadTime=toc





%%% Make Hydraulic  equation matrices

pipes = [pipes valves];
reservoirs=[reservoirs tanks];
nn = length(junctions);
no = length(reservoirs);
np = length(pipes);
nv=length(valves);

[A12, A10]=getIncidenceMatrices(pipes,nodesIdListMap,np,nn,no);

%%
% %Get  diameters, Length for pipes and valves
tic,
D=ones(np,1);%Pipe Diameter
L=D;%Pipe Length
r=D; %Pipe Roughness
n_exp=1.85*ones(np,1);% HW loss exponent
% n_exp=2*ones(np,1);% quadratic loss exponent

for i=1:np
    D(i)=pipes{i}.Diameter;
    D (i)= D(i) / 1000; %Convert to m
    if i<=np-nv
        L(i)=pipes{i}.Length;
        r(i)=pipes{i}.Roughness;%convert to m/ft from 1e-3ft/mm
    else
        L(i)=2*D(i);%Valve approximated as a pipe whose length is 2 times the diameter; see EPANET Manual
        %         r(i)= pipes{i}.Setting;
        r(i)= 100;
        
        %         r(i)= pipes{i}.MinorLoss;%beware of using minroloss and
        %         settings...minor-loss can be zero and Settings can have large
        %         numbers like 1e9...bad numerical conditioning.
        n_exp(i)=2;
    end
    
end
toc



%%% Get H0
H0=zeros(no,1);
for i=1:no
    H0(i)=reservoirs{i}.Head;
end

%%% Change Units If necessary
if (strcmp(SimOptionsMap('Units'),'GPM'))
    ft2metres=0.3048;
    inch2mm=25.400;%1 inch is 25.4 mm %%%
    USgpm2lps=0.06309;
    L= ft2metres*L;%1 ft is 0.3048 m  %%%Change link length
    D = inch2mm*D; %%Change link Diameter
    H0 = ft2metres*H0;%%% 1 ft is 0.3048 m - Change fixed heads
    r = ft2metres*r;% Change Roughness in 1e-3 ft to mm;
    for junind=1:length(junctions)
        junctions{junind}.Elev=ft2metres*junctions{junind}.Elev;%%%  Change Elevation units
        junctions{junind}.Demand=USgpm2lps*junctions{junind}.Demand;%%%  Change base Demand units
    end
elseif    (strcmp(SimOptionsMap('Units'),'LPS'))
    %%%do nothing
end

elev=[];
for junind=1:length(junctions)
    elev(junind)=junctions{junind}.Elev;
end

% % % %Get  Demand at the unknown head nodes of junctions
tic,
d=ones(nn,1);
for i=1:nn
    d(i)=junctions{i}.Demand;
end
d = d/ 1000; %Convert from l/s to m^3/s

toc

%% %% Make Null space Data
% % % LU method
nc=np-nn;
%%% USE QR TO GET LS SOLUTION OF A12'x=d
x=A12'\d;

[L_lu,U,P_lu] = lu(A12);%[L,U,P] = lu(A12); luflops(L,U);
L1=L_lu(1:nn,:);
L2=L_lu(nn+1:nn+nc,:);
K=-(L1')\L2';
Z = P_lu'*[K;speye(nc)];
% % Elhay null bases
[Pt,~,T]=Permute_cotree(A12);%[Pt,Rt,T]=Permute_cotree(A12);
tic,L21 = -T(nn+1:np,1:nn)/T(1:nn,1:nn);toc% since T1 is lower triangular with nonzero diagonals, we can do forward substitution OR even T(nn+1:np,1:nn)*(T(1:nn,1:nn)\speye(nn))
%%%%tic,L21_=- T(nn+1:np,1:nn)*(T(1:nn,1:nn)\speye(nn));toc This is orders
%%%%of Magnitude faster than forward slash
Z1 = Pt'*([L21 speye(nc)]');%null space of A12 -- see (26) in Elhay...[L21 I_nc](Pt*A12*Rt) = 0;
Z=Z1;
% opts.Q='Householder';
% opts.tol=1e-5;
% N = spqr_null (A12',opts) ;              % find nullspace of A
% Z2 = (spqr_null_mult (N,speye(np)))' ;    % compute A*N, which will have a low norm.


[L_A12,~,q_LA12] = chol(A12'*A12,'lower','vector');%Matlab's chol is the same as lchol
%[L,p] = chol(A12'*A12);
% % %when A is sparse, returns a permutation matrix S. R'*R=S'*A*S. The factor of S'*A*S tends to be sparser than the factor of A.
% % %Chol is denser than lchol which chooses a column reordering that reduces
% % %fill-in
P_L=speye(nn);
Pr=P_L(q_LA12,:);%Fill-reducing Column ordering/Permutation Matrix
% P_c=P_L(:,q_L);
nulldata.x=x;
nulldata.Pr=Pr;
nulldata.L_A12=L_A12;
nulldata.Z=Z;
clear L_lu P_lu K L1 L2

nulldata.Z=Z;%update nullspace


%%%Initialise flow and unknown  heads
q_0 = x ; h_0 =  130*ones(nn,1);% Here q_0 satisfied the continuity equatiosn



%%
% % Common data to be passed to all the different solvers
% % Hazen-Williams loss equation coefficients for the pipes
fDW=ones(np,1);
if strcmp(SimOptionsMap('Headloss'),'D-W')
    n_exp = 2*ones(np,1);
    r = r/1000; %change mm DW roughness to m
    fDW = DWcoeff(q_0, D,r);%Note that roughness r is in mm for DW
    ResCoeff = (fDW./12.1).*L.*(D.^-5);
    n_exp = 2*ones(size(n_exp));
    %Pipe resistance r, Diameters D, lengths L, fDW, cHW
    data.ResCoeff=ResCoeff;% % Hazen-Williams loss equation coefficients for the pipes
    data.fDW=fDW;%DW friction factor
    data.Diam=D;%pipe Diameters
    data.Rough=r;%pipe roughness
    data.Len=L;%pipe lengths
else
    ResCoeff = 10.654 * r.^-n_exp .* D.^-4.87 .* L;%%%c_hw can be made an input argument --- this is R in Todini
    data.chw=ResCoeff;
end

%Pipe resistance r, Diameters D, lengths L, fDW, cHW
data.ResCoeff=ResCoeff;% % Hazen-Williams loss equation coefficients for the pipes
data.fDW=fDW;%DW friction factor
data.Diam=D;%pipe Diameters
data.Rough=r;%pipe roughness
data.Len=L;%pipe lengths

%%%% Print out condition numbers for some important linear systems at q=q_0
G=ResCoeff.*abs(q_0).^(n_exp-1);% G in Elhay is A11 in Todini
normZ=norm(A12'*Z,1)
nnzV=nnz(Z'*spdiags(G,0,np,np)*Z)%unless spqr() is used, Q and therefore Z fill up
nnzW=nnz(A12'*spdiags(G,0,np,np)*A12)
condZZ=condest(Z'*Z)  %condition number should be 1 as Q is orthonormal
condA12GA12=condest(A12'*spdiags(G,0,np,np)*A12)
%%%


%% %%
numiters=100;
data.max_iter=100;
epsilon=1e-3;
data.epsilon=epsilon;
kappa = condest(A12'*A12);
tol_err=1e-6;

alpha_vec=0.5;%half the flows in E_2 have converged ?
timeofDay_vec=1:3:60;


%% % Scur
cpuMat=zeros(length(timeofDay_vec),length(alpha_vec));
errorsMat=zeros(length(timeofDay_vec),length(alpha_vec));
kkMat=zeros(length(timeofDay_vec),length(alpha_vec));

for iter_i=1:length(timeofDay_vec)
    %%% set demand
    [demands,~,basedemand]=makeDemands(junctions,patternsMap,timeofDay_vec(iter_i));
    d=demands/1000; %Convert from l/s to m^3/s
    %x=A12'\d;% solve A12' A12 w = d; then x=A12 w
    xtemp=(L_A12)\(Pr*d);
    w=Pr'*(L_A12'\xtemp);
    x=A12*w;
    nulldata.x=x;
     q_0=x;
%    q_0 = 0.003*ones(np,1); h_0 =  130*ones(nn,1);% q_0 need not satisfy the continuity equatios as the method will use one from the next iteration
    for iter_j=1:length(alpha_vec)
        %%% set alpha
        data.alpha=alpha_vec(iter_j);
        %%%Compute
        
        tic
        errors=0;
        kiters=0;
        for iii=1:numiters
            
            if strcmp(SimOptionsMap('Headloss'),'D-W')
                [h,q,errors_persolve,maxiter_taken] = solveDW_Schur_all3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,data);
            else
                [h,q,errors_persolve,maxiter_taken] = solveHW_Schur_all3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,data);
            end
            errors = errors+errors_persolve;
            kiters=kiters+maxiter_taken;
        end
        cpu_time_alpha=toc;
        av_cpu_time_alpha=cpu_time_alpha/numiters;
        av_erorr=errors/numiters;
        kiters=kiters/numiters;
        
        cpuMat(iter_i,iter_j)=av_cpu_time_alpha;
        errorsMat(iter_i,iter_j)=av_erorr;
        kkMat(iter_i,iter_j)=kiters;
        
    end
end
times=mean(cpuMat);
solverTimes=[];
solverTimes=[solverTimes times];
save(['CPU-Schur',filename,num2str(tol_err),'.mat'],'alpha_vec','kkMat','cpuMat','errorsMat','tol_err','kappa','epsilon')

%% % NS1
cpuMat=zeros(length(timeofDay_vec),length(alpha_vec));
errorsMat=zeros(length(timeofDay_vec),length(alpha_vec));
kkMat=zeros(length(timeofDay_vec),length(alpha_vec));
for iter_i=1:length(timeofDay_vec)
    %%% set demand
    [demands,~,basedemand]=makeDemands(junctions,patternsMap,timeofDay_vec(iter_i));
    d=demands/1000; %Convert from l/s to m^3/s
    %x=A12'\d;% solve A12' A12 w = d; then x=A12 w
    xtemp=(L_A12)\(Pr*d);
    w=Pr'*(L_A12'\xtemp);
    x=A12*w;
    nulldata.x=x;
    q_0=x;
    for iter_j=1:length(alpha_vec)
        %%% set alpha
        data.alpha=alpha_vec(iter_j);
        %%%Compute
        
        tic
        errors=0;
        kiters=0;
        for iii=1:numiters
            
            if strcmp(SimOptionsMap('Headloss'),'D-W')
                [h,q,errors_persolve,maxiter_taken] = solveDW_Nullspace_all3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            else
                [h,q,errors_persolve,maxiter_taken] = solveHW_Nullspace_all3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            end
            errors = errors+errors_persolve;
            kiters=kiters+maxiter_taken;
        end
        cpu_time_alpha=toc;
        av_cpu_time_alpha=cpu_time_alpha/numiters;
        av_erorr=errors/numiters;
        kiters=kiters/numiters;
        
        cpuMat(iter_i,iter_j)=av_cpu_time_alpha;
        errorsMat(iter_i,iter_j)=av_erorr;
        kkMat(iter_i,iter_j)=kiters;
        
    end
end
times=mean(cpuMat)
solverTimes=[solverTimes times];

save(['CPU-NS1',filename,num2str(tol_err),'.mat'],'alpha_vec','kkMat','cpuMat','errorsMat','tol_err','kappa','epsilon')

%% % NS1
cpuMat=zeros(length(timeofDay_vec),length(alpha_vec));
errorsMat=zeros(length(timeofDay_vec),length(alpha_vec));
kkMat=zeros(length(timeofDay_vec),length(alpha_vec));
for iter_i=1:length(timeofDay_vec)
    %%% set demand
    [demands,~,basedemand]=makeDemands(junctions,patternsMap,timeofDay_vec(iter_i));
    d=demands/1000; %Convert from l/s to m^3/s
    %x=A12'\d;% solve A12' A12 w = d; then x=A12 w
    xtemp=(L_A12)\(Pr*d);
    w=Pr'*(L_A12'\xtemp);
    x=A12*w;
    nulldata.x=x;
    q_0=x;
    for iter_j=1:length(alpha_vec)
        %%% set alpha
        data.alpha=alpha_vec(iter_j);
        %%%Compute
        
        tic
        errors=0;
        kiters=0;
        for iii=1:numiters
            
            if strcmp(SimOptionsMap('Headloss'),'D-W')
                [h,q,errors_persolve,maxiter_taken] = solveDW_Nullspace_Ups3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            else
                [h,q,errors_persolve,maxiter_taken] = solveHW_Nullspace_Ups3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            end
            errors = errors+errors_persolve;
            kiters=kiters+maxiter_taken;
        end
        cpu_time_alpha=toc;
        av_cpu_time_alpha=cpu_time_alpha/numiters;
        av_erorr=errors/numiters;
        kiters=kiters/numiters;
        
        cpuMat(iter_i,iter_j)=av_cpu_time_alpha;
        errorsMat(iter_i,iter_j)=av_erorr;
        kkMat(iter_i,iter_j)=kiters;
        
    end
end
times=mean(cpuMat)
solverTimes=[solverTimes times];

save(['CPU-NS2',filename,num2str(tol_err),'.mat'],'alpha_vec','kkMat','cpuMat','errorsMat','tol_err','kappa','epsilon')

% 
%% % NS1
cpuMat=zeros(length(timeofDay_vec),length(alpha_vec));
errorsMat=zeros(length(timeofDay_vec),length(alpha_vec));
kkMat=zeros(length(timeofDay_vec),length(alpha_vec));

for iter_i=1:length(timeofDay_vec)
    %%% set demand
    [demands,~,basedemand]=makeDemands(junctions,patternsMap,timeofDay_vec(iter_i));
    d=demands/1000; %Convert from l/s to m^3/s
    %x=A12'\d;% solve A12' A12 w = d; then x=A12 w
    xtemp=(L_A12)\(Pr*d);
    w=Pr'*(L_A12'\xtemp);
    x=A12*w;
    nulldata.x=x;
    q_0=x;
    
    for iter_j=1:length(alpha_vec)
        %%% set alpha
        data.alpha=alpha_vec(iter_j);
        %%%Compute
        
        tic
        errors=0;
        kiters=0;
        for iii=1:numiters
            
            if strcmp(SimOptionsMap('Headloss'),'D-W')
                [h,q,errors_persolve,maxiter_taken] = solveDW_Nullspace_Ups_errcheck3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            else
                [h,q,errors_persolve,maxiter_taken] = solveHW_Nullspace_Ups_errcheck3(A12,A12',A10,...
                    H0,n_exp,q_0,h_0,d,np,nn,tol_err,kappa,nulldata,data);
            end
            errors = errors+errors_persolve;
            kiters=kiters+maxiter_taken;
        end
        cpu_time_alpha=toc;
        av_cpu_time_alpha=cpu_time_alpha/numiters;
        av_erorr=errors/numiters;
        kiters=kiters/numiters;
        
        cpuMat(iter_i,iter_j)=av_cpu_time_alpha;
        errorsMat(iter_i,iter_j)=av_erorr;
        kkMat(iter_i,iter_j)=kiters;
        
    end
end
times=mean(cpuMat)
save(['CPU-NS3',filename,num2str(tol_err),'.mat'],'alpha_vec','kkMat','cpuMat','errorsMat','tol_err','kappa','epsilon')
solverTimes=1000* [solverTimes times] %ms

ratios=[solverTimes(1)./solverTimes]
%  profsave(profile('info'),['ProfInforfor_',filename,num2str(tol_err),'.mat'])
% 
