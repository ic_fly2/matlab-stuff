function [pipes,mapId2List]=makePipes(pipeData,mapId2List)


%% This function runs through a pipe blcok data returned by WN_INP_reader() 
% and returns a vector of pipe objects/data structures.


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% See WN_INP_reader(),  makeJunctions(), makeReservoirs()

%%
% tic
pipes=[];
% pipes=containers.Map();%%%Having pipes as a map of data objects costs a
% bit more in computation time, than having a cell array of them...

len=length(pipeData);
%read first line of pipesdata
strLine = textscan(pipeData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Node1           	Node2           	Length      	Diameter    	Roughness"   	MinorLoss   	Status
pipestr={';ID',    'Node1',    'Node2',    'Length',    'Diameter',    'Roughness',    'MinorLoss',    'Status'};
assert(isempty(setdiff(pipestr,strLine))|isempty(setdiff(pipestr',strLine)), 'Please Check columns names and data for [PIPES] are correct!' );

kk=1;

if len >= 2
    
    for ii=2:len
        strLine = textscan(pipeData{ii},'%s'); strLine=strLine{:};
        if (length(strLine)==9 && strLine{9}==';')%check nothing is ammis in the data
            %             C = textscan(pipeData{ii},'%s %s %s %f %f %f %f %s');
            pipe.Id = strLine{1};
            pipe.startNodeId=strLine{2};
            pipe.endNodeId=strLine{3};
            %change to relevant data type while copying, if there is an
            pipe.Length=str2double(strLine{4});
            pipe.Diameter=str2double(strLine{5});
            pipe.Roughness=str2double(strLine{6});
            pipe.MinorLoss=str2double(strLine{7});
            pipe.Status=strLine{8};% Can keep the string 'Open', 'Closed', 'CV' - check valve
            %             pipe,ii,kk
            
            %%%Add to the pipe list only if the pipe is connected. 
            if (~isempty(setdiff('Closed',strLine{8})))%if status is not 'Closed'
                pipes{kk}=pipe;% pipes is cell array of many a pipe
                %             pipes(pipe.Id)=pipe;
                kk=kk+1;
            else
                fprintf('Pipe is Closed and so not added!')
            end            
        end
% %         error -- report
        
    end
    
else
    error('','No pipes in the network? :-) ');
end

% toc



% read line and use data
%check important parameters are there
%if pipe is closed, DO NOT ADD to list



end