function [Pt,Rt,A]=Permute_cotree(A)
%%% Try Matlab code generation with mex for faster implementations
%%% n.=m must be valid, else transpose and permute


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

P=[];R=[];Pt=[];Rt=[];
[n,m]=size(A);
P=speye(n); R=speye(m);
Pt=speye(n); Rt=speye(m);
% [r,c]=find_firstRow(A)
for i=1:m
    [r,c]=find_firstRow(A(i:n,i:m));
%         P=speye(n); R=speye(m);
%         P(i,i)=0;P(i+r-1,i+r-1)=0;   P(i,i+r-1)=1; P(i+r-1,i)=1;
%         R(i,i)=0;R(i+c-1,i+c-1)=0;  R(i,i+c-1)=1; R(i+c-1,i)=1;
%         Pt=P*Pt; Rt=R*Rt;
%         A=P*A*R;
    
    if (r~=1)%don't do operations unless row permutations are needed
        P=speye(n);
        P(i,i)=0;P(i+r-1,i+r-1)=0;   P(i,i+r-1)=1; P(i+r-1,i)=1;
        Pt=P*Pt;
        A=P*A;
    end
    
    if (c~=1)%don't do operations unless column permutations are needed
        R=speye(m);
        R(i,i)=0;R(i+c-1,i+c-1)=0;  R(i,i+c-1)=1; R(i+c-1,i)=1;
        Rt=R*Rt;
        A=A*R;
    end
       
    
end

end

%
function [r,c,K1]=find_firstRow(K)
r=find(sum(abs(K),2)==1,1);%assume there is only one
c=find(abs(K(r,:))==1);
end


% %%%%Test code efficiency
% average_time=0;
% num_iter=10;
% for i=1:num_iter
%     tic,[Pt,Rt,A]=Permute_cotree(A12);
%     time=toc;
%     average_time=average_time+time;
% end    
% average_time=average_time/num_iter;