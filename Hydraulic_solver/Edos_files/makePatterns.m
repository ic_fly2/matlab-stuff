function patternMap=makePatterns(patternData,patternMap)

%% This function runs through a junction blcok data returned by WN_INP_reader()
% and returns a vector of junction objects/data structures.


%   Edo Abraham
%   Copyright 2014, Imperial College London
%   $Revision: 0.1 $  $Date: 25/07/2014 $

% See WN_INP_reader(),  makeJunctions(), 

%%


len=length(patternData);
%read first line of data
strLine = textscan(patternData{1},'%s'); strLine=strLine{:};
% assert it has ";ID              	Elev           	Demand           	Pattern
juncstr={';ID',    'Multipliers'};
assert(isempty(setdiff(juncstr,strLine))|isempty(setdiff(juncstr',strLine)),...
    'Please Check columns names and data for [PATTERNS] are correct!' );

if len >= 2
    
    for ii=2:len
        strLine = textscan(patternData{ii},'%s'); strLine=strLine{:};
        temp_len=length(strLine);
        %if it starts with ';' ignore as it is a comment
        if (temp_len>=1 && strcmp(strLine{1},';'))%check nothing is ammis in the data or that there is a pattern data
            %do nothing
            
            %if it starts with a str pattern Id, get the rest of the data and
            %append/push back to the vector member of the pattern in patternMap using the patternId as key
        elseif (temp_len>=2 && ~strcmp(strLine{1},';'))%check nothing is ammis in the data or that there is a pattern data
            %             C = textscan(patternData{ii},'%s %s %s %f %f %f %f %s');
            pattern = strLine{1};
            patt_vec=ones(temp_len-1,1);
            for jj=1:temp_len-1
                patt_vec(jj)=str2double(strLine{jj+1});
            end
            
            patterExists = isKey(patternMap,pattern);% looks for the specified keys in mapObj, and returns logical true (1)
            if patterExists
                patternMap(pattern)=[patternMap(pattern);patt_vec];
            else
                patternMap(pattern)=patt_vec;
            end
        end
        
        %error -- report
        
    end
    
else
    error('','No Patterns in the network data? :-) ');
end



end