try
    wdsfile = ['D:\Phd\MATLAB\Phd\epanet\EPAnet\' def '_N' num2str(N) '_d' num2str(demand_pattern)];
    % wdsfile = 'D:\Phd\MATLAB\Phd\epanet\EPAnet\van_zyl_N12_d1';
    fid = fopen([wdsfile '.inp'],'r');
    fid2 = fopen([wdsfile '_out.inp'],'w');
    i = 1;
    tline = fgetl(fid);
    while ischar(tline)
        if strcmp(tline,'[PATTERNS]')
            % Print the new patterns
            while i <= 2*ceil(N/6)+3; % for D1,D2 and the new line characters
                fprintf(fid2, '%s  \r\n', tline);
                tline = fgetl(fid);
                i = i +1;
            end
            tline = fgetl(fid);
            for i = 1:size(schedule_tmp,2)
                new_line = ';';
                text = [num2str(i) '            ' num2str(schedule_tmp(:,i)')];
                fprintf(fid2, '%s  \r\n', new_line);
                fprintf(fid2, '%s  \r\n', text);
            end
        elseif strcmp(tline,'[TANKS]')
            fprintf(fid2, '%s  \r\n', tline);
            tline = fgetl(fid);
            
            fprintf(fid2, '%s  \r\n', tline);
            tline = fgetl(fid);
            
            for i = 1:length(location_res)
                j = location_res(i);
                init_head = num2str(head_display(1,j)+0.0001);
                init_head = init_head(1:5);
                tline(strfind(tline,'PLACE'):strfind(tline,'E')) = init_head;
                fprintf(fid2, '%s  \r\n', tline);
                tline = fgetl(fid);
            end
                
                
                
            %print the initial fill status
   
            %print the rest
        else
            fprintf(fid2, '%s  \r\n', tline);
            %         disp(tline); % for checking
        end
        tline = fgetl(fid);
    end
    fclose(fid);
    fclose(fid2);
catch
    disp('Writing failed')
    
end

%% old epa net stuff
% epanetloadfile([wdsfile '_out.inp']);
%
% %get flow and pressure from epanet:
% Pressure_Epa = getdata('EN_PRESSURE');
% Flow_Epa = getdata('EN_FLOW');
% unloadlibrary('epanet2');
%
%
%
% % results from EPAnet to my format
% % pick middle of each time slot:
% picker = round(linspace(1,length(Pressure_Epa)-2,N)); %results picker
% Pressure = Pressure_Epa(picker,:);
% Flow     = Flow_Epa(picker,:);
% % for nice and well behaved ones:
% % Pressure_Epa = Pressure_Epa(1:N,:); %cut at last time interall and any extra stuff epanet produces
% % Flow_Epa     = Flow_Epa(1:N,:);



if N ~= 6
    disp('N isn''t six')
    deviance = NaN;
    
else
    
    
    
    %% New epanet stuff
%     try
        cd([drive ':\Phd\MATLAB\Phd\epanet\epanet4ruben\'])
        str = ['epanet2.exe '  wdsfile '_out.inp ' wdsfile '_out.out' ];
        status = dos(str);

    
    
    fid = fopen(['extra.out'],'r');
    fid2 = fopen(['extra2.out'],'w');
    for i = 1:N+1 % subject to change
        tline = fgetl(fid);
        if i ~= 1;
            fprintf(fid2, '%s  \r\n', tline);
        end
    end
    fclose(fid);
    fclose(fid2);
    load('extra2.out')
    
    cd D:\Phd\MATLAB\Phd
    Flow = extra2(:,2:end);
    
    %% 6. u' is compared to u and the error is recorded
    % Sort the result columns
    if strcmp(def,'van_zyl')  % needs to be unified as theme
        %     Flow(:,7) =  Flow(:,7)+Flow(:,8);
        Flow(:,6) = []; %check valve flow
        
        Flow = Flow/1000;
        %%
%         flow_display = flow_display(1:N/6:end,:);
        flow_pipes = flow_display(:,loc_pipes);
        
%         if size(Flow) ~= size(flow_pipes )
            
        
        deviance = flow_pipes - Flow;
        deviance = abs(flow_pipes - Flow)./flow_pipes ;
        deviance = deviance.*(abs(deviance) <= 1000);
        
        deviance = deviance(isfinite(deviance))*100; % to make percentage
        
        
        % plotting
        if strcmp(plotting_switch,'on')
            deviance = deviance.*(abs(deviance) <= 100); % "cheat" throw out redicoulous values
            deviance(  deviance == 0) =  [];
            boxplot(abs(deviance))
            axis([0.5 2.5 0 100])
        end
        %     hist(reshape(abs(deviance),[numel(deviance) 1]));
    elseif strcmp(def,'Lecture_example')
        Flow = [];
        Flow_Epa = Flow_Epa(1:N,:);
        Flow(:,2) = Flow_Epa(:,1);
        Flow(:,1) =  Flow_Epa(:,3)+Flow_Epa(:,4) + Flow_Epa(:,5);
        Flow(:,3) = Flow_Epa(:,2);
        Flow  = Flow /1000;
        deviance = abs(flow_display - Flow)./flow_display ;
        deviance = deviance(isfinite(deviance))*100; % to make percentage
        % plotting
        if strcmp(plotting_switch,'on')
            boxplot(deviance)
            axis([0.5 2.5 0 100])
        end
    end
%         catch
%         disp('epanetcrashed')
    if ~exist('deviance','var'); deviance = NaN; end 
%     end
    
end




