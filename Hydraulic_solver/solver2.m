function [H,Q] = solver2(A12,A10,H0,q,K,Nexp,tol)

%Solves a pipe network for Q (link flow rates) and H (nodal hydraulic heads)
%Based on the Nodal Newton-Raphson method as defined by Todini & Pilati (1988) 'A
%Gradient Algorithm for the Analysis of Pipe Networks'
nn = size(A12,2);
np = size(A12,1);
%Initial values of H and Q
H = 20*ones(nn,1); %Heads
Q = 0.03*ones(np,1); %Flows 
tol_A11 = 1e-5; %Tolerence for inverting A11
NR_Max_Its = 200;

A21 = A12';

%Begin iterations
for ii = 1:NR_Max_Its

A11_Diag = K .* abs(Q).^(Nexp-1);

A11_Diag(A11_Diag<tol_A11) = tol_A11; %Small values in A11 make convergence unsteady therefore we need to define a lower bound, see Todini (1988), page 7.

A11 = sparse(diag(A11_Diag));

Inv_A11_Diag = 1 ./ A11_Diag;

InvA = sparse(diag(Inv_A11_Diag));

InvN = sparse(diag(1./Nexp));

DD = InvA * InvN;

b = -(A21*InvN*(Q + InvA*(A10*H0) ) + (q - A21*Q));   %Todini

A = A21 * DD * A12;

H = A\b;

Q = (sparse(eye(np,np)) - InvN)*Q - DD*(A12*H + (A10*H0));  %Todini
    

%Convergence check
C1 = abs((A11*Q + A12*H + A10*H0)) < tol; %Energy Conservation
C2 = abs(A21*Q-q)<tol; %Mass conservation
if   [all(C1);all(C2)] == 1;
    break
elseif sum(isnan(H)) >= 1
    disp('Failed to converge')
    break
end

fprintf('Iteration %1.0f: NR norm error for energy cons eqs %2.10f mH2O \n',ii,norm((A11*Q + A12*H + A10*H0)))

end