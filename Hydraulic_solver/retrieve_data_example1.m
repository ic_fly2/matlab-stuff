%Retrieve water distribution network data and run hydraulic simulation using solver.m
%Based on the Nodal Newton-Raphson method as defined by Todini & Pilati (1988) 'A
%Gradient Algorithm for the Analysis of Pipe Networks'
close all; clearvars; % clc;
% clear all;

%% Load Epanet input file

% wdsfile = 'lecture_example.inp';
% wdsfile = 'example1.inp';
wdsfile = 'res_only.inp'
epanetloadfile(wdsfile);

%% Retrieve all network data from .inp file using Epanet-Matlab Toolkit

%Number of Nodes = Number of junctions + number of reservoirs
N_Nodes = getdata('EN_NODECOUNT'); 

%Number of reservoirs
no = getdata('EN_TANKCOUNT');

%Number of unknown hydraulic head nodes
nn = N_Nodes - no;

%Number of links (includes pipes, valves and pumps)
np = getdata('EN_LINKCOUNT');

%Customer demand at all junctions for all times in extended period
%simulation
qall = getdata('EN_DEMAND');
qall(:,nn + 1:N_Nodes) = []; %Final columns are for the known head nodes (Epanet convention). In Todini's method, we assume q is of size nn. We therefore delete these nodes in our demand matrix. This is common for other network parameters.
qall = qall';
qall = qall / 1000; %Convert from l/s to m^3/s

%Elevations of all junctions
z = getdata('EN_ELEVATION');
z(:,nn + 1:N_Nodes) = [];
z = z';

%Friction factors of all pipes
r = getdata('EN_ROUGHNESS');
r = r';

%Pipe diameters [m]
D = getdata('EN_DIAMETER');
D = D';
D = D / 1000; %Convert to m

%Lengths [m]
L = getdata('En_LENGTH');
L = L';

%% Define incidence matrix (describes how nodes are connected to links)
A_Incidence = zeros(np,N_Nodes);

%Create pointers to find connecting nodes
n1=libpointer('int32Ptr',0);
n2=libpointer('int32Ptr',0);

for i = 1:np
    calllib('epanet2','ENgetlinknodes',i,n1,n2);
    Node1=get(n1,'Value');
    Node2=get(n2,'Value');
    A_Incidence(i,Node1) = -1;
    A_Incidence(i,Node2) = 1;
end

A12 = sparse(A_Incidence(:,1:nn));
A21 = A12';
A10 = sparse(A_Incidence(:,nn+1:N_Nodes));

%Define pipe resistance using Hazen-Williams
K = 10.675 * r.^-1.852 .* D.^-4.8704 .* L;

%Exponent for the Hazen-Williams equation
Nexp = 1.852 * ones(np,1);

%Specify a tolerence for the convergence of Newton-Raphson's Method
tol = 1e-9;

%% Select time of day. In a steady state simulation this is just set to 1. If however an extended period simulation is being run (where q and H0 vary according to the time of day), t can be set according to the time of day that you wish to model
t = 1;


%% Fixed head nodes, Epanet Toolbox doesn't seem to have an option to import this so it is manually entered here.
H0all = [120];

%% Run simulation
q = qall(:,t); %Customer demand at time t
H0 = H0all(t); %Fixed head node at time t

%Use solver.m to get nodal heads and link flow rates
 [H,Q] = solver2(A12,A10,H0,q,K,Nexp,tol);

 Pressure = H - z; %Pressure is equal to hydraulic head minus elevation, see Bernoulli Principle for details.

%% Comparison between Matlab model and Epanet engine.
Pressure_Epa = getdata('EN_PRESSURE');
Pressure_Epa(:,nn + 1:N_Nodes) = [];
Pressure_Epa = Pressure_Epa';
% fprintf('Average nodal pressure difference between Matlab model and Epanet engine is %2.3f mH2O \n',mean(abs(Pressure - Pressure_Epa(:,t))))

Flow_Epa = getdata('EN_FLOW');
Flow_Epa = Flow_Epa';
% fprintf('Average link flow difference between Matlab model and Epanet engine is %2.3f l/s \n',mean(abs(Q*1000 - Flow_Epa(:,t))))

figure
plot(1:np,Q*1000,1:np,Flow_Epa(:,t),'o')
xlabel('Link number')
ylabel('Flow (l/s)')
legend('Matlab Solver','EPANET')
title('Comparison of Flows for Epanet and Matlab Solver')

figure
plot(1:nn,Pressure,1:nn,Pressure_Epa(:,t),'o')
xlabel('Node number')
ylabel('Pressure Head (mH2O)')
legend('Matlab Solver','EPANET')
title('Comparison of Pressures for Epanet and Matlab Solver')