% clearvars;
% filename = 'Richmond_skeleton.inp';
% T = [1 0 1 0 0 1 0 ]; % pump configuration
clearvars;
% T = [ 1 1 1 1 1 1 1 1 1 1 1 1 0 0 1 1 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0];
% filename = 'van_zyl.inp';
% def = 'van_zyl';
def = 'example1';
% def = 'Richmond_skeleton';
% T = [1 0 1 ];
% def = 'Lecture_example';
% def = 'res_only';
prepNetwork;
t = 5; %time step


prepGGA;
pump_control;

[A12,A10,H0] = makeA12A10(pipes,nodesIdListMap,Allreservoirs,pumpsOn);
tol = 0.00001;
[H,Q] = solver3(A12,A10,H0,q,K,Nexp,tol,CV);

%% mass balance
for i = 1:length(tanks)
    Area = (pi/4)*tanks{i}.Diameter^2;
    Delta_H = Feeds(i,:)*Q/Area;
    tanks{i}.Init =  tanks{i}.Init + Delta_H;
end
Allreservoirs = [reservoirs tanks];


% Pressure = H - Elevation_j; %Elevation_j is wrong!
