[rows,cols,vals] = find(connection_matrix');
for ii = 1:np
    
        %-1 first term, 1 second term, pipe flow to second node
        rows(ii*2-1)
        rows(ii*2) %out
        
        
        for i = 1:N
            pipe_index = num2str((ii-1)*N+i);
            pipe_out = num2str( rows(ii*2) 
            ['abs(x(' pipe_index '))^0.85*x( ' pipe_index ')'] 