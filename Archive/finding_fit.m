clear all
cdf = [0 100; 15 94.444; 30 87.1; 45 74.7;  60 60; 75 47; 90 36.5; 105 26.7; 120 20; 135 14.1; 150 11; 165 8.8; 180 6.1; 195 4.1; 210 4; 225 2.6; 240 2.5; 255 1; 270 0.6; 285 0.2; 300 0.1; 330 0];
cdf2 = cdf; cdf2(:,2) =cdf2(:,2)/100;

% v = cdf2(:,1); p = cdf2(:,2);
% av = trapz(v,p);
% var = trapz(p,(v-av).^2);

% plot(cdf2(:,1),cdf2(:,2)); hold on;
pdf1(1,1:2) =0;
for i = 1:length(cdf)-1
    pdf1(i+1,1) = (cdf2(i+1,1)+cdf2(i,1))/2;
    pdf1(i+1,2) = -(cdf2(i+1,2)-cdf2(i,2))/(cdf2(i+1,1)-cdf2(i,1));
end
 pdf_smooth = smooth(pdf1(:,2));
%  plot(pdf1(:,1),pdf1(:,2)*100,'c');
% plot(pdf1(:,1),pdf_smooth*100,'r');
% pd = fitdist(pdf_smooth,'poisson');
pd = makedist('Weibull','a',1,'b',1.5) ;
% y = poisspdf(0:length(pdf_smooth)-1,pd.lambda);
%  y = poisspdf(0:length(pdf_smooth)-1,4.18);
% y = wblpdf(pdf1(:,1),pd.A,pd.B);

%   plot(pdf1(:,1),y*50,'g')
% disp(pd.lambda)
% xlabel('Minutes')
% ylabel('percentage of calls');
% plot([30:30:180],[10 18 24 18 15 15 ]/100)
add = [];
for i = [30:30:180]
 
 add = [add ;i interp1q(pdf1(:,1),pdf1(:,2),i) ];
end
pdf2 = sortrows([ pdf1 ; add]);
% 30
prob(1) = trapz(pdf2(1:4,1),pdf2(1:4,2));
prob(2) = trapz(pdf2(4:7,1),pdf2(4:7,2)); % 60
prob(3) = trapz(pdf2(7:10,1),pdf2(7:10,2)); % 90
prob(4) = trapz(pdf2(10:13,1),pdf2(10:13,2)); % 120
prob(5) = trapz(pdf2(13:16,1),pdf2(13:16,2)); % 150
prob(6) = trapz(pdf2(16:28,1),pdf2(16:28,2)); % <150
% legend('cdf','pdf','smoothed pdf','poisson with mean =  4.18')





