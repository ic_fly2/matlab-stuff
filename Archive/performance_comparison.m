%DR vs standard operation comparison
% STOR makes 30000� per year per MW
% FFR makes 60000� per year per MW
clear all; close all;
%% number of pumps comparison and demand multiplier
% switch_penatly = 0.00001;
% maxtime = 20;
% demand_modifier = linspace(180,800,15);

tic;
demand_modifier = [0.5 0.75 1 1.25 1.5 1.75];

pump_curves = [-75	0	214.52; ...
-100	0	219.36; ...
-125	0	224.2; ...
-150	0	229.04; ...
-175	0	233.88; ...
-200	0	238.72; ...
-225	0	243.56; ...
-250	0	248.4; ...
-275	0	253.24; ...
-300	0	258.08; ...
-350	0	267.76; ...
-400	0	277.44];
           
system_curves = [1.95	 	0	 	195.05;...
4.70	 	0	 	188.07;...
8.87	 	0	 	177.48;...
15.95	 	0	 	159.52;...
26.53	 	0	 	132.67;...
39.70	 	0	 	99.25;...
47.57	 	0	 	79.28;...
52.80	 	0	 	66.00;...
56.53	 	0	 	56.53;...
65.84	 	0	 	32.92;...
73.06	 	0	 	14.61];

res_time = [ 6 9 12];
price_strech = [0.8 1 1.2];
demand_strech = [0.8 1 1.2];

             
 for p = 1:length(pump_curves )
     K_P = pump_curves(p,:);
     for s = 1:length(system_curves )
         K_S = system_curves(s,:);
         for i = 2:5 % pumps
             for j = 1:length(demand_modifier)
                 try
                     [cost(i,j),outputmi,P_sced(i,j,:)] = optimisation_fun_v8_standard(demand_modifier(j),switch_penatly,i,maxtime,K_P,K_S);
                     Quality(i,j) = outputmi.cplexstatus;
                     disp(Quality(i,j))
                 catch
                     cost(i,j) = 0;
                 end
                 try
                     [costdr(i,j),outputmi,P_sceddr(i,j,:)] = optimisation_fun_v8_fullDR(demand_modifier(j),switch_penatly,i,maxtime,K_P,K_S);
                     Quality_dr(i,j) = outputmi.cplexstatus;
                     disp(Quality_dr(i,j))
                 catch
                     costdr(i,j) = 0;
                 end

             end
         end
         toc;
         %% year cost:
         yr_cost = cost*365;
         yr_cost_dr = costdr*365;
         yr_cost(yr_cost == 0) = NaN;
         yr_cost_dr(yr_cost_dr == 0) = NaN;
         diff = yr_cost - yr_cost_dr;
         results.pumps(p).sys(s).res = diff;
     end
      save
 end
disp(p)
% diff(diff == 0) = NaN;
% STOR = diff + 30000;
% FFR  = diff + 60000;
% figure;
% contourf(FFR)
% figure;
% contourf(STOR)