%% Pumps
% method 1
% [pump  ] = PieceWiseApproxGenPump(-225.27,21.423,12.948,3,[],'off');
% Pump_station(1).pump(1).curves =  [-1 0]; % for every pump station
% for i = 1:length(pump) % for all pumps
%     Pump_station(1).pump(i+1).curves = [ pump(i).ms' pump(i).Cs' ];
% end


% method 2
Pump_station(1).pump(1).curves =  [-1 0];
Pump_station(1).pump(2).curves = [ -1 10];% [-1 3]; % mx + c of defining inequalities
Pump_station(1).pump(3).curves =  [-0.5 10 ];
Pump_station(1).pump(4).curves =  [-0.33 10]; % such as for more than one curve:[ -0.5 2; -1.5 3]; 

% Pump_station(2).pump(1).curves =  [-1 0];
% Pump_station(2).pump(2).curves = [ -1 3];% [-1 3]; % mx + c of defining inequalities
% Pump_station(2).pump(3).curves =  [-1 6 ];
% Pump_station(2).pump(4).curves =  [-1 9]; % such as for more than one curve:[ -0.5 2; -1.5 3]; 
% 
% Pump_station(3).pump(1).curves =  [-1 0];
% Pump_station(3).pump(2).curves = [ -1 9];% [-1 3]; % mx + c of defining inequalities

n_pumps = [3]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  
      
%% Pipes
pipedata(1).data = [0.9,0.001,1500,2,5]; %D,e,L,vmax,pmax,
pipedata(2).data = [0.9,0.001,500,2,5];
pipedata(3).data = [0.9,0.001,500,2,5];
pipedata(4).data = [0.9,0.001,500,2,5]; 
pipedata(5).data = [0.9,0.001,500,2,5]; 


%% network
% van zyl network
connection_matrix = [...
    1 -1  0  0  0  0 ;... %P1
    0  0  1  0  0 -1;... %P3
    0  1 -1  0  0  0;... %q3
    0  0  1 -1  0  0;... %q4
    0  0  0  1 -1  0;... %q5
    0  0  0  0  1 -1;... %q6
    0  0  0  0  1  0]; % demand out


connection_type = {... 
    'Pump',...
    'pipe_piece',...
    'pipe_piece',...
    'pipe_piece',...
    'pipe_piece',...
    'pipe_piece',...
    'demand'}; 


%% nodes
% Elevation
Elevation = [0 0 0 0 0 0];

% Reservoir
% feed reservoir fix head
location_fix = [1];
fixed_val = [1]; % m

%Variable Tank
location_res = [4 6]; 
Reservoir_area = [24*60*60/N 24*60*60/N]; %m2

%% misc
% mods for analysis (not used)
demand_multiplier = 0.1;
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;
switch_penalty = 0;
 
% demand 
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
d = d';%m3/h
n_demands = 1;
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
T_bounds = [0 1];
h_min = [0  1  1  1  1  1  ]; % max and min values of the hea dat the nodes
h_max = [10 10 10 10 10 10 ];
Qmax = 10; % there is no bound on q for now jsut make it large
q_bounds = [0 Qmax] ; %can have any value
lambda_bounds = [0 1];