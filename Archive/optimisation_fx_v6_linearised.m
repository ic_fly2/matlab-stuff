%% set pump schedule and outflow demands
clear all;close all
% Create new or run old network definition
% network_definition;

%% Initialisation

%Network topology
% load('Nodes_14_12_2013.mat'); %test case
% Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit % test case

% pump and system features
pump_and_sys_curves; %get model data
plot_on = 0;% turn ploting for all on with 1 for only final =0
N = 48;
no_pumps = 5;
demand_multiplier = 450;

% solver type
solver_type = 'MIQP_cplex'; % Options include 'QP_cplex' 'MILP_matlab' and  'MIQP_cplex' use matlab solvers with the optimisation toolkit cplex requires cplex from IBM to work
maxtime = 20;
% controller
controller = 'on'; %options are on or off in on then provide h_initial
h_initial =1;

%electricity pricing
Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
% Pe(1:24) = Electricity_Price(1:2:48); % test case
Pe = Electricity_Price;
% demand
x = 1:N;
% d = sin(x/(pi*1.2))*400 + 800;%d =d*180;
% demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d*demand_multiplier; %m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!
% resevoir
Area = pi*(50/2)^2;
% h0 = 15; % removed in new layput
h_max = 2;
h_min = 0;
reservoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping over stuff that doesn't change
% MILP vars
%A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear A
Top(1,N*no_pumps+N) = 0; 
Top(1,N*no_pumps+1) = 1; Top(1,N*no_pumps+N) = -1;   
% ad limit on what pump switches are allowed
limit = eye(N);
i = 1;
while i <= no_pumps-1
limit = [limit eye(N)];
i = i+1;
end
A = [Top ; limit zeros(N)];
%b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% b(1,1) = d(1)/Area;
b(1,1) =0;
b(2:N+1,1) = 1;

% c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = zeros(N,1);
i = 1;
c = Pe;
while i <= no_pumps-1
c = [c Pe*(i+1)];
i = i+1;
end
c = [c h']'; % added transpose for cplex

%H % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% super complicated way of doing it:
H = eye(N*no_pumps);
for i = 1:N*no_pumps-1
   H(i,i+1) = - 0.5;
  % H(i+1,i)= - 0.5;
end
j = 2;
multiple(1:N,1) = 1;
while j <= no_pumps
    add(1:N,1) = j;
  multiple = [multiple; add];
  j = j+1;
end
for i = 1:N*no_pumps
   H1(i,:)=  multiple(i,:) .* H(i,:); 
end
H = H1;
H(N*no_pumps+1:N*(no_pumps+1),N*no_pumps+1:N*(no_pumps+1)) = 0;
H = H +H'; % ensure symetry
H = H*2; %price the switch
% for debuggin purposes
H(:,:) = 0;

%number of intigers

M(1:N*no_pumps) = 1:N*no_pumps;

% beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for k = 2:N
%     beq(k,1) = sum(d(1:k))/Area - h0;
% end
beq(1,1) = 0;
beq(2:N,1) = d(2:N)/Area/2; %m3/0.5h !!!!!!!!!!!!
if strcmpi('on',controller) % H_1 = h_initial
    beq(N+1,1) = h_initial;
else
end
%bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ub(1:N*no_pumps,1) = 1;
ub(N*no_pumps+1:N*(no_pumps+1),1) = h_max;
lb(1:N*no_pumps,1) = 0;
lb(N*no_pumps+1:N*(no_pumps+1),1) = h_min;
%% loop
iter = 1;
max_iter = 20;
err = 21;
while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached

%% Curve fitting / Hydraulic solver
% regressoin model fit polynomials to flow and find operating points
for t = 1:N;
%% compute pump settings and outflows
% system_curve_builder_v3;
% operating_point_finder_v2;
% Qp(t,:) = Q_operation;
hydraulic_curve_fitting
end
Qp_hist(:,:,iter) =  Qp(:,:);
h_hist(:,iter) = h(:);
%Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j = 1;
Aeq =[];
while j <= no_pumps
% for i = 2:N 
    flow  = diag(Qp(:,j))/2; %from m3/h to m3/0.5h
    flow(1,1) = 0; % first flow is an inequality
% end
Aeq = [Aeq flow];
j = j + 1;
end
h_i(1:N) = -1;
h_i_1(1:N-1) = 1;
H_treatment = diag(h_i) + diag(h_i_1,-1) ;
% H_treatment(1,N) = -1; 
H_treatment(1,1) =0;
Aeq = [Aeq/Area H_treatment ]; % added /Area for good measure
if strcmpi('on',controller) % H_1 = h_initial
    Aeq(N+1,no_pumps*N+1) = 1;
else
end

%% produce a feasable solution on the first iteration to solve this non solving error
if iter ==1
    if strcmpi('on',controller) % H_1 = h_initial
        h0(1) = h_initial;
    else
        h0(1) = 0; 
    end
    if h_initial >= 0.7*h_max
        disp('initial and max filling are too close for sensible operation, press Ctrl - C and change the input')
        pause
    end
    % % based on an optimisation 
%     [Y0,val,status]= linprog(c,A,b,Aeq,beq,lb,ub); % produces a solutionc quickly
%     Y0_schedule = round(Y0(1:no_pumps*N));
%     
%     for i = 1:no_pumps
%         init_schedule(:,i) = Y0_schedule((i-1)*N+1:i*N);
%     end
%     pump_flow = sum(init_schedule.*Qp,2);
%     for i = 1:N-1
%         h0(i+1) = h0(i)+(pump_flow(i) -d(i))/Area;
%     end
% % based on a tank trigger level method
set = 1;
Y0_schedule(1) = set;
pump_flow = Qp(:,no_pumps);
for i = 1:N-1
    h0(i+1) = (pump_flow(i)*set - d(i))/Area + h0(i);
    if h0(i+1) >= h_max
        set = 0;
        h0(i+1) = (pump_flow(i)*set - d(i))/Area + h0(i);
    elseif h0(i+1) <= h0(1) 
        set = 1;
        h0(i+1) = (pump_flow(i)*set - d(i))/Area + h0(i);
    end
    Y0_schedule(i+1) = set; 
end
Y0((no_pumps-1)*N+1:no_pumps*N,1) =  Y0_schedule;
Y0(no_pumps*N+1:(no_pumps+1)*N,1) = h0;

  
    
end

%% solvers
%% QP
if strcmpi('QP_cplex',solver_type)
% QP
[Y, fval, exitflag, output] = cplexqp(H,c,A,b,Aeq,beq,lb,ub);
% figure;
if plot_on ==1
hold on
subplot(2,1,1); 
for i = 1:no_pumps
  pic(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(pic,'stack')
  
end
subplot(2,1,2); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)))
else
end

elseif  strcmpi('MIQP_cplex',solver_type)
%% MIQP
int(1:N*no_pumps) = 'I';
cont(1:N) = 'C';
ctype = strcat([int cont])   ;
options = cplexoptimset('MaxTime',maxtime);
tic;
[Y, fvalmi, exitflagmi, outputmi] = cplexmiqp(H,c,A,b,Aeq,beq,[],[],[],lb,ub,ctype,Y0,options);
toc;
fvalmi_hist(iter+1) = fvalmi; % for convergence
if plot_on ==1
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack')
end

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)))

subplot(2,2,2);
bar(d,'DisplayName','Demand')
subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
else
end
elseif  strcmpi('LP_matlab',solver_type)
%% LP Matlab optimisation
% calls the lp solver linprog that is supplied as standard by matlab.
% does not produce initger schedules but can be used at home

[Y,val,status]= linprog(c,A,b,Aeq,beq,lb,ub);

if plot_on ==1
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack')
end

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)))

subplot(2,2,2);
bar(d,'DisplayName','Demand')
subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
else
end

else
    disp('No solver selected!')
end
% find resulting height of water tank
Y_hist(:,iter) = Y;
reservoir_level = Y((length(Y)-N+1):(length(Y))); %resevoir_level(1) = h0;
h(:,1) = reservoir_level;
err = sum(abs(res_old - reservoir_level'));
if iter <= max_iter % leave last result for analysis if no convergence occurs
res_old = reservoir_level';
else
end
if  isempty(findstr(solver_type,'cplex'))
if  fvalmi_hist(iter) - fvalmi == 0
    iter = iter+2; % reduce iterations if improvement
end
end    
iter = iter +1;
end
%% show schedule
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack');axis([1 48 0 no_pumps+0.2]);title('Pump schedule');xlabel('Interval');ylabel('number of pumps on')
end

subplot(2,2,2);
% bar(d,'DisplayName','Demand');
plot(d,'g'); hold on; plot(schedule.*Qp,'r')
% axis([1 48 0 max(d)+2])
axis([1 48 0 max(max(schedule.*Qp))+2])
title('System Flow');xlabel('Interval'); ylabel('Flow m3/h')

subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
axis([1 48 0 100])
title('Electricity Price');xlabel('Interval');ylabel('� per MWh')

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)));axis([1 48 h_min h_max])
title('Tank level'); xlabel('Interval');ylabel('Tank level (m)')
%% export to EPAnet
% convert the schedule of several pumps to one pump (Not working yet!!)
% for i = 1:no_pumps
%  pattern = Y(N*(i-1)+1:N*i);
%  
%     save('pump_schedule_' num2str(i) '.txt','pattern','-ascii')
% %     status = dos('COPY pump_schedule.txt pump_schedule.pat')
% end
%% convergance checks
if plot_on == 1
figure;
for i = 1:N
        pump_perf(i,:,1) = Qp_hist(i,1,:);
    hold on; plot(pump_perf(i,:,1))
  end

figure;
for i = 1:N
  hold on; plot(h_hist(i,:),'r')
end
end

if no_pumps == 1
  schedule = [ 0; 0;  schedule];% add lines epanet ignores  
save('pump_schedule.txt','schedule','-ascii')
status = dos('COPY pump_schedule.txt pump_schedule.pat');
end