%% define pumps:
a_org = [];
b_org = [];
c_org = [];
q_lim = [];
pump_curve_data = [];
Power_nameplate = [];
index_of_dublicates = zeros(length(pumps));
station_number = 1;
            
                
    



 set = 1:length(pumps);
for i = 1:length(pumps);
    if ismember(i,set)
        for j = 1:length(pumps)
            % identigy dublicates i.e. Stations
            if strcmp(pumps{i}.startNodeId,pumps{j}.startNodeId) && ...
               strcmp(pumps{i}.endNodeId,pumps{j}.endNodeId) && ...
               strcmp(pumps{i}.headCurveId,pumps{j}.headCurveId) &&...
                    index_of_dublicates(i,j) ~=1
                % we have a dublicate
                index_of_dublicates(i,j) = 1;
                %             dublicates(1,i) = j;
                %             dublicates(2,j) =i;
                 set(set == j)= [];
            else
                pumps{i}.Station = num2str(station_number);
                station_number = station_number+1;
            end

        end
     else
        pumps{i}.Station = num2str(station_number);
     end
    %     i = i+1;
end
% index_of_dublicates = index_of_dublicates + eye(length(pumps));
n_pumps = sum(index_of_dublicates,2)';

for i = 1:length(pumps)
    if sum(index_of_dublicates(i,:)) ~= 0
        
        % power:
%         if ~isempty(pumps{i}.power)
%             Power_nameplate =  [Power_nameplate pumps{i}.power]; %please make sure it is in KW!!
%         else
%             q = linspace(0,max(roots(p)));
%             h = polyval(p,q);
%             power = max(q.*h*9.81)/0.75; % 75% efficiency
%             Power_nameplate =  [Power_nameplate power];
%         end
        if isempty(pumps{i}.power)
            q = linspace(0,max(roots(p)));
            h = polyval(p,q);
            pumps{i}.power = max(q.*h*9.81)/0.75; % 75% efficiency
        end
    end
end
%  needs to be here:
n_pumps(n_pumps == 0) = [];