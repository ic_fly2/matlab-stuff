%% set pump schedule and outflow demands
clear all;clf
% Create new or run old network definition
% network_definition;

%% Initialisation

%Network topology
% load('Nodes_14_12_2013.mat'); %test case
% Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit % test case

% pump and system features
pump_and_sys_curves; %get model data
plot_on = 0;% turn ploting for all on with 1 for only final =0
N = 48;
no_pumps = 1;
demand_multiplier = 100;

% solver type
solver_type = 'MIQP_cplex'; % Options include 'QP' and  'MIQP_cplex'
% controller

%electricity pricing
Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
% Pe(1:24) = Electricity_Price(1:2:48); % test case
Pe = Electricity_Price;
% demand
x = 1:N;
% d = sin(x/(pi*1.2))*400 + 800;%d =d*180;
demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le.txt');
d = demand.data;
d = d*demand_multiplier;
% resevoir
Area = pi*(50/2)^2;
% h0 = 15; % removed in new layput
h_max = 2;
h_min = 0;
reservoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping repeatedly
% MILP vars
%b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% b(1,1) = d(1)/Area;
b(1,1) =0;
b(2:N+1,1) = 1;
% b = b'; % for cplex
% c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = zeros(N,1);
i = 1;
c = Pe;
while i <= no_pumps-1
c = [c Pe*(i+1)];
i = i+1;
end
c = [c h']'; % added transpose for cplex

%H % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% super complicated way of doing it:
H = eye(N*no_pumps);
for i = 1:N*no_pumps-1
   H(i,i+1) = - 0.5;
  % H(i+1,i)= - 0.5;
end
j = 2;
multiple(1:N,1) = 1;
while j <= no_pumps
    add(1:N,1) = j;
  multiple = [multiple; add];
  j = j+1;
end
for i = 1:N*no_pumps
   H1(i,:)=  multiple(i,:) .* H(i,:); 
end
H = H1;
H(N*no_pumps+1:N*(no_pumps+1),N*no_pumps+1:N*(no_pumps+1)) = 0;
H = H +H'; % ensure symetry
H = H*2; %price the switch
%number of intigers
M(1:N*no_pumps) = 1:N*no_pumps;

% beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% for k = 2:N
%     beq(k,1) = sum(d(1:k))/Area - h0;
% end
beq(1,1) = 0;
beq(2:N,1) = d(2:N)/Area;
%bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ub(1:N*no_pumps,1) = 1;
ub(N*no_pumps+1:N*(no_pumps+1),1) = h_max;
lb(1:N*no_pumps,1) = 0;
lb(N*no_pumps+1:N*(no_pumps+1),1) = h_min;
%% loop
iter = 1;
max_iter = 5;
err = 21;
while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached

%% Curve fitting / Hydraulic solver
% regressoin model fit polynomials to flow and find operating points
for t = 1:N;
%% compute pump settings and outflows
% system_curve_builder_v3;
% operating_point_finder_v2;
% Qp(t,:) = Q_operation;
hydraulic_curve_fitting
end


%% MILP
% create matracies for LP
%Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j = 1;
Aeq =[];
while j <= no_pumps
% for i = 2:N 
    flow  = diag(Qp(:,j))/2; %from m3/h to m3/0.5h
    flow(1,1) = 0; % first flow is an inequality
% end
Aeq = [Aeq flow];
j = j + 1;
end
h_i(1:N) = -1;
h_i_1(1:N-1) = 1;
H_treatment = diag(h_i) + diag(h_i_1,-1) ;
% H_treatment(1,N) = -1; 
H_treatment(1,1) =0;
Aeq = [Aeq/Area H_treatment ]; % added /Area for good measure

%A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% clear A
Top(1,N*no_pumps+N) = 0; 
% for i = 1:no_pumps % flow at t = 1; (negative for inequality)
% Top(1,(i-1)*N+1) = -Qp(1,i)/Area;
% end
Top(1,N*no_pumps+1) = 1; Top(1,N*no_pumps+N) = -1;   
% ad limit on what pump switches are allowed
limit = eye(N);
i = 1;
while i <= no_pumps-1
limit = [limit eye(N)];
i = i+1;
end
A = [Top ; limit zeros(N)];

%% solvers
%% QP
if strcmpi('QP',solver_type)
% QP
[X, fval, exitflag, output] = cplexqp(H,c,A,b,Aeq,beq,lb,ub);
% figure;
if plot_on ==1
hold on
subplot(2,1,1); 
for i = 1:no_pumps
  pic(:,i) = X(N*(i-1)+1:N*i)*i;
  bar(pic,'stack')
  
end
subplot(2,1,2); 
plot(X(1+N*no_pumps:N*(1+no_pumps)))
else
end

elseif  strcmpi('MIQP_cplex',solver_type)
%% MIQP
int(1:N*no_pumps) = 'I';
cont(1:N) = 'C';
ctype = strcat([int cont])   ;
[Y, fvalmi, exitflagmi, outputmi] = cplexmiqp(H,c,A,b,Aeq,beq,[],[],[],lb,ub,ctype);


if plot_on ==1
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack')
end

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)))

subplot(2,2,2);
bar(d,'DisplayName','Demand')
subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
else
end
else
    disp('No solver selected!')
end
% % find resulting height of water tank
% reservoir_level = Y((length(Y)-N+1):(length(Y))); %resevoir_level(1) = h0;
% h(:,1) = reservoir_level;
% err = sum(abs(res_old - reservoir_level'));
% if iter <= max_iter % leave last result for analysis if no convergence occurs
% res_old = reservoir_level';
% else
% end
iter = iter +1;
end
%% show schedule
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack');axis([0 48 0 3])
end

subplot(2,2,2);
bar(d,'DisplayName','Demand')
axis([0 48 0 max(d)+2])

subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
axis([0 48 0 100])

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)));axis([0 48 h_min h_max])

%% export to EPAnet
% convert the schedule of several pumps to one pump (Not working yet!!)
% for i = 1:no_pumps
%  pattern = Y(N*(i-1)+1:N*i);
%  
%     save('pump_schedule_' num2str(i) '.txt','pattern','-ascii')
% %     status = dos('COPY pump_schedule.txt pump_schedule.pat')
% end


if no_pumps == 1
  schedule = [ 0; 0;  schedule];% add lines epanet ignores  
save('pump_schedule.txt','schedule','-ascii')
status = dos('COPY pump_schedule.txt pump_schedule.pat')
end