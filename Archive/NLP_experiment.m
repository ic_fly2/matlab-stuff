clear all
% %% quadratic
% % Objective
% H = eye(3);                 %Objective Function (min 0.5x'Hx + f'x)
% f = -[2 2 2]';                
% 
% % Linear Constraints
% A = [-1,1; 1,3];            %Linear Inequality Constraints (Ax <= b)
% b = [2;5];    
% lb = [0;0];                 %Bounds on x (lb <= x)
% 
% % Quadratic Constraint
% Q = [1 0; 0 1];             %Quadratic Inequality (x'Qx + l'x <= r)
% l = [0;-2];
% r = 1;
% 
% % Create OPTI Object
% Opt = opti('qp',H,f,'ineq',A,b,'lb',lb,'qc',Q,l,r)
% 
% % Solve the QCQP problem
% [x,fval,exitflag,info] = solve(Opt)
% 
% plot(Opt)

%% Non linear constraints :)
% % Objective
% fun = @(x) -x(2);    %Objective Function Vector (min f(x))
% 
% % Nonlinear Constraints
% text = '@(x) x(1)^3 - x(2)';
% nlcon = eval(text); %Nonlinear Equality Constraint
% nlrhs = 0;
% nle = 0;                              %Constraint type: -1 <=, 0 ==, 1 >=
% 
% % Aeq = [1 1];
% % beq = 1;
% 
% A = [-1 0;...
%      1  1];
% b = [0;...
%     1];
% 
% grad = @(x) [0,-1];
% jac  = @(x) [3*x(1)^2, -1];
% % Create OPTI Object
% Opt = opti('fun',fun,'nlmix',nlcon,nlrhs,nle,'ndec',2,'A',A,'b',b,'grad',grad,'jac', jac);
%  
% % Solve the NLP problem
% x0 = [ 1.259;2]; %Initial Guess does not have to satisfy the constraints!!
% [x,fval,exitflag,info] = solve(Opt,x0)
% plot(Opt)

%% new thing
fun = @(x) x(1) + x(2) + x(3) + x(4) + x(5)+ x(6)+ x(7)+ x(8)+ x(9)+ x(10)+ x(11);    %Objective Function Vector (min f(x))
f = ones(11,1);
a = [1 2 3 2 4 3 8 8 2 1 2];
b = [2 6 4 2 3 2 4 3 8 5 1];
pile = [];
max_len = 33; 
for i = 1:length(a)
    if i <= length(a)-1
        text = ['a(' num2str(i) ')*x(' num2str(i) ')^3 - b(' num2str(i+1) ')*x(' num2str(i+1) ');'];
    else 
        text = ['a(' num2str(i) ')*x(' num2str(i) ')^3 - b(1)*x(1)]   '];
    end
%     pile = [pile text];
    spacing_arg = ['%-', num2str(max_len),'s'];
    padded_string = sprintf(spacing_arg, text);
    pile =  [pile ; padded_string];
    cell_test{i} = text;
end
pile = ['nlcon = @(x)  [ '   strjoin(cell_test) ];
% pile = [ sprintf(['%-', num2str(max_len),'s'],'nlcon = @(x)  [');pile];
fileID = fopen('nlcon_m.m','w'); 
fprintf(fileID,'%s\n' ,pile')
fclose(fileID);
nlcon_m
% fileID = fopen('exp.txt','w');
% fprintf(fileID,'%6s %12s\n','x','exp(x)');
% fprintf(fileID,'%6.2f %12.8f\n',A);
% fclose(fileID);
%    [T] = evalc('disp(pile)');
% nlcon = @(x) [a(1)*x(1)^2 - b(2)*x(2);...      
% a(2)*x(2)^2 - b(3)*x(3);     ... 
% a(3)*x(3)^2 - b(4)*x(4);      ...
% a(4)*x(4)^2 - b(5)*x(5);    ...  
% a(5)*x(5)^2 - b(6)*x(6);    ...  
% a(6)*x(6)^2 - b(7)*x(7);      ...
% a(7)*x(7)^2 - b(8)*x(8);    ...  
% a(8)*x(8)^2 - b(9)*x(9); ...     
% a(9)*x(9)^2 - b(10)*x(10);  ...  
% a(10)*x(10)^2 - b(11)*x(11);...
% a(11)*x(11)^2 - b(1)*x(1)]; 
% eval('def_nlcon')

% for i = 1: length(a)
%     Q{i,1} = zeros(11);
%     Q{i}(i,i) = a(i);
%     
%     Qneg{i,1} = zeros(11);
%     Qneg{i}(i,i) = -a(i);
% end
% Q = [Q;Qneg];
% for i = 1: length(a)
%     l{i,1} = zeros(11,1);
%     l{i,1}(i) = b(i);
%     
%     lneg{i,1} = zeros(11,1);
%     lneg{i,1}(i) = -b(i);
% end
% l = [l;lneg];

% a = [1 2  3];
% b = circshift(a,[0 -1]);
% all_num = [a;a;b;b];
% 
% argument = sprintf('a(%i)*x(%i)^3 - b(%i)*x(%i)\r\n',all_num)

%  a = [1 2  3];
% b = [2 6 4 ]; 
% % this is stupid

%  nlcon = @(x) eval(argument);
nlrhs1 = zeros(11,1);    
nle1   = zeros(11,1); 
% blub = ['(x)' '[' eval('pile') ']' ];
% nl_con = str2func(blub);
% nl = @(x) [a(1)*x(1)^3 - b(2)*x(2);a(2)*x(2)^3 - b(3)*x(3);a(3)*x(3)^3 - b(4)*x(4);a(4)*x(4)^3 - b(5)*x(5);a(5)*x(5)^3 - b(6)*x(6);a(6)*x(6)^3 - b(7)*x(7);a(7)*x(7)^3 - b(8)*x(8);a(8)*x(8)^3 - b(9)*x(9);a(9)*x(9)^3 - b(10)*x(10);a(10)*x(10)^3 - b(11)*x(11);a(11)*x(11)^3 - b(1)*x(1);];

% nlcon2 = str2func(pile');

nlrhs2 = 1;    
nle2   = 0; 

A = [-1 0 3 0 0 1 0 0 0 4 0 ;...
     1  1 4 0 3 0 -1 0 1 0 0];
b = [1;...
     1];
 x0 = ones(11,1)*0;
%   opts = optiset('maxnodes', 100000,'display','iter','tolrfun',1e-06,'tolafun',1e-06);
opts = optiset('solver','SCIP','maxnodes', 100000,'display','iter','tolrfun',1e-06,'tolafun',1e-06);

  % Opt = opti('f',f,'qc',Q,l,[nlrhs1; -nlrhs1],'A',A,'b',b,'x0',x0,'opts',opts)%,'grad',grad,'jac', jac);
Opt = opti('fun',fun,'nlmix',nlcon,nlrhs1,nle1,'ndec',11,'A',A,'b',b,'x0',x0,'opts',opts)%,'grad',grad,'jac', jac);
% Opt = opti('fun',fun,'nlmix',S,nlrhs,nle,'ndec',3,'A',A,'b',b);%,'grad',grad,'jac', jac);
tic;
 [x,fval,exitflag,info] = solve(Opt);
 toc;
% % Solve the NLP problem
% x0 = [ 0;0;0]; %Initial Guess does not have to satisfy the constraints!!
% [x,fval,exitflag,info] = solve(Opt,x0)
% % plot(Opt)
% %% MIQCQP
% % 
% % Objective
% H = [zeros(4)];  %Objective Function (min 0.5x'Hx + f'x)
% f = -[0 0 1 0]';                
% 
% % Linear Constraints
% % A = [-1,1; 1,3];            %Linear Inequality Constraints (Ax <= b)
% % b = [2;5];    
% % lb = [0;0];                 %Bounds on x (lb <= x)
% 
% Aeq = [1 0 0 0;
%        0 1 0 0];
% beq = [5; 1] ; % enforce a head difference
% 
% 
% 
% % Quadratic Constraints
% a = 1;
% b = 1;
% M = 99;
% 
%             % Q
%             % identity matix in flow parts where a is q^2 coefficient
%             i = 1;
%             for j = 1:N
%                 q_a = zeros((N*np));
%                 q_a((i-1)*N+j,(i-1)*N+j) =  a(i);
%                 %             q((i-1)*N+1:(i-1)*N+N,(i-1)*N+1:(i-1)*N+N) = a(i);
%                 
%                 % quadratic part:                            T               h                            q        lambda
%                 Q{1,j,pipe_quad_number} = [ zeros(N*sum(n_pumps)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... %T
%                     zeros(size(connection_matrix,2)*N  ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... % h
%                     zeros(N*np                         ,N*sum(n_pumps)+ size(connection_matrix,2)*N) q_a zeros(N*np,size(lambda,2));... %q
%                     zeros(size(lambda,2)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2)); ];  %lambda
%                 
%                 Q{2,j,pipe_quad_number} = [ zeros(N*sum(n_pumps)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... %T
%                     zeros(size(connection_matrix,2)*N  ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... % h
%                     zeros(N*np                         ,N*sum(n_pumps)+ size(connection_matrix,2)*N) -q_a zeros(N*np,size(lambda,2));... %q
%                     zeros(size(lambda,2)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2)); ];  %lambda
%                 
%                 Q{3,j,pipe_quad_number} = [ zeros(N*sum(n_pumps)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... %T
%                     zeros(size(connection_matrix,2)*N  ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... % h
%                     zeros(N*np                         ,N*sum(n_pumps)+ size(connection_matrix,2)*N) q_a zeros(N*np,size(lambda,2));... %q
%                     zeros(size(lambda,2)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2)); ];  %lambda
%                 
%                 Q{4,j,pipe_quad_number} = [ zeros(N*sum(n_pumps)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... %T
%                     zeros(size(connection_matrix,2)*N  ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2));... % h
%                     zeros(N*np                         ,N*sum(n_pumps)+ size(connection_matrix,2)*N) -q_a zeros(N*np,size(lambda,2));... %q
%                     zeros(size(lambda,2)               ,N*sum(n_pumps)+ size(connection_matrix,2)*N + N*np +  size(lambda,2)); ];  %lambda
%                 
%                 
%                 j_driver = zeros(N,1); j_driver(j) =1;
%                 delta_h = place_matrix(j_driver,connection_matrix(i,:)',1);
%                 q_b = place_matrix(j_driver,flow_matrix(i,:)',1);
%                 %                  lambda = zeros(length_m*N,1);
%                 lambda = [];
%                 for k = 1:n_pipes
%                     if k == 2%pipe_number
%                         lambda_part = j_driver;
%                     else
%                         lambda_part =  zeros(lambda_parts_length(k)*N,1); % make zeros where ever it isn't needed
%                     end
%                     lambda = [lambda; lambda_part ]; % add it all together
%                 end
%                 
%                 l{1,j,pipe_quad_number} = [ zeros(N*sum(n_pumps),1);...
%                     -delta_h ; ...
%                     q_b*b(i); ...
%                     lambda*M];
%                 l{2,j,pipe_quad_number} = [ zeros(N*sum(n_pumps),1);...
%                     delta_h ; ...
%                     -q_b*b(i); ...
%                     lambda*M];
%                 l{3,j,pipe_quad_number} = [ zeros(N*sum(n_pumps),1);...
%                     -delta_h ; ...
%                     q_b*b(i); ... % b negative == b positive
%                     -lambda*M];
%                  l{4,j,pipe_quad_number} = [ zeros(N*sum(n_pumps),1);...
%                     delta_h ; ...
%                     -q_b*b(i); ...
%                     -lambda*M];
%                 
%                 r{1,j,pipe_quad_number} = M;
%                 r{1,j,pipe_quad_number} = M;
%                 r{1,j,pipe_quad_number} = 0;
%                 r{1,j,pipe_quad_number} = 0;
%             end
% % Q = {[0 0 0 0;... %Quadratic Inequalities (x'Qx + l'x <= r)
% %       0 0 0 0;...
% %       0 0 +a 0;...
% %       0 0 0 0]... 
% %       ...       
% %      [0 0 0 0;... 
% %       0 0 0 0;...
% %       0 0 -a 0;...
% %       0 0 0 0]... 
% %             ...       
% %      [0 0 0 0;... 
% %       0 0 0 0;...
% %       0 0 -a 0;... % revers sign for neg to pos for a_neg and b_neg :)
% %       0 0 0 0]... 
% %       ...       
% %      [0 0 0 0;... 
% %       0 0 0 0;...
% %       0 0 +a 0;...
% %       0 0 0 0]... 
% %       };
% %   
% % l = [[-1;...
% %        1;...
% %        b;...
% %        M] ...
% %        ...
% %      [ 1;...
% %       -1;...
% %       -b;...
% %        M] ...
% %        ...
% %      [-1;...
% %        1;...
% %       -b;...% revers sign for neg to pos for a_neg and b_neg :)
% %       -M] ...
% %        ...
% %      [ 1;...
% %       -1;...
% %        b;...
% %       -M] ...       
% %         ];
% % l{1} = [-1 1 b M]';
% % % l{} = {-1 1 b M}';
% %        
% % l{2} = [ 1;...
% %       -1;...
% %       -b;...
% %        M];
% %        
% % l{3} = [-1;...
% %        1;...
% %       -b;...% revers sign for neg to pos for a_neg and b_neg :)
% %       -M];
% %   
% % l{4} = [ 1;...
% %       -1;...
% %        b;...
% %       -M];
% %   
% % r = [M; M; 0; 0];
% 
% % Integer Constraints
% xtype = 'CCCI';
% 
% % Create OPTI Object
% 
% opts = optiset('solver','SCIP'); %'mosek'
% Opt = opti('qp',H,f,'qc',Q,l,r,'xtype',xtype,'options',opts,'eq',Aeq,beq);
% 
% % Solve the MIQCQP problem
% [x,fval,exitflag,info] = solve(Opt)
% 
% % plot(Opt)Q





    