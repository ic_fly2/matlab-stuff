% n coose k
clear all;
for n = 1:10;
for k = 0:n
    top = factorial(n);
    bottom = factorial(k)*factorial(n-k);
    result(k+1) = top/bottom;
end
cost(n) = sum(result);
end
savings_factor = cost./[1:n];
log_savings_factor = log(savings_factor);
x = [2:n+1];
plot(x,log(savings_factor))