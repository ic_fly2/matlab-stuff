clear all;
% % Objective
% H = [1 -1; -1  2];          %Objective Function (min 0.5x'Hx + f'x)
% f = -[2 6]';                
% 
% % Constraints
% A = [1 1; -1 2; 2  1];      %Linear Inequality Constraints (Ax <= b)
% b = [2;2;3];    
% 
% lb = [0;0];                 %Bounds on x (lb <= x)
% 
% % Integer Constraints
% xtype = 'BC';
% % Create OPTI Object
% Opt = opti('qp',H,f,'ineq',A,b,'lb',lb,'xtype',xtype,'solver','cplex')
% 
% % Solve the MIQP problem
% [x,fval,exitflag,info] = solve(Opt)

%% Defenition

% KPump =  [ -0.5 2;... % mx + c of defining inequalities
%            -1.5 3;...
%            -3   4];       % added for giggles
       KPump =  [ -0.5 2;... % mx + c of defining inequalities
           -1.5 3];    
Kpipe =  [ 0.5  0; ...
        2  -1.5]; %        3 -3 ];

m_Pu  = [KPump(:,1)']; % m part of stuff
m_pi  = [Kpipe(:,1)'];

c_Pu  = [KPump(:,2)']; % c part of stuff
c_pi  = [Kpipe(:,2)'];

nc_Pu = size(KPump,1); % number of curves
nc_pi = size(Kpipe,1);
    
N = 3; % number of intervals;
Delta_T = 24*60*60/N; % duratiion of one interval in seconds
Area = 500; % area of the resevoir;
M = 50000; %1000000; % large number

n_pumps = [2]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  
pump_number = 1; % counter to know which pump station work is being done on
pipe_number = 1; % counter to inform with pipe we are working on
A = [];
b = [];

%% some more system configs copied from previous files
% %defs:
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;
% demand_multiplier = 1;
% %electricity pricing
% Electricity_Price(1:11) = 28.59; Electricity_Price(12:19) = 85.76; Electricity_Price(20:33) = 48.49; Electricity_Price(34:40) = 85.76; Electricity_Price(41:44) = 48.49; Electricity_Price(45:48) = 28.59;
% Pe = Electricity_Price;
% Pe = strech(Pe,price_strech);
% % demand
% % demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
% demand = importdata('Peak_le.txt'); % coice of demand pattern
% d = demand.data;
% d = d/mean(d)*demand_multiplier; % % misses the sizing
% d = d';%m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!
% d = strech(d,demand_strech);
% % resevoir
% % D = 500; %diameter of the resevoir
% % Area = pi*(D/2)^2; %m2
% V_res = max(d)*res_time*60*60* 12; %resevoir volume in terms of hours of suppliable peak demand. 
% Area = V_res/(h_max - h_min); % area required to provide this as function of the resevoir range. 
% 
% % res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

% pseudo just to get it going
Pe = rand(1,N);
n_demands = 1;
d  = rand(n_demands,N);
switch_penalty = 0.001;

% bounds
T_bounds = [0 1];
h_min = [0 0 0]; % max and min values of the hea dat the nodes
h_max = [10 10 10];
Qmax = 10; % there is no bound on q for now jsut make it large
q_bounds = [-Qmax Qmax] ; %can have any value
lambda_bounds = [0 1];


%% new method
% define connections (from epanet)
% connection_matrix = [...
%     1 -1 0 0;... %q1
%     0 1 -1 0;... %q2
%     0 0 1 -1;... %q3
%     0 1 0 -1 ;...  %q4
%     0 0 0 1]; % demand out

connection_matrix = [...
    1 -1 0;... %q1
    0 1 -1;... %q2
    0 0  1]; % demand out

connection_type = {... 
    'Pump', ...
    'pipe',...
    'demand'}; 

% connection_type = {... 
%     'Pump', ...
%     'pipe',...
%     'pipe',...
%     'pipe',...
%     'demand'}; 

% some system checks to avoid chasing ghosts

if length(h_min) ==  length(h_max) && length(h_max) == size(connection_matrix,2) 
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number  of nodes and node bounds don''t match')
end   
if size(connection_matrix,1) ==  size(connection_type,2)
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number and types of connections don''t match')
end   

if sum(strcmp(connection_type,'demand')) ==  n_demands
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number of demands and demand nodes specified don''t match')
end 


n_pipes = length(strmatch('pipe', char(connection_type))); % number of pipes (needed for lambda)
driver_pipes(place_matrix(1,strmatch('pipe', char(connection_type)),1)) = 1; % driver for

% info about the system
location_res = [3]; 
location_fix = [1];
fixed_val = [1];
% little check
if length(location_fix) == length(fixed_val)
    %some thing nice could go here such as disp('well done')
else
    error('Inconsistent system defenition: location and values of vixed locations don''t match')
end


%% Inequality matrix
[np, nn] = size(connection_matrix); % np ~= n_pipes as np is number of conncetions and n_pipes is just pipes
flow_matrix = eye(np); % assuming it is always the identety then this would work 
for i = 1:np
    A_part = []; % empty that one 
    b_part = [];
    switch connection_type{i}
        case 'Pump' %
            %% A 
            % driver_pumps = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement 
            driver_pumps = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
            end
            %needs to modified to make sure it has the size of the maximum and only fills the parts that will be used
            for j = 1:n_pumps(pump_number)
            T = place_matrix(-M*eye(N),driver_pumps(j,:),2);
            h = place_matrix(eye(N),-connection_matrix(i,:),2);
            q = place_matrix(eye(N)*m_Pu(j),flow_matrix(i,:),2); % not so sure
            lambda = zeros(N,N*n_pipes); % positive or negative flow in pipe
            A_part = [A_part; T h q lambda]; % add a new line
            % b
%             b_part1(1:N) = c_Pu(j) ;
%             b_part = [b_part b_part1];
            
            end
            %% b 
            % just add the compomnent
            %           add this below here: *n_pumps(pump_number)  if it doesn't work
            b_part = place_matrix(ones(1,N  ),c_Pu(1:n_pumps(pump_number)),2); % make a long row of all Cs
            b = [b b_part];
            pump_number = pump_number + 1; % increase the pump station counter

            
            % alternativly add gleixner style cnstraints to enforce a flow
            % and set a maximum head. Possibly enforce it as well through a
            % lower bound M constraint too.
            
        case 'pipe'  
            %% pipes part
            % A
            % positive flow
            T = zeros(N*nc_pi,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
            h = place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(nc_pi,1),1);
            q = place_matrix(place_matrix(eye(N),m_pi,1),flow_matrix(i,:),2); % not so sure
            driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector
            lambda = place_matrix(place_matrix(-M*eye(N),ones(nc_pi,1),1),driver_pipes,2); % positive or negative flow in pipe
            A_part = [T h q lambda];
                       
            % b
            b_part = place_matrix(ones(1,N),-c_pi - M,2);
            b = [b b_part];
            pipe_number = pipe_number + 1;            
            
            
            %% lambda part
            % A
            T = zeros(N*2,N*sum(n_pumps));
            h = place_matrix(place_matrix(eye(N),connection_matrix(i,:),2),[1 -1],1);
            q = place_matrix(place_matrix(eye(N),[ 0 0],1),flow_matrix(i,:),2); % 
            lambda = place_matrix(place_matrix(M*eye(N),[-1 1 ],1),driver_pipes,2); % positive or negative flow in pipe
            A_part = [A_part; T h q lambda];
            %b
            
            b = [b zeros(1,N) ones(1,N)*M ]; % b part for the lambda section 
%             
%             b = [b zeros(1,N) ones(1,N)*M zeros(1,N)]; % b part for the lambda section 
            
        case 'demand'
            disp('lol') 
        otherwise
            error(['Unknown system definition in connection' num2str(1)])
    end
    % make A from components
    A = [A; A_part]; 
end  


%% equality matrix
% mass balance for the nodes
T = zeros(N*nn,N*sum(n_pumps)); 
h = zeros(N*nn,N*nn);
%q
q_part = [];
q = [];
for i = 1:nn 
    %      in(i,:) = connection_matrix(:,i) == 1;
    %      out(i,:)= connection_matrix(:,i) ==-1;
    flows = transpose(connection_matrix)*-1; % same as in out above just faster 
    if i == location_fix
        q_part = zeros(N,N*np);
    elseif i == location_res %need to change h as well
        h((i-1)*N+1:i*N,(i-1)*N+1:i*N) = -eye(N)*Area/Delta_T; % mass balance term for reservoirs
        q_part = place_matrix(eye(N),flows(i,:),2);
    else
        q_part = place_matrix(eye(N),flows(i,:),2);
    end
    q = [q; q_part]; 
end 
lambda = zeros(N*nn,N*n_pipes);
Aeq = [T h q lambda];
beq =  zeros(nn*N,1); %all zero as all mass balances

% Add section to fix locations of fixed head
for i = length(location_fix)
    driver = zeros(1,nn); driver(i) = 1; 
    Aeq = [Aeq; zeros(N,N*sum(n_pumps)) place_matrix(eye(N),driver,2) zeros(N,N*np) zeros(N,N*n_pipes) ];
    beq = [beq; fixed_val(i)*ones(N,1)];
end

% add demand flow
% probably also needs loop and better demand node defenitions as ther emay
% be several demands
% driver = strcmp(connection_type,'demand') ; %only works for one demand
j = 1; % demand counter
for i = find(strcmp(connection_type,'demand') == 1)
    driver = zeros(1,np); driver(i) = 1;
    Aeq = [Aeq; zeros(N,N*sum(n_pumps)) zeros(N,N*nn) place_matrix(eye(N),driver,2)  zeros(N,N*n_pipes) ];
    beq = [beq; d(j,:)'];
    j = j+1;
end

% checks
size(A) 
size(b) %needs transposing

size(Aeq) 
size(beq)



% To do:%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Objective
% Objective Function (min 0.5x'Hx + f'x)

% electricty price for all N adds up for all
% linear cost part
driver = [];
f = []; % pricing for pump operations 
for i = 1:length(n_pumps)
     driver{i} = ones(1,n_pumps(i));
     cost = place_matrix(Pe,driver{i},2); % possibly vary Pe if it is defferent for different pumps
     f = [f cost]; 
end
% now the rest is zeros
f = [f zeros(1,N*nn + N*np +N*n_pipes)];

% H % assume cost of switching are the same for every pump
H = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
% zero every where else 
H(N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*n_pipes,N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*n_pipes ) = 0; 
H = H*switch_penalty;


%% bounds
% lb
% ub

lb = [ repmat(T_bounds(1),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_min,2) repmat(q_bounds(1),1,N*np) repmat(lambda_bounds(1),1,N*n_pipes) ];
ub = [ repmat(T_bounds(2),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_max,2) repmat(q_bounds(2),1,N*np) repmat(lambda_bounds(2),1,N*n_pipes) ];

% Integer Constraints
pumps_int(1:N*sum(n_pumps))  = 'I'; % could be B for binary or I for intiger
lambda_int(1:N*n_pipes) = 'I';
cont(1:N*nn + N*np) = 'C';
xtype = strcat([pumps_int cont lambda_int])   ;
% xtype = 'II';               %x1 & x2 are Integer

% Create OPTI Object
% Create OPTI Object
Opt = opti('qp',H,f,'ineq',A,b,'eq',Aeq,beq,'lb',lb,'ub',ub,'xtype',xtype);

% Solve the MIQP problem
[x,fval,exitflag,info] = solve(Opt);

% make pretty result matrix
% note quite working
header_pumps = repmat('Pump on?  ',[1,n_pumps]);
header_head  = repmat('head (m)  ',[1,nn]);
header_flow  = repmat('Flow (m3s)',[1,np]);
header_lambda= repmat('Lambda    ',[1,n_pipes]);
data = reshape(x,[N length(x)/N ]); %Make it pretty
disp(['  ' header_pumps header_head header_flow header_lambda]);disp(data)
% following approach needs statistics toolbox:
% header = {'Quarter', 'monthly amount remaining', 'annual amountremaining'}
% data =    [ 1          30000    150000;
%     2        20000    130000];
% ds = dataset({data,header{:}});
% for excel : [header;num2cell(data)]

%% old stuff
% %% b
% pump_number = 1;
% pipe_number = 1;
% connection_number = 1;
% b = [];
% for i = 1:np
%     if location_fix == connection_number;
%     switch connection_type(i,:)
%         case 'Pump' 
%             b_part = h1;
%             pump_number = pump_number + 1;
%         case 'pipe'
%             b_part
%             pipe_number = pipe_number + 1;
%         otherwise
%     end
%     b = [b b_part];
% end


% %% A
% % pumps section (np*N rows) 1 --> 2
% pumps_T = -M*eye(N*np); 
% pumps_h = [repmat(eye(N),np,1) zeros(N*np,N)]; 
% pumps_q = [-m_PU(1)*eye(N) zeros(N)]; 
%     for i = 2:nc_Pu
%         pumps_q = [pumps_q ; -m_PU(i)*eye(N) zeros(N)];
%     end
% pumps_q = [pumps_q zeros(N*np,N)];  
% pumps_L = zeros(N*np,N);
% 
% % pipe section 2 --> 3 
% pipes_T = zeros(2*N,N*np);
% pipes_h = [repmat(eye(N),np,1) repmat(-eye(N),np,1)];
% pipes_q = m_pi(1)*eye(N); 
%     for i = 2:nc_pi
%         pipes_q = [pipes_q m_pi(i)*eye(N)];
%     end
% pipes_q = [zeros(N*np,N) pipes_q];
% pipes_L = repmat(M*eye(N),np,1);
% 
% % lambda selection -M(1-L) <= h2- h3 and h2 - h3 <= M*L and q23 <= M*L 
% lamda_T = zeros(3*N,np*N);
% lamda_h = [eye(N) - eye(N);-eye(N) eye(N); zeros(N,2*N)];
% lamda_q = [zeros(2*N); zeros(N) eye(N)];
% lamda_L = [-M*eye(N); M*eye(N); -M*eye(N)];
% 
% % mass balance
% diff = eye(N) + diag(-ones(N,1),-1);
% mass_pipes = [repmant(zeros(N),1,np) repmant(zeros(N),1,np) eye(N) -eye(N) zeros(N)];
% mass_res = [repmant(zeros(N),1,np) repmant(zeros(N),1,np) eye(N) -eye(N) zeros(N)];
% 
% %% b




        
    