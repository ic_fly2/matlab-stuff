%DR vs standard operation comparison
% STOR makes 30000� per year per MW
% FFR makes 60000� per year per MW
clear all; close all;
%% number of pumps comparison and demand multiplier
% switch_penatly = 0.00001;
% maxtime = 20;
% demand_modifier = linspace(180,800,15);

% tic;
demand_modifier = [0.5 0.75 1 1.25 1.5 1.75];

pump_curves = [   -31.3873         0  279.6500;...
                  -62.7746         0  359.3000;...
                  -94.1620         0  438.9500;...
                 -125.5493         0  518.6000;...
                 -156.9366         0  598.2500];
           
system_curves = [  3.0995         0  192.1346;...
                   8.5746         0  178.2406;...
                   11.5053         0  170.8035;...
                   13.7087         0  165.2121;...
                   15.5215         0  160.6118];

res_time = [ 6 9 12];
price_strech = [0.8 1 1.2];
demand_strech = [0.8 1 1.2];

             
 for p = 1:length(pump_curves )
     K_P = pump_curves(p,:);
     for s = 1:length(system_curves )
         K_S = system_curves(s,:);
         for r = 1:length(res_time) % pumps
             for d = 1:length(demand_modifier)
                 for ps = 1:length(price_strech)
                     for ds = 1:length(demand_strech)
                         try
                             [costdr,outputmidr,schedulesdr,flagdr] = opt_v11_dr(demand_modifier(d),K_P,K_S,res_time(r),price_strech(ps),demand_strech(ds));
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).costdr = costdr
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).qualitydr = outputmidr.cplexstatus;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).schedulesdr = schedulesdr;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).flagdr = flagdr;
%                              disp(Quality(i,d))
                         catch
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).costdr = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).qualitydr = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).schedulesdr = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).flagdr = -1;
                             costdr = NaN
                         end
                         if isnan(costdr)
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).cost = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).quality = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).schedules = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).flag = -2;
                             cost = NaN;
                         else
                         try
                             [cost,outputmi,schedules,flag] = opt_v11_std(demand_modifier(d),K_P,K_S,res_time(r),price_strech(ps),demand_strech(ds));
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).cost = cost;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).quality = outputmi.cplexstatus;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).schedules = schedules;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).flag = flag;
%                              disp(Quality(i,d))
                         catch
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).cost = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).quality = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).schedules = NaN;
                             Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).flag = -1;
                             cost = NaN;
                         end
                         end
                                                 
                         diff = cost - costdr;
                         Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff = diff;
                         save('Result3.mat','Result')
                     end
                 end

             end
         end
         
%          %% year cost:
%          yr_cost = cost*365;
%          yr_cost_dr = costdr*365;
%          yr_cost(yr_cost == 0) = NaN;
%          yr_cost_dr(yr_cost_dr == 0) = NaN;
%          diff = yr_cost - yr_cost_dr;
%          results.pumps(p).sys(s).res = diff;
     end
      
 end
% disp(p)
% diff(diff == 0) = NaN;
% STOR = diff + 30000;
% FFR  = diff + 60000;
% figure;
% contourf(FFR)
% figure;
% contourf(STOR)