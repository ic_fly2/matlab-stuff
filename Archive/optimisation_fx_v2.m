%% set pump schedule and outflow demands
clear all;

% Create new or run old network definition
% network_definition;

%% Initialisation

%Network topology
load('Nodes_14_12_2013.mat');

pump_def_C = 5000;
plot_on = 0;% turn ploting on with 1
N = 24;% more detailed node info needed for larger N (serious modification of network_definition.m needed)
Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit
no_pumps = 5;

%electricity pricing
Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
Pe(1:24) = Electricity_Price(1:2:48);

% resevoir
Area = 500;
h0 = 10;
h_max = 50;
h_min = 10;
resevoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping repeatedly
%MILP vars
%b
b(1) = -h0; b(2:N+1) = 1;
b = b'; % for cplex
%A
clear A
A(1,N*no_pumps+N) = 0; A(1,N*no_pumps+N) = -1;
% ad limit on what pump switches are allowed
limit = eye(N);
i = 1;
while i <= no_pumps-1
limit = [limit eye(N)];
i = i+1;
end
A = [A ; limit zeros(N)];
% c
h = zeros(N,1);
i = 1;
c = Pe;
while i <= no_pumps-1
c = [c Pe*(i+1)];
i = i+1;
end
c = [c h']'; % added transpose for cplex

%H % pricing for switching (make this code more elegant)
H = eye(N*no_pumps);
for i = 1:N*no_pumps-1
   H(i,i+1) = - 0.5;
  % H(i+1,i)= - 0.5;
end
j = 2;
multiple(1:N,1) = 1;
while j <= no_pumps
    add(1:N,1) = j;
  multiple = [multiple; add];
  j = j+1;
end
for i = 1:N*no_pumps
   H1(i,:)=  multiple(i,:) .* H(i,:); 
end
H = H1;
H(N*no_pumps+1:N*(no_pumps+1),N*no_pumps+1:N*(no_pumps+1)) = 0;
H = H +H'; % ensure symetry
%number of intigers
M(1:N*no_pumps) = 1:N*no_pumps;

%% loop
iter = 1;
max_iter = 5;
err = 21;
while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached

%% Curve fitting / Hydraulic solver
% regressoin model fit polynomials to flow and find operating points
for t = 1:N;
%% compute pump settings and outflows
system_curve_builder_v3;
operating_point_finder_v2;
Qp(t,:) = Q_operation;
end


%% MILP
% create matracies for LP
%Aeq
j = 1;
Aeq =[];
while j <= no_pumps
for i = 1:N 
    flow(i,:,j) = Qp(:,j)';
end
Aeq = [Aeq tril(flow(:,:,j))];
j = j + 1;
end
Aeq = [Aeq -eye(N)];

% %A
% clear A
% A(1,N*no_pumps+N) = 0; A(1,N*no_pumps+N) = -1;
% % ad limit on what pump switches are allowed
% limit = eye(N);
% i = 1;
% while i <= no_pumps-1
% limit = [limit eye(N)];
% i = i+1;
% end
% A = [A ; limit zeros(N)];

%beq
for t = 1:N
d(t) = sum(Node(t+1,2,:));
end

for k = 1:N
    beq(k) = sum(d(1:k))/Area - h0;
end
beq = beq'; % cplex added
% %b
% clear b
% b(1) = -h0; b(2:N+1) = 1;
  
%bounds
ub(1:N*no_pumps) = 1;
ub(N*no_pumps+1:N*(no_pumps+1)) = h_max;
lb(1:N*no_pumps) = 0;
lb(N*no_pumps+1:N*(no_pumps+1)) = h_min;

lb = lb';
ub =ub';

% % c
% h = zeros(N,1);
% c = [Pe 2*Pe h'];

% error allowed
e=2^-24;
% optimize
% tic




%% mystuff
int(1:N*no_pumps) = 'I';
cont(1:N) = 'C';
  ctype = strcat([int cont])   ;
%    options = cplexoptimset;
%    options.Display = 'on';
%[x, fval, exitflag, output] = cplexlp(c,A,b,Aeq,beq,lb, ub);%sum(X'.*c)
% toc
% tic
%Z=cplexmilp(c,A,b,Aeq,beq, [ ], [ ], [ ],lb,ub,ctype);%sum(Z'.*c)
Y = cplexmiqp(H,c,A,b,Aeq,beq,[],[],[],lb,ub,ctype);
% toc
% tic another MILP solver also try CPLEX
% [Y,val,status]=IP(c,A,b,Aeq,beq,lb,ub,M,e);sum(Y'.*c)
% toc
%type
% find resulting height of water tank
resevoir_level = Z(length(Z)-N:length(Z)-1); resevoir_level(1) = h0;
err = sum(abs(res_old - resevoir_level'));
if iter <= max_iter % leave last result for analysis if no convergence occurs
res_old = resevoir_level';
else
end
iter = iter +1;
end
X