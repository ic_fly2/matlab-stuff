%% set pump schedule and outflow demands
clear all;

% Create new or run old network definition
% network_definition;
%% Initialisation
load('Nodes_14_12_2013.mat');
plot_on = 0;% turn ploting on with 1
N = 24;% more detailed node info needed for larger N (serious modification of network_definition.m needed)

%% Curve fitting / Hydraulic solver
for T = 1:N;
%% compute pump settings and outflows
system_curve_builder_v3;
operating_point_finder_v2;
% pump_set = pump_schedule(T)+1; 
% result(1,T) =tank_flow(pump_set);
% result(2,T) =power(pump_set);
% result(3,T) =pwr_loss(pump_set);
% Tank_elevation_hist(T) = Tank_elevation; 
Qp(T,:) = Q_operation;
end

% minimise cost
cost = result(2,:).*electricity_price;
cost_total = sum(cost);
disp(cost_total)
% subject to 
% Tank_elevation_hist(1) = Tank_elevation_hist(24) 
% same as
% cumsum(result(1,T)) = 0
% plus the above stuff