%% result analysis
clear all
%% load file 
load('Result2.mat');
demand_modifier = [0.5 0.75 1 1.25 1.5 1.75];

pump_curves = [   -31.3873         0  279.6500;...
                  -62.7746         0  359.3000;...
                  -94.1620         0  438.9500;...
                 -125.5493         0  518.6000;...
                 -156.9366         0  598.2500];
             grad = [-100  -200  -300  -400  -500];
           
system_curves = [  3.0995         0  192.1346;...
                   8.5746         0  178.2406;...
                   11.5053         0  170.8035;...
                   13.7087         0  165.2121;...
                   15.5215         0  160.6118];
               
               Coefficients = linspace(0.05,1.5,5);
               

res_time = [ 6 9 12];
price_strech = [0.8 1 1.2];
demand_strech = [0.8 1 1.2];

[~,sizePC] = size(Result.PumpCurve);
[~,sizeSC] = size(Result.PumpCurve(1).SysCurve);
% [~,sizeSC] = size(Result.PumpCurve(sizePC).SysCurve);
[~,sizeRS] = size(Result.PumpCurve(1).SysCurve(1).ResSize);
[~,sizeUR] = size(Result.PumpCurve(1).SysCurve(1).ResSize(1).UtilRate);
[~,sizePS] = size(Result.PumpCurve(1).SysCurve(1).ResSize(1).UtilRate(1).PS);
[~,sizeDS] = size(Result.PumpCurve(1).SysCurve(1).ResSize(1).UtilRate(1).PS(1).DS);

total_PC(2,1:sizePC)=0;k_pc = 1;
total_SC(2,1:sizeSC)=0;k_sc = 1;
total_RS(2,1:sizeRS)=0;k_rs = 1;
total_UR(2,1:sizeUR)=0;k_ur = 1;
total_PS(2,1:sizePS)=0;k_ps = 1;
total_DS(2,1:sizeDS)=0;k_ds = 1;

 %% demand strech
for p = 1:sizePC
    for s = 1:sizeSC
        for r = 1:sizeRS
            for d = 1:sizeUR % demand and utilisation rate
                for ps = 1:sizePS
                    for ds = 1:sizeDS 
                        if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                            total_DS(1,ds) = total_DS(1,ds);
                        else
                            total_DS(1,ds) = total_DS(1,ds)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                            total_DS(2,ds) =  total_DS(2,ds) +1;
                            DS_list(k_ds,ds) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                        end
                        
                     end
                    k_ds = k_ds +1;
                end
               
            end
              
        end
    end
end
 av_DS = total_DS(1,:)./ (total_DS(2,:)-1); % minus 1 is dubious :S
 [p_DS,S_DS] = polyfit(demand_strech,av_DS,1);

 %% Price strech
 for p = 1:sizePC
    for s = 1:sizeSC
        for r = 1:sizeRS
            for d = 1:sizeUR % demand and utilisation rate
                for ds = 1:sizeDS
                    for ps = 1:sizePS
                        if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                            total_PS(1,ps) = total_PS(1,ps);
                        else
                            total_PS(1,ps) = total_PS(1,ps)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                            total_PS(2,ps) =  total_PS(2,ps) +1;
                            PS_list(k_ps,ps) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                        end
                    end
                     k_ps = k_ps +1;
                end
            end
        end
    end
 end
 av_PS = total_PS(1,:)./ (total_PS(2,:)-1); % minus 1 is dubious :S
 [p_PS,S_PS] = polyfit(price_strech,av_PS,1);
 
 
 %% Utilisation Rate
 for p = 1:sizePC
     for s = 1:sizeSC
         for r = 1:sizeRS
             for ds = 1:sizeDS
                 for ps = 1:sizePS
                     for d = 1:sizeUR % demand and utilisation rate
                         if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                             total_UR(1,d) = total_UR(1,d);
                         else
                             total_UR(1,d) = total_UR(1,d)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                             total_UR(2,d) =  total_UR(2,d) +1;
                             UR_list(k_ur,d) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                         end
                     end
                     k_ur = k_ur+1;
                 end
             end
         end
     end
 end
   av_UR = total_UR(1,:)./ (total_UR(2,:)-1); % minus 1 is dubious :S
 [p_UR,S_UR] = polyfit(demand_modifier,av_UR,1);
 
  %% Resevoir size
 for p = 1:sizePC
     for s = 1:sizeSC
         for ds = 1:sizeDS
             for ps = 1:sizePS
                 for d = 1:sizeUR % demand and utilisation rate
                     for r = 1:sizeRS
                         if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                             total_RS(1,r) = total_RS(1,r);
                         else
                             total_RS(1,r) = total_RS(1,r)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                             total_RS(2,r) =  total_RS(2,r) +1;
                             RS_list(k_rs,r) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                         end
                     end
                     k_rs = k_rs+1;
                 end 
             end
         end
     end
 end
   av_RS = total_RS(1,:)./ (total_RS(2,:)-1); % minus 1 is dubious :S
 [p_RS,S_RS] = polyfit(res_time,av_RS,1);
 
 
 
   %% System Curve
   for p = 1:sizePC     
       for ds = 1:sizeDS
           for ps = 1:sizePS
               for d = 1:sizeUR % demand and utilisation rate
                   for r = 1:sizeRS
                       for s = 1:sizeSC
                           if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                               total_SC(1,s) = total_SC(1,s);
                           else
                               total_SC(1,s) = total_SC(1,s)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                               total_SC(2,s) =  total_SC(2,s) +1;
                               SC_list(k_sc,s) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                           end
                       end
                       k_sc = k_sc+1;
                   end
               end
           end
       end
   end
   av_SC = total_SC(1,:)./ (total_SC(2,:)-1); % minus 1 is dubious :S
 [p_SC,S_SC] = polyfit(Coefficients,av_SC,1);
 
 
    %% Pump Curve
  
    for ds = 1:sizeDS
        for ps = 1:sizePS
            for d = 1:sizeUR % demand and utilisation rate
                for r = 1:sizeRS
                    for s = 1:sizeSC
                        for p = 1:sizePC
                            if isnan(Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff)
                                total_PC(1,p) = total_PC(1,p);
                            else
                                total_PC(1,p) = total_PC(1,p)+ Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                                total_PC(2,p) =  total_PC(2,p) +1;
                                PC_list(k_pc,p) = Result.PumpCurve(p).SysCurve(s).ResSize(r).UtilRate(d).PS(ps).DS(ds).diff;
                            end
                        end
                        k_pc = k_pc+1;
                    end
                end
            end
        end
    end
 av_PC = total_PC(1,:)./ (total_PC(2,:)-1); % minus 1 is dubious :S
 [p_PC,S_PC] = polyfit(grad,av_PC,1);
 
 factors = [p_PC(1) p_SC(1) p_RS(1) p_UR(1) p_PS(1) p_DS(1)];
 S = [S_PC.normr S_SC.normr S_RS.normr S_UR.normr S_PS.normr S_DS.normr];
 numbers = 1:length(S);
 array2latex([numbers' factors' S'])
 
 
 
 
 
 
 