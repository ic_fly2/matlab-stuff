%% plotting differences
clear all;
 load('C:\Users\rmm08\documents\MATLAB\matlab.mat')
for i = 1:length(results.pumps)
    for j = 1:length(results.pumps(i).sys);
        X = results.pumps(i).sys(j).res;
        [v(i,j,:), ind(i,j,:)] = max(X);
        [v1(i,j),ind1(i,j)]=max(max(X));
        
        c(i,j) = ind1(i,j);
        r(i,j) = ind(i,j,ind1(i,j));
        val(i,j) = v1(i,j);  
    end
end