%% set pump schedule and outflow demands
clear all;clf
plot_on = 0;
% Create new or run old network definition
% network_definition;

%% Initialisation

%Network topology
% load('Nodes_14_12_2013.mat'); %test case
% Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit % test case

pump_def_C = 5000;
plot_on = 0;% turn ploting on with 1
N = 48;% more detailed node info needed for larger N (serious modification of network_definition.m needed)
no_pumps = 3;

%electricity pricing
Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
% Pe(1:24) = Electricity_Price(1:2:48); % test case
Pe = Electricity_Price;
% demand
x = 1:N;
d = sin(x/(pi*1.2))*500 + 800;%d =d*180;

% resevoir
Area = 500;
h0 = 15;
h_max = 50;
h_min = 10;
reservoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

%% prealocate some variables to avoid looping repeatedly
% MILP vars
%b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
b(1) = -h0; b(2:N+1) = 1;
b = b'; % for cplex
%A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear A
A(1,N*no_pumps+N) = 0; A(1,N*no_pumps+N) = -1;
% ad limit on what pump switches are allowed
limit = eye(N);
i = 1;
while i <= no_pumps-1
limit = [limit eye(N)];
i = i+1;
end
A = [A ; limit zeros(N)];
% c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
h = zeros(N,1);
i = 1;
c = Pe;
while i <= no_pumps-1
c = [c Pe*(i+1)];
i = i+1;
end
c = [c h']'; % added transpose for cplex

%H % pricing for switching (make this code more elegant) %%%%%%%%%%%%%%%%%%
H = eye(N*no_pumps);
for i = 1:N*no_pumps-1
   H(i,i+1) = - 0.5;
  % H(i+1,i)= - 0.5;
end
j = 2;
multiple(1:N,1) = 1;
while j <= no_pumps
    add(1:N,1) = j;
  multiple = [multiple; add];
  j = j+1;
end
for i = 1:N*no_pumps
   H1(i,:)=  multiple(i,:) .* H(i,:); 
end
H = H1;
H(N*no_pumps+1:N*(no_pumps+1),N*no_pumps+1:N*(no_pumps+1)) = 0;
H = H +H'; % ensure symetry
H = H*2; %price the switch
%number of intigers
M(1:N*no_pumps) = 1:N*no_pumps;

% beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for k = 1:N
    beq(k,1) = sum(d(1:k))/Area - h0;
end

%bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ub(1:N*no_pumps,1) = 1;
ub(N*no_pumps+1:N*(no_pumps+1),1) = h_max;
lb(1:N*no_pumps,1) = 0;
lb(N*no_pumps+1:N*(no_pumps+1),1) = h_min;
%% loop
iter = 1;
max_iter = 5;
err = 21;
while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached

%% Curve fitting / Hydraulic solver
% regressoin model fit polynomials to flow and find operating points
for t = 1:N;
%% compute pump settings and outflows
% system_curve_builder_v3;
% operating_point_finder_v2;
% Qp(t,:) = Q_operation;
test_case
end


%% MILP
% create matracies for LP
%Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
j = 1;
Aeq =[];
while j <= no_pumps
for i = 1:N 
    flow(i,:,j) = Qp(:,j)';
end
Aeq = [Aeq tril(flow(:,:,j))];
j = j + 1;
end
Aeq = [Aeq/Area -eye(N)]; % added /Area for good measure
  

%% solver % mystuff
int(1:N*no_pumps) = 'I';
cont(1:N) = 'C';
  ctype = strcat([int cont])   ;
%    options = cplexoptimset;
%    options.Display = 'on';
%[x, fval, exitflag, output] = cplexlp(c,A,b,Aeq,beq,lb, ub);%sum(X'.*c)
% toc
% tic
%Z=cplexmilp(c,A,b,Aeq,beq, [ ], [ ], [ ],lb,ub,ctype);%sum(Z'.*c)

% QP
[X, fval, exitflag, output] = cplexqp(H,c,A,b,Aeq,beq,lb,ub);
% figure;
if plot_on ==1
hold on
subplot(2,1,1); 
for i = 1:no_pumps
  pic(:,i) = X(N*(i-1)+1:N*i)*i;
  bar(pic,'stack')
end
subplot(2,1,2); 
plot(X(1+N*no_pumps:N*(1+no_pumps)))
else end

% MIQP
% if isempty(Y) == 1
% else
% Y_old = Y; 
% end
[Y, fvalmi, exitflagmi, outputmi] = cplexmiqp(H,c,A,b,Aeq,beq,[],[],[],lb,ub,ctype);
% in case of failure to find feasable solution
% if isempty(Y) == 1
%     disp('works')
% else
%     [Y, fvalmi, exitflagmi, outputmi] = cplexmiqp(H,c,A,b,Aeq,beq,[],[],[],lb,ub,ctype,Yold);
% end

if plot_on ==1
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack')
end

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)))

subplot(2,2,2);
bar(d,'DisplayName','Demand')
subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
else
end

% find resulting height of water tank
reservoir_level = Y((length(Y)-N+1):(length(Y))); %resevoir_level(1) = h0;
h(:,1) = reservoir_level;
err = sum(abs(res_old - reservoir_level'));
if iter <= max_iter % leave last result for analysis if no convergence occurs
res_old = reservoir_level';
else
end
iter = iter +1;
end
%% show schedule
figure;
subplot(2,2,1); 
for i = 1:no_pumps
 schedule(:,i) = Y(N*(i-1)+1:N*i)*i;
  bar(schedule,'stack');axis([0 48 0 3])
end

subplot(2,2,2);
bar(d,'DisplayName','Demand')
axis([0 48 0 2000])

subplot(2,2,3);
plot(Pe,'r');%bar(Pe,'r')
axis([0 48 0 100])

subplot(2,2,4); 
plot(Y(1+N*no_pumps:N*(1+no_pumps)));axis([0 48 h_min h_max])

%% perform DR

