clear all;
%% general declarations
N = 6; % number of intervals;
Delta_T = 24*60*60/N; % duratiion of one interval in seconds

M = 999; %1000000; % large number

pump_number = 1; % counter to know which pump station work is being done on
pipe_number = 1; % counter to inform with pipe we are working on
A = [];
b = [];
Aeq = [];
beq = [];

%% Network specific details 

% def_van_zyl_simple
% def_simple_network
def_simple_loop
% def_van_zyl_network


%% some more system configs copied from previous files
% %defs:
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;
% demand_multiplier = 1;
% %electricity pricing
% Pe = strech(Pe,price_strech);
% % demand
% % demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
% demand = importdata('Peak_le.txt'); % coice of demand pattern
% d = demand.data;
% d = d/mean(d)*demand_multiplier; % % misses the sizing
% d = d';%m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!
% d = strech(d,demand_strech);
% % resevoir
% % D = 500; %diameter of the resevoir
% % Area = pi*(D/2)^2; %m2
% V_res = max(d)*res_time*60*60* 12; %resevoir volume in terms of hours of suppliable peak demand. 
% Area = V_res/(h_max - h_min); % area required to provide this as function of the resevoir range. 
% 
% % res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';

% pseudo just to get it going
% Pe =  rand(1,N);%[0.5 0.9 0.6];

% d  =  rand(n_demands,N)*0.3; %[0.1 0.2 0.2];%[ 0.0509    0.0561    0.3352];%
%0.001;


%% new method
% define connections (from epanet)


% some system checks to avoid chasing ghosts

% need making: 
% length(Pump) == length(n_pumps) 
% && sum(strcmp(connection_type,'demand')) ==  length(n_pumps)


if length(h_min) ==  length(h_max) && length(h_max) == size(connection_matrix,2)  && length(h_max) == length(Elevation)
    %some thing nice could go here 
else 
    error('Inconsistent system defenition: number  of nodes, elevation and node bounds  definition doesn''t match')
end   
if size(connection_matrix,1) ==  size(connection_type,2)
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number and types of connections don''t match')
end   

if sum(strcmp(connection_type,'demand')) ==  n_demands
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number of demands and demand nodes specified don''t match')
end 

if length(Reservoir_area) ==  length(location_res)
    %some thing nice could go here
else 
    error('Inconsistent system defenition: number of reservoirs and reservoir areas specivied daon''t match')
end 


n_pipes = length(strmatch('pipe', char(connection_type))); % number of pipes (needed for lambda)
driver_pipes(place_matrix(1,strmatch('pipe', char(connection_type)),1)) = 1; % driver for

for i = 1:n_pipes
[pipe(i).m_data , pipe(i).C_data ,pipe(i).Q_lim_data  ]  = PieceWiseApproxGen(pipedata(i).data(1),pipedata(i).data(2),pipedata(i).data(3),pipedata(i).data(4),pipedata(i).data(5),'off');
length_m_part(i) = length(pipe(i).m_data);

% Elevation for pipes_piece and pipes_simple
pipe(i).C_data = pipe(i).C_data +  sum(connection_matrix(i,:).*Elevation);
end
length_m_part = (length_m_part-2)*2 + 1; % I hope this is the correct final size of the m vector
length_m = sum(length_m_part); % needed to make lambda matrix of the correct size

% info about the system
% location_res = [4 6]; 
% location_fix = [1];
% fixed_val = [1];
% little check
if length(location_fix) == length(fixed_val)
    %some thing nice could go here such as disp('well done')
else
    error('Inconsistent system defenition: location and values of vixed locations don''t match')
end

%add original state of reservoir and minimum fill height at end
%% Inequality matrix
[np, nn] = size(connection_matrix); % np ~= n_pipes as np is number of conncetions and n_pipes is just pipes
flow_matrix = eye(np); % assuming it is always the identety then this would work 
for i = 1:np
    A_part = []; % empty that one 
    b_part = [];
    switch connection_type{i}
        case 'Pump' %
            %% A 
            % driver_pumps = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement 
%             pump_number =2; % testing only
            driver_pumps1 = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps1 = [driver_pumps1 driver_pumps_part]; % add it all together
            end
            
            
            driver_pumps2 = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    if n_pumps(pump_number) == 1 %in case there is only one pump in the station
                        driver_pumps_part = 1;
                    else                        
                        driver_pumps_part = eye(n_pumps(pump_number)) - diag(ones(1,n_pumps(pump_number)-1),1); % driving matrix for T identy matrix placement
                    end
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps2 = [driver_pumps2 driver_pumps_part]; % add it all together
            end
            
            %%%%%%%%%%%%%% new stuff %%%%%%%%%%%%%%%%%%%%%
            % first bound (q<= 0) as in q <= MT1 
%%%%%%%%%%%%%%%!!!!!!!!!!!!!!!!!!!!!!!!!!!THIS IS WRONG !!!!!!!!!!!!! (need to use driver to generate correct length)                                  
             T = place_matrix(-M*eye(N),driver_pumps1(1,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
             h = zeros(N,N*nn);
             q = place_matrix(eye(N),flow_matrix(i,:),2); % not so sure
        lambda = zeros(N,N*length_m); % positive or negative flow in pipe
        b_part = zeros(N,1) ;
        
             A = [A; T h q lambda];
             b = [b;b_part];
            
             %%% all other bounds but the last one %%%%%%%%%
            for j = 1:n_pumps(pump_number)
                m_Pu =  Pump_station(pump_number).pump(j).curves(:,1);   
                c_Pu =  Pump_station(pump_number).pump(j).curves(:,2);
                
                m_Pu_next =  Pump_station(pump_number).pump(j+1).curves(:,1);   
                c_Pu_next =  Pump_station(pump_number).pump(j+1).curves(:,2);
              
                T1 = place_matrix(place_matrix(M*eye(N),ones(length(m_Pu_next),1),1),driver_pumps2(j,:),2);
                h1 = place_matrix(place_matrix(eye(N),ones(length(m_Pu_next),1),1),-connection_matrix(i,:),2);
                q1 = place_matrix(place_matrix(eye(N),-m_Pu_next,1),flow_matrix(i,:),2); % not so sure
                lambda1 = zeros(N*length(m_Pu_next),N*length_m); % positive or negative flow in pipe
                b_part1 = place_matrix(ones(N,1),c_Pu_next +M,1); 

                %second bound  -MT1 +(-h1 +h2) -c0 -m0q <=0 
                T2 = place_matrix(place_matrix(-M*eye(N),ones(length(m_Pu),1),1),driver_pumps1(j,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
                h2 = place_matrix(place_matrix(eye(N),ones(length(m_Pu),1),1),-connection_matrix(i,:),2);
                q2 = place_matrix(place_matrix(eye(N),-m_Pu,1),flow_matrix(i,:),2);
                lambda2 = zeros(N*length(m_Pu),N*length_m); % positive or negative flow in pipe
                b_part2 = place_matrix(ones(N,1),c_Pu,1) ;

                % adding it all together
                A = [A; T1 h1 q1 lambda1; T2 h2 q2 lambda2]; % add a new line
                b = [b; b_part1; b_part2]; 
            end
          m_Pu = Pump_station(pump_number).pump(end).curves(:,1);
          c_Pu = Pump_station(pump_number).pump(end).curves(:,2);
             
            T3 = zeros(N*length(m_Pu),N*sum(n_pumps)); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
            h3 = place_matrix(place_matrix(eye(N),ones(length(m_Pu),1),1),-connection_matrix(i,:),2);
            q3 = place_matrix(place_matrix(eye(N),-m_Pu,1),flow_matrix(i,:),2); % not so sure
%             lambda3 = zeros(N,N*n_pipes); % positive or negative flow in pipe
            lambda3 = zeros(N*length(m_Pu),N*length_m); % positive or negative flow in pipe
            
             A = [A; T3 h3 q3 lambda3]; % add a new line
             b = [b; place_matrix(ones(N,1),c_Pu,1)];
            %%%%% old stuff %%%%%%%%%%%%%%%%
            %needs to modified to make sure it has the size of the maximum and only fills the parts that will be used
%             for j = 1:n_pumps(pump_number)
%                   
%     %             T = place_matrix(-M*eye(N),driver_pumps(j,:),2);
%     %             h = place_matrix(eye(N),-connection_matrix(i,:),2);
%     %             q = place_matrix(eye(N)*m_Pu(j),flow_matrix(i,:),2); % not so sure
%     %             lambda = zeros(N,N*n_pipes); % positive or negative flow in pipe
% 
%     %           %flow control penalise each breaking of boundary with forcing 
%     %           the corresponding T = 1
%     %            h -c -mq <= MT which becomes: MT1 +(-h1 +h2) -m1q <= c1 +MT1 (were c = c0 c1 c2 ...,m = m0 m1 m2 ... and t = T1 T2 ...) 
%                 T1 = place_matrix(M*eye(N),driver_pumps2(j,:),2);
%                 h1 = place_matrix(eye(N),-connection_matrix(i,:),2);
%                 q1 = place_matrix(eye(N)*-m_Pu(j+1),flow_matrix(i,:),2); % not so sure
%     %             lambda1 = zeros(N,N*n_pipes); % positive or negative flow in pipe
%                 lambda1 = zeros(N,N*length_m); % positive or negative flow in pipe
%                 b_part1 = ones(N,1)* (c_Pu(j+1) + M);
% 
%                 %second bound  -MT1 +(-h1 +h2) -c0 -m0q <=0 
%                 T2 = place_matrix(-M*eye(N),driver_pumps1(j,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
%                 h2 = place_matrix(eye(N),-connection_matrix(i,:),2);
%                 q2 = place_matrix(eye(N)*-m_Pu(j),flow_matrix(i,:),2); % not so sure
%     %             lambda2 = zeros(N,N*n_pipes); % positive or negative flow in pipe
%                 lambda2 = zeros(N,N*length_m); % positive or negative flow in pipe
%                 b_part2(1:N,1) = c_Pu(j) ;
% 
%                 % adding it all together
%                 A_part = [A_part; T1 h1 q1 lambda1; T2 h2 q2 lambda2]; % add a new line
%                 b_part = [b_part; b_part1; b_part2];
%             end
%             
%             % final bound (-h1 + h2) m2 q <= c2 where 2 is the last curve
%             T3 = zeros(N,N*sum(n_pumps)); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
%             h3 = place_matrix(eye(N),-connection_matrix(i,:),2);
%             q3 = place_matrix(eye(N)*-m_Pu(j+1),flow_matrix(i,:),2); % not so sure
% %             lambda3 = zeros(N,N*n_pipes); % positive or negative flow in pipe
%             lambda3 = zeros(N,N*length_m); % positive or negative flow in pipe
%             
%             A = [A_part; T3 h3 q3 lambda3]; % add a new line
%             
%             b_part3 = ones(N,1)*c_Pu(n_pumps+1);
%             b = [b; b_part; b_part3;];
            %% b 
            % just add the compomnent
            %           add this below here: *n_pumps(pump_number)  if it doesn't work
%             b_part1 = place_matrix(ones(1,N  ),c_Pu(1:n_pumps(pump_number)+1),2); % make a long row of all Cs
%             b_part1 = place_matrix(ones(1,N),c_Pu(2:n_pumps(pump_number)+1) +  [M*ones(1,N-1)],2);
%             b_part2 = place_matrix(ones(1,N),c_Pu(1:n_pumps(pump_number)),2); 
%             b_part3 = ones(1,N)*c_Pu(n_pumps+1);
%             b = [b b_part1 b_part2 b_part3];
%             
            pump_number = pump_number + 1; % increase the pump station counter
%             x0 = [ ones(1,N) zeros(1,N) ones(1,N)  1.5 1.5 1.5  ones(1,N) 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 ones(1,N) ];
%             x0 = [ 1 1 0 0 0 1 ones(1,N) 1.5 1.5 1.5  ones(1,N) 0.5 0.5 1.5 0.5 0.5 1.5 0.5 0.5 1.5 ones(1,N) ];
           
         
        case 'Pump_simple'
            % based on the method used in gleixner et al, towards globally optimal operation
            % Qmin T <= q <= Qmax T ; enforce some flow constraints if pump is
            % on
            % M(T - 1) <= h2 - h1 - Delta_h <= M(1-T) enforce a certain
            % head
            
            driver_pumps = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
            end
            
            % pump specific data Q_min and max and delta H for each set of
            % pumps!
            
            % min & max flow
            T = [place_matrix( eye(N),Q_pump_min(pump_number,:),2);...
                 place_matrix(-eye(N),Q_pump_max(pump_number,:),2)];
            h =  zeros(2*N,size(connection_matrix,2)*N);
            q =  [place_matrix(-eye(N),flow_matrix(i,:),2);...
                  place_matrix( eye(N),flow_matrix(i,:),2)];
       lambda = zeros(2*N,N*length_m);
            A = [A;...
                T h q lambda]; 
            b = [b;...
                 zeros(N,1);...
                 zeros(N,1)];
            
             % enforce the head difference
            for j = 1:n_pumps(pump_number)
            T = [place_matrix(eye(N)*M          ,driver_pumps(j,:),2);...
                 place_matrix(eye(N)*M          ,driver_pumps(j,:),2)];
             
            h = [place_matrix(eye(N),connection_matrix(i,:),2);...
                 place_matrix(eye(N),-connection_matrix(i,:),2)];
                 
            q = [zeros(N,N*np);...
                 zeros(N,N*np)];
       lambda = zeros(2*N,N*length_m);
            
            A = [A; T h q lambda]; 
            b = [b;...
                 M-Delta_H(pump_number,j)*ones(N,1);...
                 M+Delta_H(pump_number,j)*ones(N,1)];
            end
        case 'Pump_linear'
            % todo
            
        case 'pipe_convex' %(use less intigers)
% %%%%%%%%%%%% OLD SECTION WITH CONVEX APPROXIMATION  %%%%%%%%%%%%%%%%%%%%
            
% positive flow
            T = zeros(N*nc_pi,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
            h = place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(nc_pi,1),1);
            q = place_matrix(place_matrix(eye(N),m_pi,1),flow_matrix(i,:),2); % not so sure
            driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector
            lambda = place_matrix(place_matrix(-M*eye(N),ones(nc_pi,1),1),driver_pipes,2); % positive or negative flow in pipe
            A_part1 = [T h q lambda];
                       
            % b
            b_part1 = place_matrix(ones(1,N),-c_pi - M,2);

            
            % negative flow %not working yet :S
            T = zeros(N*nc_pi,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
            h = place_matrix(place_matrix(eye(N),connection_matrix(i,:),2),ones(nc_pi,1),1);
            q = place_matrix(place_matrix(eye(N),-m_pi,1),flow_matrix(i,:),2); % not so sure
            driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector
            lambda = place_matrix(place_matrix(M*eye(N),ones(nc_pi,1),1),driver_pipes,2); % positive or negative flow in pipe
            A_part2 = [T h q lambda];
                       
            % b
            b_part = place_matrix(ones(1,N),c_pi ,2);
            b = [b b_part];
%             
            pipe_number = pipe_number + 1;            
%             
%             
            %% lambda part
            % A
%             T = zeros(N*2,N*sum(n_pumps));
%             h = place_matrix(place_matrix(eye(N),connection_matrix(i,:),2),[1 -1],1);
%             q = place_matrix(place_matrix(eye(N),[ 0 0],1),flow_matrix(i,:),2); % 
%             lambda = place_matrix(place_matrix(M*eye(N),[-1 1 ],1),driver_pipes,2); % positive or negative flow in pipe
            T = zeros(N*2,N*sum(n_pumps));
            h = place_matrix(place_matrix(zeros(N),connection_matrix(i,:),2),[1 -1],1);
            q = place_matrix(place_matrix(eye(N),[ 1 -1],1),flow_matrix(i,:),2); % 
            lambda = place_matrix(place_matrix(M*eye(N),[-1 1 ],1),driver_pipes,2); % positive or negative flow in pipe
%             A_part = [A_part1; A_part2; T h q lambda];
            A = [A; A_part1;  T h q lambda];
            %b
            
            b = [b; b_part1; zeros(1,N) ones(1,N)*M ]; % b part for the lambda section 
%             
%             b = [b zeros(1,N) ones(1,N)*M zeros(1,N)]; % b part for the lambda section 
% %%%%%%%%%%% OLD SECTION END %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            
        case 'pipe_piece'  
            %% pipes part
            % A

%%%%%%%%%%%% Piecewise approximation section %%%%%%%%%%%%%%%%%%%%%%%%%%%
%% make pipe matracies
% for each pipe

            m = [];
            c = [];
            q_min = [];
            q_max = [];
            m(1) = pipe(pipe_number).m_data(1);
            c(1) = pipe(pipe_number).C_data(1);
            q_min(1) = -pipe(pipe_number).Q_lim_data(2);
            q_max(1) = pipe(pipe_number).Q_lim_data(2);
            for l = 2:length(pipe(pipe_number).m_data)-1;
                %+ve
                m_pos(l) = pipe(pipe_number).m_data(l);
                c_pos(l) = pipe(pipe_number).C_data(l);  
                q_min_pos(l) = pipe(pipe_number).Q_lim_data(l);
                q_max_pos(l) = pipe(pipe_number).Q_lim_data(l+1);
                %-ve
                m_neg(l) = pipe(pipe_number).m_data(l);
                c_neg(l) = -pipe(pipe_number).C_data(l);  
                q_min_neg(l) = -pipe(pipe_number).Q_lim_data(l+1);
                q_max_neg(l) = -pipe(pipe_number).Q_lim_data(l);
                m = [m m_pos(l) m_neg(l)]; 
                c = [c c_pos(l) c_neg(l)];
                q_min = [q_min q_min_pos(l) q_min_neg(l)];
                q_max = [q_max q_max_pos(l) q_max_neg(l)];
            end

% A %%%%%%%%%%%%%
            T = zeros(N*4*length(m),N*sum(n_pumps)); % assuming all curves are used for the pipe definition
            
            h =[place_matrix(place_matrix(eye(N), connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
                place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
                zeros(2*length(q_min)*N,size(connection_matrix,2)*N); %For switching lambda on
                ]; %lambda summation
            
            q = [place_matrix([place_matrix(eye(N),-m,1);...  %for curves
                place_matrix(eye(N),m,1);...
                place_matrix(eye(N),ones(length(q_min),1),1);...  for switching lambda
                place_matrix(-eye(N),ones(length(q_max),1),1)],flow_matrix(i,:),2); %massive vertical stack :)
                ]; %lambda summation
                
 driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector for which one is active (does not consider different lengths of m vector!!)
       lambda = [place_matrix(place_matrix(M*eye(N*length(m)),ones(4,1),1),driver_pipes,2);... % positive or negative flow in pipe
                 ]; %lambda summation
            
             A = [A; T h q lambda]; 
             
       b_part = [ place_matrix(ones(1,N), c,2)    + M... % for curves
                  place_matrix(ones(1,N),-c,2)    + M...
                  place_matrix(ones(1,N),q_max,2) + M... %for switching lambda
                  place_matrix(ones(1,N),-q_min,2)+ M];
                  
              b = [b; b_part'];
              
% Aeq %%%%%%%%%     
            Aeq = [Aeq; zeros(N,N*sum(n_pumps)) zeros(N,size(connection_matrix,2)*N) zeros(N,size(flow_matrix(i,:),2)*N)  place_matrix(repmat(eye(N),1,length(m)),driver_pipes,2)];
            pipe_number = pipe_number + 1;    
               beq = [beq; ones(N,1)]; %lambda summation
        case 'pipe_quad'
            %todo
               T
               h
               q
               A
               
        case 'demand'
             disp('demand registered') 
        case 'valve'
            %todo
            disp('Valves not implimented yet') 
        otherwise
            error(['Unknown system definition in connection' num2str(1)])
    end
    % make A from components
%     A = [A; A_part]; %% needed for old style
end   
 

%% equality matrix
% mass balance for the nodes
T = zeros(N*nn,N*sum(n_pumps)); 
h = zeros(N*nn,N*nn);
%q
q_part = [];
q = [];
for i = 1:nn 
    %      in(i,:) = connection_matrix(:,i) == 1;
    %      out(i,:)= connection_matrix(:,i) ==-1;
    flows = transpose(connection_matrix)*-1; % same as in out above just faster 
    if i == location_fix
        q_part = zeros(N,N*np);
    elseif i == location_res %need to change h as well (h(T-1) - h(T))/*Area/Delta_T + q - d = 0
        [ ~,Reservoir_number] = max(i == location_res);
        Area = Reservoir_area(Reservoir_number);
        h((i-1)*N+1:i*N,(i-1)*N+1:i*N) = -eye(N)*Area/Delta_T  + diag(ones(1,N-1),-1); % mass balance term for reservoirs
        h((i-1)*N+1,i*N) = 1; %add continous part (h(N)-h(1))/*Area/Delta_T + q - d = 0
        q_part = place_matrix(eye(N),flows(i,:),2);
    else
        q_part = place_matrix(eye(N),flows(i,:),2);
    end
    q = [q; q_part]; 
end 
lambda = zeros(N*nn,N*length_m);
Aeq = [Aeq; T h q lambda];
beq = [beq; zeros(nn*N,1)]; %all zero as all mass balances

% Add section to fix locations of fixed head
for i = length(location_fix)
    driver = zeros(1,nn); driver(i) = 1; 
    Aeq = [Aeq; zeros(N,N*sum(n_pumps)) place_matrix(eye(N),driver,2) zeros(N,N*np) zeros(N,N*length_m) ];
    beq = [beq; fixed_val(i)*ones(N,1)]; % value *  everywhere
end

% add demand flow
% probably also needs loop and better demand node defenitions as ther emay
% be several demands
% driver = strcmp(connection_type,'demand') ; %only works for one demand
j = 1; % demand counter
for i = find(strcmp(connection_type,'demand') == 1)
    driver = zeros(1,np); driver(i) = 1;
    Aeq = [Aeq; zeros(N,N*sum(n_pumps)) zeros(N,N*nn) place_matrix(eye(N),driver,2)  zeros(N,N*length_m) ];
    beq = [beq; d(j,:)'];
    j = j+1;
end

% checks
% size(A) 
% size(b) %needs transposing
% 
% size(Aeq) 
% size(beq)
% To do:%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Objective
% Objective Function (min 0.5x'Hx + f'x)

% electricty price for all N adds up for all
% linear cost part
driver = [];
f = []; % pricing for pump operations 
for i = 1:length(n_pumps)
     driver{i} = ones(1,n_pumps(i));
     cost = place_matrix(Pe,driver{i},2); % possibly vary Pe if it is defferent for different pumps
     f = [f cost]; 
end
% now the rest is zeros
f = [f zeros(1,N*nn + N*np + N*length_m)];
% f(4:6) = f(4:6)*5;

% H % assume cost of switching are the same for every pump
H = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
% zero every where else (rewrite this in elegant)
H(N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_m,N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_m ) = 0; 
H = H*switch_penalty;


%% bounds
% lb
% ub

lb = [ repmat(T_bounds(1),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_min,2) repmat(q_bounds(1),1,N*np) repmat(lambda_bounds(1),1,N*length_m) ];
ub = [ repmat(T_bounds(2),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_max,2) repmat(q_bounds(2),1,N*np) repmat(lambda_bounds(2),1,N*length_m) ];

% Integer Constraints
pumps_int(1:N*sum(n_pumps))  = 'I'; % could be B for binary or I for intiger
lambda_int(1:N*length_m) = 'I';
cont(1:N*nn + N*np) = 'C';
xtype = strcat([pumps_int cont lambda_int])   ;
% xtype = 'II';               %x1 & x2 are Integer

%% solve
% Create OPTI Object
tic;
Opt = opti('qp',H,f,'ineq',A,b,'eq',Aeq,beq,'lb',lb,'ub',ub,'xtype',xtype);

% Solve the MIQP problem
[x,fval,exitflag,info] = solve(Opt);
toc;
% [x2,fval2,exitflag2] = cplexmiqp
% make pretty result matrix
% note quite working

%% display
if exitflag == 1 % success
header_pumps = repmat('Pump on?     ',[1,sum(n_pumps)]);
header_head  = repmat('Head (m)     ',[1,nn]);
header_flow  = repmat('Flow m-3/s   ',[1,np]);
header_lambda= repmat('Lambda       ',[1,length_m]);
data = reshape(x,[N length(x)/N ]); %Make it pretty
disp(['         ' header_pumps header_head header_flow header_lambda]);disp(data)
% following approach needs statistics toolbox:
% header = {'Quarter', 'monthly amount remaining', 'annual amountremaining'}
% data =    [ 1          30000    150000;...
%     2        20000    130000];
% ds = dataset({data,header{:}});
% for excel : [header;num2cell(data)]

%% save to epanet usable format

for i= 1:sum(n_pumps)

    schedule = data(:,i);
    schedule = [ 5; 5;  schedule];% add lines epanet ignores using caracters that sand out
    save(['pump_schedule2_' num2str(i)  '.txt'],'schedule','-ascii')
    str = ['COPY pump_schedule_' num2str(i) '.txt pump_schedule_' num2str(i) '.pat'];
    status = dos(str);   
end
%% fail
else
    disp(['      Exitflag: ' num2str(exitflag)])
    disp(info)
end
    