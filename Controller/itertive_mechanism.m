% clearvars;
% filename = 'Richmond_skeleton.inp';
% T = [1 0 1 0 0 1 0 ]; % pump configuration
clearvars;
% T = [ 1 1 1 1 1 1 1 1 1 1 1 1 0 0 1 1 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 1 0 0 0];
% filename = 'van_zyl.inp';
% def = 'van_zyl';
% def = 'example1';
% def = 'Richmond_skeleton';
% T = [1 0 1 ];
def = 'Lecture_example';
% def = 'res_only';
N = 6;
Delta_T = 3600*24/N;
prepNetwork;
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_linear';

% call EPS
prepNetwork;
prepGGA;
T0 = [];
T = [0 0 0 0 0 0 0 ];
err = 1;

H_save = zeros(nj+no,N);
Q_save = zeros(length([pipes])+ length(n_pumps),N);
H_res = zeros(length(Allreservoirs),1);

for t = 1:N
    EPS_step; 
    
    eval([def '_control']);
end
disp(T0);
 [connection_matrix,connection_type,pumpsUnique] = makeconnection_matrix(pipes,nodesIdListMap,pumps,pipe_approx,pump_approx);
prepConstraints;

% check for CV:
loc_valves = CV;
length_lambda = length(loc_valves);


for itt = 1:5% while err > 0.1
H_tot = H_save;

%% make linear approximations:
% initianlise matrazies
A= [];
Aeq = [];
b= [];
beq = [];
lb=[];
ub=[];
        

Delta_H_pipes = zeros(npi,N);
m = zeros(npi,N);
c = zeros(npi,N);
%connection_matrix = [A12(:,:) A10(:,1:no)];
for i = 1:N
    Delta_H_pipes(:,i) = connection_matrix(1:npi,:)*H_tot(:,i);
    m(:,i) = sign(Q_save(1:npi,i)).*K(1:npi).*Nexp(1:npi).*abs(Q_save(1:npi,i)).^(Nexp(1:npi)-1);
    c(:,i) =  Delta_H_pipes(:,i) - m(:,i).*Q_save(1:npi,i);
 end
% disp(T0);


% make equations:
pipe_linearised

% pumps
pump_number =1;
for i = 1:np
    A_part = []; % empty that one 
    b_part = [];
    switch connection_type{i}
        %pumps
        case 'Pump_convex' %
            pump_convex_def
        case 'Pump_simple'
            pump_simple_def
        case 'Pump_linear' 
            pump_linear_def2
        case 'Pump_quad'
            pump_quad_def
        case 'Pump_quad_ineq'
            pump_quad_ineq_def
    end
end

mass_balance


%% mod case and rerun:
pipe_number = 1:length(loc_valves); % not so sure
n_pipes = length(loc_valves); % not so sure
lambda_parts_length = ones(1,length(loc_valves));

for i = loc_valves
    check_valve_def;
end

make_OF;

lb = [ repmat(T_bounds(1),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_min,2) place_matrix(ones(1,N),-q_lim,2) repmat(lambda_bounds(1),1,N*length_lambda) ];
ub = [ repmat(T_bounds(2),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_max,2) place_matrix(ones(1,N),q_lim,2) repmat(lambda_bounds(2),1,N*length_lambda) ];

% Integer Constraints
pumps_int(1:N*sum(n_pumps))  = 'I'; % could be B for binary or I for intiger
lambda_int(1:N*length_lambda) = 'I';
cont(1:N*nn + N*np) = 'C';
xtype = strcat([pumps_int cont lambda_int]);

Call_solver
 
end



% T0 = singlefile(T0');
% Pressure = H - Elevation_j; %Elevation_j is wrong!
