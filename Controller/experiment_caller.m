%% experiment runner
disp('Check N is disabled in the experiment and plotting is off!')
N_set = [ 6 12 24 48];
 for iii = 1:length(N_set)
     rep = 1;
    while rep <= 3   
        N = N_set(iii);
        disp(['Working on N: ' num2str(N) ' repetition number: ' num2str(rep)])
%         try
        experiment ;
        
         if ~exist('time_taken','var');  time_taken = NaN; end
         if ~exist('fval','var');  fval = NaN; end
         if ~exist('deviance','var'); deviance  = NaN; end
         if ~exist('x','var'); x  = NaN;  end 

        cd M:\Phd_access\MATLAB\Phd\Results\Approx_comparission
        save(['time_taken_' def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)],'time_taken');
        save(['cost_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)  ],'fval');
        save(['deviance_'   def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)],'deviance');
        x0 = x; save(['x0_'   def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)],'x0');
        if  isnan(x)
            rep = 3;
        end
        rep = rep + 1;
        

%         catch
% 
%         end
    end
end
try
N_set = [6 12 24 48];
name = ['results_' def '_' pump_approx '_' pipe_approx];
 for iii = 1:length(N_set)
    for rep = 1:3
%     for iii = 1:length(N_set)
        N = N_set(iii);
        
%         try['time_taken_' def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]
        load(['time_taken_' def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
        load(['cost_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
        load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);

        
        results.Ns(iii).cost(rep) =  fval;
        results.Ns(iii).time(rep) = time_taken;
        results.Ns(iii).deviance(rep,:) = deviance;
%         catch
%         end
    end
 end

 save([name '_'  num2str(approx_number) '_'  datestr(now,30)],'results')
 emailnotification('rmm08@ic.ac.uk',['Complete ' num2str(approx_number)'],['Completed, no crash :)' datestr(now)]) 
catch
    emailnotification('rmm08@ic.ac.uk',['Complete ' num2str(approx_number)'],['Completed, crash :(' datestr(now)]) 
end
    
drive = pwd; drive = drive(1);
cd([drive ':\Phd\MATLAB\Phd']);
 
 
