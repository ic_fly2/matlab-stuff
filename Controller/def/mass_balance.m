% mass balance mechanism
T = zeros(N*data.nn,N*data.npu); 
h = zeros(N*data.nn,N*data.nn);
%q
q_part = [];  
q = []; 
h_part = [];  
h = []; 
flows = transpose(connection_matrix); % same as in out above just faster 
A_mass = [];
b_mass = [];
for i = 1:data.nn 
    %      in(i,:) = connection_matrix(:,i) == 1;
    %      out(i,:)= connection_matrix(:,i) ==-1;
   
    if i == location_fix
        h_part = zeros(N,N*data.nn);
        q_part = zeros(N,N*data.np);
    elseif 1 == sum(i == location_res)
        %need to change h as well (h(T-1) - h(T))*Area/Delta_T + q - d = 0
        [ ~,Tank_number] = max(i == location_res);
%         Area = Reservoir_area(Tank_number);
        % old formulation T(j) - T(j-1)
%         h_part_mod = -eye(N)*Area/Delta_T  + diag(ones(1,N-1),-1)*Area/Delta_T;
%         h_part_mod(1,:)  = zeros(1,N);
%         driver = zeros(1,nn); driver(i) = 1;
%         h_part =  place_matrix( h_part_mod ,driver,2);
%         q_part = place_matrix([zeros(1,N); eye(N-1) zeros(N-1,1)],flows(i,:),2);
%         driver = zeros(1,nn);  driver(i) = 1;
%         A = [A;  zeros(1,sum(n_pumps)*N) place_matrix([1 zeros(1,N-2) -1],driver,2) zeros(1,(np+length_lambda)*N)];
%         b = [b; 0]    ;
        
        % new formulation, explicit and considering all flows!
        h_part_mod = (-eye(N) + diag(ones(1,N-1),1) + diag(1,-(N-1))) *double(tanks{Tank_number}.Area/Delta_T); %-eye(N)*Area/Delta_T  + diag(ones(1,N-1),-1)*Area/Delta_T;
        driver = zeros(1,data.nn); driver(i) = 1;
        h_part =  place_matrix( h_part_mod ,driver,2);
        q_part = place_matrix(-eye(N),flows(i,:),2);
        
        % final inequality
        A_mass = [A_mass;  zeros(1,data.npu*N)  h_part(end,:) q_part(end,:) zeros(1,data.length_lambda*N)];
        b_mass = [b_mass; 0];
        
        
        % limit maximum positive difference at the end
        if ~strcmp(settings.limit_overfill,'Off')
            A_mass = [A_mass;  zeros(1,data.npu*N)  -h_part(end,:)/(double(tanks{Tank_number}.Area/Delta_T)) zeros(1,N*data.np) zeros(1,data.length_lambda*N)];
            b_mass = [b_mass; tanks{Tank_number}.Max*settings.limit_overfill];
        end
        
        
        Delta_H_A(Tank_number,:) = [zeros(1,data.npu*N)  h_part(end,:)/max(max( h_part)) q_part(end,:)/max(max( h_part))      zeros(1,data.length_lambda*N)];

        
        h_part(end,:) = zeros(1,N*data.nn);
        q_part(end,:) = zeros(1,N*data.np);      

    else
        h_part = zeros(N,N*data.nn);
        q_part = place_matrix(eye(N),flows(i,:),2);
    end
    q = [q; q_part]; 
    h = [h; h_part];
end  
 A_cell{length(A_cell)+1} =  A_mass;
 b_cell{length(A_cell)+1} =  b_mass;
%% Equal section
b_part = singlefile(data.qall_gga)';
% b_part = singlefile(d)';
lambda = zeros(N*data.nn,N*data.length_lambda);
% move reocuring summation from equality to inequlaity for last time step in tanks:
insert_A =  [T h q lambda];
%old section %% 23.02.15
% A = [A; -(insert_A((location_res-1)*N+1,:))]; 
% b = [b; b_part((location_res-1)*N+1) ];
% 
% insert_A((location_res-1)*N+1,:) = [];
% b_part((location_res-1)*N+1) = [];

Aeq = [Aeq;insert_A];
beq = [beq;b_part]; %all zero as all mass balances (one less as insert is shorter)



% Add section to fix locations of fixed head % set through bounds lb ub
for i = 1:length(location_fix)
    driver = zeros(1,data.nn); driver(location_fix(i)) = 1; 
    Aeq = [Aeq; zeros(N,N*data.npu) place_matrix(eye(N),driver,2) zeros(N,N*data.np) zeros(N,N*data.length_lambda) ];
    beq = [beq; fixed_val(i)*ones(N,1)]; % value *  everywhere
end

% Initial tank status and continuety:

if isfield(settings,'h0')
    if length(tanks)~= length(settings.h0)
%         Initial_filling = settings.h0;
%     else
        error('initial filling of reservoirs not correctly defined')
    end
    driver = zeros(1,data.nn); driver(location_res) = 1; driver = diag(driver);
    driver(sum(driver,2) == 0,:) = [];
    % h
    h = [];
    for i = 1:length(location_res)
        h = [h; place_matrix([1 zeros(1,N-1)],driver(i,:),2)];
    end
    Aeq = [Aeq; zeros(length(location_res),data.npu*N)  h zeros(length(location_res),N*data.np)  zeros(length(location_res),N*data.length_lambda)];
    beq = [beq; settings.h0'];
end


