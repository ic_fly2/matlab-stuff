 %% A 
            % driver_pumps = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement 
%             pump_number =2; % testing only
            driver_pumps1 = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps1 = [driver_pumps1 driver_pumps_part]; % add it all together
            end
            
            
            driver_pumps2 = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    if n_pumps(pump_number) == 1 %in case there is only one pump in the station
                        driver_pumps_part = 1;
                    else                        
                        driver_pumps_part = eye(n_pumps(pump_number)) - diag(ones(1,n_pumps(pump_number)-1),1); % driving matrix for T identy matrix placement
                    end
                else
                    driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
                end
                driver_pumps2 = [driver_pumps2 driver_pumps_part]; % add it all together
            end
            
            %%%%%%%%%%%%%% new stuff %%%%%%%%%%%%%%%%%%%%%
            % first bound (q<= 0) as in q <= MT1 
%%%%%%%%%%%%%%%!!!!!!!!!!!!!!!!!!!!!!!!!!!THIS IS WRONG !!!!!!!!!!!!! (need to use driver to generate correct length)                                  
             T = place_matrix(-M*eye(N),driver_pumps1(1,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
             h = zeros(N,N*nn);
             q = place_matrix(eye(N),flow_matrix(i,:),2); % not so sure
        lambda = zeros(N,N*length_lambda); % positive or negative flow in pipe
        b_part = zeros(N,1) ;
        
             A = [A; T h q lambda];
             b = [b;b_part];
            
             %%% all other bounds but the last one %%%%%%%%%
            for j = 1:n_pumps(pump_number)
                m_Pu =  Pump_station(pump_number).pump(j).curves(:,1);   
                c_Pu =  Pump_station(pump_number).pump(j).curves(:,2);
                
                m_Pu_next =  Pump_station(pump_number).pump(j+1).curves(:,1);   
                c_Pu_next =  Pump_station(pump_number).pump(j+1).curves(:,2);
              
                T1 = place_matrix(place_matrix(M*eye(N),ones(length(m_Pu_next),1),1),driver_pumps2(j,:),2);
                h1 = place_matrix(place_matrix(eye(N),ones(length(m_Pu_next),1),1),connection_matrix(i,:),2);
                q1 = place_matrix(place_matrix(eye(N),-m_Pu_next,1),flow_matrix(i,:),2); % not so sure
                lambda1 = zeros(N*length(m_Pu_next),N*length_lambda); % positive or negative flow in pipe
                b_part1 = place_matrix(ones(N,1),c_Pu_next +M,1); 

                %second bound  -MT1 +(-h1 +h2) -c0 -m0q <=0 
                T2 = place_matrix(place_matrix(-M*eye(N),ones(length(m_Pu),1),1),driver_pumps1(j,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
                h2 = place_matrix(place_matrix(eye(N),ones(length(m_Pu),1),1),connection_matrix(i,:),2);
                q2 = place_matrix(place_matrix(eye(N),-m_Pu,1),flow_matrix(i,:),2);
                lambda2 = zeros(N*length(m_Pu),N*length_lambda); % positive or negative flow in pipe
                b_part2 = place_matrix(ones(N,1),c_Pu,1) ;

                % adding it all together
                A = [A; T1 h1 q1 lambda1; T2 h2 q2 lambda2]; % add a new line
                b = [b; b_part1; b_part2]; 
            end
          m_Pu = Pump_station(pump_number).pump(end).curves(:,1);
          c_Pu = Pump_station(pump_number).pump(end).curves(:,2);
             
            T3 = zeros(N*length(m_Pu),N*sum(n_pumps)); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
            h3 = place_matrix(place_matrix(eye(N),ones(length(m_Pu),1),1),connection_matrix(i,:),2);
            q3 = place_matrix(place_matrix(eye(N),-m_Pu,1),flow_matrix(i,:),2); % not so sure
%             lambda3 = zeros(N,N*n_pipes); % positive or negative flow in pipe
            lambda3 = zeros(N*length(m_Pu),N*length_lambda); % positive or negative flow in pipe
            
             A = [A; T3 h3 q3 lambda3]; % add a new line
             b = [b; place_matrix(ones(N,1),c_Pu,1)];
            
            pump_number = pump_number + 1; % increase the pump station counter
               