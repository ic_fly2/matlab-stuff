

if length(n_pumps) <=3
else
    error('Pipe Linear not defined for more than three pump stations')
end
% of the type
% M(T1 - 1) <= hk1- hk2 - m1q - c1 <=  M(1 - T1)
% Thus: MT - h1 +h2 +mq <= M - c
%       MT + h1 -h2 -mq <= M + c




for j = 1:length(OP_drivers)
    T = [place_matrix(eye(N)*M/sum(OP_drivers{j}),OP_drivers{j},2);...
        place_matrix(eye(N)*M/sum(OP_drivers{j}),OP_drivers{j},2)];
    h = [place_matrix( eye(N), connection_matrix(i,:),2);...
        place_matrix( eye(N),-connection_matrix(i,:),2)];
    q = [place_matrix( eye(N)*m_linear_pipe(pipe_number,j),flow_matrix(i,:),2);...
        place_matrix(-eye(N)*m_linear_pipe(pipe_number,j),flow_matrix(i,:),2)];
    lambda = zeros(2*N,N*length_lambda); %only if using mixed stuff
    A = [A; T h q lambda];
    b = [b;...
        M - c_linear_pipe(pipe_number,j)*ones(N,1);...
        M + c_linear_pipe(pipe_number,j)*ones(N,1)];
end


%             for j = 0:prod([n_pumps+1])-1 % for all combinations
%                 if j == 0
%                     % no pumps on
%                     T = [];h=[]; q=[]; lambda=[]; b_part = [];
%                 elseif j <= n_pumps(1)
%                     driver = zeros(1,sum(n_pumps)); driver(j) = 1;
%                     T = [place_matrix(eye(N)*M,driver,2);...
%                          place_matrix(eye(N)*M,driver,2)];
%                     h = [place_matrix(-eye(N),-connection_matrix(i,:),2);...
%                          place_matrix( eye(N),-connection_matrix(i,:),2)];
%                     q = [place_matrix( eye(N)*m_linear_pipe(j),flow_matrix(i,:),2);...
%                          place_matrix(-eye(N)*m_linear_pipe(j),flow_matrix(i,:),2)];
%                     lambda = zeros(2*N,N*length_lambda);
%
%                     b_part = [M - c_linear_pipe(j)*ones(N,1);...
%                               M + c_linear_pipe(j)*ones(N,1)];
%                 elseif j > n_pumps(1);
%                     switch def % short patch
%                         case 'def_van_zyl_network'
%                             if j == 3
%                             driver = [0 1 1];
%                             T = [place_matrix(eye(N)*M,driver,2);...
%                                 place_matrix(eye(N)*M,driver,2)];
%                             h = [place_matrix(-eye(N),-connection_matrix(i,:),2);...
%                                 place_matrix( eye(N),-connection_matrix(i,:),2)];
%                             q = [place_matrix( eye(N)*m_linear_pipe(j),flow_matrix(i,:),2);...
%                                 place_matrix(-eye(N)*m_linear_pipe(j),flow_matrix(i,:),2)];
%                             lambda = zeros(2*N,N*length_lambda);
%
%                             b_part = [M - c_linear_pipe(j)*ones(N,1);...
%                                 M + c_linear_pipe(j)*ones(N,1)];
%                             elseif j == 4
%                             driver = [1 0 1];
%                             T = [place_matrix(eye(N)*M,driver,2);...
%                                 place_matrix(eye(N)*M,driver,2)];
%                             h = [place_matrix(-eye(N),-connection_matrix(i,:),2);...
%                                 place_matrix( eye(N),-connection_matrix(i,:),2)];
%                             q = [place_matrix( eye(N)*m_linear_pipe(j),flow_matrix(i,:),2);...
%                                 place_matrix(-eye(N)*m_linear_pipe(j),flow_matrix(i,:),2)];
%                             lambda = zeros(2*N,N*length_lambda);
%
%                             b_part = [M - c_linear_pipe(j)*ones(N,1);...
%                                 M + c_linear_pipe(j)*ones(N,1)];
%                             elseif j == 5
%                             driver =[0 0 1];
%                             T = [place_matrix(eye(N)*M,driver,2);...
%                                 place_matrix(eye(N)*M,driver,2)];
%                             h = [place_matrix(-eye(N),-connection_matrix(i,:),2);...
%                                 place_matrix( eye(N),-connection_matrix(i,:),2)];
%                             q = [place_matrix( eye(N)*m_linear_pipe(j),flow_matrix(i,:),2);...
%                                 place_matrix(-eye(N)*m_linear_pipe(j),flow_matrix(i,:),2)];
%                             lambda = zeros(2*N,N*length_lambda);
%
%                             b_part = [M - c_linear_pipe(j)*ones(N,1);...
%                                 M + c_linear_pipe(j)*ones(N,1)];
%                             end
%
%
%                         otherwise
%                             error(['Not defined yet'])
%                     end
% %                     error('Not implimeted yet! Call 07794007114 to complain.')
%                 end
%
%                 A = [A; T h q lambda]; % should be equal!!
%                 b = [b;b_part];
%             end
pipe_number = pipe_number +1;
