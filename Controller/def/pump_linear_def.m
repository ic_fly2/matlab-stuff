% Simple linear aproximation of pump as a single straight line
% Qmin T <= q <= Qmax T ; enforce some flow constraints if pump is
% on same as in simple case.
% M(T - 1) <= h2 - h1 - mq -c <= M(1-T) enforce a head flow
% relationship

driver_pumps = [];
for k = 1:length(n_pumps)
    if k == pump_number
        driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
    else
        driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
    end
    driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
end

% pump specific data Q_min and max and delta H for each set of
% pumps!
if ~exist('ii','var')
    ii = 0;

end
    
for ii = ii+1:prod(n_pumps(1:pump_number)+1)-1  %1:length(OP_drivers)
    % min & max flow % must be shorter but may get complicated with more
    % pumps active at the same time
    % linit flow to OP point range if T1 = 1 and Qmax for all other Ts
    %  T1*q_min1 + <= q <= T*q_max
    % T*q_min - q <= 0
    % -T*q_max + q <= 0
    qmins = zeros(1,sum(n_pumps)); % they are for different flows!!!!!
    qmins(find(OP_drivers{ii} >= 1)) = Q_pump_min(Q_pump_min(:,ii) ~= 0,ii);
    qmins(qmins == 0 )= -Qmax;
    qmaxs = zeros(1,sum(n_pumps));
    qmaxs(find(OP_drivers{ii} >= 1)) = Q_pump_max(Q_pump_max(:,ii) ~= 0,ii);
    qmaxs(qmaxs == 0 )= Qmax;
    T = [place_matrix( eye(N),qmins,2);...
         place_matrix(-eye(N),qmaxs,2)];
    h =  zeros(2*N,size(connection_matrix,2)*N);
    q =  [place_matrix(-eye(N),flow_matrix(i,:),2);...
        place_matrix( eye(N),flow_matrix(i,:),2)];
    lambda = zeros(2*N,N*length_lambda);
    A = [A;...
        T h q lambda];
    b = [b;...
        zeros(N,1);...
        zeros(N,1)];
end 


% for ii = 1: n_pumps(pump_number) + sum(n_pumps(1:pump_number-1))
%     OP_test(ii,:)


% enforce the head flow relationship
% MT -(-h1+h2) +mq <= M -c
% MT  -h1 +h2  -mq <= M +c

for j = 1:n_pumps(pump_number)
    T = [place_matrix(eye(N)*M,driver_pumps(j,:),2);...
         place_matrix(eye(N)*M,driver_pumps(j,:),2)];
    
    h = [place_matrix(eye(N),-connection_matrix(i,:),2);...
         place_matrix(eye(N), connection_matrix(i,:),2)];
    
    q = [place_matrix(m_linear_pump(pump_number,j)*eye(N), flow_matrix(i,:),2);...
         place_matrix(m_linear_pump(pump_number,j)*eye(N),-flow_matrix(i,:),2)];
    lambda = zeros(2*N,N*length_lambda);
    
    A = [A; T h q lambda];
    b = [b;...
        M-c_linear_pump(pump_number,j)*ones(N,1);...
        M+c_linear_pump(pump_number,j)*ones(N,1)];
end
pump_number =  pump_number +1;
