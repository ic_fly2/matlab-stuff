% _CV section:
% replace all the negative bits of the code with this simple rule:
%(For convex and quadratic approximations)

%q/Qmax - lambda <= 0
T = zeros(N,N*sum(n_pumps));
h = zeros(N, nn*N);
q = place_matrix(eye(N),flow_matrix(i,:),2)/Qmax;

lambda = [];
for k = 1:n_pipes +n_valves %lambda sizer and driver
    if k == pipe_number
        lambda_part = eye(N);
    else
        lambda_part =  zeros(N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end


A = [A;T h q lambda];
b = [b; zeros(N,1)];