
%% pipes part
% A

%%%%%%%%%%%% Piecewise approximation section %%%%%%%%%%%%%%%%%%%%%%%%%%%
%% make pipe matracies
% for each pipe
% M_old = M;
% M = M_pipes(pipe_number)+5;
% % 
%  if M > 999;
%       M = 999;
% % elseif M < 999
% %     M = 999;
%   end

m = [];
c = [];
q_min = [];
q_max = [];
m(1) = pipes{pipe_number}.m_data(1);
c(1) = pipes{pipe_number}.C_data(1) + pipes{pipe_number}.change_gz;
q_min(1) = -pipes{pipe_number}.Q_lim_data(2);
q_max(1) = pipes{pipe_number}.Q_lim_data(2);

for j = 2:length(pipes{pipe_number}.m_data);
    %+ve
    m_pos(j) = pipes{pipe_number}.m_data(j);
    c_pos(j) = pipes{pipe_number}.C_data(j) + pipes{pipe_number}.change_gz;
    q_min_pos(j) = pipes{pipe_number}.Q_lim_data(j);
    q_max_pos(j) = pipes{pipe_number}.Q_lim_data(j+1);
    %-ve
    m_neg(j) = pipes{pipe_number}.m_data(j);
    c_neg(j) = -pipes{pipe_number}.C_data(j)+ pipes{pipe_number}.change_gz;
    q_min_neg(j) = -pipes{pipe_number}.Q_lim_data(j+1);
    q_max_neg(j) = -pipes{pipe_number}.Q_lim_data(j);
    
    
    m = [m m_pos(j) m_neg(j)];
    c = [c c_pos(j) c_neg(j)];
    q_min = [q_min q_min_pos(j) q_min_neg(j)];
    q_max = [q_max q_max_pos(j) q_max_neg(j)];

end
pipes{pipe_number}.q_min_pieces = q_min;
pipes{pipe_number}.q_max_pieces = q_max;


%M_temp = m.*q_max-m.*q_min; % Not usable as M has to be larger than the
%largest head difference


% A %%%%%%%%%%%%%
%  h1 - h2 = mq +c if active
%
% -h1 +h2 +mq + M lambda <= c+M
%  h1 -h2 -mq + M lambda <= -c+M
%%% old:
%           q + Qmax lambda <= qmax + Qmax
%          -q + M lambda <= -qmin +  Qmax
%%% new:
%           q + ( Qmax - qmax)lambda <=  Qmax
%          -q + ( Qmax + qmin) lambda <=  Qmax
T = zeros(N*4*length(m),N*data.npu); % assuming all curves are used for the pipe definition

h =[place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
    place_matrix(place_matrix(eye(N), connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
    zeros(2*length(q_min)*N,size(connection_matrix,2)*N); %For switching lambda on
    ]; %lambda summation

q = place_matrix(...
    [place_matrix( eye(N),-m,1);...  %for curves
    place_matrix( eye(N), m,1);...
    place_matrix( eye(N),ones(length(q_min),1),1);...  for switching lambda
    place_matrix(-eye(N),ones(length(q_max),1),1)]...
    ,flow_matrix(i,:),2); 
     %lambda summation

% lambda = [];
% for k = 1:n_pipes + n_valves%lambda sizer and driver
%     if k == pipe_number
%         lambda_part = place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1);
%     else
%         lambda_part =  zeros(4*N*length(m),lambda_parts_length(k)*N); % make zeros where ever it isn't needed
%     end
%     lambda = [lambda lambda_part ]; % add it all together
% end

lambda =  [zeros(4*N*length(m),sum(data.lambda_parts_length(1:pipe_number-1))*N) ...
           [place_matrix(eye(N*length(m)),[M M]',1); ...
           diag(place_matrix(ones(N,1),data.Qmax - q_max,1));
           diag(place_matrix(ones(N,1),data.Qmax + q_min,1))] ...
           zeros(4*N*length(m),sum(data.lambda_parts_length(pipe_number+1:end))*N)];
       
% min(min(lambda2 == lambda))

driver_pipes = zeros(1,data.npi); driver_pipes(pipe_number) = 1; % make the driver vector for which one is active (does not consider different lengths of m vector!!)
 

A_cell{i} = [T h q lambda];


b_cell{i} = [ place_matrix(ones(1,N), c,2)    + M... % for curves
              place_matrix(ones(1,N),-c,2)     + M...
             ones(1,2*N*length(m))*data.Qmax ]';  %for switching lambda
            


% Aeq %%%%%%%%%
% sum of lambda = 1
% driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1;
T =  zeros(N,N*data.npu);
h = zeros(N,data.nn*N) ;
q =  zeros(N,data.np*N) ;

lambda = [];
for k = 1:(data.npi + data.nv)%lambda sizer and driver
    if k == pipe_number
        lambda_part =  repmat(eye(N),1,length(m));
    else
        lambda_part =  zeros(N,data.lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

Aeq = [Aeq; T h q lambda];
beq = [beq; ones(N,1)]; %lambda summation
pipe_number = pipe_number + 1;


% M = M_old;
