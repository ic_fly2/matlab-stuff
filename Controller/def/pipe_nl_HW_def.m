% makes a nlcon constraint type and raises a flag athat it needs to be
% processed further.

[~, hmin] = min(connection_matrix(i,:));
[~, hmax] = max(connection_matrix(i,:));

num_T = sum(n_pumps)*N; % space for T
hmin = (hmin-1)*N;
hmax = (hmax-1)*N;

for j = 1:N
    q_id = num2str(((i-1)+nn)*N+num_T+j);
    h_neg_id = num2str(hmin+j+num_T);
    h_pos_id = num2str(hmax+j+num_T);
    
    HW_form_tmp{j} = ['x(' h_neg_id  ') - x(' h_pos_id ...
    ') + K(' num2str(i) ')* abs(x(' q_id '))*(x(' q_id '))^0.85' ];
end

HW_form{i} = strjoin(HW_form_tmp,' ; ');

nlrhs_cell{i} = ones(N,1)*change(i);    
% nle(i)   = 0; % placed in the later section in make probel 

