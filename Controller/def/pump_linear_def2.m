% Simple linear aproximation of pump as a single straight line
% Qmin T <= q <= Qmax T ; enforce some flow constraints if pump is
% on same as in simple case.
% M(T - 1) <= h2 - h1 - mq -c <= M(1-T) enforce a head flow
% relationship

driver_pumps = [];
for k = 1:length(n_pumps)
    if k == pump_number
        driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
    else
        driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
    end
    driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
end

% pump specific data Q_min and max and delta H for each set of
% pumps!
% min & max flow % must be shorter but may get complicated with more
% pumps active at the same time
% linit flow to OP point range if T1 = 1 and Qmax for all other Ts
%  T1*q_min1 + <= q <= T*q_max
% T*q_min - q <= 0
% -T*q_max + q <= 0
% 
% for j = 1:n_pumps(pump_number)
%     qmins = Q_pump_min(j)*driver_pumps(j,:);
%     qmins(qmins == 0 )= -Qmax;
%     qmaxs = Q_pump_max(j)*driver_pumps(j,:);
%     qmaxs(qmaxs == 0 )= Qmax;
%     
%     T = [place_matrix( eye(N),qmins,2);...
%          place_matrix(-eye(N),qmaxs,2)];
%     h =  zeros(2*N,size(connection_matrix,2)*N);
%     q =  [place_matrix(-eye(N),flow_matrix(i,:),2);...
%           place_matrix( eye(N),flow_matrix(i,:),2)];
%     lambda = zeros(2*N,N*length_lambda);
%     A = [A;...
%         T h q lambda];
%     b = [b;...
%         zeros(N,1);...
%         zeros(N,1)];
% end 
% 

            %%%%%%%%%%%%%% new stuff %%%%%%%%%%%%%%%%%%%%% for convex pipes
                driver_pumps1 = []; 
                qmaxs = [];
                qmins = [];
            for k = 1:length(n_pumps)
                if k == pump_number
                    driver_pumps_part = ones(1,n_pumps(k)); % driving matrix for T identy matrix placement
                    qmaxs_part = Q_pump_max(k,:); qmaxs_part = qmaxs_part(qmaxs_part ~= 0);
                    qmins_part = Q_pump_min(k,:); qmins_part = qmins_part(qmins_part ~= 0);
                else
                    driver_pumps_part =  zeros(1,n_pumps(k)); % make zeros where ever it isn't needed
                    qmaxs_part =zeros(1,n_pumps(k)); 
                    qmins_part =zeros(1,n_pumps(k)); 
                end
                driver_pumps1 = [driver_pumps1 driver_pumps_part]; % add it all together
                qmaxs = [qmaxs qmaxs_part];
                qmins = [qmins qmins_part];
            end
%             T = [place_matrix( eye(N),qmins,2);...
%                 place_matrix(-eye(N),qmaxs,2)];
%             h =  zeros(2*N,size(connection_matrix,2)*N);
%             q =  [place_matrix(-eye(N),flow_matrix(i,:),2);...
%                 place_matrix( eye(N),flow_matrix(i,:),2)];
%             lambda = zeros(2*N,N*length_lambda);
%             A = [A;...
%                 T h q lambda];
%             b = [b;...
%                 zeros(N,1);...
%                 zeros(N,1)];
            
            % first bound (q<= 0) as in q <= Qmax T1 
             T = place_matrix(-Qmax*eye(N),driver_pumps1(1,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
             h = zeros(N,N*nn);
             q = place_matrix(eye(N),flow_matrix(i,:),2); % not so sure
        lambda = zeros(N,N*length_lambda); % positive or negative flow in pipe
        b_part = zeros(N,1) ;
        
             A = [A; T h q lambda];
             b = [b;b_part];
             
             %q >= 0 no reverse flow
             T = zeros(N,N*sum(n_pumps)); 
             h = zeros(N,N*nn);
             q = place_matrix(-eye(N),flow_matrix(i,:),2); % not so sure
        lambda = zeros(N,N*length_lambda); % positive or negative flow in pipe
        b_part = zeros(N,1) ;
        
             A = [A; T h q lambda];
             b = [b;b_part];

% for ii = 1: n_pumps(pump_number) + sum(n_pumps(1:pump_number-1))
%     OP_test(ii,:)


% enforce the head flow relationship
% MT -(-h1+h2) +mq <= M -c
% MT  -h1 +h2  -mq <= M +c

for j = 1:n_pumps(pump_number)
    T = [place_matrix(eye(N)*M,driver_pumps(j,:),2);...
         place_matrix(eye(N)*M,driver_pumps(j,:),2)];
    
    h = [place_matrix(eye(N),-connection_matrix(i,:),2);...
         place_matrix(eye(N), connection_matrix(i,:),2)];
    
    q = [place_matrix(m_linear_pump(pump_number,j)*eye(N), flow_matrix(i,:),2);...
         place_matrix(m_linear_pump(pump_number,j)*eye(N),-flow_matrix(i,:),2)];
    lambda = zeros(2*N,N*length_lambda);
    
    A = [A; T h q lambda];
    b = [b;...
        M-c_linear_pump(pump_number,j)*ones(N,1);...
        M+c_linear_pump(pump_number,j)*ones(N,1)];
%     remove last bit
%  A(end-N:end,:) = [];
%  b(end-N:end,:) = [];
end
pump_number =  pump_number +1;
