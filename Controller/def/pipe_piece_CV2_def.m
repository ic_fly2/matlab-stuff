
%% pipes part
% A

%%%%%%%%%%%% Piecewise approximation section %%%%%%%%%%%%%%%%%%%%%%%%%%%
%% make pipe matracies
% for each pipe

m = [];
c = [];
q_min = [];
q_max = [];
m(1) = pipe(pipe_number).m_data(1);
c(1) = pipe(pipe_number).C_data(1) + change_pipes(i);
q_min(1) = -pipe(pipe_number).Q_lim_data(2);
q_max(1) = pipe(pipe_number).Q_lim_data(2);


% modify m etc.
for j = 2:length(pipe(pipe_number).m_data)-1;
    %+ve
    m_pos(j) = pipe(pipe_number).m_data(j);
    c_pos(j) = pipe(pipe_number).C_data(j) + change_pipes(i);
    q_min_pos(j) = pipe(pipe_number).Q_lim_data(j);
    q_max_pos(j) = pipe(pipe_number).Q_lim_data(j+1);
    %-ve only one value needed!
    
    m_neg(j) = pipe(pipe_number).m_data(j);
    c_neg(j) = -pipe(pipe_number).C_data(j)+ change_pipes(i);
    q_min_neg(j) = -pipe(pipe_number).Q_lim_data(j+1);
    q_max_neg(j) = -pipe(pipe_number).Q_lim_data(j);
    m = [m m_pos(j) m_neg(j)];
    c = [c c_pos(j) c_neg(j)];
    q_min = [q_min q_min_pos(j) q_min_neg(j)];
    q_max = [q_max q_max_pos(j) q_max_neg(j)];
end

% A %%%%%%%%%%%%%
%  h1 - h2 = mq +c if active
% h1 - h2 -mq + M lambda <= c+M
% -h1 +h2 +mq + M lambda <= c+M
%           q + M lambda <= qmax +M
%          -q + M lambda <= -qmin + M
T = zeros(N*4*length(m),N*sum(n_pumps)); % assuming all curves are used for the pipe definition

h =[place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
    place_matrix(place_matrix(eye(N), connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
    zeros(2*length(q_min)*N,size(connection_matrix,2)*N); %For switching lambda on
    ]; %lambda summation

q = place_matrix(...
    [place_matrix( eye(N),-m,1);...  %for curves
    place_matrix( eye(N), m,1);...
    place_matrix( eye(N),ones(length(q_min),1),1);...  for switching lambda
    place_matrix(-eye(N),ones(length(q_max),1),1)]...
    ,flow_matrix(i,:),2); 
     %lambda summation

lambda = [];
for k = 1:n_pipes + n_valves%lambda sizer and driver
    if k == pipe_number
        lambda_part = place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1);
    else
        lambda_part =  zeros(4*N*length(m),lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector for which one is active (does not consider different lengths of m vector!!)


A = [A; T h q lambda];

b_part = [ place_matrix(ones(1,N), c,2)    + M... % for curves
    place_matrix(ones(1,N),-c,2)     + M...
    place_matrix(ones(1,N), q_max,2) + Qmax... %for switching lambda
    place_matrix(ones(1,N),-q_min,2) + Qmax];

b = [b; b_part'];

% Aeq %%%%%%%%%
% sum of lambda = 1
driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1;
T =  zeros(N,N*sum(n_pumps));
h = zeros(N,nn*N) ;
q =  zeros(N,np*N) ;

lambda = [];
for k = 1:n_pipes +n_valves%lambda sizer and driver
    if k == pipe_number
        lambda_part =  repmat(eye(N),1,length(m));
    else
        lambda_part =  zeros(N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

Aeq = [Aeq; T h q lambda];
pipe_number = pipe_number + 1;
beq = [beq; ones(N,1)]; %lambda summation
