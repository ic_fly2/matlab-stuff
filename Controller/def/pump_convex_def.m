%% A
% driver_pumps = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
%             pump_number =2; % testing only
% driver_pumps = [];
% for k = 1:length(data.npu)
%     if k == pump_number
%         driver_pumps_part = ones(1,data.pumps_vec(k)); % driving matrix for T identy matrix placement
%     else
%         driver_pumps_part =  zeros(1,data.pumps_vec(k)); % make zeros where ever it isn't needed
%     end
%     driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
% end

driver_pumps =  zeros(1,data.npu); driver_pumps(pump_number)  = 1;

% driver_pumps2 = [];
% for k = 1:length(data.n_pumps)
%     if k == pump_number
%         if data.n_pumps(pump_number) == 1 %in case there is only one pump in the station
%             driver_pumps_part = 1;
%         else
%             driver_pumps_part = eye(data.n_pumps(pump_number));
%         end
%     else
%         driver_pumps_part =  zeros(data.n_pumps(pump_number),data.n_pumps(k)); % make zeros where ever it isn't needed
%     end
%     driver_pumps2 = [driver_pumps2 driver_pumps_part]; % add it all together
% end

%%%%%%%%%%%%%% new stuff %%%%%%%%%%%%%%%%%%%%%
%% first bound (q<= 0) as in q <= Qmax T1
T1 = place_matrix(-data.Qmax*eye(N),driver_pumps,2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
h1 = zeros(N,N*data.nn);
q1 = place_matrix(eye(N),flow_matrix(i,:),2); % not so sure
lambda1 = zeros(N,N*data.length_lambda); % positive or negative flow in pipe
% b_part1 = zeros(N,1) ; % dealt with below

%% q >= 0 no reverse flow ever!
T2 = zeros(N,N*data.npu);
h2 = zeros(N,N*data.nn);
q2 = place_matrix(-eye(N),flow_matrix(i,:),2); % not so sure
lambda2 = zeros(N,N*data.length_lambda); % positive or negative flow in pipe


A_part1 = [T1 h1 q1 lambda1; T2 h2 q2 lambda2];
b_part1 = zeros(2*N,1) ; % for both

%% second bound (Delta h <= mq + c ...)
A_part2 = [];
b_part2 = [];
% for j = 1:data.n_pumps(pump_number)
    %                 m_Pu =  Pump_station(pump_number).pump(j).curves(:,1);
    %                 c_Pu =  Pump_station(pump_number).pump(j).curves(:,2);
    
    m_Pu =  pumps{pump_number}.conv_m';
    c_Pu =  pumps{pump_number}.conv_C';
    
    
    % new form:
    % MT +h2 -h1 -mq <= M+c - elevation_change
    T3 = place_matrix(place_matrix(M*eye(N),ones(length(m_Pu),1),1),driver_pumps,2);
    h3 = place_matrix(place_matrix(eye(N),ones(length(m_Pu),1),1), connection_matrix(i,:),2);
    q3 = place_matrix(place_matrix(eye(N),-m_Pu,1),flow_matrix(i,:),2); % not so sure
    lambda3 = zeros(N*length(m_Pu),N*data.length_lambda); % positive or negative flow in pipe
    b_part3 = place_matrix(ones(N,1),M+c_Pu-pumps{pump_number}.change_gz,1);
    
    A_part2 = [A_part2; T3 h3 q3 lambda3]; % add a new line
    b_part2 = [b_part2; b_part3];
% end

%% third bound (Delta h >= 0) as in Delta h >= M(1 - T) (inserted to ensure serial pump stuff)
% MT +h2 -h1  >= M
% -MT -h2 +h1 <= -M
A_part3 = [];
b_part3 = [];
% for j = 1:data.n_pumps(pump_number)
    
    T4 = place_matrix(-M*eye(N) ,  driver_pumps   ,2);
    h4 = place_matrix(eye(N)    ,-connection_matrix(i,:),2);
    q4 = zeros(N,data.np*N); % not so sure
    lambda4 = zeros(N,N*data.length_lambda); % positive or negative flow in pipe
    b_part4 = place_matrix(ones(N,1),M-pumps{pump_number}.change_gz,1);
    
    A_part3 = [A_part3; T4 h4 q4 lambda4]; % add a new line
    b_part3 = [b_part3; b_part4];


% end
%% Add it all together
A_cell{i} = sparse([A_part1;A_part2 ]);
b_cell{i} = [b_part1;b_part2];
% 
% A_cell{i} = sparse([A_part1;A_part2; A_part3 ]);
% b_cell{i} = [b_part1;b_part2; b_part3];

% possibly add sum T  <= 1

% debugging
% A_test = [A_part1;A_part2 ]; A_test(:,[2,3,5,6,7,9:16,18:end]) = []
% b_test = [b_part1;b_part2];

pump_number = pump_number + 1; % increase the pump station counter
