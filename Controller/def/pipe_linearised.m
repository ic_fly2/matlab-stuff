% run the linearised verion of the LP;
% mass balance:
% A12' q = d

% length_of_lambda (only for CVs) length_lambda 
mass_balance

% Energy balance
% A12 Dh = m eye(q) + c
% A12 Dh - m eye(q) = c

T = zeros(N*npi,sum(n_pumps)*N);

h = [];
for i = 1:npi 
    h = [h; place_matrix(eye(N), connection_matrix(i,:),2)];
end

q = [eye(N*npi) zeros(N*npi,N*(np-npi))];
lambda = zeros(N*npi,N*length_lambda);

Aeq = [Aeq; T h q lambda];
beq = [beq; singlefile(c)'];

