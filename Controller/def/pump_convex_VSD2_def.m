%% A
% driver_pumps = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
%             pump_number =2; % testing only
% driver_pumps1 = [];
% for k = 1:data.npu
%     if k == pump_number
%         driver_pumps_part = ones(1,data.pumps_vec(k)); % driving matrix for T identy matrix placement
%     else
%         driver_pumps_part =  zeros(1,data.pumps_vec(k)); % make zeros where ever it isn't needed
%     end
%     driver_pumps1 = [driver_pumps1 driver_pumps_part]; % add it all together
% end
% 
% driver_pumps2 = [];
% for k = 1:length(n_pumps)
%     if k == pump_number
%         if n_pumps(pump_number) == 1 %in case there is only one pump in the station
%             driver_pumps_part = 1;
%         else
%             driver_pumps_part = eye(n_pumps(pump_number));
%         end
%     else
%         driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
%     end
%     driver_pumps2 = [driver_pumps2 driver_pumps_part]; % add it all together
% end

driver_pumps =  zeros(1,data.npu); driver_pumps(pump_number)  = 1;

%%%%%%%%%%%%%% new stuff %%%%%%%%%%%%%%%%%%%%%
% first bound (q<= 0) as in q <= Qmax T1
T1 = place_matrix(-data.Qmax*eye(N),driver_pumps(1,:),2); %place_matrix(-M*eye(N),driver_pumps(j,:),2);
h1 = zeros(N,N*data.nn);
q1 = place_matrix(eye(N),flow_matrix(i,:),2); % not so sure
lambda1 = zeros(N,N*data.length_lambda); % positive or negative flow in pipe
b_part1 = zeros(N,1) ; % dealt with below

%q >= 0 no reverse flow
T2 = zeros(N,N*data.npu);
h2 = zeros(N,N*data.nn);
q2 = place_matrix(-eye(N),flow_matrix(i,:),2); % not so sure
lambda2 = zeros(N,N*data.length_lambda); % positive or negative flow in pipe

A_part1 = [T2 h1 q1 lambda1 T1 ;T2 h2 q2 lambda2 T2];
% A_part1 = [T1 h1 q1 lambda1; T2 h2 q2 lambda2];
b_part1 = zeros(2*N,1) ; % for both


A_part2 = [];
b_part2 = [];
% for j = 1:n_pumps(pump_number)
    %                 m_Pu =  Pump_station(pump_number).pump(j).curves(:,1);
    %                 c_Pu =  Pump_station(pump_number).pump(j).curves(:,2);
    
     m_Pu =  pumps{pump_number}.conv_m';
    c_Pu =  pumps{pump_number}.conv_C';
    
     m_Pu_next = m_Pu; % Pump_station(pump_number).pump(j+1).curves(:,1);
     c_Pu_next = c_Pu; % Pump_station(pump_number).pump(j+1).curves(:,2);
    
    
    % new form:
    % MT +h2 -h1 -mq <= M+c - elevation_change
    T_bar = -place_matrix(place_matrix(eye(N),c_Pu_next,1),driver_pumps,2);
    T3 = place_matrix(place_matrix(M*eye(N),ones(length(m_Pu_next),1),1),driver_pumps,2);
    h3 = place_matrix(place_matrix(eye(N),ones(length(m_Pu_next),1),1), connection_matrix(i,:),2);
    q3 = place_matrix(place_matrix(eye(N),-m_Pu_next,1),flow_matrix(i,:),2); % not so sure
    lambda3 = zeros(N*length(m_Pu_next),N*data.length_lambda); % positive or negative flow in pipe
    b_part3 = place_matrix(ones(length(c_Pu_next)*N,1),M-change(i),1);
    
    A_part2 = [A_part2; T_bar h3 q3 lambda3  T3]; % add a new line
    b_part2 = [b_part2; b_part3];
    
    
    A_part3 = [ place_matrix(eye(N),driver_pumps,2)... %T
                zeros(N,N*data.nn)... %h
                zeros(N,N*data.np)... %q
                zeros(N,N*data.length_lambda)... %lambda
               -place_matrix(eye(N),driver_pumps,2)]; % T_bar
            
   b_part3 = zeros(N,1) ;             
                
% end





A_cell{i} = sparse([A_part1;A_part2;A_part3]);
b_cell{i} = [b_part1;b_part2;b_part3];

% possibly add sum T  <= 1


pump_number = pump_number + 1; % increase the pump station counter
