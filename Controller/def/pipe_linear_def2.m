% find OP for each time step!
% find m and c at OP
%if i == 1 || i == 5 %|| i == 3 
change_lin = change(i)*ones(N,1);
% if exist('flow_display','var')
    %which iteration is it update heads
%     change_lin = change_lin
% else
    % first run
%     q_star = q_lim_maker(pipedata(pipe_number))*0.7*ones(N,1);
    
    % flow from van_zyl piece:
%     flup = [0.1632    0.1021    0.0986   -0.0445    0.1391  ];
%     q_star = flup(pipe_number)*ones(N,1);
        load('flow_display')
        q_star = abs(flow_display(:,i) );
        q_star(q_star == 0) = mean(q_star(q_star ~= 0));
        q_star(q_star == 0) = q_lim_maker(pipedata(pipe_number))*0.7
        
% end
% head loss for pipe at q_star flow:
% h_star = a_quad_pipe(pipe_number)*q_star.^2 + b_quad_pipe(pipe_number)*q_star + change_lin;
% supossed headloss equation gradient:
h_star_dash = 2*a_quad_pipe(pipe_number)*q_star + b_quad_pipe(pipe_number);

% cheat:
load('head_display')
h_star = abs(head_display*-connection_matrix(i,:)');
% gradient needed for y = mx style intercept at (q_star,h_star):
m_orig = (h_star)./q_star;%-change_lin


% some thing clever to choose m and c:
% STUFF GOES HERE
% for the first one:
m_stuff = diag(m_orig);
% c_stuff = change_lin;

c_stuff = 0*ones(N,1);

% finally:
% m_stuff = diag(h_star_dash);
% c_stuff = h_star - h_star_dash.*q_star;




% make the matrix (Save the matrix somewhere and then regen later)
% h1 -h2 -mq = c %but for every timestep m can differ.
T = zeros(N,N*sum(n_pumps));
h = place_matrix( eye(N),  connection_matrix(i,:),2);
q = place_matrix(-m_stuff,  flow_matrix(i,:),2);
lambda = zeros(N,N*length_lambda);
Aeq_pipes(1:N,:,pipe_number) = [T h q lambda];


beq_pipes(:,1,pipe_number) = c_stuff;

% else
% end
pipe_number =  pipe_number +1;