% tester1
%%%%%%%%%%%% Piecewise approximation section %%%%%%%%%%%%%%%%%%%%%%%%%%%
%% make pipe matracies
% for each pipe

% M_old = M;
% M = M_pipes(pipe_number)+5;
% % 
%  if M > 999;
%       M = 999;
% % elseif M < 999
% %     M = 999;
%   end

m = [];
c = [];
q_min = [];
q_max = [];
m(1) = pipes{pipe_number}.m_data(1);
c(1) = pipes{pipe_number}.C_data(1) + pipes{pipe_number}.change_gz;
q_min(1) = -pipes{pipe_number}.Q_lim_data(2);
q_max(1) = pipes{pipe_number}.Q_lim_data(2);

for j = 2:length(pipes{pipe_number}.m_data);
    %+ve
    m_pos(j) = pipes{pipe_number}.m_data(j);
    c_pos(j) = pipes{pipe_number}.C_data(j) + pipes{pipe_number}.change_gz;
    q_min_pos(j) = pipes{pipe_number}.Q_lim_data(j);
    q_max_pos(j) = pipes{pipe_number}.Q_lim_data(j+1);
    %-ve
%     m_neg(j) = pipe(pipe_number).m_data(j);
%     c_neg(j) = -pipe(pipe_number).C_data(j)+ change_pipes(i);
%     q_min_neg(j) = -pipe(pipe_number).Q_lim_data(j+1);
%     q_max_neg(j) = -pipe(pipe_number).Q_lim_data(j);
    m = [m m_pos(j)];
    c = [c c_pos(j)];
    q_min = [q_min q_min_pos(j)];
    q_max = [q_max q_max_pos(j)];
end
q_min(q_min <= 0) = 0;

% placefiller matrix to replace eye(N) with sufficient zeros for CV usage
% EYE = diag([ones(1,lambda_parts_length(pipe_number)-1) 0]);
% A %%%%%%%%%%%%%
%  h1 - h2 = mq +c if qin range

% -h1 +h2 +mq + M lambda <= c+M
%  h1 -h2 -mq + M lambda <= -c+M
%           q + M lambda <= qmax + M
%          -q + M lambda <= -qmin + M
T = zeros(N*4*length(m),N*data.npu); % assuming all curves are used for the pipe definition

%For the curves
h1 =[place_matrix(...
        place_matrix(eye(N),-connection_matrix(i,:),2)...
    ,ones(length(m),1),1);... %For the curves
    place_matrix(...
    place_matrix(eye(N), connection_matrix(i,:),2)...
    ,ones(length(m),1),1);... %For the curves
    ]; %lambda summation


q1 = place_matrix(...
    [place_matrix( eye(N), -m ,1);...  %for curves
    place_matrix(eye(N), m ,1)]...
    ,flow_matrix(i,:),2); 
    

h2 = zeros(2*length(m)*N,size(connection_matrix,2)*N); %For switching lambda on

q2 =   place_matrix([...
    place_matrix( eye(N),ones(length(q_min),1),1);...  for switching lambda
    place_matrix(-eye(N),ones(length(q_max),1),1)]...
    ,flow_matrix(i,:),2); 


%lambda summation

% lambda = [];
% for k = 1:n_pipes + n_valves%lambda sizer and driver
%     if k == pipe_number
%         lambda_part = place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1);
%     else
%         lambda_part =  zeros(4*N*length(m),lambda_parts_length(k)*N); % make zeros where ever it isn't needed
%     end
%     lambda = [lambda lambda_part ]; % add it all together
% end


% lambda =  [zeros(4*N*length(m),sum(lambda_parts_length(1:pipe_number-1))*N) ...
%             place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1)  zeros(4*N*length(m),N) ...
% %            place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1) ...
%            zeros(4*N*length(m),sum(lambda_parts_length(pipe_number+1:end))*N)];

% lambda =  [zeros(4*N*length(m),sum(lambda_parts_length(1:pipe_number-1))*N)...
%            place_matrix(eye(N*length(m)),[M M Qmax Qmax]',1)  zeros(4*N*length(m),N)...  
%            zeros(4*N*length(m),sum(lambda_parts_length(pipe_number+1:end))*N)];

lambda =  [zeros(4*N*length(m),sum(data.lambda_parts_length(1:pipe_number-1))*N) ...
           [place_matrix(eye(N*length(m)),[M M]',1); ...
           diag(place_matrix(ones(N,1),data.Qmax - q_max,1));
           diag(place_matrix(ones(N,1),data.Qmax + q_min,1))] zeros(4*N*length(m),N) ...
           zeros(4*N*length(m),sum(data.lambda_parts_length(pipe_number+1:end))*N)];


A_part1 = [ T [h1; h2] [q1; q2] lambda];
 b_part1 = [ place_matrix(ones(1,N), c,2)   + M... % for curves
              place_matrix(ones(1,N),-c,2)    + M...
             ones(1,2*N*length(m))*data.Qmax ]'; 
% b_part1 = [ place_matrix(ones(1,N), c,2)     + M... % for curves
%             place_matrix(ones(1,N),-c,2)     + M...
%             place_matrix(ones(1,N), q_max,2) + Qmax... %for switching lambda
%             place_matrix(ones(1,N),-q_min,2) + Qmax]';



% Aeq %%%%%%%%%
% sum of lambda = 1
% driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector for which one is active (does not consider different lengths of m vector!!)

driver_pipes = zeros(1,data.npi); driver_pipes(pipe_number) = 1;
T =  zeros(N,N*data.npu);
h = zeros(N,data.nn*N) ;
q =  zeros(N,data.np*N) ;

lambda = [];
for k = 1:data.npi +data.nv%lambda sizer and driver
    if k == pipe_number
        lambda_part =  repmat(eye(N),1,data.lambda_parts_length(k));
    else
        lambda_part =  zeros(N,data.lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

Aeq = [Aeq;T h q lambda];

beq  = [beq;ones(N,1)]; %lambda summation

%% % h1 - h2 <= 0 (change) if q-ve and q = 0
% -q +M lambda(last) <= 0
%  q -M lambda(last) <= 0
% h1 - h2 -M lambda(last)  <= change



T = zeros(N,N*data.npu);
h = place_matrix(eye(N),-connection_matrix(i,:),2);
q =  zeros(N,data.np*N) ;
lambda = [zeros(N,sum(data.lambda_parts_length(1:pipe_number-1))*N)...
           place_matrix(eye(N),[zeros(1,data.lambda_parts_length(pipe_number)-1) 1],2)...
           zeros(N,sum(data.lambda_parts_length(pipe_number+1:end))*N)];

A_part2 = [T h q  [M-pipes{pipe_number}.change_gz]*lambda];
b_part2  = ones(N,1)*M;




% q >= 0 for all lambda
A_part3 = [zeros(N, N*data.npu+ data.nn*N) place_matrix(-eye(N),flow_matrix(i,:),2) zeros(N,data.length_lambda*N)];
b_part3 = [zeros(N,1)];

% q <= 0 if last lambda = 1
A_part4 = [zeros(N, N*data.npu+ data.nn*N)...
            place_matrix(eye(N),flow_matrix(i,:),2)...
            lambda*data.Qmax];
b_part4 = [ones(N,1)*data.Qmax];

A_cell{i} = [A_part1; A_part2; A_part3; A_part4];
b_cell{i} = [b_part1; b_part2; b_part3; b_part4];


pipe_number = pipe_number + 1;
% M = M_old;