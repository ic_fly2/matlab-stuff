
M_old = M;
% M =  ceil(abs(h_max*connection_matrix(i,:)'))+5;
m_convex = [];
c_convex = [];

for j = 1:length(pipe(pipe_number).m_data);
    m_convex(j) = pipe(pipe_number).m_data(j);
    c_convex(j) = pipe(pipe_number).C_data(j);
end

% positive flow
%  h1 - h2 >= mq +c+ elevation
% -h1 + h2 + mq + M*lambda <= M -c  -elevation 
no_eq = length(m_convex); % number of equations needed to define convex bound region

T = zeros(no_eq*N,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
h = place_matrix(place_matrix(eye(N),  connection_matrix(i,:),2),ones(no_eq,1),1);
q = place_matrix(place_matrix(eye(N), m_convex,1),flow_matrix(i,:),2); % not so sure

lambda = [];
for k = 1:n_pipes +n_valves %lambda sizer and driver
    if k == pipe_number
        lambda_part = place_matrix(M*eye(N),ones( no_eq,1),1);
    else
        lambda_part =  zeros(no_eq*N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

A_part1 = [T h q lambda];

% b
b_part1 = place_matrix(ones(N,1),-c_convex + M - change(i),1);


% negative flow
% h1 - h2 <= mq -c
% -h1 + h2 + mq - M*lambda <=-c %old
% h1 - h2 - mq - M*lambda <=-c + elevation %new
T = zeros(no_eq*N,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
h = place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(no_eq,1),1);
q = place_matrix(place_matrix(eye(N),-m_convex,1),flow_matrix(i,:),2); % not so sure

lambda = [];
for k = 1:n_pipes +n_valves
    if k == pipe_number
        lambda_part = place_matrix(-M*eye(N),ones( no_eq,1),1);
    else
        lambda_part =  zeros(no_eq*N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

A_part2 = [T h q lambda];

% b
b_part2 = place_matrix(ones(N,1),-c_convex + change(i),1); %negative because double negative to get them above the y axis

%
%
%% lambda part
% Qmax(lambda- 1) <= q <= Qmax*lambda
%   Qmax*lambda -q <= Qmax
% - Qmax*lambda  + q <= 0

%A
T = zeros(N*2,N*sum(n_pumps));
h = zeros(2*N,size(connection_matrix,2)*N);
q = [place_matrix(-eye(N),flow_matrix(i,:),2);...
     place_matrix( eye(N),flow_matrix(i,:),2)];

lambda = [];
for k = 1:n_pipes +n_valves %lambda sizer and driver
    if k == pipe_number
        lambda_part = [Qmax*eye(N);...
            -Qmax*eye(N)]; % place_matrix(M*eye(N*length(m)),ones(4,1),1);
    else
        lambda_part =  zeros(2*N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

A_part3 = [ T h q lambda  ];
%b
b_part3 = [Qmax*ones(N,1);...
    zeros(N,1)];

% add the parts
A = [A; A_part1; A_part2; A_part3];
b = [b; b_part1; b_part2; b_part3];

% limit flow at the top:
% +q
% h1 - h2 <= m_lim q + elevation
% h1 - h2 -m_lim q + M*lambda <= M + elevation 

%-q
% h1 - h2 >= m_lim q + elevation
% -h1 + h2 +m_lim q - M*lambda <= - elevation

%combined:
%  h1 - h2 -m_lim q + M*lambda <= M + elevation 
% -h1 + h2 +m_lim q - M*lambda <=   - elevation

m_lim = m_convex(end) + c_convex(end)/q_lim(i);

T = zeros(2*N,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
h = place_matrix(place_matrix(eye(N),  connection_matrix(i,:),2), [-1 1],1);
q = place_matrix(place_matrix(eye(N), [-m_lim m_lim] ,1),flow_matrix(i,:),2); % not so sure
lambda = lambda*M*2; % is correct from above
A = [A; T h q lambda];

% b
b = [b; ones(N,1)*(change(i)+M*2) ;-ones(N,1)*change(i)];

%additional contraints for speed: (turned out to be pointless)
% if strcmp(def,'van_zyl') && pipe_number ~= 3
% %     H_max_set = [113.3820   60.0671  36.5957 80.5106 120.1342];
% %     H_max = H_max_set(pipe_number);
% %     Q_max = q_lim(i);
% % %     General:  h1 - h2 <= m(q - Q_max) + H_max
% % %     For top:  h1 - h2 -mq <= -m*Q_max + H_max
% % %     For bot:  -h1 + h2 +mq <= -m*Q_max + H_max
% %     T = zeros(1*N,N*sum(n_pumps));
% %     h = place_matrix(eye(N),-connection_matrix(i,:),2);
% %     q = place_matrix(m_convex(2)*eye(N),flow_matrix(i,:),2);
% %     lambda = zeros(1*N,N*(n_pipes + n_valves));
% %     A = [A; T h q lambda];
% %     b = [b; ones(N,1)*m_convex(2)*Q_max + H_max];


%     T = zeros(1*N,N*sum(n_pumps));
%     h = [place_matrix(eye(N),-connection_matrix(i,:),2)];
%     q = zeros(1*N,N*np);
%     lambda = zeros(1*N,N*(n_pipes + n_valves));
%     A = [A; T h q lambda];
%     b = [b; ...
%         ones(N,1)*Delta_H_max(pipe_number) + change(i)];
    
    
% %     T = zeros(2*N,N*sum(n_pumps));
% %     h = [place_matrix(eye(N),-connection_matrix(i,:),2);...
% %          place_matrix(eye(N), connection_matrix(i,:),2);];
% %     q = zeros(2*N,N*np);
% %     lambda = zeros(2*N,N*(n_pipes + n_valves));
% %     A = [A; T h q lambda];
% %     b = [b; ...
% %         ones(N,1)*Delta_H_max(pipe_number) + change(i);...
% %         ones(N,1)*Delta_H_max(pipe_number) - change(i) ];
% end

pipe_number = pipe_number + 1;
M = M_old;
