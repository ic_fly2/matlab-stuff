
% swith lambda to 1 for positive q
% same as in pipe_convex
% eq 1: -q +M lambda <= M
% eq 2: q - M lambda <= 0
T = zeros(N*2,N*sum(n_pumps));
h = zeros(2*N,size(connection_matrix,2)*N);
q = [place_matrix(-eye(N),flow_matrix(i,:),2);...
    place_matrix( eye(N),flow_matrix(i,:),2)];

lambda = [];
for k = 1:n_pipes +n_valves%lambda +ve -ve flow
    if k == pipe_number
        lambda_part = [Qmax*eye(N);...
            -Qmax*eye(N)]; % place_matrix(M*eye(N*length(m)),ones(4,1),1);
    else
        lambda_part =  zeros(2*N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
    end
    lambda = [lambda lambda_part ]; % add it all together
end

% addition of eq 1 & 2 to A & b
A = [A; T h q lambda  ];
b= [b;...
    Qmax*ones(N,1);...
    zeros(N,1)];

% Q
% eq 3: -(h1 - h2) + a+ve q^2 + b+ve + M lambda <= M -c
% eq 4:  (h1 - h2) - a+ve q^2 - b+ve + M lambda <= M +c % sticken
% eq 5: -(h1 - h2) + a-ve q^2 + b-ve - M lambda <=   -c 
% eq 6:  (h1 - h2) - a-ve q^2 - b-ve - M lambda <=    c % sticken
% identity matix in flow parts where a is q^2 coefficient
for j = 1:N
    q_a = zeros((N*np));
    q_a((i-1)*N+j,(i-1)*N+j) =  a_quad_pipe(pipe_quad_number);
    
    % quadratic part:                 T                        h                            q        lambda
    Q_pipe{1,j,pipe_quad_number} = sparse(blkdiag(zeros(N*sum(n_pumps)),zeros(size(connection_matrix,2)*N), q_a,zeros(length_lambda*N)));
%     Q_pipe{2,j,pipe_quad_number} = sparse(blkdiag(zeros(N*sum(n_pumps)),zeros(size(connection_matrix,2)*N),-q_a,zeros(length_lambda*N)));
    Q_pipe{3,j,pipe_quad_number} = sparse(blkdiag(zeros(N*sum(n_pumps)),zeros(size(connection_matrix,2)*N),-q_a,zeros(length_lambda*N)));
%     Q_pipe{4,j,pipe_quad_number} = sparse(blkdiag(zeros(N*sum(n_pumps)),zeros(size(connection_matrix,2)*N), q_a,zeros(length_lambda*N)));
    
    j_driver = zeros(N,1); j_driver(j) =1;
    delta_h = place_matrix(j_driver,-connection_matrix(i,:)',1);
    q_b = place_matrix(j_driver,flow_matrix(i,:)',1);
    
    lambda = [];
    for k = 1:n_pipes +n_valves
        if k == pipe_number
            lambda_part = j_driver;
        else
            lambda_part =  zeros(lambda_parts_length(k)*N,1); % make zeros where ever it isn't needed
        end
        lambda = [lambda; lambda_part ]; % add it all together
    end
    
    l_pipe{1,j,pipe_quad_number} = sparse([ zeros(N*sum(n_pumps),1);...
        -delta_h ; ...
        q_b*b_quad_pipe(pipe_quad_number); ...
        lambda*M]);
    
    %     l_pipe{2,j,pipe_quad_number} = sparse([ zeros(N*sum(n_pumps),1);...
    %         delta_h ; ...
    %         -q_b*b_quad_pipe(pipe_quad_number); ...
    %         lambda*M]);
    %
    l_pipe{3,j,pipe_quad_number} = sparse([ zeros(N*sum(n_pumps),1);...
        -delta_h ; ...
        q_b*b_quad_pipe(pipe_quad_number); ... % b negative == b positive
        -lambda*M]);
    
%         l_pipe{4,j,pipe_quad_number} = sparse([ zeros(N*sum(n_pumps),1);...
%             delta_h ; ...
%             -q_b*b_quad_pipe(pipe_quad_number); ...
%             -lambda*M]);
    
    r_pipe{1,j,pipe_quad_number} = M-c_quad_pipe(pipe_number); % assumes quad_pipe_number = pipe_number!
    %     r_pipe{2,j,pipe_quad_number} = M+c_quad_pipe(pipe_number);
    r_pipe{3,j,pipe_quad_number} = -c_quad_pipe(pipe_number);
%         r_pipe{4,j,pipe_quad_number} =  c_quad_pipe(pipe_number);
end
pipe_quad_number = pipe_quad_number +1;
pipe_number =  pipe_number +1;