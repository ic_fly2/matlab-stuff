

%             % pump specific data Q_min and max and delta H for each set of
%             % pumps!

driver_pumps1 = [];
qmaxs = [];
qmins = [];
for k = 1:data.npu
    if k == pump_number
        driver_pumps_part = ones(1,data.pumps_vec(k)); % driving matrix for T identy matrix placement
        qmaxs_part = Q_pump_max(k,:); qmaxs_part = qmaxs_part(qmaxs_part ~= 0);
        qmins_part = Q_pump_min(k,:); qmins_part = qmins_part(qmins_part ~= 0);
    else
        driver_pumps_part =  zeros(1,n_pumps(k)); % make zeros where ever it isn't needed
        qmaxs_part =zeros(1,n_pumps(k));
        qmins_part =zeros(1,n_pumps(k));
    end
    driver_pumps1 = [driver_pumps1 driver_pumps_part]; % add it all together
    qmaxs = [qmaxs qmaxs_part];
    qmins = [qmins qmins_part];
end

% Idea:
% q >= T1qmin1 + T2qmin2 ... >> T1qmin1 + T2qmin2 -q =< 0
% q <= T1qmax1 + T2qmax2 ... >> -T1qmax1 -T2qmax2 +q <= 0

T = [place_matrix( eye(N),qmins,2);...
    place_matrix(-eye(N),qmaxs,2)];
h =  zeros(2*N,size(connection_matrix,2)*N);
q =  [place_matrix(-eye(N),flow_matrix(i,:),2);...
    place_matrix( eye(N),flow_matrix(i,:),2)];
lambda = zeros(2*N,N*length_lambda);
A_part1 = [T h q lambda];
b_part1 = [zeros(N,1);...
           zeros(N,1)];

% enforce the head difference
% M(T - 1) <= h2 - h1 - (Delta_h - Elevation_change) <= M(1-T) enforce a certain head
% MT  +h1 -h2 <= M - Delta_h + Elevation_change
% MT  -h1 +h2 <= M + Delta_h - Elevation_change
% based on the method used in gleixner et al, towards globally optimal operation
driver_pumps = [];
for k = 1:data.npu
    if k == pump_number
        driver_pumps_part = eye(n_pumps(pump_number)); % driving matrix for T identy matrix placement
    else
        driver_pumps_part =  zeros(n_pumps(pump_number),n_pumps(k)); % make zeros where ever it isn't needed
    end
    driver_pumps = [driver_pumps driver_pumps_part]; % add it all together
end
 A_part2 = [];
    b_part2 = [];
for j = 1:n_pumps(pump_number)
    T = [place_matrix(eye(N)*M          ,driver_pumps(j,:),2);...
        place_matrix(eye(N)*M          ,driver_pumps(j,:),2)];
    
    h = [place_matrix(eye(N),-connection_matrix(i,:),2);...
        place_matrix(eye(N), connection_matrix(i,:),2)];
    
    q = [zeros(N,N*np);...
        zeros(N,N*np)];
    lambda = zeros(2*N,N*length_lambda);
    
    A_part2 = [A_part2; T h q lambda];
    b_part2 = [b_part2;...
        M+change(i)-Delta_H(pump_number,j)*ones(N,1)  ;...
        M-change(i)+Delta_H(pump_number,j)*ones(N,1)];%
end
A_cell{i} = [A_part1; A_part2];
b_cell{i} = [b_part1; b_part2];

pump_number =  pump_number +1;