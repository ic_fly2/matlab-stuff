% k = 0 so head in = head out if head in > head out else q = 0
            % need to take care of elevation change
            % 0 <= q <= Qmax*lambda
            % q - Qmax*lambda <= 0
            % -q <= 0 
            T =  [zeros(N,N*sum(n_pumps));...
                  zeros(N,N*sum(n_pumps))];
            h = [zeros(N,size(connection_matrix,2)*N);...
                 zeros(N,size(connection_matrix,2)*N)];
            q = [place_matrix( eye(N),flow_matrix(i,:),2);...
                 place_matrix(-eye(N),flow_matrix(i,:),2)];
            lambda = [];
            for k = 1:n_pipes +n_valves %lambda sizer and driver
                if k == pipe_number
                    lambda_part = [-eye(N)]; % place_matrix(M*eye(N*length(m)),ones(4,1),1);
                else
                    lambda_part =  zeros(N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
                end
                lambda = [lambda lambda_part ]; % add it all together
            end 
            lambda1 = [lambda;zeros(N,N*length_lambda)]*Qmax ;
            A = [A; T h q lambda1]; 
            b = [b;zeros(2*N,1)]; 
            
            % h1 - h2 = change (in elevation) if lambda is positive
            % M(lambda - 1) <= h1 - h2 - change <= M(1 - lambda)
            % -h1 +h2 + M*lambda <= M - change
            %  h1 -h2 + M*lambda <= M + change
            
            T =  zeros(2*N,N*sum(n_pumps));
            h = [place_matrix(eye(N), connection_matrix(i,:),2);...
                 place_matrix(eye(N),-connection_matrix(i,:),2)];
            q =  zeros(2*N,size(flow_matrix,2)*N);
            lambda = [-lambda*M; ...
                      -lambda*M];
                 
            A = [A; T h q lambda];
            
            b = [b;
                 ones(N,1)*(M - change(i));...
                 ones(N,1)*(M + change(i))];  