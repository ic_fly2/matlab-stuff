M_orig = M;
% set the derivativ to zero and place in the equation
% M = c_quad_pump(pump_number,1)-b_quad_pump(pump_number,1)^2/(4*a_quad_pump(pump_number,1))+1;

% No flow if T = 0
% -QmaxT + q <= 0
% -q <= 0
driver_pumps0 = [];
for k = 1:data.npu %pump driver
    if k == pump_number
        driver_pumps_part = ones(1,data.pumps_vec(pump_number)); % driving matrix for T identy matrix placement
    else
        driver_pumps_part =  zeros(1,data.pumps_vec(k)); % make zeros where ever it isn't needed
    end
    driver_pumps0 = [driver_pumps0 driver_pumps_part]; % add it all together
end
T = [place_matrix(-data.Qmax*eye(N),driver_pumps0,2);...
    zeros(N,N*data.npu)];
h = [zeros(N,size(connection_matrix,2)*N);...
    zeros(N,size(connection_matrix,2)*N)];
q = [place_matrix( eye(N),flow_matrix(i,:),2);...
    place_matrix(-eye(N),flow_matrix(i,:),2)];
lambda = zeros(2*N,N*data.length_lambda);


A_cell{i} = [T h q lambda];
b_cell{i} = [zeros(2*N,1)];

% Q
% M(T - 1) <= h2 -h1 -aq^2  -bq -c <= M(1-T) 
% MT  +h1 -h2 +aq^2 +bq <= M -c
% MT  -h1 +h2 -aq^2 -bq <= M +c
% identity matix in flow parts where a is q^2 coefficient
for k = 1:data.pumps_vec(pump_number)
    for j = 1:N
       
%         q_a = zeros((N*np));
%         q_a((i-1)*N+j+(k-1)*N ,(i-1)*N+j+(k-1)*N ) =  a_quad_pump(pump_number,k);
%         (i-1)*N+j+(k-1)*N
%         sum(n_pumps(1:pump_number-1))*N+(k-1)*N+j
        
%         q_a(sum(n_pumps(1:pump_number-1))*N+(k-1)*N+j,
        
%         better:
        q_a = blkdiag(zeros((i-1)*N),...
                    blkdiag(zeros(j-1), pumps{pump_number}.a,zeros(N-j)),... %bits of j either side
             zeros((data.np-i)*N));

        % quadratic part:                 T                             h                               q        lambda
        Q_pump{1,j,k,pump_number} = sparse(blkdiag(zeros(N*data.npu),zeros(data.nn*N), q_a,zeros(data.length_lambda*N)));
        Q_pump{2,j,k,pump_number} = sparse(blkdiag(zeros(N*data.npu),zeros(data.nn*N),-q_a,zeros(data.length_lambda*N)));
        
        
        %                      T = zeros(N*sum(n_pumps),1);
        %                      T(k+sum(n_pumps(1:pump_number)) - n_pumps(pump_number)) = M;
        T= [];
        for g = 1:data.npu
            T_part = zeros(N,1);
            if g == pump_number % sum(n_pumps(1:pump_number-1))+k%
                T_part(j) = M;
            else
                T_part(j) = 0;
            end
            T = [T; T_part]; % add it all together
        end
        j_driver = zeros(N,1); j_driver(j) =1;
        delta_h = place_matrix(j_driver,-connection_matrix(i,:)',1);
        q_b = place_matrix(j_driver,flow_matrix(i,:)',1);

        l_pump{1,j,k,pump_number} = sparse([ T;...
            delta_h ; ...
            q_b*pumps{pump_number}.b; ...
            zeros(data.length_lambda*N,1)]);

        
        l_pump{2,j,k,pump_number} = sparse([ T;...
            -delta_h ; ...
            -q_b*pumps{pump_number}.b; ...
            zeros(data.length_lambda*N,1)]);

        
%         r_pump{1,j,k,pump_number} = M - c_quad_pump(pump_number,k)+(connection_matrix(i,:) ==1)*Elevation; % multipyl by smaller number to shrink
%         r_pump{2,j,k,pump_number} = M + c_quad_pump(pump_number,k)-(connection_matrix(i,:) ==1)*Elevation; %c_includes change due to elevation
        
        r_pump{1,j,k,pump_number} = M - pumps{pump_number}.c; % multipyl by smaller number to shrink
        r_pump{2,j,k,pump_number} = M + pumps{pump_number}.c; %c_includes change due to elevation
        
        
    end
end
M = M_orig;
pump_number =  pump_number +1;