function [A_dr,b_dr] = minimum_power_setting(pumps,Power,OF_spec,...
                        N,nn,length_lambda,desired_share,loc_Pumps)
% minimum power consumtions
% power of the demand
power_T_list = zeros(1,length(pumps));
power_q_list = zeros(1,length(pumps));
switch OF_spec
    case 'F1'
        % the sum of the powers has to be at least
        % the minimum at all times
        % power_p1 + power_p1 + power_p1 ... > P_min
        
        for i = 1:length(pumps)
            power_T_list(i) = pumps{i}.power.nameplate;            
        end
        
    case 'F3'
        for i = 1:length(pumps)
            power_T_list(i) = pumps{i}.power.lin_approx(2);
            power_q_list(i) = pumps{i}.power.lin_approx(1);
        end
     
    case 'V1'
        for i = 1:length(pumps)
            power_T_list(i) = pumps{i}.OF.qt(2);
            power_q_list(i) = pumps{i}.OF.qt(1);
        end
    otherwise
        error('DR not implimited for selected pump power approximation. Linear approximations only!')
end

T_dr = -place_matrix(eye(N),power_T_list,2);

driver_q(loc_Pumps) = power_q_list; %assumes pumps are the last in connection_matrix
q_dr = -place_matrix(eye(N),driver_q,2);

A_dr = [ T_dr zeros(N,nn*N) q_dr zeros(N,length_lambda*N)];
b_dr = -ones(N,1)*abs(Power.total(1))*desired_share;