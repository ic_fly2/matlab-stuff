% [H_save,Q_save,H_res,power_cons] = function EPS_step(pipes,tanks,Feeds,nodesIdListMap,Allreservoirs,pumpsOn,A12,A10,H0,q,K,Nexp,tol,CV,tol)
% pump_control;
[K,Nexp,Feeds,pumpsOn,connections] = pump_control(pipes,pumps,data.n_pumps,tankss,fields_rm([pipes pumps],'Nexp'),fields_rm([pipes pumps],'K'),data.npi,fitting_type,T); 
%% demand time step:
%%% NEEDS FIXING!!!

for i = 1:nj
    q(i) = qall_gga(i,t);
end



[A12,A10,H0] = makeA12A10(pipes,nodesIdListMap,Allreservoirs,pumpsOn);
tol = 0.000001; % comment out for function use
[H_gga,Q_gga] = solver4(A12,A10,H0,q*settings.DR.demand_factor,K,Nexp,tol,CV);

%% mass balance
for i = 1:length(tanks)
    Area = (pi/4)*tanks{i}.Diameter^2;
    Delta_H = Feeds(i,:)*Q_gga*(3600*24/N)/Area; % assume 24h schedule
    tanks{i}.Init =  tanks{i}.Init + Delta_H;
end
Allreservoirs = [reservoirs tanks];
for i = 1:no
    H_res(i,1) = Allreservoirs{i}.Elev + Allreservoirs{i}.Init;
end

%% save current

H_save(:,t) = [H_gga;H_res]-Elevation;
Q_save(:,t) = Q_gga;
% eval([def '_control']);
% end
% disp(T0);
lim = length(Q_gga);
q_pump = round(Q_gga(lim-length(pumpsOn)+1:lim),2);
power_con(t,:) = pump_power_consumtion2(q_pump,pumpsOn);
%  disp(power_con)

