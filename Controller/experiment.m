function [output] = experiment(def,N,approx_number,vmax_min,M,time_limit,asymmetry) 
% Generate the optimal pump schedule for a given WDS network and times it
% the optimisation proceedure.
%
%
% x = experiment(def,N,approx_number) generates the schedule with loose
% bounds and conservative settings, will probably solve but may be too
% slow.
% 
% x = experiment(def,N,approx_number,vmax_min,M) solves the problem with
% the specified bounds for vmax and M the big-M constant
% 
% x = experiment(def,N,approx_number,vmax_min,M,time_limit) solves the
% specified problem in the given time limit in seconds
%
% x = experiment(def,N,approx_number,vmax_min,M,time_limit,asymmetry) solves
% the problem with the asymmetric constraints as set by their switch. only
% disable if you know what you are doing
%
% x = experiment(problem) where problem is a structure  with the fields:
%                 N: 6
%     approx_number: 7 (setting)
%          vmax_min: 4.5000 (setting)
%                 M: 99
%        time_limit: 600 (setting)
%     Desired_ratio: NaN (setting)
%               def: 'van_zyl' Network defenition
%         asymmetry: 'on' (setting)
%          settings: [1x1 struct]
% 
% where settings is a structure:
% settings = 
% 
%        time_limit: 600
%     approx_number: {7} 1:13 wide range of options
%                DR: [1x1 struct]
%         asymmetry: {'on'} Do not disable unless you hate yourself
%           OF_spec: 'Simple' 'Linear' 'Quadratic'
%               sos: {'Off'} 'On'
%              GHG : vector of time dependant GHG emissions
%   limit_overfill : {'Off'} 'x' Limit the maximum fill of the final
%   resevoir to x times the maximum fill
%              plot: {'Off'} 'On' 'all' activates a range of plotting
%              settings and stations 'all' is very slow!
%            solver: {'Cplex'}, 'Gurobi', 'Scip'
%
% DR = 
%           status: 'On' , 'Optimal' , {'Off'}  Status
%      rest_period: 30  period for which DR has to last in minutes
%             Flex: 'Yes' 'No' 'Windows' 'Window1' If DR is allowed to 
%                    skip the high price period, nothing, only bid
%                    the fist National Grid period or both
%    desired_ratio: ratio of pump power to demand, optinal setting to
%                   modify the overal demand
%
%
% Some tips:
%                N: number of time steps 6, 12, 24, or 48
%    approx_number:  see below use 7 or 
%         vmax_min: if infeasible increase start with 3, decrease to 
%                   improve the hydraulic simulation accuracy, if the 
%                   problem is still infeasible at 6 m/s the issue is 
%                   somewhere else, check you .inp file for odd pipes.
% Coupled to vmax_min, try M = 100 for van_zyl and 1000 for richmond, if the
% solver comes back infesable increase, if performance is too slow
% decrease.
%       time_limit: in seconds  try 600


if nargin == 1
    
    output.settings = def.settings;
    
    if ~isfield(def,'N')
        def.N = 6;
        disp('No # of timesteps (N) specified, picking 6')
    end
    N = def.N;
    
    if ~isfield(def,'vmax_min')
        def.vmax_min = 6;
        disp('No minimum max. velocity specified, picking 6')
    end
    vmax_min = def.vmax_min;
    
    if ~isfield(def,'M')
        def.M = 999;
        disp('No M specified, picking 999')
    end    
    M = def.M;
    
    %%% optional settings
    if  isfield(def,'settings')
        settings = def.settings;
    else
        settings =[];
    end
    
    if ~isfield(def,'time_limit') && ~isfield(settings,'time_limit')
        settings.time_limit = 600;
        disp('No time limit specified, picking 600')
    end
        
    if ~isfield(settings,'solver')
        settings.solver = 'Cplex';
        disp('No sover specified, picking CPLEX')
    end
    
    if  ~isfield(def,'approx_number') && ~isfield(settings,'approx_number')
        settings.approx_number = 7;
        disp('No approx_number specified, picking 7')
    elseif isfield(def,'approx_number')
        settings.approx_number = def.approx_number;        
    end
          
    if ~isfield(def,'asymmetry') && ~isfield(settings,'asymmetry')
        settings.asymmetry = 'on';
        disp('Asymmetry on')
    else isfield(def,'asymmetry')
        settings.asymmetry = def.asymmetry;
    end
    
    if ~isfield(def,'DR')
        settings.DR.Desired_ratio = NaN;
        settings.DR.status = 'Off';
    end
    
    if ~isfield(def,'Desired_ratio') && ~isfield(settings.DR,'Desired_ratio')
        settings.DR.Desired_ratio = NaN;
        settings.DR.status = 'Off';
    end
    
    if ~isfield(settings,'Robust')
        settings.Robust.status ='No';
    end
    
    % finally relieve def of its double duty
    def = def.def;    
elseif    nargin == 3
    vmax_min = 6;
    M = 999;
    time_limit = 600;
    asymmetry = 'on';
elseif nargin == 5
    time_limit = 600;
    asymmetry = 'on';
elseif nargin == 6
    asymmetry = 'on';    
end

%%% settings
settings  = settings_check(settings);

settings.price_adj = pattern_length_adjustment(settings.price,N);
settings.GHG_adj = pattern_length_adjustment(settings.GHG,N);

settings.price_adj= circshift(settings.price_adj',settings.Time_shift)';
settings.GHG_adj = circshift(settings.GHG_adj',settings.Time_shift)';
% 

switch settings.approx_number
    case  1 %1a
        pump_approx = 'Pump_quad'; settings.pmax_pump = 1;% max complication quad
        pipe_approx = 'pipe_quad'; settings.pmax_pipe = 1;
    case 2 %2
        pump_approx = 'Pump_quad'; settings.pmax_pump = 1;s% quad with piece
        pipe_approx = 'pipe_piece'; settings.pmax_pipe = 7;
    case 3 %3
        pump_approx = 'Pump_quad';   settings.pmax_pump = 1; % quad with less pieces
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
    case 4 %4
        pump_approx = 'Pump_convex'; settings.pmax_pump = 7; %convex pump
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 7;
    case 5 %5 %works
        pump_approx = 'Pump_convex'; settings.pmax_pump = 7; %convex pump, less pieces
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
    case 6 %6
        pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % less equations
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 7;
    case 7 %7 %works
%         settings.OF_spec = 'Simple';
        pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
%      case 8 %not used
%         pump_approx = 'Pump_convex'; settings.pmax_pump = 2; %OF = QP
%         pipe_approx = 'pipe_piece';  settings.pmax_pipe = 2;
%     case 9 %not used
%         pump_approx = 'Pump_convex'; pmax_pump = 5; %OF = LP
%         pipe_approx = 'pipe_piece';  pmax_pipe = 3;
    case 8 %case 10 %8
        pump_approx = 'Pump_simple'; settings.pmax_pump = 1;
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
        
%     case 6
%         pump_approx = 'Pump_convex'; pmax_pump = 7;
%         pipe_approx = 'pipe_piece';  pmax_pipe = 3;
%     case 7
%         pump_approx = 'Pump_convex'; pmax_pump = 7;
%         pipe_approx = 'pipe_piece';  pmax_pipe = 3;

%     case 9  % useless as no new info gained
%         pump_approx = 'Pump_convex'; pmax_pump = 7;
%         pipe_approx = 'pipe_quad';
%     case 10
%         pump_approx = 'Pump_simple';
%         pipe_approx = 'pipe_piece';  pmax_pipe = 3;
%     case 9 %case 77 %7b
%         settings.OF_spec = 'Sum_only';
%         pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % smallest case
%         pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
%     case 10 %case 777 %7c
%         settings.OF_spec = 'Linear_power_no_switch';
%         pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % smallest case
%         pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
%     case 11% case 7777  %7d
%         settings.OF_spec = 'Quad_power';
%         pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % smallest case
%         pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
%     case 12 %case 17 %1c
%         settings.OF_spec = 'Quad_power';
%         pump_approx = 'Pump_quad'; % max complication quad
%         pipe_approx = 'pipe_quad';
    case 10
        pump_approx = 'Pump_convex';
        pipe_approx = 'pipe_quad';
%     case 11
%         pump_approx = 'Pump_convex';
%         pipe_approx = 'pipe_nl_HW';
        
%     case 15 %7 %works
%         settings.OF_spec = 'Simple';
%         pump_approx = 'Pump_convex_VSD'; settings.pmax_pump = 3; % smallest case
%         pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
        
    case 14 %7 %works
        pump_approx = 'Pump_convex_VSD2'; settings.pmax_pump = 7; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 7;   
    case 15 %7 %works
        pump_approx = 'Pump_convex_VSD2'; settings.pmax_pump = 7; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
    case 16 %7 %works
        pump_approx = 'Pump_convex_VSD2'; settings.pmax_pump = 3; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 7;
    case 17 %7 %works
        pump_approx = 'Pump_convex_VSD2'; settings.pmax_pump = 3; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 3;
    case 18 %7 %works
        pump_approx = 'Pump_convex_VSD2'; settings.pmax_pump = 5; % smallest case
        pipe_approx = 'pipe_piece';  settings.pmax_pipe = 5;
        
    case 11
        pump_approx = 'Pump_convex'; settings.pmax_pump = 3; % smallest case
        pipe_approx = 'pipe_lin';  settings.pmax_pipe = 0;
        
   
    otherwise 
        error('Are you sure you wanted the function, not enough inputs')
end

disp(['Analysing ' def ' with ' num2str(N) ' time steps and ' pump_approx ' ' pipe_approx ])
disp(datestr(now))





%% EPS for initial solution generation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Removed EPS section not needed as no inital schedule generated!
%prepNetwork;
[pumps, pipes, junctions, reservoirs, tanks, nodes, data, nodesIdListMap] = prepNetwork(def,settings,N);

% load('PumpPower')
if strcmp(pumps{1}.type,'VSD') && ...
        strcmp(settings.OF_spec(1),'V') && ...
        settings.approx_number >= 14

elseif strcmp(pumps{1}.type,'FSD') && ...
        strcmp(settings.OF_spec(1),'F') && ...
        settings.approx_number <= 11
else
    error('In correct pump and pipe approximation type for pump type in network. See the <a href="https://bitbucket.org/ic_fly2/matlab-stuff/wiki/Settings" >wiki</a> for details.')
end


[data, pipes] = prepGGA(pipes, data);

Make_problem; % Formulate problem matracies to be passed to solver

[x,fval,exitflag,time_taken, Remaining_gap] = Call_solver(problem,settings);

Display_results;


if strcmp(settings.DR.status,'Off')
    DR_D =  NaN;
elseif strcmp(settings.DR.status,'On')
    DR_D = settings.DR.desired_share;
end

if exitflag ~= -1
    if ~isfield(settings,'h')
        for i = 1:length(tanks)
            settings.h0(i) = head_display(1,nodesIdListMap(tanks{i}.Id));
            tanks{i}.Init = head_display(1,nodesIdListMap(tanks{i}.Id));
        end
    end
    pump_flow = flow_display(:,data.npi+(1:data.npu));
    if strcmp(pump_approx,'Pump_convex_VSD2')
        disp(round(schedule_tmp,3))
        
    else
        disp(round(schedule_tmp))
        
    end
    schedule_tmp_flip = schedule_tmp;
    
    for i = 1:data.npi
        pipes{i}.flow = flow_display(:,i);
        pipes{i}.headloss = head_display(:,nodesIdListMap(pipes{i}.startNodeId))...
            -head_display(:,nodesIdListMap(pipes{i}.endNodeId))-pipes{i}.change_gz;
        
        pipes{i}.lin_data = pipes{i}.headloss./ pipes{i}.flow;

    end
    
    for i = 1:data.npu
        pumps{i}.flow = flow_display(:,i+data.npi);
    end
    
    
    % Make an EPS simulation
   [H_save, Q_save, power_con,power_opt,Omega_stor] = EPS_from_T0(schedule_tmp_flip,N,data,pipes,pumps,tanks,reservoirs,d,nodesIdListMap,pump_flow); %% needs to get initial filling from Optimal solution!!

    Q_save_rounded = round(Q_save*1000)/1000;
    
    % odd plot command
%     if isfield(settings,'h0') && ~strcmp(settings.plot,'Off');
%         colours = colormap(summer(length(tanks)+1));
%         figure;
%         title('Tank level evolution')
%         % Simulation results
%         for t = 1:length(tanks)
%             hold on;
%             plot(linspace(0,1,N+1),[tanks{t}.Init; H_save(:,nodesIdListMap(tanks{t}.Id))],'-','color',colours(t,:) )
%             legend_string{t} = [tanks{t}.Id ' sim'];
%         end
%         
%         % optimisation results
%         for t = 1:length(tanks)
%             hold on;
%             plot(linspace(0,1,N+1),[ head_display(:,nodesIdListMap(tanks{t}.Id)); NaN ],'--','color',colours(t,:))
%             legend_string{t+length(tanks)} = [tanks{t}.Id ' opt' ];
%         end
%         legend(legend_string) 
%         
%         %tank minimums
%         if exist('tank_minimums','var')
%             for t = 1:length(tanks)
%                 hold on;
%                 plot(linspace(0,1,N+1),[tank_minimums(t,:) tank_minimums(t,1)],':','color',colours(t,:))
%             end
%         end
%         datetick('x','HH:MM')
%         xlabel('Time of day')
%         ylabel('Tank fill')
%         
%         
%     end
    
    
    
    
    
%% Some calcs

    for i = 1:length(pipes)
        C{1,size(C,2)+1} = [pipes{i}.Id ' ' pipes{i}.Status];
        for ii = 1:N
            C{ii+1,size(C,2)} = Q_save(ii,i);
        end
    end
    
    fval_sim = sum(Pe*double(Delta_T)/double(3600)*power_con);
    fval_diff = abs(fval-fval_sim);
    fval_diff_per = abs(fval-fval_sim)/fval_sim;
    
    
    
    diff_H = head_display - H_save;
    diff_Q = flow_display - Q_save;
    
    diff_Q_per = (sum(abs(diff_Q))./sum(diff_Q~= 0 ))./(sum(abs(Q_save))./sum(Q_save~= 0 ));
    diff_H_per = (sum(abs(diff_H))./sum(diff_H~= 0 ))./(sum(abs(H_save))./sum(H_save~= 0 ));
    
    
    if strcmp(settings.DR.status,'Optimal')
        g = length(x);
        cost_pump =  x'*H(1:g,1:g)*x + f(1:g)*x;
        rev_DR = 0.5*DR_D*H(g+1,g+1)*DR_D + f(g+1)*DR_D;
    elseif strcmp(settings.DR.status,'On')
        cost_pump =  0.5*x'*H*x + f*x;
        rev_DR = double(-DR_D*data.Power.total(1)*settings.DR.reward/1000/365);
    else
        cost_pump = fval;
        rev_DR = double(0);
    end
    
    
    if isfield(settings,'GHG')
        power_schedule = ones(N,1)*data.Power.supply'.*schedule_tmp;
        GHG_emissions =  sum(sum(settings.GHG_adj'*ones(1,data.npu).*power_schedule*double(24/N)));
    else
        GHG_emissions = NaN;
    end
    
    
    if isfield(settings,'plot') && strcmp(settings.plot,'On')
        figure
        hold on
        plot(head_display)
        legend(keys(nodesIdListMap))
        plot(H_save,'--')
    end
    
%% Success output
     output.power_sim = power_con;
    output.fval_sim = fval_sim;
    output.time_taken = time_taken;
    output.fval = fval;
    output.diff_H = diff_H;
    output.diff_H_per = diff_H_per;
    output.diff_Q = diff_Q;
    output.diff_Q_per = diff_Q_per;
    output.flow_display = flow_display;
    output.head_display =  head_display;
    output.Remaining_gap = Remaining_gap;
    output.H_save = H_save;
    output.Q_save = Q_save;
    output.x0 = x0;
%     output.MipStart = cplex.MipStart;
    output.fval_sim =fval_sim;
    output.fval_diff = fval_diff;
    output.fval_diff_per = fval_diff_per;
    output.Power = data.Power;
    output.schedule_tmp = schedule_tmp;
    output.DR_D = DR_D;
    output.problem = problem;
    output.f = problem.f;
    
    output.shed = schedule_tmp;
    output.GHG = GHG_emissions;
    output.cost_pump = cost_pump;
    output.rev_DR = rev_DR;
    output.power_opt = power_opt;
    try
        output.h0 = tank_init;
    catch
        output.h0 = ones(1,length(tanks))*NaN;
    end
    output.Omega_stor =  Omega_stor;
    
    
else % failed
    
    output.power_sim = NaN;
    output.fval_sim = NaN;
    output.time_taken = time_taken;
    output.fval = NaN;
    output.diff_H = ones(N,data.nn)*NaN;
    output.diff_H_per =  ones(1,data.nn)*NaN;
    output.diff_Q = ones(N,data.np)*NaN;
    output.diff_Q_per = ones(1,data.np)*NaN;
    output.flow_display = ones(N,data.np)*NaN;
    output.head_display =  ones(N,data.nn)*NaN;
    output.Remaining_gap = Inf;
    output.H_save = ones(N,data.nn)*NaN;
    output.Q_save =ones(N,data.np)*NaN;
    output.x0 = NaN;
    output.fval_sim =NaN;
    output.fval_diff = NaN;
    output.fval_diff_per = NaN;
    output.Power = data.Power;
    output.schedule_tmp = NaN;
    output.DR_D = NaN;
    output.problem = problem;
    output.GHG = NaN;
    output.cost_pump = NaN;
    output.Power = NaN;
    output.power_opt = NaN;
    output.h0 = ones(1,length(tanks))*NaN;
    output.Omega_stor = NaN;
% for python
%       output.shed = NaN;
%       output.f = NaN;
%       output.schedule_tmp = NaN;
%       output.x0 = NaN;
%       output.Remaining_gap = NaN;
%       output.fval = NaN;
%       output.time_taken = NaN;
end


output.pipes = pipes;
output.pumps = pumps;



% output.Solution = cplex.Solution;
% output.cplex = cplex;


  