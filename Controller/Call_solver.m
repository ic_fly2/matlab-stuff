function [x, fval, exitflag,time_taken,Remaining_gap, info] = Call_solver(problem,settings)
% Takes the problem structure and passes it to the solver specified in the
% settings. Don't call this yourself, is part of experiment.
%
%



options = cplexoptimset('Display','iter','BranchStrategy','Pseudo','Algorithm','Barrier');%'TolFun',0.05, 'TolRLPFun',0.05,'TolXInteger',0.05);,'ExportModel','Out.m'
problem.options = options;



% If initial solution is provided:
if isfield(settings,'x0')
    if length(settings.x0) == length(lb)
        problem.x0 	= settings.x0;
    else
        problem.x0 	= [];
    end
else
    problem.x0 	= [];
end


%% Call solver:


switch settings.solver
    case 'SCIP'
        % MIQPCP --> call Scip, as no other solvers for this are implimented
        %scip
        opts = optiset('solver','SCIP','display','final','maxnodes', 1000000000,'maxtime',settings.time_limit,'tolafun',problem.gap); %'display','final'
        Opt = opti('qp',problem.H,problem.f,'qc',problem.Q,problem.l,problem.r,'xtype',problem.xtype,'eq',problem.Aeq,problem.beq,'ineq',problem.A,problem.b,'lb',problem.lb,'ub',problem.ub,'options',opts);
        
        [x,fval,exitflag,info] = solve(Opt);
        time_taken =  info.Time;
        Remaining_gap = info.BBGap;
        
        
    case 'gurobi'
        % MIQP
        model.A = sparse([problem.Aineq; problem.Aeq]);
        model.rhs = [problem.bineq;problem.beq];
        model.sense(1:length(problem.bineq)) = '<';
        model.sense(length(problem.bineq)+(1:length(problem.beq))) = '=';
        
        model.obj = problem.f;
        model.Q = sparse(problem.H);
        
        model.vtype = problem.ctype;
        model.lb = problem.lb;
        model.ub = problem.ub;
        result = gurobi(model);
        
        if strcmp(result.status,'INFEASIBLE')
            exitflag = -1;
            x = NaN;
            fval = NaN;
            info = result.status;
            time_taken =  NaN;
            Remaining_gap = NaN;
        else
            exitflag = 1;
            x = result.x;
            fval = result.objval;
            info = result.status;
            time_taken =  result.runtime;
            Remaining_gap = (result.objval-result.objbound)/result.objval;
        end
        
    case 'intlinprog'
        % MILP
        if max(max(problem.H)) > 0
            disp('IGNORING QUADRATIC OBJECTIVE INTLINPROG IS LINEAR ONLY!')
        end
        rmfield(problem,'options');
        rmfield(problem,'H');
        problem.intcon =  find(problem.ctype == 'I');
        problem.options = optimoptions('intlinprog','MaxTime',settings.time_limit,'LPMaxIter',...
        100000000);
        problem.solver = 'intlinprog'; % required field
        tic
        [x,fval,exitflag,status] = intlinprog(problem);
        time_taken = toc;
        if exitflag <= 0
            exitflag = -1;
            x = NaN;
            fval = NaN;
            info = status.message;
            time_taken =  NaN;
            Remaining_gap = NaN;
        else
            info = status.message;
            Remaining_gap =  status.relativegap;
        end
        
        
        
    case 'cplex'
        % MIQP
        cplex = problem2cplex(problem);
        options = cplexoptimset('Display','off');
        if isfield(settings,'MipStart')
            blub = settings.MipStart.x;
            if length(blub) == length(lb)
                cplex.MipStart = settings.MipStart;
            end
        end
        if strcmp(settings.sos,'On')
            cplex = addSOS2cplex(cplex,N,nn,np,data.pumps_vec,lambda_parts_length);
        end
        cplex.Param.timelimit.Cur = settings.time_limit; %time limit in seconds
        cplex.Param.mip.strategy.variableselect.Cur = 2;%4 %pseudo = 2; strong = 3; info: http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex137.html
        %     cplex.Param.emphasis.mip.Cur = 0; %0 and 4 work best            %emphasisset(i_e); %0:4 http://www-eio.upc.edu/lceio/manuals/cplex-11/html/refparameterscplex/refparameterscplex64.html
        disp('Starting Solver');
        if strcmp(settings.time_analysis ,'Off')
            cplex.solve();
            info = cplex.Solution.statusstring;
            if ~isfield(cplex.Solution,'x')
                time_taken = 600;
                Remaining_gap= Inf;
    
            else
                time_taken = cplex.Solution.time;
                Remaining_gap= cplex.Solution.miprelgap;
                
            end
        elseif strcmp(settings.time_analysis ,'Time')
            cplex.Param.timelimit.Cur = 10;
            tic
            for i = 1:ceil(settings.time_limit/10)
                cplex.solve();
                if ~isfield(cplex.Solution,'x')
                    Remaining_gap(i) = Inf;
                    time_taken(i) = cplex.Param.timelimit.Cur;
                else
                    Remaining_gap(i)= cplex.Solution.miprelgap;
                    time_taken(i)= cplex.Solution.time;
                    if i > 6 && Remaining_gap(i)-Remaining_gap(i-1) == 0
                        break
                    end
                end
            end
            toc
        elseif strcmp(settings.time_analysis ,'Percentage')
            tic
            i =1;
            gap = 1;
            cplex.Param.mip.tolerances.mipgap.Cur = 1;
            while gap > 0 && i < 50
                cplex.solve();
                if ~isfield(cplex.Solution,'x')
                    time_taken(i) = settings.time_limit;
                    Remaining_gap(i) = Inf;
                    break
                else
                    time_taken(i) = cplex.Solution.time;
                    Remaining_gap(i) = cplex.Solution.miprelgap;
                    gap = cplex.Solution.miprelgap;
                    if gap > 0.02 % use fix() to remove if loop
                        cplex.Param.mip.tolerances.mipgap.Cur = gap -0.02;
                    else
                        cplex.Param.mip.tolerances.mipgap.Cur =  0;
                    end
                    T0_tmp = cplex.Solution.x(1:data.npu*N);
                    T0_tmp= reshape(T0_tmp ,[N length( T0_tmp )/N ]);
                    pump_flow_tmp=cplex.Solution.x((data.npu+data.nn)*N+(1:data.npu*N));
                    pump_flow_tmp=reshape(pump_flow_tmp,[N length(pump_flow_tmp)/N]);
                    [H_save_tmp{i},Q_save_tmp{i},power_sim_tmp{i},power_opt_tmp{i}] =...
                        EPS_from_T0(T0_tmp...
                        ,N,data,pipes,pumps,tanks,reservoirs,d...
                        ,nodesIdListMap,pump_flow_tmp);
                    fval_sim_tmp(i) = sum(Pe*Delta_T/3600*power_sim_tmp{i});
                    i = i + 1;
                    output.H_save_tmp = H_save_tmp;
                    output.Q_save_tmp = Q_save_tmp;
                    output.power_sim_tmp = power_sim_tmp;
                    output.power_opt_tmp = power_opt_tmp;
                    output.fval_sim_tmp = fval_sim_tmp;
                    
                    if cplex.Solution.time > settings.time_limit*0.98
                        break
                    end
                end
            end
            toc
        end
        
        
        if ~isfield(cplex.Solution,'x')
            fval = NaN;
            exitflag =  double(-1);
            x = NaN;
        else
            fval = cplex.Solution.objval;
            exitflag =  double(cplex.Solution.status);
            x = cplex.Solution.x;
        end
        
        
        
end
end


