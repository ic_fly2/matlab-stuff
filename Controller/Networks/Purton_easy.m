%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
% clear all;
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
% Elevation
% Elevation = [0 1 1 1 18 5 5]; % elevation change for pumps is ignored!
Elevation = [0 1 1 1 1 1 1 1 1  1]; 
% Reservoir
% fixed head
location_fix = [1];
fixed_val = [0];% m


% variable head tanks
location_res = [3 5];
%   Initial_filling = [1.9 3.2]; %comment out if no initial settings are desired
Diameter = [50 50]; 
Reservoir_area = pi*Diameter.^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)

% definition
connection_matrix = -[...
    1 -1    0  0  0  0  0  0  0  0;... %q1 main pump
    0  1    -1  0  0  0  0  0  0  0;... %q2 pipe 1     0  0  1 -1  0  0  0  0  0  0  0;... %q3 pipe 2
    0  0    1 -1  0  0  0  0  0  0;... %q4 pipe 3
    0  0    0  1 -1  0  0  0  0  0;... % q5 pipe 4
    0  0    0  0  1 -1  0  0  0  0;... % q6 pipe 5
    0  0    0  0  1  0  0  0  0 -1;... % q6 pipe 6      0  0  0  0  0  0  0 -1  0  0  1;... % q6 pipe 7    
    0  0    1  0  0  0  0  0 -1  0;... % q6 pipe 8      0  0  0  1  0  0  0  0 -1  1  0;... % q6 pipe 9     0  0  0  0  0  0  0  0  1  0  0;... % demand out T1
    0  0    0  1  0  0  0  0  0  0];%... % demand out5     0  0  0  0  0  0  0  1  0  0  0;... % demand out T2    0  0  1  0  0  0  0  0  0  0  0];   % demand out 3
%   1  2  3  4  5  6  7  8  9  T1  T2
connection_type = {pump_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...                   pipe_approx,... %                    pipe_approx,...%                    pipe_approx,...%                   'demand',...                   'demand',...                   'demand',...
                   'demand'}; 

%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-420.33]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [  94.29] ;
c_org = [ 236.82];
n_pumps = 3; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power:
Power_nameplate = [1]; %MW
% Power_linear = [0.2 1.8 ;...
%                 0.4 1.8];% [kW/m3/s kW] % just made up
% Power_quad = [ -0.1 0.4 1.6 ];% [kW/(m3/s)^2 kW/m3/s kW]

%operation points: (From EPAnet)

OP_drivers = {[1 0  0],[0 1 0],[0 0 1]}; 
% OPs          [10 0] [01 0][10 1] [01 1]  [00 1]  [000 0]Ommitted  
%flows (each row is a pump station)
OP_flow_pump = [165.78 237.96 182.39 ]/1000; %flow through pump station. not individual pumps! m3/s
%Heads(each row is a pump station)
OP_head_pump = [78.44 90.2  72.94];
             
%% pipes
%1
pipedata(1).data = [0.45,0.001,2600,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) = [165.78 237.96 182.39]/1000; % flow in pipes (Same as pumps)

%2
pipedata(2).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [140  167.93  47.15]/1000; %


%3
pipedata(3).data = [0.3,0.001,500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(3,:) = [0.05632 0.05632 0.05632]; % flow in pipes (Same as pumps but in m3/s)


%4
pipedata(4).data = [0.3,0.001,1100,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(4,:) = [-93.68 -93.68 -93.68]/1000; % flow in pipes (Same as pumps but in m3/s)

%5
pipedata(5).data = [0.35,0.001,2000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(5,:) = [25.78 70.04 135.24]/1000; % flow in pipes (Same as pumps but in m3/s)

%6
pipedata(6).data = [0.35,0.001,2000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(6,:) = [25.78 70.04 135.24]/1000; % flow in pipes (Same as pumps but in m3/s)

%7
pipedata(7).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(7,:) = [25.78 70.04 135.24]/1000; % flow in pipes (Same as pumps but in m3/s)

%8
% pipedata(8).data = [0.35,0.001,2000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(8,:) = [25.78 70.04 135.24]/1000; % flow in pipes (Same as pumps but in m3/s)
% 
% %9
% pipedata(9).data = [0.35,0.001,10,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(9,:) = [25.78 70.04 135.24]/1000; % flow in pipes (Same as pumps but in m3/s)


%% misc
M = 999; %1000000; % large number
% mods for analysis (not used)
demand_multiplier = [0.001]; %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;

 
% demand  rows are time and colums are nodes!
demand1 = importdata('Peak_1.pat'); % coice of demand pattern
demand2 = importdata('Peak_2.pat'); % coice of demand pattern
demand3 = importdata('Peak_3.pat'); % coice of demand pattern
demand4 = importdata('Peak_4.pat'); % coice of demand pattern

d = [demand1.data demand2.data demand3.data demand4.data];
d = d';%m3/h
n_demands = 1;

d = d(:,1:floor(48/N):48); % skip some if needed
d = d(:,1:N); %shorten if required
% d = d/mean(d)*demand_multiplier;
for i = 1:n_demands
    d(i,:)/mean(d(i,:))*demand_multiplier(i);
end
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
T_bounds = [0 1];
h_min = [0   0   0   0   0   0   0   0   0   0] ; % max and min values of the head at the nodes
h_max = [150 150 150 150 150 150 150 150 150 150];
Qmax = 10; % there is no bound on q for now just make it large
q_bounds = [-Qmax Qmax]*10 ; %can have any value
lambda_bounds = [0 1];

Network_tests

%% plotting
% works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end