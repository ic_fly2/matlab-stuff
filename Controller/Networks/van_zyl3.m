%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
% clear all;
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
% Elevation
Elevation = [0 1 20 10 18 5 7]; % elevation change for pumps is ignored!
% Elevation = [0 1 1 1 1 1 1];  % testing only
% Reservoir
% fixed head
location_fix = [1];
fixed_val = [0];% m


% variable head tanks
location_res = [3 5];
%   Initial_filling = [1.9 3.2]; %comment out if no initial settings are desired
Diameter = [20 25]; 
Reservoir_area = pi*Diameter.^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)
if ~exist('pump_approx','var')
pump_approx = 'Pump_convex';
pipe_approx = 'pipe_quad';

end
% definition
% connection_matrix = -[...
%     1 -1  0  0  0  0  0;... %q1 main pump
%     0  0  1  0  0  0 -1;... %q7 pump booster
%     0  1 -1  0  0  0  0;... %q2 pipe 1 
%     0  0  1 -1  0  0  0;... %q3 pipe 2
%     0  0  0  1 -1  0  0;... %q4 pipe 3
%     0  0  0  0  1 -1  0;...% q5 pipe 4
%     0  0  0  0  0  1 -1;...% q6 pipe 5  
%     0  0  1  0  0  0 -1;... %q8 check valve
%     0  0  0  1  0  0  0]; % demand out
% %   1  2  3  4  5  6  7
% connection_type = {pump_approx,...
%                    pump_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    'check_valve',...
%                    'demand'}; 
               
               % definition
connection_matrix = -[...
    0  1 -1  0  0  0  0;... %q2 pipe 1 
    0  0  1 -1  0  0  0;... %q3 pipe 2
    0  0  0 -1  1  0  0;... %q4 pipe 3    0  0  0  0  1 -1  ;...% q5 pipe 4
    0  0 -1  0  0  1  0;...% q6 pipe 5
    0  0  0  0  0  1 -1;... % more pipe
    1 -1  0  0  0  0  0;... %q1 main pump
    0  0  0  0  0  0  1;]; % demand out
%   1  2  3  T2  5  T1  7
connection_type = {...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pump_approx,...
                   'demand'}; 

%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-1100 ]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [45   ] ;
c_org = [99.43 ];
n_pumps = [2 ]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power:
Power_nameplate = [ 2.2/1000]; %MW %needs correcting 2.2 kW is wrong


%operation points: (From EPAnet)


% OPs          [10 0] [01 0][10 1] [01 1]  [00 1]  [000 0]Ommitted  
%flows (each row is a pump station)
% OP_flow_pump = [165.78 237.96 182.39    260.26  0; ...
%                  0      0      135.24   138.42  130.25]/1000; %flow through pump station. not individual pumps! m3/s
% %Heads(each row is a pump station)
% OP_head_pump = [78.44 90.2  72.94    87.87   0;...
%                  0     0    21.64    17.16   28.49];
OP_flow_pump = [174.0850 249.1100]./1000; %flow through pump station. not individual pumps! m3/s
%Heads(each row is a pump station)
OP_head_pump = [ 75.6900 89.0350]./2;
       
%% pipes
OP_drivers = {[1 0  0],[0 1 0],[1 0 1], [0 1 1],[0 0 1]}; 
%1
pipedata(1).data = [0.45,0.001,2600,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) = [165.78 237.96]/1000; % flow in pipes (Same as pumps)

%2
pipedata(2).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [140  167.93 ]/1000; %


%3
pipedata(3).data = [0.3,0.001,500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(3,:) = [0.05632 0.05632 ]; % flow in pipes (Same as pumps but in m3/s)


%4
pipedata(4).data = [0.3,0.001,1100,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(4,:) = [-93.68 -93.68 ]/1000; % flow in pipes (Same as pumps but in m3/s)

%5
pipedata(5).data = [0.35,0.001,2000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(5,:) = [25.78 70.04 ]/1000; % flow in pipes (Same as pumps but in m3/s)


%% misc
% mods for analysis (not used)
M = 999; %1000000; % large number
demand_multiplier = [0.15]; %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;

 
% demand  rows are time and colums are nodes!
demand1 = importdata('Peak_1.pat'); % coice of demand pattern
% demand2 = importdata('Peak_2.pat'); % coice of demand pattern
% demand3 = importdata('Peak_3.pat'); % coice of demand pattern
% demand4 = importdata('Peak_4.pat'); % coice of demand pattern

d = [demand1.data];% demand3.data demand4.data];
d = d';%m3/h
n_demands = 1;


% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test
d = d(:,1:floor(48/N):48); % skip some if needed
d = d(:,1:N); %shorten if required
% d = d/mean(d)*demand_multiplier;
for i = 1:n_demands
   d(i,:) =  d(i,:)/mean(d(i,:))*demand_multiplier(i);
end

% bounds
T_bounds = [0 1];
h_min = [0  0  0   0   0   0   0]; % max and min values of the head at the nodes
h_max = [50 50 50 50 50 50 50]*2; % keep h max as small as possible
Qmax = 2; % there is no bound on q for now just make it large enough
q_bounds = [-Qmax Qmax] ; %can have any value
lambda_bounds = [0 1];

Network_tests


%% plotting
% works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end