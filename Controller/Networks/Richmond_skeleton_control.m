% Richmond skeleton:

% LINK 1A 1.0000 IF NODE A BELOW 2.3685 7
% LINK 1A 0.0000 IF NODE A ABOVE 2.9799
% LINK 3A 1.0000 IF NODE A BELOW 2.8888 5
% LINK 3A 0.0000 IF NODE A ABOVE 3.1126
% LINK 2A 1.0000 IF NODE A BELOW 3.0405 2
% LINK 2A 0.0000 IF NODE A ABOVE 3.2513


% LINK 4B 1.0000 IF NODE B BELOW 3.2623
% LINK 4B 0.0000 IF NODE B ABOVE 3.5789

% LINK 5C 1.0000 IF NODE C BELOW 0.7185
% LINK 5C 0.0000 IF NODE C ABOVE 1.8850
% 
% LINK 6D 1.0000 IF NODE D BELOW 1.5907
% LINK 6D 0.0000 IF NODE D ABOVE 1.9708

% LINK 7F 1.0000 IF NODE F BELOW 1.7037
% LINK 7F 0.0000 IF NODE F ABOVE 2.1095

if tanks{2}.Init < 0.5 && tanks{2}.Init > 0.25
    T(5) = 1;
    T(7) = 0;
    T(2) = 0;
elseif tanks{2}.Init < 0.25 && tanks{2}.Init > 0.1
    T(5) = 1;
    T(7) = 1;
    T(2) = 0;  
elseif tanks{2}.Init < 0.1
    T(5) = 1;
    T(7) = 1;
    T(2) = 1;   
    
elseif tanks{2}.Init > 3 && tanks{2}.Init < 3.2
    T(5) = 1;
    T(7) = 1;
    T(2) = 0;
elseif tanks{2}.Init > 3.2 && tanks{2}.Init < 3.3
    T(5) = 1;
    T(7) = 0;
    T(2) = 0;  
elseif tanks{2}.Init > 3.3
    T(5) = 0;
    T(7) = 0;
    T(2) = 0; 
end

% if tanks{4}.Init < 0.2
%     T(6) = 1;
% elseif tanks{4}.Init > 3.4
%     T(6) = 0;
% end

if tanks{1}.Init < 0.2
    T(3) = 1;
elseif tanks{1}.Init > 1.8
    T(3) = 0;
end

% if tanks{3}.Init < 0.2
%     T(4) = 1;
% elseif tanks{3}.Init > 1.9
%     T(4) = 0;
% end

% if tanks{3}.Init < 0.2
%     T(4) = 1;
% elseif tanks{3}.Init > 1.9
%     T(4) = 0;
% end

    % if tanks{6}.Init < 0.2
    %     T(1) = 1;
    % elseif tanks{6}.Init > 2
    %     T(1) = 0;
    % end