%% control logi needs to be written for every network

% Example
% if tanks{1}.Init < X
%     T(Y) = 1;
% elseif tanks{1}.Init > X
%      T(Y) = 0;
% end
% main pumps:

if tanks{2}.Init < 0.3 && tanks{2}.Init > 0.1
    T(1) = 1;
    T(2) = 0;
elseif tanks{2}.Init < 0.1
    T(1) = 1;
    T(2) = 1;    
elseif tanks{2}.Init > 4.7 && tanks{2}.Init < 4.8
     T(1) = 1;
     T(2) = 0;
elseif tanks{2}.Init > 4.8
     T(1) = 0;
     T(2) = 0;
end

if tanks{1}.Init < 0.2
    T(3) = 1;
elseif tanks{1}.Init > 9.5
     T(3) = 0;
end
