%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
% clear all;
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
pressure_conv = 1; % conversion_factor 
% Elevation
Elevation = ([20 30 75 80 30 85 100]-20)/pressure_conv;
% Elevation =Elevation./1.5;
% Elevation = [0 1 20 10 18 5 5]; % elevation change for pumps is ignored!
% Elevation = [0 1 1 1 1 1 1];  % testing only
% Reservoir
% fixed head
location_fix = [1];
fixed_val = [0];% m


% variable head tanks
location_res = [4 6];
% Initial_filling = [5 2.5]; %comment out if no initial settings are desired
Diameter =[ 25 20];% [50 50];% 
Reservoir_area = pi*Diameter.^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)

% definition
% connection_matrix = -[...
%     1 -1  0  0  0  0  0;... %q1 main pump
%     0  0  1  0  0  0 -1;... %q7 pump booster
%     0  1 -1  0  0  0  0;... %q2 pipe 1 
%     0  0  1 -1  0  0  0;... %q3 pipe 2
%     0  0  0  1 -1  0  0;... %q4 pipe 3
%     0  0  0  0  1 -1  0;...% q5 pipe 4
%     0  0  0  0  0  1 -1;...% q6 pipe 5  
%     0  0  1  0  0  0 -1;... %q8 check valve
%     0  0  0  1  0  0  0]; % demand out
% %   1  2  3  4  5  6  7
% connection_type = {pump_approx,...
%                    pump_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    pipe_approx,...
%                    'check_valve',...
%                    'demand'}; 
               
               % definition
connection_matrix = -[...
    0  1 -1  0  0  0  0;... %q2 pipe 1 
    0  0  1 -1  0  0  0;... %q3 pipe 2
    0  0  0  -1 1  0  0;... %q4 pipe 3
    0  0  0  0  1 -1  0;...% q5 pipe 4
    0  0  0  0  0  1 -1;...% q6 pipe 5  
    0  0  1  0  0  0 -1;... %q8 check valve
    1 -1  0  0  0  0  0;... %q1 main pump
    0  0  1  0  0  0 -1;... %q7 pump booster    
    0  0  0  1  0  0  0]; % demand out
%   1  2  3  4  5  6  7
connection_type = {...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   'check_valve',...
                   pump_approx,...
                   pump_approx,...
                   'demand'}; 

%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-1100  -4900 ]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [45     -66.2] ;
c_org = [99.43  120.67];
n_pumps = [2 1]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power:
Power_nameplate = [ 2.2/1000; 1.2/1000]; %MW %needs correcting 2.2 kW is wrong


%operation points: (From EPAnet)


% OPs          [10 0] [01 0][10 1] [01 1]  [00 1]  [000 0]Ommitted  
%flows (each row is a pump station)
% OP_flow_pump = [165.78 237.96 182.39    260.26  0; ...
%                  0      0      135.24   138.42  130.25]/1000; %flow through pump station. not individual pumps! m3/s
% %Heads(each row is a pump station)
% OP_head_pump = [78.44 90.2  72.94    87.87   0;...
%                  0     0    21.64    17.16   28.49];
OP_flow_pump = [182.39 260.26 ; ...
                135.24      0  ]/1000; %flow through pump station. not individual pumps! m3/s
%Heads(each row is a pump station)
OP_head_pump = [ 72.94      87.87 ;...
                 21.4300    0];%./2;
       
%% pipes
OP_drivers = {[1 0  0],[0 1 0],[1 0 1], [0 1 1],[0 0 1]}; 
%1
pipedata(1).data = [0.45,0.001,2600,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) = [165.78 237.96 182.39    260.26  0]/1000; % flow in pipes (Same as pumps)

%2
pipedata(2).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [140  167.93  47.15  121.84  -130.25]/1000; %


%3
pipedata(3).data = [0.3,0.001,500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(3,:) = [0.05632 0.05632 0.05632 0.05632 0.05632]; % flow in pipes (Same as pumps but in m3/s)


%4
pipedata(4).data = [0.3,0.001,1100,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(4,:) = [-93.68 -93.68 -93.68 -93.68 -93.68]/1000; % flow in pipes (Same as pumps but in m3/s)

%5
pipedata(5).data = [0.35,0.001,2000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(5,:) = [25.78 70.04 135.24 138.42 130.25]/1000; % flow in pipes (Same as pumps but in m3/s)


%% misc
% mods for analysis (not used)
switch [pump_approx pipe_approx]
    case 'Pump_quadpipe_quad'
        M = 200;
    case 'Pump_quadpipe_piece'
        M = 200;
    case 'Pump_convexpipe_piece'
        M = 110;
    case 'Pump_convexpipe_convex'
%         M = 110;%/pressure_conv/0.1;
        M = 80;
    case 'Pump_linearpipe_linear'
        M = 200;
    case 'Pump_simplepipe_piece'
        M = 999;
    otherwise
        M = 999;
end
% M = 999;


% M = 200; %1000000; % large number
demand_multiplier = 0.15; %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;

 
% demand  rows are time and colums are nodes!
demand = importdata('Peak_1.pat'); % coice of demand pattern

d = demand.data;
d = d';%m3/h
n_demands = 1;
if exist('x','var')
    load('variation')
    d = d+0.2*variation;
end
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
T_bounds = [0 1];
h_min = [0  0  0   0   0   0   0]; % max and min values of the head at the nodes
h_max = [100 100 100 100 100 100 100]/pressure_conv; % keep h max as small as possible
% h_max = [100 100 100 100 100 100 100]/2;
Qmax = 1; % there is no bound on q for now just make it large enough
q_bounds = [-Qmax Qmax] ; %can have any value
lambda_bounds = [0 1];

Network_tests


%% plotting
% loc = [0 0; 1 0; 2 2 ; 3 2; 3 4; 2 4; 2 3 ];
% mInc = connection_matrix;
% mInc((sum(abs(mInc),2) == 1),:) = [];
%  mAdj = inc2adj(mInc);
%  gplot(mAdj,loc,'-r*');
% % works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end