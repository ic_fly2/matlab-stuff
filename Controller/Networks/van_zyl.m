if exist('live','var')
   

%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
% clear all;
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
% Elevation
% old node order
% Elevation = [20 30 75 80 30 85 100]';
% new node order
Elevation = [ 30 75  30  100 20 85 80]';
                        %100 originally

% Elevation =Elevation./1.5;
% Elevation = [0 1 20 10 18 5 5]; % elevation change for pumps is ignored!
% Elevation = [0 1 1 1 1 1 1];  % testing only
% Reservoir
% fixed head
% old node order:
% location_fix = [1];
% fixed_val = [0];% m
location_fix = [5];
fixed_val = [0];% m

% variable head tanks
% old order
% location_res = [4 6];
location_res = [6 7];
% Initial_filling = [5 2.5]; %comment out if no initial settings are desired
% old order:  [ 25 20]
Diameter =[  20 25];% [50 50];% 
Reservoir_area = pi*Diameter.^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)
               
               % definition
            % new definition
connection_matrix = [...
    -1     1     0     0     0     0     0;...
     0    -1     0     0     0     0     1;...
     0     0     1     0     0     0    -1;...
     0     0    -1     0     0     1     0;...
     0     0     0    -1     0     1     0;...
     0    -1     0     1     0     0     0;...
     1     0     0     0    -1     0     0;...
     0    -1     0     1     0     0     0];
       %old one           
% connection_matrix = -[...
%     0  1 -1  0  0  0  0;... %q2 pipe 1 
%     0  0  1 -1  0  0  0;... %q3 pipe 2
%     0  0  0  1 -1  0  0;... %q4 pipe 3
%     0  0  0  0  1 -1  0;...% q5 pipe 4
%     0  0  0  0  0 -1  1;...% q6 pipe 5  
%     0  0  1  0  0  0 -1;... %q8 check valve
%     1 -1  0  0  0  0  0;... %q1 main pump
%     0  0  1  0  0  0 -1];%;... %q7 pump booster    
% %     0  0  0  0  1  0  0]; % demand out
%   1  2  3  4  5  6  7
connection_type = {...
                   pipe_approx,...%pipe_approx,...%[pipe_approx '_CV'],...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,... %pipe_approx,...%[pipe_approx '_CV']
                   'check_valve',...
                   pump_approx,...
                   pump_approx};%,...
%                    'demand'}; 
               % debug for piecewise mode:
%                connection_type{5} = 'pipe_convex'

     
%     -1     0     0     0     0     0     1     0
%      1    -1     0     0     0    -1     0    -1
%      0     0     1    -1     0     0     0     0
%      0     0     0     0    -1     1     0     1
%      0     0     0     0     0     0    -1     0
%      0     0     0     1     1     0     0     0
%      0     1    -1     0     0     0     0     0
%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-1100  -4900 ]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [45     -66.2] ;
c_org = [99.43  120.67];
n_pumps = [2 1]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power: max(q.*H*1000*9.81)/0.75
Power_nameplate = [ 169.66; 88.316]; %kW 


%operation points: (From EPAnet)


% OPs          [10 0] [01 0][10 1] [01 1]  [00 1]  [000 0]Ommitted  
%flows (each row is a pump station)
% OP_flow_pump = [165.78 237.96 182.39    260.26  0; ...
%                  0      0      135.24   138.42  130.25]/1000; %flow through pump station. not individual pumps! m3/s
% %Heads(each row is a pump station)
% OP_head_pump = [78.44 90.2  72.94    87.87   0;...
%                  0     0    21.64    17.16   28.49];
OP_flow_pump = [182.39 260.26 ; ...
                135.24      0  ]/1000; %flow through pump station. not individual pumps! m3/s
%Heads(each row is a pump station)
OP_head_pump = [ 72.94      87.87 ;...
                 21.4300    0];%./2;
       
%% pipes
% OP_drivers = {[1 0  0],[0 1 0],[1 0 1], [0 1 1],[0 0 1]}; 
% %1
% pipedata(1).data = [0.45,0.001,2600,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(1,:) = [165.78 237.96 182.39    260.26  0]/1000; % flow in pipes (Same as pumps)
% 
% %2
% pipedata(2).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(2,:) = [140  167.93  47.15  121.84  -130.25]/1000; %
% 
% 
% %3 
% pipedata(3).data = [0.3,0.001,500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(3,:) = [0.05632 0.05632 0.05632 0.05632 0.05632]; % flow in pipes (Same as pumps but in m3/s)
% 
% 
% %4
% pipedata(4).data = [0.3,0.001,1100,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(4,:) = [-93.68 -93.68 -93.68 -93.68 -93.68]/1000; % flow in pipes (Same as pumps but in m3/s)
% 
% %5 %increased vmax?
% pipedata(5).data = [0.35,0.001,2000,2.5,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(5,:) = [25.7
friction_factor_setting = 'DW';% or 'HW' (depends of entered friction values, not used yet!)
%1
pipedata(1).data = [0.45,0.001,2600,2,3]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) = [165.78 237.96 182.39    260.26  0]/1000; % flow in pipes (Same as pumps)

%2
pipedata(2).data = [0.35,0.001,1000,2,3]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [140  167.93  47.15  121.84  -130.25]/1000; %


%3 
pipedata(3).data = [0.3,0.001,500,2,3]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(3,:) = [0.05632 0.05632 0.05632 0.05632 0.05632]; % flow in pipes (Same as pumps but in m3/s)


%4
pipedata(4).data = [0.3,0.001,1100,2,3]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(4,:) = [-93.68 -93.68 -93.68 -93.68 -93.68]/1000; % flow in pipes (Same as pumps but in m3/s)

%5 %increased vmax?
pipedata(5).data = [0.35,0.001,2000,2,3]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(5,:) = [25.78 70.04 135.24 138.42 130.25]/1000; % flow in pipes (Same as pumps but in m3/s)

%max head losses at vmax:


% Delta_H_max = [  113.3820 60.0671 36.5957 80.5106 120.1342];

%% misc
% mods for analysis (not used)
switch [pump_approx pipe_approx]
    case 'Pump_quadpipe_quad'
        M = 999;
    case 'Pump_quadpipe_piece'
        M = 200;
    case 'Pump_convexpipe_piece'
        M =95;
    case 'Pump_convexpipe_convex'
        M = 95;
    case 'Pump_linearpipe_linear'
        M = 200;
    case 'Pump_simplepipe_piece'
        M = 200;
    case 'Pump_simplepipe_convex'
        M = 200; % no difference observed
    otherwise
        M = 999;
end
M = 999;


% M = 200; %1000000; % large number
demand_multiplier = 0.15;%04 %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
% old method
% Electricity_Price(1:floor(N*11/48)) = 28.59;
% Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
% Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
% Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
% Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
% Electricity_Price(floor(N*44/48)+1:N) =  28.59;
% Pe = Electricity_Price;

% new electricit price
Electricity_Price(1:11) = 28.59;
Electricity_Price(12:19) = 85.76;
Electricity_Price(13:33) = 48.49;
Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49; 
Electricity_Price(45:48) =  28.59;

Pe = zeros(1,N);
for ii = 1:N;
    Pe(ii) = sum(Electricity_Price((ii-1)*48/N+(1:48/N)));
end
Pe = Pe/ (48/N);

% demand  rows are time and colums are nodes!
demand = importdata('Peak_1.pat'); % coice of demand pattern

demand.data = demand.data/mean(demand.data);

% d = zeros(1,N);
for ii = 1:N;
    dd(:,ii) = mean(demand.data((ii-1)*48/N+(1:48/N)));
end



d   = zeros(7,N);
dd = dd*demand_multiplier;
d(3,:) = dd;
%  d(	3,:) = demand.data; %location of the demand! Node 5!
%  d = d';%m3/h
% n_demands = 1;

% Account for total demand and devide it amongst the nodes again like for
% Pe


% d = d(:,1:floor(48/N):48); % skip some if needed
% d = d(:,1:N)*demand_multiplier; %shorten if required
% d = d./mean(d,2)*demand_multiplier;
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
 
T_bounds = [0 1];
%new node order:
h_min = [0 0 0 -15 0 0 0 ]-20;
% Old node order:
% h_min = [0    0   0   0   0   0  -15]; % max and min values of the head at the nodes

% h_max = [100 100 100 100 100 100 100]; % keep h max as small as possible

% keep h max as small as possible
% h_max = [0    90      90         90      90         90       90];
% h_max = [ 0   77.6229   25.8848    9.7769   55.9468    0.2675   83.0290]+1;

% h_max = [0    90      45      10       90         10       90];

% h_max = [ 1   90        45      5      90          10     170];

% % old
% h_max = [0   98.70 50 1 53.99 6 20 ]+4;
% h_max(4) = 5;  h_max(6) = 10;
%new node order
h_max = [98.70  50   53.99 20 0 6 1 ]+24;
h_max(7) = 5;  h_max(6) = 10;

              
            

% h_max = [0   90.7847   55.0000   50.0000         0   70.0328  100.0000]+2;
%  h_max =   [ 1    95        45         5       90        10       20];
Qmax =0.4; % there is no bound on q for now just make it large enough
% q_bounds = [-Qmax  Qmax] ; %can have any value
% old : 
%  q_lim = [Qmax  Qmax Qmax  Qmax Qmax Qmax Qmax    Qmax    Qmax   ];%max(demand.data*demand_multiplier) ];


 % corrected:

 q_lim_pipe =  q_lim_maker(pipedata);
% q_lim = [q_lim_pipe 0.2405 0.3181 0.2405 max(demand.data*demand_multiplier) ];
q_lim = [Qmax  Qmax Qmax  Qmax Qmax Qmax Qmax    Qmax    ];
% q_lim = [0.3181    0.1924    0.1414    0.1414    0.1924    1.5708    0.6702    0.1500];
lambda_bounds = [0 1];


if exist('OP_flow_pump','var')&& exist('OP_flow','var')
[a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow_pump,OP_head_pump,plotting_switch);
else
   disp('Pumps OP points not specified, will estimate them but may not be accurate! Please measure and report back')
    [a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,plotting_switch);
end

Network_tests


%% plotting
% loc = [0 0; 1 0; 2 2 ; 3 2; 3 4; 2 4; 2 3 ];
% mInc = connection_matrix;
% mInc((sum(abs(mInc),2) == 1),:) = [];
% mAdj = inc2adj(mInc);
% gplot(mAdj,loc,'-r*');


% % works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end
end