%% for single case running, set defaults
% input from: http://emps.exeter.ac.uk/media/universityofexeter/emps/research/cws/downloads/anytown.inp
units = 'US' % 'SI';
inp = [ 1      1	 2	12000	 12	120     ;...
  2	 1      12	12000	 12	 70	;...
  3      1	13	12000	 16	 70	;...
  4      1	20	  100	 30	130	;...
  5	 2 	 3	 6000	 10	120     ;...
  6      2	 4 	 9000    10	120     ;...
  7      2	13	 9000	 12	 70     ;...
  8      2	14	 6000	 10	120     ;...
  9	 3	 4	 6000	 10	120     ;...
  11     4	 8	12000	  8	120     ;...
  12	 4	15	 6000	 10	120     ;...
  17     8	 9	12000	  8	120     ;...
  18     8	15	 6000	 10	120     ;...
  19     8	16	 6000	  8	120     ;...
  20     8	17       6000	  8	120     ;...
  21     9	10	 6000	  8	120     ;...
  22    10	11	 6000	  8	120     ;...
  23    10	17	 6000	 10	120     ;...
  24    11	12	 6000	  8	120     ;...
  26    12	17	 6000	 10	120     ;...
  27    12	18       6000	  8	 70	;...
  28    13	14	 6000	 12	 70	;...
  29    13	18       6000	 12	 70	;...
  30    13	19	 6000	 10	 70	;...
  31    14	15	 6000	 12	 70	;...
  32    14	19	 6000	 10	 70	;...
  33    14	21	  100	 12	120	;...
  34    15	16	 6000	 10	 70	;...
  35    15	19	 6000	 10	 70	;...
  36    16	17	 6000	  8	120	;...
  37    16	18	 6000	 12	 70	;...
  38    16	19	 6000	 10	 70	;...
  39    17	18	 6000	  8	120	;...
  40    17	22	  100	 12	120	;...
  41    18	19	 6000	 10	 70	;...
  142	21	23	    1	 12	120	;... % conection to tanks formerly 41 
  143	22	24	    1	 12	120];%	;... % conection to tanks formerly  42

inp(:,1) = []; % get rid of the pipe ID not needed anyway
cons = inp(:,1:2); % just the conection info

if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 48;
end

extra = [ 1	20	500		1;...
      2     50	200		1;...
      3     50	200		1;...
      4     50	600		1;...
      5     80	600		1;...
      6     80	600		1;...
      7 	80	600		1;...
      8     80	400		1;...
      9    120	400		1;...
      10   120	400		1;...
      11   120	400		1;...
      12    50	500		1;...
      13    50	500		1;...
      14	50	500		1;...
      15	50	500		1;...
      16   120	400		1;...
      17   120  1000		1;...
      18    50	500		1;...
      19	50  1000		1;...
      20	20        0		1;...
      21    50        0		1;...
      22   120        0		1;...
      23    215       0       0;... % tanks
      24    0       0       0;...
      25    0       0       0]; % pump reservoir
%% nodes
% Elevation
 Elevation = extra(:,2); % elevation change for pumps is ignored!

% Reservoir
% fixed head
location_fix = [25];
fixed_val = [0];% m

% variable head tanks
location_res = [23 24];

Reservoir_area = [24*60*60/N 24*60*60/N]; % m2
%% network

%connection matrix
%first colums is from second is to
% pipes
connection_matrix = zeros(size(cons,1),max(cons));
for i = 1:size(cons,1)
    connection_matrix(i,cons(i,1)) = 1;
    connection_matrix(i,cons(i,2)) = -1;
end
% pumps
connection_matrix(i+1,25) = 1; % from reservoir
connection_matrix(i+1,20) = -1;

% demand and elevation
  connection_matrix(i+2:i+20,1:19) = eye(19);

  
% connection_type
  for i = 1:size(inp,1)
    connection_type{i} = 'pipe_quad';
  end
  connection_type{i+1} =  'Pump_quad';
  for i = 1:19 
    connection_type{size(inp,1)+1+i} =  'demand'; 
  end
 





%% Pumps
% Method 1
a_org = [-0.5]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [0];
c_org = [100 ];
n_pumps = [3]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  
      

[pump  ] = PieceWiseApproxGenPump(a_org,b_org,c_org,3,[],plotting_switch); 
Pump_station(1).pump(1).curves =  [-1 0]; % for every pump station
for i = 1:length(pump) % for all pumps
    Pump_station(1).pump(i+1).curves = [ pump(i).ms' pump(i).Cs' ];
end


% Method 2
Q_pump_min = [ 0.236*0.8 0.2388*0.8 0.2388*0.8  ]; % New row for each  pump sation fill with zeros for non needed stuff
Q_pump_max = [ 0.236*1.2 0.2388*1.2  0.2388*1.4];
Delta_H    = [ 5.462 10.32 12.6] ;

% Method 3
for i = 1:length(n_pumps)
    for n = 1:n_pumps(i)
        a_quad_pump(i,n) = a_org(i)/(n^2);
        b_quad_pump(i,n)  = b_org(i)/n;
        c_quad_pump(i,n) = c_org(i);
    end
end



%% Pipes
% vmax = f(D) needs to be implimented!
% record pipe data
switch units
    case 'SI'
for i = 1:size(inp,1)
    pipedata(i).data = [inp(i,4),inp(i,5),inp(i,3),2,5]; %D,e,L,vmax,pmax,
end
case 'US'
    for i = 1:size(inp,1)
        pipedata(i).data = [inp(i,4)*0.0254 ,inp(i,5),inp(i,3)*0.3048,2,5]; %D,e,L,vmax,pmax,
    end
end



%% misc
% mods for analysis (not used)
demand_multi = extra(:,3)*0.227; % demand and gpm / m3/h US case
demand_multi(demand_multi == 0) = []; % delete zero elements
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*41/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;
% Pe = strech(Pe,price_strech);
switch_penalty = 0;

% demand 
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
d = d';%m3/h
% d = strech(d,demand_strech);
n_demands = 1;


% bounds
T_bounds = [0 1];
h_min = [zeros(1:22) 10*0.3048 10*0.3048 0]; % max and min values of the head at the nodes
h_max = [ones(1:22)*200 35*0.3048 35*0.304810 400];
Qmax = 10; % there is no bound on q for now jsut make it large
q_bounds = [0 Qmax] ; %can have any value
lambda_bounds = [0 1];