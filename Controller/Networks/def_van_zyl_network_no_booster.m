%% for single case running, set defaults
if exist('plotting_switch','var'); 
else
    plotting_switch = 'on';
end

if exist('N','var'); 
else
   N = 24;
end

%% Pumps
% Pump characteristics:
a_org = [-1226.8]; %[a1 a2 ] % for each type of pump  along n_pumps 			
b_org = [79.6];
c_org = [98.4 ];
n_pumps = [2 ]; %number of pumps matrix style for several pump stations: [2 3 5 ..]

%Power: % fake only!!
Power_nameplate = [2.2/1000  ]; %MW 
Power_linear = [0.2 ];% [kW/m3/s kW] % just made up
Power_quad = [ -0.1 0.4 ];% [kW/(m3/s)^2 kW/m3/s kW]

%operation points: (From EPAnet)
OP_flow = [0.165 0.237 ]; %flow through pump station. not individual pumps! m3/s
OP_head = [88.44 110.2 ];

%make all the approximations
  [a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow,OP_head,plotting_switch);


      
%% Pipes
% settings:
% [1 0 0] % one main pump on
% [0 1 0] % both main pump on
% [0 1 1] % both main pumps and the booster on 
% [1 0 1] % one main pump on + booster
% [0 0 1] % only the booster on 

%1
pipedata(1).data = [0.45,0.001,2600,2,5]; %D,e,L,vmax,pmax,
OP_flow_pipe(1,:) = [0.165 0.237   ];

%2
pipedata(2).data = [0.35,0.001,1000,2,5];
OP_flow_pipe(2,:) = [0.140  0.16493 ];

%3
pipedata(3).data = [0.3,0.001,500,2,5];
OP_flow_pipe(3,:) = [0.056  0.056  ];

%4
pipedata(4).data = [0.3,0.001,1100,2,5]; 
OP_flow_pipe(4,:) = [-0.09368 -0.09368  ];

%5
pipedata(5).data = [0.35,0.001,2000,2,5];
OP_flow_pipe(5,:) = [0.025  0.070 ];

%6
pipedata(6).data = [0.35,0.001,1,2,5];
OP_flow_pipe(6,:) = [0.025  0.070 ];




%% network
% van zyl network
connection_matrix = [...
    1 -1  0  0  0  0  0;... %P1 main1
    0  1 -1  0  0  0  0;... %q3 1
    0  0  1 -1  0  0  0;... %q4 2
    0  0  0  1 -1  0  0;... %q5 3
    0  0  0  0  1 -1  0;... %q6 4
    0  0  0  0  0 -1  1;... %q7 5
    0  0  1  0  0  0  1;... %q8 6 % booster bypass
    0  0  0  0  1  0  0]; % demand out
 %  1  2  3  4  5  6  7

pump_approx = 'Pump_simple';
pipe_approx = 'pipe_quad';
 
connection_type = {... 
   pump_approx,...
   pipe_approx,...
   pipe_approx,...
   pipe_approx,...
   pipe_approx,...
   pipe_approx,...
   pipe_approx,...
    'demand'}; 


%% nodes
% Elevation
Elevation = [20 10 75 80 30 85 100];

% Reservoir
% feed reservoir fix head
location_fix = [1];
fixed_val = [20]; % m

%Variable Tank
location_res = [4 6]; 
% Initial_filling = [1]; %comment out if no initial settings are desired
Diameter =  [25 20];
Reservoir_area = pi*Diameter.^2/4;
% Reservoir_area = [24*60*60/N 24*60*60/N]; %m2 Diameter of

%% misc
% mods for analysis (not used)
demand_multiplier = 0.0015;
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;
switch_penalty = 0;
 
% demand 
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
d = d';%m3/h
n_demands = 1;
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
T_bounds = [0 1];
h_min = [0  0 0  0  0 0 0]; % max and min values of the head at the nodes
h_max = [10 10 10 10 10 10 10];
Qmax = 10; % there is no bound on q for now jsut make it large
q_bounds = [0 Qmax] ; %can have any value
lambda_bounds = [0 1];