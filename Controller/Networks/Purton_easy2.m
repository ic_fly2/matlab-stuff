%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
% Elevation
Elevation = [0 1 20 1 20]; % elevation change for pumps is ignored!

% Reservoir
% fixed head
location_fix = [1];
fixed_val = [0];% m


% variable head tanks
location_res = [5];
% Initial_filling = [1]; %comment out if no initial settings are desired
Diameter = 50;
Reservoir_area = pi*Diameter^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)
if ~exist('pump_approx','var')
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_quad';
end
% definition
connection_matrix = [...
   -1  1  0  0  0 ;... %q1 main pump
    0 -1  1  0  0 ;... %q2 pipe 1
    0  0  -1 0  1; 
    0  0  -1 1  0  ;... %q3 pipe 5
    0  0  0 -1  0  ;
    0  0  0  0  -1]; % demand out Bristol
%   1  2  3  4  T1

% connection_matrix = -[...
%     1 -1  0  0  0  0  0  0  0  0  0;... %q1 main pump
%     0  1 -1  0  0  0  0  0  0  0  0;... %q2 pipe 1 
%     0  0  1 -1  0  0  0  0  0  0  0;... %q3 pipe 2
%     0  0  0  1 -1  0  0  0  0  0  0;... %q4 pipe 3
%     0  0  0  0  1 -1  0  0  0  0  0;... % q5 pipe 4
%     0  0  0  0  0  1 -1  0  0  0  0;... % q6 pipe 5
%     0  0  0  0  0  1  0  0  0  0 -1;... % q6 pipe 6      0  0  0  0  0  0  0 -1  0  0  1;... % q6 pipe 7    
%     0  0  0  1  0  0  0  0  0 -1  0;... % q6 pipe 8      0  0  0  1  0  0  0  0 -1  1  0;... % q6 pipe 9
%     0  0  0  0  0  0  0  0  1  0  0;... % demand out T1
%     0  0  0  0  1  0  0  0  0  0  0;... % demand out5
%     0  0  0  0  0  0  0  1  0  0  0;... % demand out T2
%     0  0  1  0  0  0  0  0  0  0  0];   % demand out 3
% %   1  2  3  4  5  6  7  8  9  T1  T2

connection_type = {pump_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   pipe_approx,...
                   'demand',...
                   'demand'}; 

%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-225.27]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [21.423] ;
c_org = [32.948 ];
n_pumps = [3]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power:
Power_nameplate = 2.2/1000; %MW
Power_linear = [0.2 1.8 ];% [kW/m3/s kW] % just made up
Power_quad = [ -0.1 0.4 1.6 ];% [kW/(m3/s)^2 kW/m3/s kW]

%operation points: (From EPAnet)
OP_drivers = {[1 0 0] [0 1 0] [0 0 1]};
OP_flow_pump = [229.15 318.12 344.6781]/(1000); %flow through pump station. not individual pumps! m3/s
OP_head_pump = [25.96 23.03 25.1];
       
%make all the approximations % move to script
  [a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow_pump,OP_head_pump,plotting_switch);
%% pipes
%1
pipedata(1).data = [0.5,0.001,1500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) =[229.15 318.12 344.6781]/(1000); % flow in pipes (Same as pumps but in m3/s)

%2
pipedata(2).data = [0.35,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [140  167.93  47.15]/1000; %

%3
pipedata(3).data = [0.3,0.001,500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(3,:) = [0.05632 0.05632 0.05632]; % flow in pipes (Same as pumps but in m3/s)

%% misc
% mods for analysis (not used)
demand_multiplier =  [ 0.015  0.1]; %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;

 
% demand  rows are time and colums are nodes!
demand1 = importdata('Peak_1.pat'); % coice of demand pattern
demand2 = importdata('Peak_2.pat'); % coice of demand pattern
% demand3 = importdata('Peak_3.pat'); % coice of demand pattern
% demand4 = importdata('Peak_4.pat'); % coice of demand pattern

d = [demand1.data demand2.data];% demand3.data demand4.data];
d = d';%m3/h
n_demands = 2;


% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test
d = d(:,1:floor(48/N):48); % skip some if needed
d = d(:,1:N); %shorten if required
% d = d/mean(d)*demand_multiplier;
for i = 1:n_demands
   d(i,:) =  d(i,:)/mean(d(i,:))*demand_multiplier(i);
end

% bounds
T_bounds = [0 1];
M = 999;
h_min = [0 0 0 0 50]; % max and min values of the head at the nodes
h_max = [50 50 50 50 50 ];
Qmax = 20; % there is no bound on q for now just make it large
q_bounds = [-Qmax Qmax] ; %can have any value
lambda_bounds = [0 1];


Network_tests

% disp('Network ok, run optimisation now')

%% plotting
% works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end