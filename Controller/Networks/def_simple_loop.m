%% for single case running, set defaults
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 48;
end

%% nodes
% Elevation
Elevation = [0 0 0.5 0.3]; % elevation change for pumps is ignored!

% Reservoir
% fixed head
location_fix = [1];
fixed_val = [1];% m

% variable head tanks
location_res = [4];

Reservoir_area = 24*60*60/N; % m2

%% network

% aproximations: (Also used in naming schedules)
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_quad';
% simple looped network
connection_matrix = [...
    1 -1 0 0;... %q1
    0 1 -1 0;... %q2
    0 0 1 -1;... %q3
    0 1 0 -1 ;...  %q4
    0 0 0 1]; % demand out


connection_type = {...
    pump_approx, ...
    pipe_approx,...
    pipe_approx,...
    pipe_approx,...
    'demand'}; 



%% Pumps
% Pump characteristics:
a_org = [-0.5]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [0];
c_org = [100 ];
n_pumps = [3]; %number of pumps matrix style for several pump stations: [2 3 5 ..]

%operation points: (From EPAnet)
OP_flow = [0.236 0.2388 0.2388]; %flow through pump station. not individual pumps! m3/s
OP_head = [5.462 10.32 12.6];

%Power: % fake only!!
Power_nameplate = [2.2/1000 ] ; %MW
Power_linear = [0.2 1.8 ];% [kW/m3/s kW] % just made up
Power_quad = [ -0.1 0.4 1.6 ];% [kW/(m3/s)^2 kW/m3/s kW]

%make all the approximations
  [a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow,OP_head,plotting_switch);


%% Pipes
% 1
pipedata(1).data = [0.5,0.001,1500,5,5]; %D,e,L,vmax,pmax,
OP_flow_pipe(1,:) = [0.0767  0.1459    0.2006]*2; %only for testing

%2
pipedata(2).data = [0.1,0.001,500,2,5];
OP_flow_pipe(2,:) = [0.0767  0.1459    0.2006]*1.45;

%3
pipedata(3).data = [0.9,0.001,1500,2,5];
OP_flow_pipe(3,:) = [0.0767  0.1459    0.2006]*1.8;

%% misc
% mods for analysis (not used)
demand_multiplier = 0.7;
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*41/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;
% Pe = strech(Pe,price_strech);
switch_penalty = 0;

% demand 
demand = importdata('Peak_le.txt'); % coice of demand pattern
d = demand.data;
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
d = d';%m3/h
% d = strech(d,demand_strech);
n_demands = 1;


% bounds
T_bounds = [0 1];
h_min = [0  1  1  1 ]; % max and min values of the hea dat the nodes
h_max = [10 10 10 10];
Qmax = 10; % there is no bound on q for now jsut make it large
q_bounds = [0 Qmax] ; %can have any value
lambda_bounds = [0 1];