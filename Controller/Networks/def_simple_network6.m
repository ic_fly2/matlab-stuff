%% simple network definition
% Lecture_example.net for the Epanet file
%% for single case running, set defaults
% clear all;
if exist('plotting_switch','var'); 
else
    plotting_switch = 'off';
end

if exist('N','var'); 
else
   N = 24;
end
%% nodes
% Elevation
Elevation = [0 1 18 10]; % elevation change for pumps is ignored!
% Elevation = [0 1 1 1 1 1 1]; 
% Reservoir
% fixed head
location_fix = [1];
fixed_val = [0];% m


% variable head tanks
location_res = [3];
%   Initial_filling = [1.9 3.2]; %comment out if no initial settings are desired
Diameter = [50]; 
Reservoir_area = pi*Diameter.^2/4; %24*60*60/N; % m2 Diameter: [50]

%% network
% aproximations: (Also used in naming schedules)
if ~exist('pump_approx','var')
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_quad';

end
% definition
connection_matrix = -[...
    1 -1  0  0  ;... %q1
    0  1 -1  0  ;... %q2
    0  0  1 -1  ;... %q3
    1 -1  0  0  ;... %check valve
    0  0  0  1  ]; % demand out
%   1  2  3   4  5  6  6a
connection_type = {pump_approx,...
                   pipe_approx,...
                   pipe_approx,...                    
                   'check_valve',...
                   'demand'}; 

%% Pumps
% Choose either Method 1 or 2
% Method 1:
% from polynomial: 
a_org = [-225.27 -225.27 ]; %[a1 a2 ] % for each type of pump  along n_pumps
b_org = [21.423 21.423] ;
c_org = [32.948  32.948];
n_pumps = [3]; %number of pumps matrix style for several pump stations: [2 3 5 ..]  

%Power:
Power_nameplate = [ 2.2/1000]; %MW
Power_linear = [0.2 1.8];% [kW/m3/s kW] % just made up
Power_quad = [ -0.1 0.4 1.6 ];% [kW/(m3/s)^2 kW/m3/s kW]

%operation points: (From EPAnet)

% OP_drivers = {[1 0 0 0],[0 1 0 0],[0 0 1 0],[1 0 0 1], [0 1 0 1],[0 0 1 1],[0 0 0 1]}; 
% OPs          [100 0][010 0][001 0][100 1] [010 1] [001 1]  [000 1]  [000 0]Ommitted  
%flows (each row is a pump station)
OP_flow_pump = [276.18 525.35 722]/(3600); %flow through pump station. not individual pumps! m3/s
%Heads(each row is a pump station)
OP_head_pump = [21.57 23.03 25.1];
       
%make all the approximations % move to script
  [a_quad_pump,b_quad_pump,c_quad_pump,...
  m_linear_pump,c_linear_pump,...
  Q_pump_min,Q_pump_max,Delta_H,...
  Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow_pump,OP_head_pump,plotting_switch);
%% pipes
%1
pipedata(1).data = [0.5,0.001,1500,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(1,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)

%2
pipedata(2).data = [0.5,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
OP_flow_pipe(2,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)

% 
% %3
% pipedata(3).data = [0.1,0.001,50,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(3,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)
% % 
% 
% %4
% pipedata(4).data = [0.35,0.001,400,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(4,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)
% 
% %4
% pipedata(5).data = [0.5,0.001,1000,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(5,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)
% 
% %4
% pipedata(6).data = [0.5,0.001,10,2,5]; %D,e,L,vmax,pmax, in m / m /m note that Epanet needs D and e in mm!!
% OP_flow_pipe(6,:) = [0.0767  0.1459    0.2006]; % flow in pipes (Same as pumps but in m3/s)

%% misc
% mods for analysis (not used)
demand_multiplier = 0.15; %34 max in quad quad
switch_penalty = 0.15; %� from Savic et al.
% res_time      = 6;
% price_strech  = 1;
% demand_strech = 1;

% Electricity
Electricity_Price(1:floor(N*11/48)) = 28.59;
Electricity_Price(floor(N*11/48)+1:floor(N*19/48)) = 85.76;
Electricity_Price(floor(N*19/48)+1:floor(N*33/48)) = 48.49;
Electricity_Price(floor(N*33/48)+1:floor(N*40/48)) = 85.76;
Electricity_Price(floor(N*40/48)+1:floor(N*44/48)) = 48.49; 
Electricity_Price(floor(N*44/48)+1:N) =  28.59;
Pe = Electricity_Price;

 
% demand  rows are time and colums are nodes!
demand = importdata('Peak_le.txt'); % coice of demand pattern

d = demand.data;
d = d(1:floor(48/N):48); % skip some if needed
d = d(1:N); %shorten if required
d = d/mean(d)*demand_multiplier;
d = d';%m3/h
n_demands = 1;
if exist('x','var')
    load('variation')
    d = d+0.2*variation;
end
% d = strech(d,demand_strech);
% d = [ 0.19; 0.21; 0] % test


% bounds
T_bounds = [0 1];
h_min = [0     0 0   0 ]; % max and min values of the head at the nodes
h_max = [100 100 30 100 ];
Qmax = 10; % there is no bound on q for now just make it large
q_bounds = [-Qmax Qmax] ; %can have any value
lambda_bounds = [0 1];

% Network_tests


%% plotting
% works
% pumps
% hold all;
% color = ['rgbkrgbkrgbkrgbk'];
% for j = 1:length(Pump_station)
%     for i = 2:length(Pump_station(j).pump)
%         x = 0:10;
%         fun = @(x) Pump_station(j).pump(i).curves(:,1)*x + Pump_station(j).pump(i).curves(:,2)*ones(1,length(x));
%         plot(x,fun(x),color(i))
%     
%     end
%     figure
% end