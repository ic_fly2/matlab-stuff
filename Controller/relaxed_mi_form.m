% Regularisation method
% mu = 50; %Small number at first
x0 = lb;

% loop with increasing mu
for mu = -100:-100:-500
H_mu = diag(xtype == 'I')*mu;  % not sure about sign -x(1-x) or x(1-x)
H_mod = H_mu+H;
f_mod = f+(xtype == 'I')*mu; 
optObj = opti('qp',H_mod,f_mod,'eq',Aeq,beq,'ineq',A,b,'lb',lb,'ub',ub)
% [x,fval,exitflag,output,lambda]  = cplexqp(H,f,A,b,Aeq,beq,lb,ub,x0);
[x,fval,exitflag,info] = solve(optObj);

x0 = x;
x((xtype ~= 'I')) = [];
sum(abs(round(x)-x))
if sum(abs(round(x)-x)) < 0.1;

    break
end
end
