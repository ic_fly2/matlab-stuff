function [ K,Nexp,Feeds,pumpsOn,connections] = pump_control(pipes,pumps,n_pumps,tanks,Nexp,K,npi,T) 
% pump_control moved from prep GGA to seperate script as it is called repeatadly in the EPS

% if ~exist('T','var')
%     T = [];
% end

if nargin == 7
    T = [];
end

[pumpsOn,T] = selectPumps(pumps,n_pumps,T);

a_org = zeros(length(pumpsOn),1);
b_org = zeros(length(pumpsOn),1);
c_org = zeros(length(pumpsOn),1);
for i = 1:length(pumpsOn)
    if strcmp(pumpsOn{i}.Status,'Closed')
        K(npi+i) =  10000000;
        Nexp(npi+i) = 2;
    elseif  strcmp(pumpsOn{i}.type,'FSD')
        %         a_org(i) = pumpsOn{i}.a;
        %         b_org(i) = pumpsOn{i}.b;
        %         c_org(i) = pumpsOn{i}.c;
        %         switch fitting_type
        %             case 'poly' % if the info in the pumpcurve is polynomial. Use this crude approximation waiting for implimentation in GGA
        %                 [K_pump,Nexp_pump,~] = PowerLawConverter([a_org(i) b_org(i) c_org(i)]);
        %                 K(npi+i) = -K_pump;
        %                 Nexp(npi+i) = Nexp_pump;
        %             case 'power'
        %                 K(npi+i) = a_org(i);
        %                 Nexp(npi+i) = b_org(i);
        %         end
%         K(npi+i) = pumps{i}.K;
%         Nexp(npi+i) = pumps{i}.Nexp;

    elseif strcmp(pumpsOn{i}.type,'VSD')
        [K(npi+i),Nexp(npi+i),omega] = state2rpm(h,q,pumps{i}.name);
    end
end

 



%% EPS feeds into tanks
connections = [pipes pumpsOn];
Feeds = zeros(length(tanks),length(connections));
for i = 1:length(tanks)
    for j = 1:length(connections)
        if isfield(connections{j},'endNodeId') && strcmp(connections{j}.endNodeId,tanks{i}.Id)
            % feed in
            Feeds(i,j) = 1;
        elseif isfield(connections{j},'endNodeId') && strcmp(connections{j}.startNodeId,tanks{i}.Id)
            % feed out
            Feeds(i,j) = -1;
        end
    end
end

