%resize the A matracies
Aeq = [Aeq zeros(size(Aeq,1),np*N)];
A   = [A   zeros(size(A  ,1),np*N)];

%modify f and H
f = [f zeros(1,np*N)];
b_addition = zeros(np*N,1); b_addition(1:max(loc_pipes*N)) = 1000000; 
added_H = blkdiag(zeros(size(H)),diag(b_addition));
H = blkdiag(H,diag(b_addition));% works: eye(np*N)*1000000

%Mod lb ub and xtype
%  0 <=  b <= Qmax
lb = [lb  zeros(1,np*N)];
ub = [ub  ones(1,np*N)*Qmax]; %could be more refined butshouldn't affect performance
b_cont(1,1:np*N) = 'C';
xtype = strcat([xtype b_cont]);

%Add bits to Aeq and beq
% b = Qmax - q
% -q + b = Qmax
for ii = 1:length(loc_pipes);
    driver = zeros(1,np);
    driver(loc_pipes(ii)) = 1;
    q = place_matrix(eye(N),driver,2);
    Aeq = [Aeq;...
           zeros(N,N*sum(n_pumps) + N*nn) -q   zeros(N,length_lambda*N) q];
    %               pumps (T)       (h)     (q) (lambda)                (b)    
    beq = [beq;...
           ones(N,1)*q_lim(loc_pipes(ii))];
end