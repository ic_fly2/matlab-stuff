
switch settings.OF_spec
    case 'F1' % the original
        f_cost = []; % pricing for pump operations
        for i = 1:data.npu
            %driver = zeros(1,data.npu);driver(i) = 1;
            
            cost = double(double(pumps{i}.power.nameplate)*...
                    Pe*double(Delta_T)/double(3600));
%             cost = place_matrix(pumps{i}.power*Pe*Delta_T/3600,driver,2);
            f_cost = [f_cost cost];
        end
        f = [f_cost zeros(1,N*data.nn + N*data.np + N*data.length_lambda)];
        H_switch = 2*eye(N*data.npu) + diag(-ones(1,N*data.npu-1),1) + diag(-ones(1,N*data.npu-1),-1);
        % zero every where else (rewrite this in elegant) blkdiag()
        H_switch = H_switch*settings.switch_penalty;
        H_head = zeros(N*data.nn);
        H_flow = zeros(N*data.np);
        H_lambda = zeros(N*data.length_lambda);
        
        %H(N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda,N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda ) = 0;
        H = blkdiag(H_switch,H_head,H_flow, H_lambda)    ;
    case 'F2' 
        
        % for T
        f_cost = []; % pricing for pump operations
        f_flow_cost = [];
        for i = 1:length(data.pumps_vec)
%             driver = 1:data.pumps_vec(i);
            cost = pumps{i}.power.lin_approx(2)*Pe*Delta_T/3600;
%             cost  = place_matrix(p1{i}(2)*Pe*Delta_T/3600,driver,2);
            f_cost = [f_cost cost];
        end
        
        %for q
        for i = 1:length(data.pumps_vec)   
%             flow_cost  = p1{i}(1)*Pe*Delta_T/3600;
            flow_cost  =pumps{i}.power.lin_approx(1)*Pe*Delta_T/3600;
%             flow_cost  = place_matrix(p1{i}(1)*Pe*Delta_T/3600,driver,2);
            f_flow_cost = [f_flow_cost flow_cost];            
        end
        f_flow_cost = [zeros(1,data.npi*N) f_flow_cost ];
        
        % make f
        f = [f_cost zeros(1,N*data.nn) f_flow_cost zeros(1,N*data.length_lambda)];

        H_switch = 2*eye(N*data.npu) + diag(-ones(1,N*data.npu-1),1) + diag(-ones(1,N*data.npu-1),-1);
        H_switch = H_switch*settings.switch_penalty;
        H_head = zeros(N*data.nn);
        H_flow = zeros(N*data.np);
        H_lambda = zeros(N*data.length_lambda);
        
        %H(N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda,N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda ) = 0;
        H = blkdiag(H_switch,H_head,H_flow, H_lambda)    ;
        
    case 'F3'
        % like F1 but without considering switches
        f_cost = []; % pricing for pump operations
        for i = 1:data.npu
            %driver = zeros(1,data.npu);driver(i) = 1;
            cost = pumps{i}.power.nameplate*Pe*Delta_T/3600;
%             cost = place_matrix(pumps{i}.power*Pe*Delta_T/3600,driver,2);
            f_cost = [f_cost cost];
        end
        f = [f_cost zeros(1,N*data.nn + N*data.np + N*data.length_lambda)];

        
        
%         f_cost = []; % pricing for pump operations
%         for i = 1:data.npu
% %             driver = 1:data.n_pumps(i);
%             pumps{i}.power.nameplate*Pe*Delta_T/3600;
%             cost = place_matrix(Power_nameplate(i)*Pe*Delta_T/3600,driver,2);
%             f_cost = [f_cost cost];
%         end
%         f = [f_cost zeros(1,N*data.nn + N*data.np + N*data.length_lambda)];
        
        H = zeros( (data.npu+data.nn+data.np+data.length_lambda)*N );
                
    case 'F4'
       % [p1, ~] = add_power_curve(a_quad_pump,b_quad_pump,c_quad_pump,data.n_pumps);
        
        
        % for T
        f_cost = []; % pricing for pump operations
        f_flow_cost = [];
        for i = 1:data.npu
%             driver = 1:data.n_pumps(i);
            cost = place_matrix( pumps{i}.power.lin_approx(2)*Pe*Delta_T/3600,driver,2);
%             cost  = place_matrix(p1{i}(2)*Pe*Delta_T/3600,driver,2);
            f_cost = [f_cost cost];
        end
        
        %for q
        for i = 1:data.npu   
%             flow_cost  = p1{i}(1)*Pe*Delta_T/3600;
            flow_cost  = place_matrix(pumps{i}.power.lin_approx(1)*Pe*Delta_T/3600,driver,2);
%             flow_cost  = place_matrix(p1{i}(1)*Pe*Delta_T/3600,driver,2);
            f_flow_cost = [f_flow_cost flow_cost];            
        end
        f_flow_cost = [zeros(1,data.npi*N) f_flow_cost ];
        
        % make f
        f = [f_cost zeros(1,N*data.nn) f_flow_cost zeros(1,N*data.length_lambda)];
        
        
        %H
        H = zeros((data.npu+data.nn+data.np+data.length_lambda)*N);
    case 'F5'
  
        %%% f 
        % f for T
        f_cost = []; % pricing for pump operations
        f_flow_cost = [];
        for i = 1:data.npu
%             driver = 1:data.n_pumps(i);
            cost = pumps{i}.power.quad_approx(3)*Pe*Delta_T/3600;
%             cost  = place_matrix(pumps{i}.power.quad_approx(3)*Pe*Delta_T/3600,driver,2);
%             cost  = place_matrix(p2{i}(3)*Pe*Delta_T/3600,driver,2);
            f_cost = [f_cost cost];
        end
        
        %for q
        for i = 1:data.npu                  
            flow_cost  = pumps{i}.power.quad_approx(2)*Pe*Delta_T/3600;
            f_flow_cost = [f_flow_cost flow_cost];            
        end
        f_flow_cost = [zeros(1,data.npi*N) f_flow_cost ];
        
        % make f
        f = [f_cost zeros(1,N*data.nn)  f_flow_cost zeros(1,N*data.length_lambda)];
        
        %%% H
        q_flow_cost = [];
        H_switch = 2*eye(N*data.npu) + diag(-ones(1,N*data.npu-1),1)+...
            diag(-ones(1,N*data.npu-1),-1);
        H_switch = H_switch*settings.switch_penalty;
        H_head = zeros(N*data.nn);
        
        %for q
        for i = 1:data.npu            
            flow_cost_quad  = pumps{i}.power.quad_approx(1)*Pe*Delta_T/3600;
            q_flow_cost = [q_flow_cost flow_cost_quad];            
        end
        H_flow = blkdiag(zeros(N*data.npi),diag(q_flow_cost)) ;
        H_lambda = zeros(N*data.length_lambda);
        H = blkdiag(H_switch,H_head,H_flow,H_lambda);
        
        
    case 'V1' %Q Tau
        %for q
         q_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qt(1);
            q_flow_cost = [q_flow_cost flow_cost];            
        end
        q_flow_cost = [zeros(1,data.npi*N) q_flow_cost ];
        
        % for Tau
        Tau_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qt(2);
            Tau_flow_cost = [Tau_flow_cost flow_cost];            
        end
        
        % make f
%         f = [zeros(1,N*data.npu) zeros(1,N*data.nn) q_flow_cost zeros(1,N*data.length_lambda) Tau_flow_cost ];
        f = [Tau_flow_cost zeros(1,N*data.nn) q_flow_cost zeros(1,N*data.length_lambda) zeros(1,N*data.npu)];
        
        
        
%         H_switch = 2*eye(N*sum(data.npu)) + diag(-ones(1,N*sum(data.npu)-1),1) + diag(-ones(1,N*sum(data.npu)-1),-1);
%         H_switch = H_switch*settings.switch_penalty;
        H_switch = zeros(N*data.npu);
        H_head = zeros(N*data.nn);
        H_flow = zeros(N*data.np);
        H_lambda = zeros(N*data.length_lambda);
        H_Tau = zeros(N*data.npu);
        
        H = blkdiag(H_Tau,H_head,H_flow, H_lambda,H_switch)*2;
               
    case 'V2' % Q Tau Tau.^2
                %for q
        q_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qtt2(1);
            q_flow_cost = [q_flow_cost flow_cost];            
        end
        q_flow_cost = [zeros(1,data.npi*N) q_flow_cost ];
        
        % for Tau
        Tau_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qtt2(2);
            Tau_flow_cost = [Tau_flow_cost flow_cost];            
        end
        
        % make f
        f = [zeros(1,N*data.npu) zeros(1,N*data.nn) q_flow_cost zeros(1,N*data.length_lambda) Tau_flow_cost ];

%         H_switch = 2*eye(N*sum(data.npu)) + diag(-ones(1,N*sum(data.npu)-1),1) + diag(-ones(1,N*sum(data.npu)-1),-1);
%         H_switch = H_switch*settings.switch_penalty;
        H_switch = zeros(N*data.npu);
        H_head = zeros(N*data.nn);
        H_flow = zeros(N*data.np);
        H_lambda = zeros(N*data.length_lambda);
        
        Tau_flow_cost2 =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qtt2(3);
            Tau_flow_cost2 = [Tau_flow_cost2 flow_cost];            
        end
        H_Tau = diag(Tau_flow_cost2);
        
        H = blkdiag(H_Tau,H_head,H_flow, H_lambda,H_switch)*2;
    
    case 'V3' % Q Q.^2 Tau
        %for q
        q_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2t(1);
            q_flow_cost = [q_flow_cost flow_cost];            
        end
        q_flow_cost = [zeros(1,data.npi*N) q_flow_cost ];
        
        % for Tau
        Tau_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2t(3);
            Tau_flow_cost = [Tau_flow_cost flow_cost];            
        end
        f = [zeros(1,N*data.npu) zeros(1,N*data.nn) q_flow_cost zeros(1,N*data.length_lambda) Tau_flow_cost ];

        %H
%         H_switch = 2*eye(N*sum(data.npu)) + diag(-ones(1,N*sum(data.npu)-1),1) + diag(-ones(1,N*sum(data.npu)-1),-1);
%         H_switch = H_switch*settings.switch_penalty;
        H_switch = zeros(N*data.npu);
        H_head = zeros(N*data.nn);
        

        q_flow_cost2 =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2t(2);
            q_flow_cost2 = [q_flow_cost2 flow_cost];            
        end
        q_flow_cost2 = [zeros(1,data.npi*N) q_flow_cost2 ];
        H_flow = diag(q_flow_cost2);
              
        H_Tau = zeros(N*data.npu);
        H_lambda = zeros(N*data.length_lambda);
        
        H = blkdiag(H_Tau,H_head,H_flow, H_lambda,H_switch)*2;
    
    case 'V4' %Q Q.^2 Tau  Tau.^2
        %for q
        q_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2tt2(1);
            q_flow_cost = [q_flow_cost flow_cost];            
        end
        q_flow_cost = [zeros(1,data.npi*N) q_flow_cost ];
        
        % for Tau
        Tau_flow_cost =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2tt2(3);
            Tau_flow_cost = [Tau_flow_cost flow_cost];            
        end
        
        % make f
        f = [zeros(1,N*data.npu) zeros(1,N*data.nn) q_flow_cost zeros(1,N*data.length_lambda) Tau_flow_cost ];

%         H_switch = 2*eye(N*sum(data.npu)) + diag(-ones(1,N*sum(data.npu)-1),1) + diag(-ones(1,N*sum(data.npu)-1),-1);
%         H_switch = H_switch*settings.switch_penalty;
        H_switch = zeros(N*data.npu);
        H_head = zeros(N*data.nn);
        
        q_flow_cost2 =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2tt2(2);
            q_flow_cost2 = [q_flow_cost2 flow_cost];            
        end
        q_flow_cost2 = [zeros(1,data.npi*N) q_flow_cost2 ];
        H_flow = diag(q_flow_cost2);
        H_lambda = zeros(N*data.length_lambda);
        
        Tau_flow_cost2 =[];
        for i = 1:data.npu
            flow_cost  = Pe*Delta_T/3600*pumps{i}.OF.qq2tt2(4);
            Tau_flow_cost2 = [Tau_flow_cost2 flow_cost];            
        end
        H_Tau = diag(Tau_flow_cost2);
        
        H = blkdiag(H_Tau,H_head,H_flow, H_lambda,H_switch)*2;
    otherwise %do the usual
        error('Incorrect specification of the power consumption!')
%         f_cost = []; % pricing for pump operations
%         for i = 1:length(data.n_pumps)
%             driver = 1:data.n_pumps(i);
%             cost = place_matrix(Power_nameplate(i)*Pe*Delta_T/3600,driver,2);
%             f_cost = [f_cost cost];
%         end
%         f = [f_cost zeros(1,N*data.nn + N*data.np + N*data.length_lambda)];
%         H = 2*eye(N*sum(data.n_pumps)) + diag(-ones(1,N*sum(data.n_pumps)-1),1) + diag(-ones(1,N*sum(data.n_pumps)-1),-1);
%         % zero every where else (rewrite this in elegant) blkdiag()
%         H(N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda,N*sum(data.n_pumps)+1:N*sum(data.n_pumps)+N*data.nn + N*data.np +N*data.length_lambda ) = 0;
%         H = H*settings.switch_penalty;
end





if strcmp(settings.DR.status,'Optimal')
    H = blkdiag(H,0);
    f = [f -settings.DR.reward/365/1000];
end

% f(7:9) = -1;

    
    
    