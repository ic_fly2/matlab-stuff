%make all the approximations % move to script


% network tests performed by Network_tests

if length(h_min) ~=  length(h_max) || length(h_max) ~= size(connection_matrix,2)  || length(h_max) ~= length(Elevation)
    error('Inconsistent system defenition: number  of nodes and node bounds don''t match')
end   

if size(connection_matrix,1) ~=  size(connection_type,2)
    error('Inconsistent system defenition: number and types of connections don''t match')
end   

% if sum(strcmp(connection_type,'demand')) ~=  n_demands
%     error('Inconsistent system defenition: number of demands and demand nodes specified don''t match')
% end 

if length(Reservoir_area) ~=  length(location_res) 
    error('Inconsistent system defenition: number of reservoirs and reservoir areas specivied daon''t match')
end 


% if length(Q_pump_min) ~= length(Q_pump_max) || length(Q_pump_min) ~= prod(n_pumps+1)-1
%     error('Simple pump flow defimitions wrong')
% end



disp('Network ok, run optimisation now')