% make matracies from problem defenition.
% Not a function, just a script.
% 
%

Delta_T = double(24*60*60/N); % duratiion of one interval in seconds

filename = [def '.inp'];
plotting_switch = 'off'; % on or off;
pump_number = 1; % counter to know which pump station work is being done on
pipe_number = 1; % counter to inform with pipe we are working on
pipe_quad_number = 1; % counter for which quadratic pipe you are on. Interim fix.
demand_counter = 1; % counter for demands to run though
A = [];
b = [];
Aeq = [];
beq = [];

[connection_matrix,connection_type] = makeconnection_matrix(...
    pipes,nodesIdListMap,pumps,pipe_approx,pump_approx);
prepConstraints;
flow_loc = N*(data.npu+data.nn)+(1:data.np*N);
nodes_loc = data.npu*N+(1:data.nn*N);


if isfield(settings,'DR') && ~strcmp(settings.DR.status,'Off')
    [data.Power] = Energy_assessment(junctions,pumps,reservoirs,settings.DR.desired_ratio);
elseif isfield(settings.DR,'demand_factor')
    [data.Power] = Energy_assessment(junctions,pumps,reservoirs,1);
else
    [data.Power] = Energy_assessment(junctions,pumps,reservoirs,1);
end


%% define some variables
n_piece_pipes = length(strmatch('pipe_piece', char(connection_type))); % number of pipes
loc_pipes  = strmatch('pipe', char(connection_type));

if strcmp(pipe_approx,'pipe_lin')
        loaded_data = load('cheddar_modified_lin_data.mat');
        G = loaded_data.ans;
        G(G == -Inf) = nan;
        G(G == Inf) = nan;
        G = nanmedian(G,2);
        G(isnan(G)) = 12.3;
        for i = 1:data.npi
            pipes{i}.lin_data = G(i);
            data.length_m_part(i) = 0;
        end
 %       pipes{i}.m_data , pipes{i}.C_data ,pipes{i}.Q_lim_data
        disp('lol')
else
    data.length_m_part = [];
    for i = 1:data.npi  ;
        if exist('OP_flow_pipe','var')
            [pipe(i).m_data , pipe(i).C_data ,pipe(i).Q_lim_data,P1,P2,P3]=...
                PieceWiseApproxGen(pipedata(i).data(1),pipedata(i).data(2),...
                pipedata(i).data(3),vmax_min,OP_flow_pipe(i,:),pmax_pipe,...
                plotting_switch,Approximation);
        else
            [pipes{i}.m_data , pipes{i}.C_data ,pipes{i}.Q_lim_data, pipes{i}.quad ]=...
                PieceWiseApproxGen2(pipes{i},settings);
        end
        data.length_m_part(i) = length(pipes{i}.m_data);

        %quadratic approx
        % a_quad_pipe(i) = P2(1);
        % b_quad_pipe(i) = P2(2);

    end
end
%% Elevation for pipes_piece and pipes_simple
% Assume pumps are the first things listed!
% Elevation change for pumps:
data.loc_Pumps = strmatch('Pump', char(connection_type));
pipes_and_valves = [strmatch('pipe', char(connection_type)); ...
    strmatch('check', char(connection_type))  ];
pipes_and_valves =  connection_type(pipes_and_valves);
length_lambda =0;
data.lambda_parts_length = [];
for i = 1:(data.npi + data.nv)
    if sum(strmatch('pipe_piece', char( pipes_and_valves)) == i) == 1
        if sum(strmatch('pipe_piece_CV', char( pipes_and_valves)) == i) == 1
            % Check valve
            data.lambda_parts_length =  [data.lambda_parts_length data.length_m_part(i)+1];
        else
            % normal pipe:
            data.lambda_parts_length =  [data.lambda_parts_length data.length_m_part(i)*2-1];
        end
    elseif sum(strmatch('pipe_convex', char( pipes_and_valves)) == i) == 1
        % Convex pipe, see Bonvin et al. or use as valve
        data.lambda_parts_length =  [data.lambda_parts_length 1];
    elseif sum(strmatch('pipe_quad', char( pipes_and_valves)) == i) == 1
        data.lambda_parts_length =  [data.lambda_parts_length 1];
    elseif sum(strmatch('pipe_lin_CV', char( pipes_and_valves)) == i) == 1
        data.lambda_parts_length =  [data.lambda_parts_length 1];
    elseif sum(strmatch('pipe_lin', char( pipes_and_valves)) == i) == 1
        % nothing :)
        %         length_lambda = length_lambda + 0; %just to make life easier
        data.lambda_parts_length =  [data.lambda_parts_length 0];

        
    elseif sum(strmatch('check_valve', char(pipes_and_valves)) == i) == 1
        %         length_lambda = length_lambda + 1;
        data.lambda_parts_length =  [data.lambda_parts_length 1];
    end
end
data.length_lambda =  sum(data.lambda_parts_length);
if length(location_fix) ~= length(fixed_val)
    error('Inconsistent system defenition: location and values of fixed locations don''t match')
end

%add original state of reservoir and minimum fill height at the end
%% Inequality matrix
% [np, nn] = size(connection_matrix); % np ~= n_pipes as np is number of conncetions
% and n_pipes is just pipes
flow_matrix = eye(data.np); % assuming it is always the identety then this would work
for i = 1:data.np
    A_part = []; % empty that one
    b_part = [];
    switch connection_type{i}
        %pumps
        case 'Pump_convex' %
            pump_convex_def
        case 'Pump_convex_VSD' %
            pump_convex_VSD_def
        case 'Pump_convex_VSD2' %
            pump_convex_VSD2_def
            % pump_convex_def % just testing if it would work (it doesn't)
        case 'Pump_simple'
            pump_simple_def
            %         case 'Pump_linear'
            %             pump_linear_def2
        case 'Pump_quad'
            pump_quad_def
            
        case 'pipe_nl_HW'
            pipe_nl_HW_def
            %         case 'Pump_quad_ineq'
            %             pump_quad_ineq_def
            % pipes
            %         case 'pipe_convex'
            %             pipe_convex_def
            %         case 'pipe_convex_CV' %(use less intigers)
            %             pipe_convex_CV_def
            
        case 'pipe_piece'
            pipe_piece2_def
        case 'pipe_piece_CV'
            pipe_piece_CV3_def
        case 'pipe_quad'
            pipe_quad_def
        case 'pipe_quad_CV'
            pipe_quad_CV_def
            %         case 'pipe_quad_ineq'
            %             pipe_quad_ineq_def
            %         case 'pipe_cube'
            %             %todo
            %         case 'pipe_linear'
            %             pipe_linear_def2
        case 'pipe_lin'
            pipe_lin_def
        case 'pipe_lin_CV'
            pipe_lin_CV_def            
        case 'check_valve'
            check_valve_def
        case 'valve'
            disp('Valves not implimented yet')
        otherwise
            error(['Unknown system definition in connection '...
                num2str(i) '. Unknown type: ' connection_type{i}])
    end
end


%% pump_station
if strcmp(settings.asymmetry,'on')
    symmety_test
end
%% mass balance
mass_balance;

%% Demand response minimum power ,VSD and concatinate

%VSD
if strcmp(pump_approx,'Pump_convex_VSD2')
    for i = 1:length(A_cell)
        length_normal = (data.nn+data.np+data.length_lambda+data.npu)*N;  % non VSD
        length_VSD = (data.nn+data.np+data.length_lambda+2*data.npu)*N;
        if size(A_cell{i},2) == length_normal
            A_cell{i}(:,length_normal+1:length_VSD) = 0;
        end
    end
    A_mass(:,length_normal+1:length_VSD) = 0;
end

%DR

if strcmp(settings.DR.status,'On')
    [A_dr,b_dr] = minimum_power_setting(pumps,data.Power,settings.OF_spec,...
        N,data.nn,data.length_lambda,settings.DR.desired_share,data.loc_Pumps);
    
    
    if strcmp(pump_approx,'Pump_convex_VSD2')
        A_dr(:,length_normal+1:length_VSD) = 0;
    end
    
    if isfield(settings.DR,'flex') && strcmp(settings.DR.flex,'Yes')
        b_dr(Pe == max(Pe)) = 0;
    end
    
    A = [cat(1,A_mass,A_cell{1:end},A_dr)];
    b = [cat(1,b_mass,b_cell{1:end},b_dr)];
elseif strcmp(settings.DR.status,'Optimal')
    % introduce extra variable
    settings.DR.desired_share = 0; %over ride any inputs as share is calculated
    [A_dr,b_dr] = minimum_power_setting(pumps,data.Power,settings.OF_spec,...
        N,data.nn,data.length_lambda,settings.DR.desired_share,data.loc_Pumps);
    
    if strcmp(pump_approx,'Pump_convex_VSD2') % Add zeros for binary switches
        A_dr(:,length_normal+1:length_VSD) = 0;
    end
    
    %formula is
    %-T*P +D*P <= 0  for all time steps
    % add D, the demand response level
    A = [cat(1,A_mass,A_cell{1:end},A_dr)];
    if isfield(settings.DR,'flex') && strcmp(settings.DR.flex,'Yes')
        % exclude peak prices
        D_tmp = ones(N,1);
        D_tmp(Pe == max(Pe)) = 0;
        D = [zeros(size(A,1)-N,1);D_tmp];
    elseif isfield(settings.DR,'flex') && strcmp(settings.DR.flex,'Windows')
        window_av = [7.0000   13.5000   16.5000   21.0000];
        window_av = window_av*N/24;
        D_tmp = zeros(N,1);
        D_tmp(window_av(1):window_av(2)) = 1;
        D_tmp(window_av(3):window_av(4)) = 1;
        D = [zeros(size(A,1)-N,1);D_tmp];
    elseif isfield(settings.DR,'flex') && strcmp(settings.DR.flex,'Window1')
        window_av = [7.0000   13.5000   16.5000   21.0000];
        window_av = window_av*N/24;
        D_tmp = zeros(N,1);
        D_tmp(window_av(1):window_av(2)) = 1;
        D = [zeros(size(A,1)-N,1);D_tmp];
    else %continously
        D = [zeros(size(A,1)-N,1);ones(N,1)];
    end
    
    A = horzcat(A,D);
    b = [cat(1,b_mass,b_cell{1:end},b_dr)];
else
    A = [cat(1,A_mass,A_cell{1:end})];
    b = [cat(1,b_mass,b_cell{1:end})];
end
%% concatinate
% A = [cat(1,A_mass,A_cell{1:end},A_dr)];
% b = [cat(1,b_mass,b_cell{1:end},b_dr)];

% A = [cat(1,A_mass,A_cell{1:end})];
% b = [cat(1,b_mass,b_cell{1:end})];


%% Objective
% Objective Function (min 0.5x'Hx + f'x)

% electricty price for all N adds up for all
% linear cost part

% for those with only one T allowed:
make_OF;

%% NLP??
if exist('HW_form','var')
    
    % constraints
    nlcon_text = ['nlcon = @(x)  [ ' strjoin(HW_form) '];'];
    fileID = fopen('nlcon_m.m','w');
    fprintf(fileID,'%s\n' ,nlcon_text')
    fclose(fileID);
    nlcon_m
    
    nlrhs = vertcat(nlrhs_cell{:});
    nle = zeros(length(nlrhs),1);
    ndec = length(f);
    
    
    % Objective function %for horizontal x
    OF_fun = @(x) sum(x'.*f); %+ 0.5*sum(x'*H*x);% 0.5*sum(x*H*x');
end


%% Manipulations and tricks
% to get it accepted in opti
if exist('Q_pump','var') &&  exist('Q_pipe','var') % both
    Q_pipe = reshape(Q_pipe,[prod([size(Q_pipe)]) 1]);
    l_pipe = reshape(l_pipe,[prod([size(l_pipe)]) 1]);
    r_pipe = reshape(r_pipe,[prod([size(r_pipe)]) 1]);
    
    Q_pump = reshape(Q_pump,[prod([size(Q_pump)]) 1]);
    l_pump = reshape(l_pump,[prod([size(l_pump)]) 1]);
    r_pump = reshape(r_pump,[prod([size(r_pump)]) 1]);
    
    Q = [Q_pipe; Q_pump];
    l = [l_pipe; l_pump];
    r = [r_pipe; r_pump];
elseif exist('Q_pipe','var') %for pipes
    Q = reshape(Q_pipe,[prod([size(Q_pipe)]) 1]);
    l = reshape(l_pipe,[prod([size(l_pipe)]) 1]);
    r = reshape(r_pipe,[prod([size(r_pipe)]) 1]);
    
elseif exist('Q_pump','var') %for pumps
    Q = reshape(Q_pump,[prod([size(Q_pump)]) 1]);
    l = reshape(l_pump,[prod([size(l_pump)]) 1]);
    r = reshape(r_pump,[prod([size(r_pump)]) 1]);
end

% remove empty arrays to avoid mistakes
if exist('Q','var')
    Q = Q(~cellfun('isempty',Q));
    l = l(~cellfun('isempty',l));
    r = r(~cellfun('isempty',r));
end
% flatten linear stuff
if exist('Aeq_pipes','var')
    for i = 1:size(Aeq_pipes,3);
        Aeq = [Aeq; Aeq_pipes(:,:,i)];
        beq = [beq; beq_pipes(:,:,i)];
    end
end

%% bounds
% lb
% ub
% % workaround for now
% if strcmp(def,'Richmond_skeleton_modified') && approx_number == 1 || ...
%      strcmp(def,'Richmond_skeleton_modified') && approx_number == 12
%     h_min = [h_min(1:34) - 500 1 ...
%         ([0     0     0     0     0     0]-500 )];
%     h_max = [h_max(1:34) + 500 1 ...
%         ([2.0000    3.3700    2.1100    3.6500    2.6900    2.1900]+500) ] ;
%     q_lim = q_lim*2;
% elseif strcmp(def,'Richmond_skeleton3') %&& approx_number == 3 ...
% %     || strcmp(def,'Richmond_skeleton3') &&  approx_number == 2
%     h_min = [[h_min(1:34)-50] h_min(35:end)];
%     h_max = [[h_max(1:34)+100] h_max(35:end)] ;
% %     q_lim = q_lim*2;
% % else
% %     h_min(h_min < 0 ) = 0;
% end

% h_min(h_min < 0 ) = 0;

% DR bounds for tanks
data.h_min = place_matrix(ones(1,N),data.h_min,1);
if ~strcmp(settings.DR.status,'Off')
%     data.h_min = gen_min_tankfill(def,data.npu,tanks,settings,nodesIdListMap,data.h_min,data,pipes,pumps,reservoirs,d);
%     h_min = gen_min_tankfill(tanks,settings,nodesIdListMap,data,pipes,pumps,reservoirs,d)    ;
      [H_24, ~, ~] = EPS_from_T0(zeros(1,data.npu),1,data,pipes,pumps,tanks,reservoirs,d,nodesIdListMap,zeros(1,data.npu));
demand_factor = sum(d)/mean(sum(d));
for i = 1:length(tanks)    
    h_min(nodesIdListMap(tanks{i}.Id),:) = -H_24(nodesIdListMap(tanks{i}.Id))...
                                            *settings.DR.rest_period/(24*1440)*demand_factor; 
                                         % change in tank level in one minute
                                         % with average demand out flow. 
end
    for i = 1:length(tanks)
        tank_minimums(i,:) = data.h_min(nodesIdListMap(tanks{i}.Id),:);
    end
elseif strcmp(settings.mod_schedule.status,'Yes')
    data.h_min =gen_min_tankfill(def,data.npu,tanks,settings,nodesIdListMap,data.h_min,data,pipes,pumps,reservoirs,d);
    rec_end = settings.DR.recovery/(24*60/N)+size(settings.mod_schedule.sched,1)+settings.mod_schedule.index -1;
    start_DR = settings.mod_schedule.index;
    for i = 1:length(tanks)
        data.h_min(nodesIdListMap(tanks{i}.Id),start_DR:rec_end) = 0;
    end
    
    %%Window bits still need to go here (remove mimumum if not in window)
end
data.h_min = singlefile(data.h_min);

% q_lim =  q_lim*5;


lb = [ repmat(T_bounds(1),1,N*data.npu) data.h_min...
    place_matrix(ones(1,N),-fields_rm([pipes pumps],'q_lim'),2) repmat(lambda_bounds(1),1,N*data.length_lambda) ];
ub = [ repmat(T_bounds(2),1,N*data.npu) place_matrix(ones(1,N),data.h_max,2) ...
    place_matrix(ones(1,N),fields_rm([pipes pumps],'q_lim'),2) repmat(lambda_bounds(2),1,N*data.length_lambda) ];

% Integer Constraints
pumps_int(1:N*data.npu)  = 'I'; % could be B for binary or I for intiger
lambda_int(1:N*data.length_lambda) = 'I';
cont(1:N*data.nn + N*data.np) = 'C';
xtype = strcat([pumps_int cont lambda_int]);
disp(['Number of intigers: ' num2str(N*data.npu+N*data.length_lambda)])

if strcmp(pump_approx,'Pump_convex_VSD2')
    pumps_vsd(1:N*data.npu)  = 'C'; %VSD pumps
    xtype = strcat([pumps_vsd cont lambda_int pumps_int]);
    
    Aeq = [Aeq zeros(size(Aeq,1),N*data.npu)];
    lb = [lb zeros(1,N*data.npu)];
    ub = [ub  ones(1,N*data.npu)];
end

%add optimality variable
if strcmp(settings.DR.status,'Optimal')
    xtype = strcat([xtype 'C']);
    lb = [lb 0];
    ub = [ub abs(data.Power.total(1))];
    
    Aeq = [Aeq zeros(size(Aeq,1),1)];
end

%if VSD remove I constraints for T
if strcmp(pump_approx,'Pump_convex_VSD2')
    xtype(1:N*data.npu) = 'C';
end

if strcmp(settings.mod_schedule.status,'Yes')
    hind_sched = settings.mod_schedule.sched;
    hind_index_t = settings.mod_schedule.index;
    [l_h_s w_h_s] = size(hind_sched);
    l_Aeq =  size(Aeq,2);
    for ih = 1:w_h_s
        hind_index = hind_index_t + N*(ih-1);
        Aeq = [Aeq; zeros(l_h_s,hind_index-1) eye(l_h_s) zeros(l_h_s,l_Aeq -l_h_s - hind_index+1)];
        beq = [beq; hind_sched(:,ih)];
    end
end

% ensure Aeq is sparse!
Aeq = sparse(Aeq);

if isfield(settings.Robust,'Wind') && strcmp(settings.Robust.Wind,'Wind') && strcmp(settings.Robust.status,'No')
    j = 1;
    wind_power_script;
end

if isfield(settings,'Robust') && strcmp(settings.Robust.status,'Yes')
    % robust problem
    % robust pricing
    % increase the size of the problem
    
    for j = 1:length(settings.Robust.probability)
        
        if isfield(settings.Robust,'Wind') && strcmp(settings.Robust.Wind,'Wind')
            wind_power_script
        else
            % Objective function
            Pe =  pattern_length_adjustment(settings.Robust.cost{j},N);
            make_OF
        end
        temp_f{j} = f;
        temp_H{j} = H;
        
        
        temp_A{j} = A;
        temp_b{j} = b;
        temp_Aeq{j} = Aeq;
        temp_beq{j} = beq;
        temp_lb{j} = lb;
        temp_ub{j} = ub;
        temp_xtype{j} =  xtype;
        

        
        
        
    end
    data.var_number = size(Aeq,2);
    
    A = blkdiag(temp_A{:});
    Aeq = blkdiag(temp_Aeq{:});
    H = blkdiag(temp_H{:});
    beq = full(vertcat(temp_beq{:}));
    b = vertcat(temp_b{:});
    ub = horzcat(temp_ub{:});
    lb = horzcat(temp_lb{:});
    f = [];
    for kk = 1:length(settings.Robust.probability)
        f = [f temp_f{kk}*settings.Robust.probability(kk) ];
    end
    xtype = horzcat(temp_xtype{:});
    
    % make all first time steps equal
    T_link_all = [];
    for i = 1:N:data.npu*N
        T_link = zeros(length(settings.Robust.probability)-1,length(lb));
        for j = 1:length(settings.Robust.probability)-1
           
            T_link(j,i) = 1;
            idx = data.var_number*j+i;
            T_link(j, idx) = -1;
        end
        T_link_all = [T_link_all; T_link];
    end
    T_link_all = sparse(T_link_all);
    b_link_all = zeros((length(settings.Robust.probability)-1)*data.npu,1);
    Aeq = [Aeq ; T_link_all];
    beq = [beq;b_link_all];
    
end


%% make problem structure
problem.Aineq = A;
problem.Aeq = Aeq;
problem.bineq = b;
problem.beq = beq;
problem.lb = lb;
problem.ub = ub;
problem.ctype = xtype;
problem.f = f;
% QPs
if exist('H','var')
    problem.H = H;
else
    problem.H = [];
end

% never used features:
problem.sos =[];
problem.sos.ind  =[];
problem.sos.wt 	=[];

if exist('Q','var')
    % MIQPCP --> call Scip, as no other solvers for this are implimented
    problem.Q = Q; % not actually used
    problem.l = l;
    problem.r = r;
    settings.solver = 'SCIP';
end

if isfield(settings,'solver_gap')
    problem.gap = settings.solver_gap;
else
    problem.gap = 0.05;
end


