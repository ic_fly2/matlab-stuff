% Water deficit at initial filling for times and reservoirs
clearvars;

% def = 'van_zyl';
% def = 'Richmond_skeleton';
def = 'Lecture_example';
% def = 'Purton' % no control yet


prepNetwork;
prepGGA;
N = 48;
Electricity_Price(1:11) = 28.59;
Electricity_Price(12:19) = 85.76;
Electricity_Price(13:33) = 48.49;
Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49; 
Electricity_Price(45:48) =  28.59;
Pe = zeros(1,N);
for ii = 1:N;
    Pe(ii) = sum(Electricity_Price((ii-1)*48/N+(1:48/N)));
end
Pe = Pe/ (48/N);

% T0 = [];
% T = zeros(1,length(pumps));
T0 = zeros(N,3);

N = size(qall,2);
H_save = zeros(nj+no,N);
Q_save = zeros(length([pipes])+ length(n_pumps),N);
H_res = zeros(length(Allreservoirs),1);

nt = length(tanks);

for i = 1:nt
    Area(i) = (pi/4)*tanks{i}.Diameter^2;
    tanks{i}.desired_level = tanks{i}.Init; % the level the volume deficit is calulated too. 
end

possible_schedules = [0 0 0;eye(3)]; % each line represents apossibel schedule (manual atm)

tt = 1; % outest loop
Energy_deficit = zeros(N+1-tt,nt);
for v = 1: size(possible_schedules,1);
    
    tanks{i}.Init = tanks{i}.desired_level;
    T0(1,:) = possible_schedules(v,:) ;
    [pumpsOn,~] = selectPumps(pumps,n_pumps,T0(1,:));
    Pump_cost(v) = 0;
    
    for i = 1:length(pumpsOn)
        Pump_cost(v) = Pump_cost(v)+ pumpsOn{i}.power*Pe(tt)*(24/N)/1000; % power in kW 
    end
    
    % call EPS
    for t = tt:N
        T = T0(t,:);
        %     eval([def '_control']);
        EPS_step;
        %     T0 = [T0;T];
        for i = 1:nt
            if tanks{i}.Init < tanks{i}.Min && t == tt %make it infeasible
                Energy_deficit(t,i) = -Inf;
            elseif tanks{i}.Init > tanks{i}.Max %&& t == tt %make it infeasible
                Energy_deficit(t,i) = -Inf;
            elseif t == N  % last time step
                Energy_deficit(t,i) = (tanks{i}.Init - tanks{i}.desired_level)*Area(i); % Volume
            else
                Energy_deficit(t,i) = (tanks{i}.Init - tanks{i}.Min)*Area(i); % Volume
            end
            Energy_deficit(t,i) = Energy_deficit(t,i)*tanks{i}.Elev*9.81*1000; %Energy J
            if tanks{i}.Init < tanks{i}.Min
                tanks{i}.Init = tanks{i}.Min;
            end
        end
        
        
    end
    
    %% calculate cost of schedule:
    Energy_cost(v) = sum(Pe(tt:N) * Energy_deficit*1/0.75 )/(3600*1000000); %Pounds (75% efficiency)
    
   
end

Total_cost = Pump_cost-Energy_cost;


