
if ~isnan(x) % success
    
    x0 = x;
    T0 = x(1:length(xtype));
    if strcmp(settings.DR.status,'Optimal')
        DR_D = x(end);
        T0 = x(1:length(xtype)-1);
        x = x(1:length(xtype)-1);
    end
    
    
    %     savename = [def '_N' num2str(N) '_d_multi' num2str(demand_multiplier) '_' pump_approx '_' pipe_approx ];
    %     save(savename,'x0','T0')
    solver_output = reshape(x,[N length(x)/N ]); %Make it pretty
    % pump settings
    schedule_tmp = zeros(N,data.npu);
    for i= 1:data.npu % get the schedule data from the solver
        schedule_tmp(:,i) =  solver_output(:,i);
    end
    if exist('live','var') && live == 1;
        schedule_tmp = round(schedule_tmp); %avoid mistakes from near roundings
        for ii = 1:length(data.pumps_vec)
            for i= n_pumps(ii)-1:-1:1; % make schedule data filled with ones at lower pump settings
                schedule_tmp(:,i) = schedule_tmp(:,i) + schedule_tmp(:,1+i);
            end
        end
        schedule_tmp(schedule_tmp >= 1) = 1; %Set everything back to 1
    end
    
    % node head:
    head_display = solver_output(:,data.npu+(1:data.nn));
    % pipe flow:
    flow_display =  solver_output(:,data.npu+data.nn+(1:data.np));
    %lambda
    lambda_display =  solver_output(:,(data.npu+data.nn+data.np+1):size(solver_output,2));
    
    % display
    %     disp('Node heads (m):');
    %     disp(head_display );
    %
    %     disp('Flows (m3/s):')
    %     disp(flow_display);
    %
    %     disp('Pump settings:');
    %     disp(round(schedule_tmp));
    %
    %     disp(['Total cost: ' num2str(fval)]);
    %     disp(['Total time: ' num2str( time_taken)]);
    
    %% make a pretty cell
    C{1,1} = 'Price';
    for ii = 1:N
        C{ii+1,1} = Pe(ii);
    end
    for i = 1:data.npu
        C{1,i+1} = pumps{i}.Id ;
        for ii = 1:N
            C{ii+1,i+1} = schedule_tmp(ii,i);
        end
    end
    for i = 1:length(tanks)
        C{1,data.npu+1+i} = [tanks{i}.Id ' (m)'];
        for ii = 1:N
            C{ii+1,data.npu+1+i} = head_display(ii,nodesIdListMap(tanks{i}.Id));
        end
    end
    disp(C)
    
else
    disp(['      Exitflag: ' num2str(exitflag)])
    %     disp(info)
end