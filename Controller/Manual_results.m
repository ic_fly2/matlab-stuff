%% Results
% for manual entry and documentation of results:

%% Lecture Example
%% convex convex M = 999 
% N     6       12  24  48
time =[257.864 ;
        ;
        ;       ];
Integers = [54 ];
cost = [2.3098];


%% Van Zyl
%% quad quad M = 999
% N     6       12  24  48
time =[257.864 ;
        ;
        ;       ];
Integers = [54 ];
cost = [2.3098];

%% convex convex M = 110 QP
% N     6     12   24     48
time =[0.505 2.174 56.91 12316.18;
       0.4917 2.296 60.32 12316.18
       0.496  2.473 58.70 12316.18];
Integers = [54 108 216 270 432];
av_time = [0.505 2.174 56.91 63.788 12316.18 ]; 
cost = [2.5129 2.6004 2.5809 2.4815];


%% convex piece M = 110
% N     6       12  24  48
time =[2.590 ;
       2.321 ;
       2.563;       ];
Integers = [114 ];
cost = [3.016];


2.3098