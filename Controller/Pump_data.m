% pump data collection
% from EPAnet and Infoworks systes
% Pump name         Ka          Kb      Kc          Comments
% pump curve 1      -1100       45      99.4346		Van Zyl main
% Pump curve 6      -4900       -66.2	120.6767	Van Zyl booster
% Purton high lift	-420.33     94.29	236.82		Purton pumps
% Etaline 125-160	-225.27     21.423	32.948		KSB test pump
% 200-50            -416.7      0       66.67		Simple curve

