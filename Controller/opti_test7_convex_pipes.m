%% general declarations
clear all;
N = 6;
% if ~exist('N','var'); clear all; N =12; end % number of intervals;
Delta_T = 24*60*60/N; % duratiion of one interval in seconds

M = 59; %1000000; % sufficiently large number
plotting_switch = 'off'; % on or off;
pump_number = 1; % counter to know which pump station work is being done on
pipe_number = 1; % counter to inform with pipe we are working on
pipe_quad_number = 1; % counter for which quadratic pipe you are on. Interim fix. 
demand_counter = 1; % counter for demands to run though
A = [];
b = [];
Aeq = [];
beq = [];

%% Network specific details 
% comment out the non needed ones
if ~exist('def','var')
def = 'def_van_zyl_network';
% def = 'def_simple_loop'; 

%     def = 'def_simple_network5';
end
eval(def); 

%% new method
% define connections (from epanet)

% some system checks to avoid chasing ghosts

% need making: 
% && sum(strcmp(connection_type,'demand')) ==  length(n_pumps)


n_piece_pipes = length(strmatch('pipe_piece', char(connection_type))); % number of pipes (needed for lambda)
%driver_pipes(place_matrix(1,strmatch('pipe', char(connection_type)),1)) = 1; % driver for
loc_pipes  = strmatch('pipe', char(connection_type)); 
n_valves = length(strmatch('check_valve', char(connection_type)));
n_pipes = length(loc_pipes)  ; 

length_m_part = [];
for i = 1:n_pipes  ;
[pipe(i).m_data , pipe(i).C_data ,pipe(i).Q_lim_data, P1 , P2, P3]  = PieceWiseApproxGen(pipedata(i).data(1),pipedata(i).data(2),pipedata(i).data(3),pipedata(i).data(4),OP_flow_pipe(i,:),pipedata(i).data(5),plotting_switch);
length_m_part(i) = length(pipe(i).m_data);

%Linear approx at OP
m_linear_pipe(i,:) = P1(:,1)';
c_linear_pipe(i,:) = P1(:,2)';


%quadratic approx
a_quad_pipe(i) = P2(1);
b_quad_pipe(i) = P2(2);

%cubic approx
a_cube(i) = P3(1); 
b_cube(i) = P3(3);

end

% Elevation for pipes_piece and pipes_simple
%add elevation term 
change =  connection_matrix*Elevation'; %elevation change between nodes
change_pipes = change(loc_pipes); %elevation change between nodes connecting pipes
for i = 1:size(c_linear_pipe,1)
c_linear_pipe(i,:) = c_linear_pipe(i,:) + change_pipes(i);
pipe(i).C_data = pipe(i).C_data         + change_pipes(i);
end
c_quad_pipe = change_pipes; 

%assume pumps are the first things listed!
% Elevation change for pumps:
j = 1;
for i = 1:size(c_linear_pump,1)
c_linear_pump(i,:) = c_linear_pump(i,:) - change(j);
j = j + n_pumps(i);
end
j = 1;
for i = 1:length(n_pumps)
c_quad_pump(i,:) = c_quad_pump(i,:) - change(j); 
j = j + n_pumps(i);
end

% clean from NaN (ignore elevation)
m_linear_pipe(isnan(m_linear_pipe)) = M*0.1; %near vertical
c_linear_pipe(isnan(c_linear_pipe)) = 0; %no flow 

length_m_part = (length_m_part-2)*2 + 1; % I hope this is the correct final size of the m vector
n_conv_pipes = length(strmatch('pipe_convex', char(connection_type))); % convex pipes 
% length_m = sum(length_m_part)+n_conv_pipes*2; % needed to make lambda matrix of the correct size

pipes_and_valves = [strmatch('pipe', char(connection_type)); strmatch('check', char(connection_type))  ];
pipes_and_valves =  connection_type(pipes_and_valves);
length_lambda =0;
lambda_parts_length = [];
for i = 1:n_pipes + n_valves
    if sum(strmatch('pipe_piece', char( pipes_and_valves)) == i) == 1
        length_lambda = length_lambda + length_m_part(i);
        lambda_parts_length =  [lambda_parts_length length_m_part(i)];
    elseif sum(strmatch('pipe_convex', char( pipes_and_valves)) == i) == 1
        length_lambda = length_lambda + 1;
        lambda_parts_length =  [lambda_parts_length 1];
    elseif sum(strmatch('pipe_quad', char( pipes_and_valves)) == i) == 1
        length_lambda = length_lambda + 1;
        lambda_parts_length =  [lambda_parts_length 1];
   elseif sum(strmatch('pipe_linear', char( pipes_and_valves)) == i) == 1
        % nothing :)
   elseif sum(strmatch('check_valve', char(pipes_and_valves)) == i) == 1
        length_lambda = length_lambda + 1;
        lambda_parts_length =  [lambda_parts_length 1];
    end 
end

if length(location_fix) == length(fixed_val)
    %some thing nice could go here such as disp('well done')
else
    error('Inconsistent system defenition: location and values of vixed locations don''t match')
end

%add original state of reservoir and minimum fill height at end
%% Inequality matrix
[np, nn] = size(connection_matrix); % np ~= n_pipes as np is number of conncetions and n_pipes is just pipes
flow_matrix = eye(np); % assuming it is always the identety then this would work 
for i = 1:np
    A_part = []; % empty that one 
    b_part = [];
    switch connection_type{i}
        case 'Pump_convex' %
            pump_convex_def;
        case 'Pump_simple'
            pump_simple_def;
        case 'Pump_linear' 
            pump_linear_def;
        case 'Pump_quad'
            pump_quad_def;
        case 'pipe_convex' %(use less intigers)
            pipe_convex_def;
        case 'pipe_piece'  
            pipe_piece_def;
        case 'pipe_quad'
            pipe_quad_def;
        case 'pipe_cube'
            %todo
        case 'pipe_linear'
            pipe_linear_def
        case 'demand'
            driver = zeros(1,np); driver(i) = 1;
            Aeq = [Aeq; zeros(N,N*sum(n_pumps)) zeros(N,N*nn) place_matrix(eye(N),driver,2)  zeros(N,N*length_lambda) ];
            beq = [beq; d(demand_counter,:)'];
            demand_counter = demand_counter + 1;
            disp('Demand registered')
        case 'check_valve'
            % k = 0 so head in = head out if head in > head out else q = 0
            % need to take care of elevation change
            % 0 <= q <= M*lambda
            % q <= M*lambda
            % -q <= 0 
            T =  [zeros(N,N*sum(n_pumps));...
                  zeros(N,N*sum(n_pumps))];
            h = [zeros(N,size(connection_matrix,2)*N);...
                 zeros(N,size(connection_matrix,2)*N)];
            q = [place_matrix( eye(N),flow_matrix(i,:),2);...
                 place_matrix(-eye(N),flow_matrix(i,:),2)];
            lambda = [];
            for k = 1:n_pipes +n_valves %lambda sizer and driver
                if k == pipe_number
                    lambda_part = [-M*eye(N)]; % place_matrix(M*eye(N*length(m)),ones(4,1),1);
                else
                    lambda_part =  zeros(N,lambda_parts_length(k)*N); % make zeros where ever it isn't needed
                end
                lambda = [lambda lambda_part ]; % add it all together
            end 
            lambda1 = [lambda;zeros(N,N*length_lambda)] ;
            A = [A; T h q lambda1]; 
            b = [b;zeros(2*N,1)]; 
            
            % h1 - h2 = change (in elevation) if lambda is positive
            % M(lambda - 1) <= h1 - h2 - change <= M(1 - lambda)
            % -h1 +h2 + M*lambda <= M + change
            %  h1 -h2 + M*lambda <= M - change
            
            T =  zeros(2*N,N*sum(n_pumps));
            h = [place_matrix(eye(N), connection_matrix(i,:),2);...
                 place_matrix(eye(N),-connection_matrix(i,:),2)];
            q =  zeros(2*N,size(flow_matrix,2)*N);
            lambda = [-lambda; ...
                      -lambda];
                 
            A = [A; T h q lambda];
            
            b = [b;
                 ones(N,1)*(M + change(i));...
                 ones(N,1)*(M - change(i))];             

        case 'valve'
            disp('Valves not implimented yet') 
        otherwise
            error(['Unknown system definition in connection ' num2str(i) '. Unknown type: ' connection_type{i}])
    end
end  




%% equality matrix
% mass balance for the nodes
T = zeros(N*nn,N*sum(n_pumps)); 
h = zeros(N*nn,N*nn);
%q
q_part = [];
q = []; 
for i = 1:nn 
    %      in(i,:) = connection_matrix(:,i) == 1;
    %      out(i,:)= connection_matrix(:,i) ==-1;
    flows = transpose(connection_matrix); % same as in out above just faster 
    if i == location_fix
        q_part = zeros(N,N*np);
    elseif 1 == sum(i == location_res) %need to change h as well (h(T-1) - h(T))/*Area/Delta_T + q - d = 0
        [ ~,Reservoir_number] = max(i == location_res);
        Area = Reservoir_area(Reservoir_number);
        h((i-1)*N+1:i*N,(i-1)*N+1:i*N) = -eye(N)*Area/Delta_T  + diag(ones(1,N-1),-1)*Area/Delta_T; % mass balance term for reservoirs
        h((i-1)*N+1,i*N) = Area/Delta_T; %add continous part (h(N)-h(1))/*Area/Delta_T + q - d = 0
        q_part = place_matrix(eye(N),flows(i,:),2);
    else
        q_part = place_matrix(eye(N),flows(i,:),2);
    end
    q = [q; q_part]; 
end  
lambda = zeros(N*nn,N*length_lambda);
% move reocuring summation from equality to inequlaity:
insert_A =  [T h q lambda];
A = [A; -(insert_A((location_res-1)*N+1,:))]; 
b = [b; zeros(length(location_res),1)];

insert_A((location_res-1)*N+1,:) = [];
Aeq = [Aeq;insert_A];
beq = [beq; zeros(nn*N-length(location_res),1)]; %all zero as all mass balances (one less as insert is shorter)

% Add section to fix locations of fixed head
for i = length(location_fix)
    driver = zeros(1,nn); driver(i) = 1; 
    Aeq = [Aeq; zeros(N,N*sum(n_pumps)) place_matrix(eye(N),driver,2) zeros(N,N*np) zeros(N,N*length_lambda) ];
    beq = [beq; fixed_val(i)*ones(N,1)]; % value *  everywhere
end

% Initial tank status and continuety:
if exist('Initial_filling','var')
%     if length(Initial_filling) ~= length(location_res)
%         error('Tank defintion is inclompete! Check definition of initial filling, tank locations and number of nodes.')
    if isempty(Initial_filling)
        Initial_filling = (h_min(location_res) +  h_max(location_res))/2;
    else
        % All good
    end
    
    driver = zeros(1,nn); driver(location_res) = 1; driver = diag(driver);
    driver(sum(driver,2) == 0,:) = [];
    % h
    h = [];
    for i = 1:length(location_res)
        h = [h; place_matrix([1 zeros(1,N-1)],driver(i,:),2)];
    end
    Aeq = [Aeq; zeros(length(location_res),n_pumps*N)  h zeros(length(location_res),N*np)  zeros(length(location_res),N*length_lambda)];
    beq = [beq;Initial_filling'];
else    
end

%% Objective
% Objective Function (min 0.5x'Hx + f'x)

% electricty price for all N adds up for all
% linear cost part

% for those with only one T allowed: % Do not delete
f = []; % pricing for pump operations 
for i = 1:length(n_pumps)
     driver = 1:n_pumps(i);
     cost = place_matrix(Power_nameplate(i)*Pe*Delta_T/3600,driver,2); 
     f = [f cost]; 
end

% for those with all T's filled % Do not delete
% driver = [];
% f = []; % pricing for pump operations 
% for i = 1:length(n_pumps)
%      driver{i} = ones(1,n_pumps(i)); 
%      cost = place_matrix(Power_nameplate*Pe*Delta_T/3600,driver{i},2); % possibly vary Pe if it is defferent for different pumps
%      f = [f cost]; 
% end
% now the rest is zeros
% switch 'OF'
% case 'SumT'
    % sum of Ts
f = [f zeros(1,N*nn + N*np + N*length_lambda)];
% H % assume cost of switching are the same for every pump
H = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
% zero every where else (rewrite this in elegant)
H(N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda,N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda ) = 0; 
H = H*switch_penalty;
%     case 'LinearOF'
            % sum of Ts + mq
% f = [f zeros(1,N*nn)  m_cost*ones(1,N*np)  (1,N*length_lambda)];
% % H % assume cost of switching are the same for every pump
% H = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
% % zero every where else (rewrite this in elegant)
% H(N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda,N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda ) = 0; 
% H = H*switch_penalty;
%     case 'quadOF'
%     case 'NonInt'
%     f = f*999;
    
% end

% % H % assume cost of switching are the same for every pump
% H = 2*eye(N*sum(n_pumps)) + diag(-ones(1,N*sum(n_pumps)-1),1) + diag(-ones(1,N*sum(n_pumps)-1),-1);
% % zero every where else (rewrite this in elegant)
% H(N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda,N*sum(n_pumps)+1:N*sum(n_pumps)+N*nn + N*np +N*length_lambda ) = 0; 
% H = H*switch_penalty;


%% Manipulations and tricks
% to get it accepted in opti
if exist('Q_pump','var') &&  exist('Q_pipe','var') % both
    Q_pipe = reshape(Q_pipe,[prod([size(Q_pipe)]) 1]);
    l_pipe = reshape(l_pipe,[prod([size(l_pipe)]) 1]);
    r_pipe = reshape(r_pipe,[prod([size(r_pipe)]) 1]);
    
    Q_pump = reshape(Q_pump,[prod([size(Q_pump)]) 1]);
    l_pump = reshape(l_pump,[prod([size(l_pump)]) 1]);
    r_pump = reshape(r_pump,[prod([size(r_pump)]) 1]);
    
    Q = [Q_pipe; Q_pump];
    l = [l_pipe; l_pump];
    r = [r_pipe; r_pump];
elseif exist('Q_pipe','var') %for pipes
    Q = reshape(Q_pipe,[prod([size(Q_pipe)]) 1]);
    l = reshape(l_pipe,[prod([size(l_pipe)]) 1]);
    r = reshape(r_pipe,[prod([size(r_pipe)]) 1]);
    
elseif exist('Q_pump','var') %for pumps
    Q = reshape(Q_pump,[prod([size(Q_pump)]) 1]);
    l = reshape(l_pump,[prod([size(l_pump)]) 1]);
    r = reshape(r_pump,[prod([size(r_pump)]) 1]);
    
else %do nothing
end
% remove empty arrays to avoid mistakes
if exist('Q','var')
    Q = Q(~cellfun('isempty',Q)); 
    l = l(~cellfun('isempty',l));
    r = r(~cellfun('isempty',r));
end
%% bounds
% lb
% ub

lb = [ repmat(T_bounds(1),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_min,2) repmat(q_bounds(1),1,N*np) repmat(lambda_bounds(1),1,N*length_lambda) ];
ub = [ repmat(T_bounds(2),1,N*sum(n_pumps)) place_matrix(ones(1,N),h_max,2) repmat(q_bounds(2),1,N*np) repmat(lambda_bounds(2),1,N*length_lambda) ];

% Integer Constraints
pumps_int(1:N*sum(n_pumps))  = 'I'; % could be B for binary or I for intiger
lambda_int(1:N*length_lambda) = 'I';
cont(1:N*nn + N*np) = 'C';
xtype = strcat([pumps_int cont lambda_int])   ;
% xtype = 'II';               %x1 & x2 are Integer

%% solve
% Create OPTI Object 
% hydraulic_solver % crashes the solver

% make the rest sparse (those that can ;) )
A =  sparse(A); 
Aeq =  sparse(Aeq);

if exist('Q','var')
%     load('x0')
    opts = optiset('solver','SCIP','maxnodes', 1000000000,'maxtime',216000,'display','iter','tolrfun',1e-06,'tolafun',1e-06);
%     Opt = opti('qp',H,f,'qc',Q,l,r,'xtype',xtype,'eq',Aeq,beq,'ineq',A,b,'lb',lb,'ub',ub,'x0',x0,'options',opts);
Opt = opti('qp',H,f,'qc',Q,l,r,'xtype',xtype,'eq',Aeq,beq,'ineq',A,b,'lb',lb,'ub',ub,'options',opts);    
tic;
    [x,fval,exitflag,info] = solve(Opt); 
    toc;
else
%     opts = optiset('solver','SCIP');
%     Opt = opti('qp',H,f,'ineq',A,b,'eq',Aeq,beq,'lb',lb,'ub',ub,'xtype',xtype,'options',opts);
    % no 64bit support atm
        tic
    [x2,fval2,exitflag2,info2]  = cplexmilp(f,A,b,Aeq,beq,[],[],[],lb,ub,xtype,[],[]);
    toc
    tic
     [x,fval,exitflag,info]  = cplexmiqp(H,f,A,b,Aeq,beq,[],[],[],lb,ub,xtype,[],[]);
     toc
     
info.time
info2.time
fval
fval2
error

end
%% display
if sum(x) ~= 0 % success
   data = reshape(x,[N length(x)/N ]); %Make it pretty
    % pump settings
    schedule_tmp = zeros(N,sum(n_pumps));
    for i= 1:sum(n_pumps) % get the schedule data from the solver
        schedule_tmp(:,i) = data(:,i);
    end
    if exist('live','var') && live == 1; 
    schedule_tmp = round(schedule_tmp); %avoid mistakes from near roundings
    for i= sum(n_pumps)-1:-1:1; % make schedule data filled with ones at lower pump settings
        schedule_tmp(:,i) = schedule_tmp(:,i) + schedule_tmp(:,1+i); 
    end
    schedule_tmp(schedule_tmp >= 1) = 1; %Set everything back to 1
    end
    
    % node head:
    head_display = data(:,sum(n_pumps)+(1:nn));
    % pipe flow:
    flow_display = data(:,sum(n_pumps)+nn+(1:np));
    %lambda
    lambda_display = data(:,sum(n_pumps)+nn+np:end);
    
    % display
    disp('Node heads (m):');
    disp(head_display );
    
    disp('Flows (m3/s):')
    disp(flow_display);
    
    disp('Pump settings:');
    disp(schedule_tmp);
    
    disp(['Total cost: ' num2str(fval)]);
    
    %plot
    bar(sum(schedule_tmp,2)) %Pump settings 
    figure;
    plot(head_display(:,1:nn)) %Head
    title('Nodal heads')
    figure;
    plot(flow_display(:,1:np)) %Head
    title('Flow in connections')
    
    
    %% save to epanet usable format as external file (works but not needed, hopefully)
    Schedules_directory = 'D:\Phd\MATLAB\Phd\Schedules\';
    for i= 1:sum(n_pumps)
         schedule(i,:) = [schedule_tmp(:,i)];
        
        if exist('save_files') && save_files == 1
        schedule = [ 5; 5;  schedule_tmp(:,i)];% add lines epanet ignores using caracters that stand out
        name_current = ['pump_schedule_' num2str(i)]; % overwritten name for testing
        name_type = [ def '_N' num2str(N) '_' pump_approx '_' pipe_approx '_pump' num2str(i) ]; %name with specification of approximations used, network, N To be used for reseach
        
        
            
            % current
            save([Schedules_directory name_current '.txt'],'schedule','-ascii')
            str11 = ['COPY ' Schedules_directory name_current '.txt ' Schedules_directory name_current '.pat'];
            str12 = ['DEL ' Schedules_directory name_current '.txt'];
            status11 = dos(str11); status12 = dos(str12);
            
            % type specific:
            save([Schedules_directory name_type '.txt'],'schedule','-ascii')
            str21 = ['COPY ' Schedules_directory name_type '.txt ' Schedules_directory name_type '.pat'];
            str22 = ['DEL ' Schedules_directory name_type '.txt'];
            status21 = dos(str21); status22 = dos(str22);
        end
    end
    %% fail
else
    disp(['      Exitflag: ' num2str(exitflag)])
    disp(info)
end   