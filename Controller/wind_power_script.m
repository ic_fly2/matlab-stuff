% add cost for wind:
% Cost(i) >= (PowerPump(i) - PowerWind(i))*Tarriff(i)
% Cost(i) >= 0
% OF = sum(Cost(i))
if j == 1
   f_diag = zeros(N,length(f));
   for p = 1:length(f)
       row = mod(p,N);
       row(row == 0) = N;
       f_diag(row,p) = f(p) ;
   end
   f_diag = [f_diag -eye(N)];

   A = [A zeros(size(A,1),N) ;f_diag];
   lb= [lb zeros(1,N)];
   ub= [ub ones(1,N)*M];
   Aeq = [Aeq zeros(size(Aeq,1),N)];
   xtype(length(xtype)+[1:N]) = 'C';
   f = [zeros(1,length(f)) ones(1,N)];
   H = blkdiag(H,zeros(N));
   org_size_b = length(b);
end

wind_power = pattern_length_adjustment(settings.Robust.Windforecast{j},N).*Pe;   
b(org_size_b+ (1:N)) =  wind_power;