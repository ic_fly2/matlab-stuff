% pump power curve data stored here and added to analysis

if strcmp(def,'Richmond_skeleton_original')
    pumps{1}.powerCurve = [ -0.0031    0.2793   -2.9849]*1.0e+09; %1A
    pumps{2}.powerCurve = [-0.0028    0.2618   -2.7564]*1.0e+09; %2A
    pumps{3}.powerCurve = [-0.0003    0.2525   -2.5167]*1.0e+08; %3A
    pumps{4}.powerCurve = [-0.0028    0.3629   -1.6488]*1.0e+08; %4B
    pumps{5}.powerCurve = [ -0.4036    3.6563   -4.8549]*1.0e+08; %5C
    pumps{6}.powerCurve = [-0.0522    1.0933   -1.9816]*1.0e+08; %6D
    pumps{7}.powerCurve = [5.1817    3.2811    0.0000]*1.0e+06; %'7F'
elseif strcmp(def,'Richmond_skeleton_pump_station1')
    pumps{1}.powerCurve = [-0.0031    0.2793   -2.9849]*1.0e+09; %1A
    pumps{2}.powerCurve = [-0.0028    0.2618   -2.7564]*1.0e+09; %2A
    pumps{3}.powerCurve = [-0.0003    0.2525   -2.5167]*1.0e+08; %3A
    pumps{4}.powerCurve = [-0.0028    0.3629   -1.6488]*1.0e+08; %4B
    pumps{5}.powerCurve = [-0.4036    3.6563   -4.8549]*1.0e+08; %5C
    pumps{6}.powerCurve = [-0.0522    1.0933   -1.9816]*1.0e+08; %6D
    pumps{7}.powerCurve = [ 5.1817    3.2811    0.0000]*1.0e+06; %'7F'
elseif strcmp(def,'van_zyl')
    pumps{1}.powerCurve = [-0.002984692211547   1.026853558466777   0]*1.0e+08; %main1
    pumps{2}.powerCurve = [-0.002984692211547   1.026853558466777   0]*1.0e+08; %main2
    pumps{3}.powerCurve = [-0.009606248214971   1.440937232245604   0]*1.0e+08; %booster
else
    warning('Unknown network pump power curve, power analysis will not yield usefull information')
end 
    