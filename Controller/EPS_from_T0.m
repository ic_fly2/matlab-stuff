function [H_save, Q_save, power_sim,power_opt,Omega_stor] = EPS_from_T0(T0,N,data,pipes,pumps,tanks,reservoirs,d,nodesIdListMap,pump_flow)
% Run EPS for a given schedule and network and settings
% Uses null space method


if  nargin == 11
    flow_display = [];
    T0 = round(T0);
    disp('Only fixed speed pumps considered in hydraulic analysis')   
end

% if nargin == 2
%     N = size(qall,2);
%     tank_init = [];
%     settings.Time_shift = 0;
%     settings.DR.demand_factor = 1;
% elseif nargin ==3
%     tank_init = [];
%     settings.Time_shift = 0;
%     settings.mods.status = 'No';
%     settings.DR.demand_factor = 1;
% elseif nargin == 4
%     settings.Time_shift = 0;
%     settings.mods.status = 'No';
%     settings.DR.demand_factor = 1;
% end

% def = 'van_zyl';
% def = 'Richmond_skeleton';
% def = 'Lecture_example';
% def = 'Purton' % no control yet


% prepNetwork;
% prepGGA;

% if ~isempty(tank_init)
%     for i = 1:length(tanks)
%         tanks{i}.Init =tank_init(i);
%     end
% end

%% add pump power curve info
% pump_power_curve_data

% T = [0 0 0 0 0 0 0 ];

% for i = 1:length(tanks)
%     Allreservoirs{i+1}.Init = tank_init(i);
% end

H_save = zeros(data.nj+data.no,N);
Q_save = zeros(length([pipes])+ data.npu,N);
% H_res = zeros(length(Allreservoirs),1);


% call EPS

K_pipe = fields_rm(pipes,'K');
Nexp_pipe = fields_rm(pipes,'Nexp');
% names = fields_rm(pumps,'name');

power_sim = zeros(N,data.npu);
power_opt = zeros(N,data.npu);


for t = 1:N
    %   violation_control;
    T = T0(t,:);
    
    [K_pump,Nexp_pump,omega,pumps] = state2rpm(T,[],pump_flow(t,:),pumps);
    Omega_stor(t,:) = omega;   
    K = [K_pipe K_pump ];
    Nexp = [Nexp_pipe Nexp_pump];
    
%     Feeds =  makeFeeds(pipes,pumps,tanks) 
%     [K,Nexp,Feeds,pumpsOn,connections] = pump_control(pipes,pumps,ones(1,data.npu),tanks,Nexp,K,data.npi,T);
    %% demand time step:
        
    for i = 1:data.nj
        q(i) = d(i,t);
    end
       
    [A12,A10,H0] = makeA12A10(pipes,nodesIdListMap,[reservoirs tanks],pumps);
    tol = 0.0000001; % comment out for function use
    [H_gga,Q_gga] = solver4(A12,A10,H0,q',K',Nexp',tol,data.CV);
    
    
    %% mass balance
    Feeds =  makeFeeds(pipes,pumps,tanks); 
    for i = 1:length(tanks)
        Delta_H = Feeds(i,:)*Q_gga*(3600*24/N)/tanks{i}.Area; % assume 24h schedule
        tanks{i}.Init =  tanks{i}.Init + Delta_H;
    end
    
    for i = 1:length(reservoirs)
        H_res(i,1) = reservoirs{i}.Elev + reservoirs{i}.Init;
    end
    
    for i = 1:length(tanks)
        H_tanks(i,1) = tanks{i}.Elev + tanks{i}.Init;
    end
        
    
    %% save current
    H_save(:,t) = double([H_gga;H_res;H_tanks])-double(data.Elevation);
    Q_save(:,t) = Q_gga;
    % eval([def '_control']);
    % end
    % disp(T0);

    
    % Power calculation 
    q_pump = round(Q_gga(length(pipes)+(1:length(pumps))),2);
    for i = 1:data.npu
        if strcmp(pumps{i}.Status,'Open')
            h_pump = H_save(nodesIdListMap(pumps{i}.endNodeId))-...
                H_save(nodesIdListMap(pumps{i}.startNodeId));
            if strcmp(pumps{i}.type,'VSD')
                % as per hydraulic sim
                power_sim(t,i) = pumps{i}.power.Powerfit(q_pump(i),h_pump);
                
                %as per optimisation
     
                power_opt(t,i,1) = pumps{i}.OF.qt(1)*pump_flow(t,i) + pumps{i}.OF.qt(2)*T(i);
                power_opt(t,i,2) = pumps{i}.OF.qtt2(1)*pump_flow(t,i) + pumps{i}.OF.qtt2(2)*T(i)+pumps{i}.OF.qtt2(3)*T(i)^2;
                power_opt(t,i,3) = pumps{i}.OF.qq2t(1)*pump_flow(t,i) +pumps{i}.OF.qq2t(2)*pump_flow(t,i)^2 + pumps{i}.OF.qq2t(3)*T(i);
                power_opt(t,i,4) = pumps{i}.OF.qq2tt2(1)*pump_flow(t,i) +pumps{i}.OF.qq2tt2(2)*pump_flow(t,i)^2 + pumps{i}.OF.qq2tt2(3)*T(i)+pumps{i}.OF.qq2tt2(4)*T(i)^2;
            elseif strcmp(pumps{i}.type,'FSD')
                % Interpolation of power and flowrate 
                % redundand through the use of interp1_ruben()
%                 if q_pump(i) > max( pumps{i}.power.curve_data.q)
%                     q_pump(i) = max( pumps{i}.power.curve_data.q);
%                 end
                power_sim(t,i) = interp1_ruben(...
                    pumps{i}.power.curve_data.q,... %flow rate
                    pumps{i}.power.curve_data.P,... % power rate
                    q_pump(i)); % computed flow rate
                if isnan(power_sim(t,i))
                    power_sim(t,i) = pumps{i}.power.nameplate;
                end
                
                power_opt(t,i,1) = pumps{i}.power.nameplate;
                power_opt(t,i,2) = pumps{i}.power.lin_approx(1)*pump_flow(t,i) + pumps{i}.power.lin_approx(2)*T(i);
                power_opt(t,i,3) = pumps{i}.power.quad_approx(1)*pump_flow(t,i)^2 + pumps{i}.power.quad_approx(2)*pump_flow(t,i) + pumps{i}.power.quad_approx(3)*T(i);
               
                
            else
                error('Pump type undefined')
            end

        end
    end   
end

% flip the lot
H_save = H_save'; Q_save = Q_save';