    %   %% convex + piece (How much cheaper is LC vs QC)
% approx_number = 4;
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_piece';
% experiment_caller
% 
%  %% convex (MIQP fast and exact method?)
% approx_number = 5; 
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_convex';
% experiment_caller
%  %% convex (MILP faster or slower thatr MIQP)
% approx_number = 6;
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_convex';
% experiment_caller
 %% Simplified pump (Can the pump be simplified even further)
approx_number = 7;
pump_approx = 'Pump_simple';
pipe_approx = 'pipe_piece';
experiment_caller
 %% simplified pump (Can pump and pipe be simplified to death)
approx_number = 8;
pump_approx = 'Pump_simple';
pipe_approx = 'pipe_convex';
experiment_caller
 %% quad + piece (are intigers or NLP more expensive)
approx_number = 3;
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_piece';
experiment_caller
%% quad ineq ()
approx_number = 2;
pump_approx = 'Pump_quad_ineq'; 
pipe_approx = 'pipe_quad_ineq';
experiment_caller

%% quad (exact solution)
approx_number = 1;
pump_approx = 'Pump_quad';
pipe_approx = 'pipe_quad';
experiment_caller


