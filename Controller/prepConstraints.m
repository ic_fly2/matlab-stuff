%% nodes
% Elevation
Elevation = zeros(length(nodes),1);
for i = 1:length(nodes)
    Elevation(nodesIdListMap(nodes{i}.Id)) = nodes{i}.Elev;
end
change = connection_matrix*Elevation;
% assign it to pipes
for i = 1:data.npi
     pipes{i}.change_gz = change(i);
    [pipes{i}.vmax, pipes{i}.q_lim, pipes{i}.M_pipe] = q_lim_maker2(pipes{i},vmax_min);
end

for i = data.npi+(1:data.nv)
    valves{i}.change_gz = change(i);
end

for i = 1:data.npu
    pumps{i}.change_gz = change(data.npi+data.nv+i);
end

    

%% pipes
% if pipes{1}.Roughness >20
%     Approximation = 'HW';
%     
% else
%     Approximation = 'DW';
% end

% for i = 1:length(pipes) 
%     if pipes{1}.Roughness >20 %HW
%         pipedata(i).data = [pipes{i}.Diameter/1000 pipes{i}.Roughness pipes{i}.Length,2,3]; %D,e,L,vmax,pmax, in m / m /m
%     else %DW
%         pipedata(i).data = [pipes{i}.Diameter/1000 pipes{i}.Roughness/1000 pipes{i}.Length,2,3]; %D,e,L,vmax,pmax, in m / m /m
%     end
% 
% end

% [q_lim,M_pipes] = q_lim_maker(pipes,Approximation,vmax_min);




%% demand
if exist('imresize') == 2
    d = imresize(data.qall,[size(data.qall,1) N], 'nearest');
    
    d_factor = mean(d,2)./mean(data.qall,2);
    d_factor(isnan(d_factor)) = 1;
    d = d./(d_factor*ones(1,N));
    
%     d = qall_gga;
else
    if size(data.qall,2) >= N
        %All cool
        for ii = 1:N;
            d(:,ii) = mean(data.qall(:,(ii-1)*size(data.qall,2)/N+(1:size(data.qall,2)/N)),2);
        end
    else
        % modify the schedule by doubling the values
        error('No image processing Addon found, cannot process deamnd with greater detail than provided, please modify input file accordingly')
        %     qall = imresize(qall, [N size(qall,2)*ceil(N/size(qall,2))], 'nearest');
        %     for ii = 1:N;
        %         d(:,ii) = mean(qall(:,(ii-1)*size(qall,2)/N+(1:size(qall,2)/N)),2);
        %     end
    end
end

% d = qall_gga;

data.np = size(connection_matrix,1);
flow_matrix = eye(data.np);

%% exteral factors:
if ~isfield(settings,'price') || strcmp(settings.price,'default')
     settings.price(1:11) = 28.59;
     settings.price(12:19) = 85.76;
     settings.price(13:33) = 48.49;
     settings.price(34:40) = 85.76;
     settings.price(41:44) = 48.49; 
     settings.price(45:48) =  28.59;
end


Pe = settings.price_adj;

if ~isfield(settings,'switch_penalty')
    settings.switch_penalty = 0.15;
end


%% bounds
T_bounds = [0 1];
lambda_bounds = [0 1];
plotting_switch = 'off';
M_pumps = [];

for i = 1:data.nj
    if length(reservoirs) ==1 && ~isfield(settings,'custom_h_max');
        %better would be an epanet simulation with all pumps on an tanks
        %full and one with all tanks empty and all pumps off!
        data.h_min(nodesIdListMap(junctions{i}.Id)) = reservoirs{1}.Elev - junctions{i}.Elev;
%         max(Elevation(nodesIdListMap(pumps.startNodeId)))
        data.h_max(nodesIdListMap(junctions{i}.Id)) = reservoirs{1}.Elev +  sum( fields_rm(pumps,'c')); %unsafe!
%         h_max(nodesIdListMap(junctions{i}.Id)) = 320;
        %     
    %else
%         error('More than one fixed reservoir found, please remove')
    elseif length(reservoirs) ==1 && isfield(settings,'custom_h_max');
        data.h_min(nodesIdListMap(junctions{i}.Id)) = reservoirs{1}.Elev - junctions{i}.Elev;
        data.h_max(nodesIdListMap(junctions{i}.Id)) = settings.custom_h_max;

    end
end

% tanks
Diameter = zeros(1,length(tanks));

location_res = zeros(1,length(tanks));
Initial_fill = zeros(1,length(tanks));

% h_min = minimum_tank_fills(N)

for i = 1:length(tanks)
    Diameter(i) = tanks{i}.Diameter;
    location_res(i) = nodesIdListMap(tanks{i}.Id);
    Initial_fill(i) = tanks{i}.Init;
    data.h_min(location_res(i)) = tanks{i}.Min;
    data.h_max(location_res(i)) = tanks{i}.Max;    
end
Reservoir_area = pi*Diameter.^2/4;
% reservoirs
fixed_val = zeros(1,length(reservoirs));
location_fix = zeros(1,length(reservoirs));
for i = 1:length(reservoirs)
    fixed_val(i) = reservoirs{i}.Head(1);
    location_fix(i) = nodesIdListMap(reservoirs{i}.Id);
    data.h_min(location_fix(i)) = fixed_val(i);
    data.h_max(location_fix(i)) = fixed_val(i);
end



%% pumps:
% for i = 1:length(pumpsUnique)
% %     q_lim = [q_lim pumpsUnique{i}.q_lim*n_pumps(i)] ;
%         q_lim = [q_lim pumpsUnique{i}.q_lim] ;
% end

% if exist('OP_flow_pump','var')&& exist('OP_flow','var')
% [a_quad_pump,b_quad_pump,c_quad_pump,...
%   m_linear_pump,c_linear_pump,...
%   Q_pump_min,Q_pump_max,Delta_H,...
%   Pump_station  ] = pump_approx_gen( a_org,b_org ,c_org,n_pumps,OP_flow_pump,OP_head_pump,pmax_pump,plotting_switch);
% else
%    disp('Pumps OP points not specified, will estimate them but may not be accurate! Please measure and report back')
%     [a_quad_pump,b_quad_pump,c_quad_pump,...
%   m_linear_pump,c_linear_pump,...
%   Q_pump_min,Q_pump_max,Delta_H,...
%   Pump_station  ] = pump_approx_gen( a_org, b_org, c_org, n_pumps,[],[],pmax_pump,plotting_switch);
% end

if exist('OP_flow_pump','var')
    for i = 1:data.npu
        [pumps{i}.conv_m, pumps{i}.conv_C, pumps{i}.m_linear,pumps{i}.c_linear,pumps{i}.OP_flow,pumps{i}.OP_Delta_H] = pump_approx_gen2(pumps{i},settings,OP_flow_pump,OP_head_pump);
    end
else
    for i = 1:data.npu
        [pumps{i}.conv_m, pumps{i}.conv_C, pumps{i}.m_linear,pumps{i}.c_linear,pumps{i}.OP_flow,pumps{i}.OP_Delta_H] = pump_approx_gen2(pumps{i},settings,[],[] );
    end
end

% if  strcmp(pump_approx ,'Pump_simple')
%     simple_pump_gen
% end

data.Qmax = max([fields_rm(pipes,'q_lim') fields_rm(pumps,'q_lim')] );

% Network_tests;
