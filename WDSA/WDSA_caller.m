clearvars;
close all;
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
EL_pricing

prob.N =24;
prob.approx_number =5;
prob.vmax_min = 7;
prob.M  = 9999;
prob.settings.time_limit  = 300;
prob.Desired_ratio = NaN;
prob.asymmetry = 'on';
% prob.settings.time_analysis = 'Percentage';
% prob.settings.custom_h_max = 250;




prob.settings.DR.status = 'Optimal';
prob.settings.price = strech(EL.price{2},1); 
prob.settings.DR.flex = 'Window1';

prob.settings.DR.desired_ratio = NaN;
prob.settings.DR.rest_period = 240; %minutes
prob.settings.DR.reward = 30000;


prob.settings.solver_gap = 0.00;
prob.settings.solver = 'cplex'; %'intlinprog'; %'cplex' 'gurobi';
%prob.settings.time_analysis = 'Percentage';
%prob.settings.DR.recovery = 240;
%prob.settings.mods.Length_factor = 1;
% 
% 
prob.settings.mods.status = 'No';
prob.settings.plot = 'Off';
% 
prob.settings.Time_shift =0;
% prob.settings.mod_schedule.status = 'No';
% prob.settings.mod_schedule.sched = [0 0 0];
% prob.settings.mod_schedule.index = 1;

prob.settings.GHG = CO2_kWh_av ;
prob.settings.limit_overfill = 0.1;

prob.settings.Robust.status = 'No';
% prob.settings.Robust.probability = [0.1 0.5 0.4];
% prob.settings.Robust.cost{1} = strech(EL.price{2}*0.8,0.2);
% prob.settings.Robust.cost{2} = strech(EL.price{2},1);
% prob.settings.Robust.cost{3} = strech(EL.price{2}*1.2,1.3);

% prob.settings.h0 = [2 2];
% prob.settings.h0 = 0.036895372569841;

 
%  
  maxi = 20;
  maxj = 5;
%  
%  fval_sim= zeros(maxi,maxj);
%  fval = zeros(maxi,maxj);
%  GHG = zeros(maxi,maxj);
%  DR_D = zeros(maxi,maxj);
%  Remaining_gap = zeros(maxi,maxj);
%  demand_factor= zeros(maxi,maxj);
%  price_strech= zeros(maxi,maxj);
%  
%  
%  
% % prob.def =  'Lecture_example_norm_FSD'; prob.settings.OF_spec = 'F3';
% % prob.def =  'Lecture_example_norm_VSD'; prob.settings.OF_spec = 'V4';
% prob.def = 'van_zyl_norm_171'; prob.settings.OF_spec = 'F3';
% % % prob.def = 'van_zyl_norm_171_VSD'; prob.settings.OF_spec = 'V1';
%  
% for i = 1:maxi
%     for j = 1:maxj
%         prob.settings.DR.demand_factor = 0.6+i/40-0.025;
%         prob.settings.price = strech(EL.price{2},(j/5)+0.4);  
%         out  = experiment(prob);
%         fval(i,j) = out.fval;
%         fval_sim(i,j) = out.fval_sim;
%         GHG(i,j)  = out.GHG;  
%         DR_D(i,j)  =  out.DR_D;
%         demand_factor(i,j) = i/10;
%         price_strech(i,j) = (j-1)/10;
%         Remaining_gap(i,j)  =  out.Remaining_gap;
%         save('FSD_vanzyl_DR_OPT.mat','fval','fval_sim','GHG','DR_D','Remaining_gap','demand_factor','price_strech')
%     end
% end
%  
%  fval_sim= zeros(maxi,maxj);
%  fval = zeros(maxi,maxj);
%  GHG = zeros(maxi,maxj);
%  DR_D = zeros(maxi,maxj);
%  Remaining_gap = zeros(maxi,maxj);
%  demand_factor= zeros(maxi,maxj);
%  price_strech= zeros(maxi,maxj);
%  
%  
%  prob.settings.DR.status = 'Off';
% % prob.def =  'Lecture_example_norm_FSD'; prob.settings.OF_spec = 'F3';
% % prob.def =  'Lecture_example_norm_VSD'; prob.settings.OF_spec = 'V4';
% prob.def = 'van_zyl_norm_171'; prob.settings.OF_spec = 'F3';
% % % prob.def = 'van_zyl_norm_171_VSD'; prob.settings.OF_spec = 'V1';
%  
% for i = 1:maxi
%     for j = 1:maxj
%         prob.settings.DR.demand_factor = 0.6+i/40-0.025;
%         prob.settings.price = strech(EL.price{2},(j/5)+0.4);  
%         out  = experiment(prob);
%         fval(i,j) = out.fval;
%         fval_sim(i,j) = out.fval_sim;
%         GHG(i,j)  = out.GHG;  
%         DR_D(i,j)  =  out.DR_D;
%         demand_factor(i,j) = i/10;
%         price_strech(i,j) = (j-1)/10;
%         Remaining_gap(i,j)  =  out.Remaining_gap;
%         save('FSD_vanzyl_NODR_OPT.mat','fval','fval_sim','GHG','DR_D','Remaining_gap','demand_factor','price_strech')
% 
%     end
% end


prob.settings.DR.status = 'Optimal';
prob.def = 'van_zyl_norm_171_VSD'; prob.settings.OF_spec = 'V1';
prob.approx_number =15;
 fval_sim= zeros(maxi,maxj);
 fval = zeros(maxi,maxj);
 GHG = zeros(maxi,maxj);
 DR_D = zeros(maxi,maxj);
 Remaining_gap = zeros(maxi,maxj);
 demand_factor= zeros(maxi,maxj);
 price_strech= zeros(maxi,maxj);
 
 
for i = 1:maxi
    for j = 1:maxj
        prob.settings.DR.demand_factor = 0.6+i/40-0.025;
        prob.settings.price = strech(EL.price{2},(j/5)+0.4); 
        out  = experiment(prob);
        fval(i,j) = out.fval;
        fval_sim(i,j) = out.fval_sim;
        GHG(i,j)  = out.GHG;  
        DR_D(i,j)  =  out.DR_D;
        demand_factor(i,j) = i/10;
        price_strech(i,j) = (j-1)/10;
        Remaining_gap(i,j)  =  out.Remaining_gap;
        save('VSD_V1_DR_vanzyl_OPT2.mat','fval','fval_sim','GHG','DR_D','Remaining_gap','demand_factor','price_strech')
        
    end
end

prob.settings.DR.status = 'Off';
prob.def = 'van_zyl_norm_171_VSD'; prob.settings.OF_spec = 'V1';
prob.approx_number =15;
 fval_sim= zeros(maxi,maxj);
 fval = zeros(maxi,maxj);
 GHG = zeros(maxi,maxj);
 DR_D = zeros(maxi,maxj);
 Remaining_gap = zeros(maxi,maxj);
 demand_factor= zeros(maxi,maxj);
 price_strech= zeros(maxi,maxj);
 
 
for i = 1:maxi
    for j = 1:maxj
        prob.settings.DR.demand_factor = 0.6+i/40-0.025;
        prob.settings.price = strech(EL.price{2},(j/5)+0.4); 
        out  = experiment(prob);
        fval(i,j) = out.fval;
        fval_sim(i,j) = out.fval_sim;
        GHG(i,j)  = out.GHG;  
        DR_D(i,j)  =  out.DR_D;
        demand_factor(i,j) = i/10;
        price_strech(i,j) = (j-1)/10;
        Remaining_gap(i,j)  =  out.Remaining_gap;
        save('VSD_V1_NO_DR_vanzyl_OPT2.mat','fval','fval_sim','GHG','DR_D','Remaining_gap','demand_factor','price_strech')
        
    end
end
