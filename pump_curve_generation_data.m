% pump_efficiency to power curve
clear all

% % 1A
% eff = [  	0           	0          ;...    
%   	20          	57          ;...   
%   	25          	65          ;...   
%    	30          	71          ;...   
%     	35          	75       ;...      
%     	40          	75      ;...       
%   	45          	72          ;...   
%     	50          	70];
% 
% hyd = [2007            	0           	129     ;...    
%  2007            	10          	128         ;...   
%  2007            	15          	127         ;...   
%  2007            	20          	126         ;...   
%  2007            	25          	124         ;...   
%  2007            	30          	121         ;...   
%  2007            	35          	116         ;...   
%  2007            	40          	110         ;...   
%  2007            	45          	103         ;...   
%  2007            	50          	91     ] ;


% %2A
% CBNEfficiency = 0;
% eff=[
%  CBNEfficiency   	0           	0           
%  CBNEfficiency   	20          	57          
%  CBNEfficiency   	25          	65          
%  CBNEfficiency   	30          	71          
%  CBNEfficiency   	35          	75          
%  CBNEfficiency   	40          	75          
%  CBNEfficiency   	45          	75          
%  CBNEfficiency   	50          	73];
% 
% 
% hyd = [2015            	0           	129         
%  2015            	10          	128         
%  2015            	15          	127         
%  2015            	20          	126         
%  2015            	25          	124         
%  2015            	30          	121         
%  2015            	35          	116         
%  2015            	40          	110         
%  2015            	45          	103         
%  2015            	50          	91  ];

% % 3A
%  STEfficiency  = 1;
% hyd = [  
%  1006            	20          	36.00            
%  1006            	40          	31.00          
%  1006            	60          	27.00       
%   ];
% 
% eff = [       
%  STEfficiency    	20          	34          
%  STEfficiency    	40          	58          
%  STEfficiency    	60          	72          
%  ];

% %4B
% hyd = [
%      1881            	0           	40.00       
%  1881            	6.94        	38.50       
%  1881            	13.88       	38.00       
%  1881            	20.83       	37.50       
%  1881            	27.77       	36.00       
%  1881            	34.72       	35.00       
%  1881            	41.66       	32.50       
%  1881            	48.61       	30.00       
%  1881            	55.55       	27.50       
%  1881            	111.5       	7.50  ];
% 
% LZHZEfficiency = 1;
% eff = [
%     LZHZEfficiency  	0           	0           
%  LZHZEfficiency  	10          	40          
%  LZHZEfficiency  	20          	50          
%  LZHZEfficiency  	30          	61          
%  LZHZEfficiency  	40          	68          
%  LZHZEfficiency  	50          	71          
%  LZHZEfficiency  	60          	72          
%  LZHZEfficiency  	70          	72          
%  LZHZEfficiency  	80          	71          
%  LZHZEfficiency  	90          	68          
%  LZHZEfficiency  	100         	61          
%  LZHZEfficiency  	110         	50          
%  LZHZEfficiency  	120         	40     ];

% 5C
% hyd =[
%  1884            	2.22        	120.000     
%  1884            	2.78        	118.000     
%  1884            	3.33        	115.000     
%  1884            	3.89        	110.000     
%  1884            	4.44        	105.000     
%  1884            	5           	96.000      
%  1884            	5.55        	85.000      
%  1884            	6.11        	72.000];
% 
% HHEfficiency  = 1;
% eff = [          
%  HHEfficiency    	1.1         	0           
%  HHEfficiency    	1.5         	48          
%  HHEfficiency    	2           	57          
%  HHEfficiency    	2.5         	62          
%  HHEfficiency    	3           	67          
%  HHEfficiency    	3.5         	71          
%  HHEfficiency    	4.5         	71          
%  HHEfficiency    	5           	69          
%  HHEfficiency    	5.5         	67          
%  HHEfficiency    	6           	62   ];

% %6 C
% hyd = [
%  %1123            	0           	88.000      
%  1123            	2.78        	87.000      
%  1123            	5.56        	84.000      
%  1123            	8.53        	76.000      
%  1123            	11.11       	63.000      
%  1123            	13.89       	47.000 ];
% 
% LZGEfficiency = 1;
% 
% eff = [ %LZGEfficiency   	0           	0           
%  LZGEfficiency   	2.78         	32          
%  LZGEfficiency   	5.56         	50          
%  LZGEfficiency   	8.53         	55          
%  LZGEfficiency   	11.11       	58          
%  LZGEfficiency   	13.89        	47
% ];

% 7F
% SEfficiency= 1;
% eff = [SEfficiency     	0           	0           
%  SEfficiency     	1.38        	31          
%  SEfficiency     	2.7         	48          
%  SEfficiency     	4.6         	54          
%  SEfficiency     	5.5         	54          
%  SEfficiency     	6.94        	43 ];
% 
% hyd = [ 4       	0           	38          
%  1883       	1.2         	37          
%  188        	2           	36];  

% % Main1
% hyd = [ 1               	0           	100         
%  1               	120         	90          
%  1               	150         	83];
% 
% leff = 1;
% eff = [leff 		50 		78
% leff 		107 	80 
% leff 		151		68 
% leff 		200 	60];



% booster

hyd = [  6               	0           	120         
 6               	90          	75          
 6               	150         	0 ];

leff = 1;
eff = [leff 		50 		78
leff 		107 	80 
leff 		151		68 
leff 		200 	60];




%% power calc

hyd(:,1) = []; % delete garbage;
eff(:,1) = []; % delete garbage;

plot(eff(:,1),eff(:,2),'r*')
hold on
plot(hyd(:,1),hyd(:,2),'+b')

% cleaned:
q = hyd(:,1);
if length(eff(:,1)) ==  length(hyd(:,1)) && eff(:,1) ==  hyd(:,1)
    nu = eff(:,2);
else
     p_eff = polyfit(eff(:,1),eff(:,2),2);
     nu = polyval(p_eff,q);
end

plot(q,nu)

    power   = q.*hyd(:,2)*999*9.81./nu;  % power = q*h*rho*g/eff 

plot(q,power,'+g')

p = polyfit(q,power,2);

plot([0:max(q)*2],polyval(p,[0:max(q)*2]))