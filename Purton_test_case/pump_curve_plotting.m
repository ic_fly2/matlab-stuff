%% plot the different pump curves
% deffinition of pump constants
P1 = [-0.002 0 80];
P2 = [-0.005 0 120];

Q = 1:200;
H1 = polyval(P1,Q); % one pump1
H2 = polyval(P2,Q); % one pump1

H3 = H1+H2;


plot(Q,H1,'b',Q,H2,'bx',Q,H3,'ro')
axis([0 200 0 120])