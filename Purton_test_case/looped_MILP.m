%% set pump schedule and outflow demands
clear all;

% Create new or run old network definition
% network_definition;

%% Initialisation

%Network topology
load('Nodes_14_12_2013.mat');

pump_def_C = 5000;
plot_on = 0;% turn ploting on with 1
N = 24;% more detailed node info needed for larger N (serious modification of network_definition.m needed)
Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit
no_pumps = 1;

%electricity pricing
Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
Pe(1:24) = Electricity_Price(1:2:48);

% resevoir
Area = 50;
h0 = 10;
h_max = 50;
h_min = 10;
resevoir_level(1:N) = 0;
res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';
%% loop
iter = 1;
max_iter = 5;
err = 21;
while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached

%% Curve fitting / Hydraulic solver
% regressoin model fit polynomials to flow and find operating points
for t = 1:N;
%% compute pump settings and outflows
system_curve_builder_v3;
operating_point_finder_v2;
Qp(t,:) = Q_operation;
end


%% LP
% create matracies for LP
%Aeq
j = 1;
Aeq =[];
while j <= no_pumps
for i = 1:N 
    flow(i,:,j) = Qp(:,j)';
end
Aeq = [Aeq tril(flow(:,:,j))];
j = j + 1;
end
Aeq = [Aeq -eye(N)];
%A
A(1,1:size(Aeq,2)) = 0; A(1,size(Aeq,2)) = -1;

%beq
for t = 1:N
d(t) = sum(Node(t+1,2,:));
end

for k = 1:N
    beq(k) = sum(d(1:k))/Area - h0;
end
%b
b(1) = -h0;
  
%bounds
ub(1:N) = 1;
ub(N+1:2*N) = h_max;
lb(1:N) = 0;
lb(N+1:2*N) = h_min;

lb = lb';
ub =ub';

% c
h = zeros(N,1); 
c = [Pe h'];

%number of intigers
M(1:24) = 1:24;
e=2^-24;
% optimize
tic
X = linprog(c,A,b,Aeq,beq,lb,ub);sum(X'.*c)
toc
tic
[Z,val,status]=milp(c,A,b,Aeq,beq,lb,ub,M,e);sum(Z'.*c)
toc
% tic another MILP solver also try CPLEX
% [Y,val,status]=IP(c,A,b,Aeq,beq,lb,ub,M,e);sum(Y'.*c)
% toc

% find resulting height of water tank
resevoir_level = Z(length(Z)-N:length(Z)-1); resevoir_level(1) = h0;
err = sum(abs(res_old - resevoir_level'));
if iter <= max_iter % leave last result for analysis if no convergence occurs
res_old = resevoir_level';
else
end
iter = iter +1;
end
Z