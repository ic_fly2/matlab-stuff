% %% set pump schedule and outflow demands
% clear all;close all
% % Create new or run old network definition
% % network_definition;
% 
% %% Initialisation
% 
% %Network topology
% % load('Nodes_14_12_2013.mat'); %test case
% % Node(2:N+1,2,:) = 2* Node(2:N+1,2,:) ;% increase demand a bit % test case
% 
% % pump and system features
% pump_and_sys_curves; %get model data
% plot_on = 0;% turn ploting for all on with 1 for only final =0
% N = 48;
% no_pumps = 1;
% demand_multiplier = 200;
% 
% % DR factors
% F = 500; % DR enforcement factor
% P_DR(1:N) = 20; % price for Dr in each time interval*probabilety - penalty 
% 
% 
% % solver type
% solver_type = 'MIQP_cplex'; % Options include 'QP_cplex' 'MILP_matlab' and  'MIQP_cplex' use matlab solvers with the optimisation toolkit cplex requires cplex from IBM to work
% maxtime = 20;
% % controller
% controller = 'on'; %options are on or off in on then provide h_initial
% h_initial =1;
% 
% %electricity pricing
% Electricity_Price(1:12) = 28; Electricity_Price(13:20) = 85; Electricity_Price(21:34) = 48; Electricity_Price(35:40) =48; Electricity_Price(41:48) = 28; 
% % Pe(1:24) = Electricity_Price(1:2:48); % test case
% Pe = Electricity_Price;
% % demand
% x = 1:N;
% % d = sin(x/(pi*1.2))*400 + 800;%d =d*180;
% % demand = importdata('E:\Phd\Hydraulics\EPAnet\Peak_le2.txt'); % coice of demand pattern
% demand = importdata('Peak_le.txt'); % coice of demand pattern
% d = demand.data;
% d = d*demand_multiplier; %m3/h !!!!!!!!!!!!!!!!!!!!!!!!!!!
% % resevoir
% Area = pi*(50/2)^2;
% % h0 = 15; % removed in new layput
% h_max = 2;
% h_min = 0;
% reservoir_level(1:N) = 0;
% res_old(1:N) = 500;%res_old(1:N) =res_old(1:N)';
% 
% %% prealocate some variables to avoid looping over stuff that doesn't change
% % MILP vars
% %b %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % b(1,1) = d(1)/Area;
% b(1,1) =0;
% b(2:N+1,1) = 1;
% b(N+2:2*N+1,1) = 0;
% b(2*N+2:3*N+1,1) = - h_min ; %from m3/h to m3/0.5h
% 
% % c %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% h = zeros(N,1);
% i = 1;
% c = Pe;
% while i <= no_pumps-1
% c = [c Pe*(i+1)];
% i = i+1;
% end
% c = [c h' P_DR]'; % added transpose for cplex
% 
% %H % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % super complicated way of doing it:
% H = eye(N*no_pumps);
% k =N:N:no_pumps*N; %usefull locations in the matrix
% j =1:N:no_pumps*N;
% for i = 1:N*no_pumps-1
%    H(i,i+1) = - 1;
% end
% H(k,j) = 0;
% for i = 1:length(k)
% H(j(i),k(i)) = -1; % H_N - H_1 
% end
% l = 2;
% multiple(1:N,1) = 1;
% while l <= no_pumps
%     add(1:N,1) = l;
%   multiple = [multiple; add];
%   l = l+1;
% end
% for i = 1:N*no_pumps
%    H1(i,:)=  multiple(i,:) .* H(i,:); 
% end
% H = H1;
% H(N*no_pumps+1:N*(no_pumps+2),N*no_pumps+1:N*(no_pumps+2)) = 0;
% H = H +H'; % ensure symetry
% 
% 
% % for debuging purposes
% % H(:,:) = 0;
% 
% % beq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % for k = 2:N
% %     beq(k,1) = sum(d(1:k))/Area - h0;
% % end
% beq(1,1) = 0;
% beq(2:N,1) = d(2:N)/Area/2; %m3/0.5h !!!!!!!!!!!!
% 
% if strcmpi('on',controller) % H_1 = h_initial
%     beq(N+1,1) = h_initial;
% else
% end
% %bounds %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ub(1:N*no_pumps,1) = 1;
% ub(N*no_pumps+1:N*(no_pumps+1),1) = h_max;
% ub(N*no_pumps+2:N*(no_pumps+2),1) =1;
% lb(1:N*no_pumps,1) = 0;
% lb(N*no_pumps+1:N*(no_pumps+1),1) = h_min;
% lb(N*no_pumps+2:N*(no_pumps+2),1) =0;
% %% loop
% iter = 1;
% max_iter = 20;
% err = 21;
% % while err >= 1 && iter <= max_iter; % stopping condition total discrepancy between tank elevations or max iterations reached
% 
% %% Curve fitting / Hydraulic solver
% % regressoin model fit polynomials to flow and find operating points
% for t = 1:N;
% %% compute pump settings and outflows
% % system_curve_builder_v3;
% % operating_point_finder_v2;
% % Qp(t,:) = Q_operation;
% hydraulic_curve_fitting
% end
% Qp_hist(:,:,iter) =  Qp(:,:);
% h_hist(:,iter) = h(:);
% %Aeq %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Aeq =[];
% for i= 1:no_pumps
%     flow  = diag(Qp(:,i))/2; %from m3/h to m3/0.5h
%     flow(1,1) = 0; % first flow is an inequality
% Aeq = [Aeq flow];
% end
% h_i(1:N) = -1;
% h_i_1(1:N-1) = 1;
% H_treatment = diag(h_i) + diag(h_i_1,-1) ;
% % H_treatment(1,N) = -1; 
% H_treatment(1,1) =0;
% Aeq = [Aeq/Area H_treatment zeros(N) ]; % added /Area for good measure
% 
% if strcmpi('on',controller) % H_1 = h_initial
%     Aeq(N+1,no_pumps*N+1) = 1;
% else
% end
% 
% %A %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % clear A
% Top(1,N*no_pumps+2*N) = 0; 
% Top(1,N*no_pumps+1) = 1; Top(1,N*no_pumps+N) = -1;   
% % ad limit on what pump switches are allowed
% limit = eye(N);
% i = 1;
% while i <= no_pumps-1
% limit = [limit eye(N)];
% i = i+1;
% end
% A_no_DR = [Top ; limit zeros(N) zeros(N)];
% % DR addition
% A_1 = [A_no_DR; -limit zeros(N) eye(N)];
% 
% % A_2=[];
% % for i= 1:no_pumps
% %     flow  = diag(Qp(:,i))/2; %from m3/h to m3/0.5h
% %     A_2 = [A_2 flow];
% % end
% % A = [A_1;A_2 eye(N) -eye(N)*F];
% A = [A_1; zeros(N,no_pumps*N) -eye(N) -1*diag(d)/(2*Area)];
%% generate
if strcmpi('on',controller) % H_1 = h_initial
    h0(1) = h_initial;
else
    h0(1) = 0;
end
if h_initial >= 0.7*h_max
    disp('initial and max filling are too close for sensible operation, press Ctrl - C and change the input')
   pause;
end

set = 1;
Y0_schedule(1) = set;
pump_flow = Qp(:,no_pumps)/2;
for i = 1:N-1
    h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    if h0(i+1) >= h_max
        set = 0;
        h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    elseif h0(i+1) <= h0(1) 
        set = 1;
        h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    end
    Y0_schedule(i+1) = set; 
end
% for i = 1:N;
%     if h0(i)-h_min >= (pump_flow(i)-d(i)/2)/(Area) && Y0_schedule(i) ==1
%         delta0(i) =1;
%     else
%         delta0(i) =0;
%     end
% end

for i = 1:N
    if -h_min >= -d(i)*50000/(2*Area) -h0(i)
        test0(i) = 1;
    elseif -h_min >= -h0(i)
        test0(i) =0;
    else
        error('delta0 not found')
    end
end

Y0((no_pumps-1)*N+1:no_pumps*N,1) =  Y0_schedule;
Y0(no_pumps*N+1:(no_pumps+1)*N,1) = h0;
Y0((no_pumps+1)*N+1:(no_pumps+2)*N,1) = 0;
%% check
if A*Y0 <= b
    disp('inequaltiy OK')
else
    disp('No initial solution found at all');
   break;
end

if Aeq*Y0 == beq
    disp('Hurray initial solution found')
else
    fail_by = ['No exact initial solution found for equaltiy constraint. Error is max of:' num2str(max(abs(Aeq*Y0 - beq)))];   
    disp(fail_by );
 end