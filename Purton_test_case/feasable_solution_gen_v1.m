%% generate
if strcmpi('on',controller) % H_1 = h_initial
    h0(1) = h_initial;
else
    h0(1) = 0;
end
if h_initial >= 0.7*h_max
    disp('initial and max filling are too close for sensible operation, press Ctrl - C and change the input')
   pause;
end

set = 1;
Y0_schedule(1) = set;
pump_flow = Qp(:,no_pumps)/2;
for i = 1:N-1
    h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    if h0(i+1) >= h_max
        set = 0;
        h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    elseif h0(i+1) <= h0(1) 
        set = 1;
        h0(i+1) = (pump_flow(i+1)*set - d(i+1)/2)/Area + h0(i);
    end
    Y0_schedule(i+1) = set; 
end
Y0((no_pumps-1)*N+1:no_pumps*N,1) =  Y0_schedule;
Y0(no_pumps*N+1:(no_pumps+1)*N,1) = h0;
%% check
if A*Y0 <= b
    disp('inequaltiy OK')
else
    disp('No initial solution found at all');
%    break;
end

if Aeq*Y0 == beq
    disp('Hurray initial solution found')
else
    fail_by = ['No exact initial solution found for equaltiy constraint. Error is max of:' num2str(max(abs(Aeq*Y0 - beq)))];   
    disp(fail_by );
 end