%% pump index stuff as excel is useless :S
clear all;
% desired duty point
duty_point = [180 0.37]; % m m^3/s % Purton
D = 1.186; % m Purton
% duty_point = [15 0.4]; % m m^3/s % small mains
% D = 0.2; % m small mains


Q = duty_point(2);
e = 0.0001;

%% systems
Coefficients = linspace(0.15,50,5);
for i = 1:length(Coefficients)
sol = roots([1/Coefficients(i) Q^2 -duty_point(1) ]);
K_a(i) = sol(sol >= 0);
K_c(i) = K_a(i)^2/Coefficients(i);
end

dyn_head = duty_point(1) -  K_c;
for i = 1:length(Coefficients);
    L(i) = dyn_head(i);
    [~,~,~,H,~,~] = DarcyWeisbachFactorCalc(D,Q,e,L(i));
    while abs(H-dyn_head(i)) >= 0.1
        err = dyn_head(i)-H;
        L(i) = L(i)+err*2;
        [f(i),~,regime{i},H,K(i),n(i)] = DarcyWeisbachFactorCalc(D,Q,e,L(i));
    end
end

% Res = [ K_a' zeros(length(Coefficients),1) K_c' Coefficients' L' K' n' f'];
Res = [ K_a' zeros(length(Coefficients),1) K_c' Coefficients' L' f'];
K_S = [K_a' zeros(length(Coefficients),1) K_c'];

array2latex(Res)

%% pump
C_p = Inf; % Ka/Kb
grad = linspace(-100,-500,5);

for i = 1:length(grad)
    Ka(i) = grad(i)/(2*Q+1/C_p);
    Kb(i) = grad(i) - 2*Ka(i)*Q;
end

Kc = duty_point(1) - Ka*Q^2 - Kb*Q;

pump_res = [ Ka' Kb' Kc' grad'];
K_P = [ Ka' Kb' Kc'];
array2latex(pump_res)


