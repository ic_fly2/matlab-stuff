%% info
% test case curves for purton trunk mains. Curves fitted to estimated data
% by hand. System stats:
% 100m resevoir elevation
% 3 pumps at 1MW each 80% efficient
% system curves assumed:

% system = 0.00005.*x.^2 + 100; %flow in m3 per 1/2 hour intervall!!
% pump = -0.00003.*x.^2 + 180;

%% test case %enable only this section to run in optimsation_fx...
elevation = 100;
for i = 1:no_pumps
    sys = [0.00005 0 h(t)+elevation];
    pump = [-0.00003/(i^2) 0 180];
    solution = roots(pump - sys);
    Qp(t,i) = solution(max(imag(roots(pump - sys))==0));
end

%% system visualistaion
% pump = [-0.00003 0 180];
%  sys1 = [0.00005 0   elevation+10]; % lower limit h_min
%  sys2 = [0.00005 0   elevation+50]; % upper limit h_max
%  power = [-0.06 220]; % efficiancy concerns
%  sys3 = [0.00005 0   (elevation+50 + elevation+10)/2 ];
%  q = 1:2000;
% y1 = polyval(sys1,1:2000);
% y2 = polyval(sys2,1:2000);
% z = polyval(pump,1:2000);
% x = polyval(power,1:2000);
% linear = polyval(linear,1:2000);
% 	
% plot(y1);hold on;plot(y2); plot(z,'c');plot(x)

% %% linearisation
% Q1 = max(roots(pump - sys1));
% Q2 = max(roots(pump - sys2));
% Qlin = (Q1+Q2)/2;
% Linear_pump = [pump(1)*2*Qlin  polyval(pump,Qlin)-pump(1)*2*Qlin*Qlin];
% lin_pump = polyval(Linear_pump,Q2:Q1);
% plot(Q2:Q1,lin_pump,'r')
% Linear_sys = [sys(1)*2*Qlin  polyval(sys3,Qlin)-sys3(1)*2*Qlin*Qlin];
% lin_sys = polyval(Linear_sys,Q2:Q1);
% plot(Q2:Q1,lin_sys,'r')