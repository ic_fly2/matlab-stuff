%% section 1
clear all;
K_P = [ -0.002   -0.5   28] ;%[   -0.002   0   28]; %pump and system curves
K_S = [0.000002392368680   0.000028961696068  19.999680417148980];
domain = 10:90; %flow range
head_at_max = polyval(K_P,domain);
sys_curve = polyval(K_S,domain);
plot(sys_curve,'r'); hold on;
% plot(head_at_max,'b')
omega =  1300:100:1800;
for i = 1:length(omega)
head_omega(i,:) = (head_at_max/1500^2)*omega(i)^2;
flow_omega(i,:) =  domain/1500 * omega(i);
dutyflow(i) = max(abs(roots(polyfit(flow_omega(i,:),head_omega(i,:),2)-K_S)));
dutyhead(i) = polyval(K_S,dutyflow(i));
plot(flow_omega(i,:),head_omega(i,:));hold on
plot(dutyflow(i),dutyhead(i),'o');
end
plot(polyval(K_P,domain),'c')

% figure
K_lin_omega = polyfit(dutyflow,omega,1);
% plot(dutyflow,omega); hold on; plot(dutyflow,polyval(K_lin_omega,dutyflow),'c')

disp(K_lin_omega)
%% Section 2
figure;
h = linspace(0,10,100);
omega = linspace(0.1,1,100);
for i = 1:length(h)
    H = h(i);
    for j = 1:length(omega)
        w = omega(j);
        Q(i,j) = q_quadratic_form(K_P,K_S,w,H );
    end
%     plot(Q(i,:)); hold on
end
% figure
% for j = 1:length(omega)
%     plot(Q(:,j),'r');hold on
% end
figure
contourf(omega,h,Q)
xlabel('Omega fraction of pump speed');
ylabel('h (m)')
