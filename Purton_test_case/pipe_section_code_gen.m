% short scrip to create pipe section matrix to be included inthe opti test
% algoythm for pipes and replace the flawed convex approximation scheme
%% pipe data
clear all
% just a short fix (need on e for each pipe)

pipedata(1).data = [0.5,0.001,1500,2,5]; %D,e,L,vmax,pmax,
pipedata(2).data = [0.1,0.001,500,2,5];
pipedata(3).data = [0.9,0.001,1500,5,5];

pipe_number = 1;
n_pipes = 3;
for i = 1:n_pipes
[pipe(i).m_data , pipe(i).C_data ,pipe(i).Q_lim_data  ]  = PieceWiseApproxGen(pipedata(i).data(1),pipedata(i).data(2),pipedata(i).data(3),pipedata(i).data(4),pipedata(i).data(5),'off');
length_m_part(i) = length(pipe(i).m_data);
end
length_m_part = (length_m_part-2)*2 + 1; % I hope this is the correct final size of the m vector
length_m = sum(length_m_part); % needed to make lambda matrix of the correct size

%% make pipe matracies
% for each pipe
m(1) = pipe(pipe_number).m_data(1);
c(1) = pipe(pipe_number).C_data(1);
q_min(1) = -pipe(pipe_number).Q_lim_data(2);
q_max(1) = pipe(pipe_number).Q_lim_data(2);
for i = 2:length(pipe(pipe_number).m_data)-1;
    %+ve
    m_pos(i) = pipe(pipe_number).m_data(i);
    c_pos(i) = pipe(pipe_number).C_data(i);  
    q_min_pos(i) = pipe(pipe_number).Q_lim_data(i);
    q_max_pos(i) = pipe(pipe_number).Q_lim_data(i+1);
    %-ve
    m_neg(i) = pipe(pipe_number).m_data(i);
    c_neg(i) = -pipe(pipe_number).C_data(i);  
    q_min_neg(i) = -pipe(pipe_number).Q_lim_data(i+1);
    q_max_neg(i) = -pipe(pipe_number).Q_lim_data(i);
    m = [m m_pos(i) m_neg(i)]; 
    c = [c c_pos(i) c_neg(i)];
    q_min = [q_min q_min_pos(i) q_min_neg(i)];
    q_max = [q_max q_max_pos(i) q_max_neg(i)];
end
 
%% make the matrix 
% A %%%%%%%%%%%%%
% interin crap
N = 3;
M = 5;
connection_matrix = [...
    1 -1 0;... %q1
    0 1 -1;... %q2
    0 0  1;... % demand out
    0 1  0];
[np, nn] = size(connection_matrix);
flow_matrix = eye(np);

% n_pipes = 1;
% pipe_number = 2;
i = pipe_number;
n_pumps = [3];
            T = zeros(N*4*length(m)+N,N*sum(n_pumps)); % assuming all curves are used for the pipe definition
            
            h =[place_matrix(place_matrix(eye(N),-connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
                place_matrix(place_matrix(eye(N), connection_matrix(i,:),2),ones(length(m),1),1);... %For the curves
                zeros(2*length(q_min)*N,size(connection_matrix,2)*N); %For switching lambda on
                zeros(N,size(connection_matrix,2)*N)]; %lambda summation
            
            q = [place_matrix([place_matrix(eye(N),-m,1);...  %for curves
                place_matrix(eye(N),m,1);...
                place_matrix(eye(N),ones(length(q_min),1),1);...  for switching lambda
                place_matrix(-eye(N),ones(length(q_max),1),1)],flow_matrix(i,:),2); %massive vertical stack :)
                zeros(N,size(flow_matrix(i,:),2)*N)]; %lambda summation
                
 driver_pipes = zeros(1,n_pipes); driver_pipes(pipe_number) = 1; % make the driver vector for which one is active (does not consider different lengths of m vector!!)
       lambda = [place_matrix(place_matrix(M*eye(N*length(m)),ones(4,1),1),driver_pipes,2);... % positive or negative flow in pipe
                 place_matrix(repmat(eye(N),1,length(m)),driver_pipes,2)]; %lambda summation non variable yet!!
            
             b = [ place_matrix(ones(1,N), c,2)    + M... % for curves
                  place_matrix(ones(1,N),-c,2)    + M...
                  place_matrix(ones(1,N),q_max,2) + M... %for switching lambda
                  place_matrix(ones(1,N),-q_min,2)+ M...
                  ones(1,N)]; %lambda summation

 
size(T)
size(h)              
size(q)
size(lambda)
size(b)