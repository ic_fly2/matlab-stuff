% linearistaion_test
%qt = u1^2 + u2^2
q = 270:290;
t = [1 0];

u1 = 0.5*(q + t(1));
u2 = 0.5*(q - t(1));

plot(u1,u1.^2)
hold on
plot(u2,u2.^2)
% 
Xq = [280 285];
% Vq = interp1(1:21,u1.^2,'linear','pp');

[lambda1,S1] = polyfit(u1,u1.^2,1);
[lambda2,S2] = polyfit(q,u2.^2,1);

plot(u1, lambda1(1)*u1 + lambda1(2),'o', u1,u1.^2,'b')