% script sets the time dependant outflows at the nodes, defines the
% pipe dimensions and sets the tank elecation
%% demand nodes
% demand node information including name elevation and interval and demand
% in m3/h
Node(:,:,1) = [1 50; ...
               1 0 ;...
               2 0;...
               3 0;...
               4 0;...
               5 2;...
               6 4;...
               7 9;...
               8 7;...
               9 4;...
              10 0;...
              11 0;...
              12 0;...
              13 0;...
              14 0;...
              15 0;...
              16 0;...
              17 3;...
              18 5;...
              19 10;...
              20 5;...
              21 2;...
              22 0;...
              23 0;...
              24 0;...
              ];
          
for i = 2:8
   for h = 1:24
      Node(1,1,i) = i;
      Node(h+1,1,i) = h;
      Node(h+1,2,i) = Node(h+1,2,1) + rand*5;
   end
end
No_nodes = size(Node,3);
% % plot demands to check for sensibleness
%           for i = 1:No_nodes
%               plot(Node(2:25,1,i), Node(2:25,2,i))
%               axis([1 24 0 20])
%                waitforbuttonpress % click yor way to bliss
%           end
%% pipe definitions
  %  D    e   L   elevation change  
     info_pipes = ... % pipes 
   [0.5 0.005 10 20;... % pipe 1 
    0.5 0.005 10 50;... % pipe 2 etc
    0.5 0.005 10 10;...
    0.5 0.005 10 30; ...
    0.5 0.005 10 20; ...
    0.5 0.005 10 15;...
    0.5 0.005 10 5; ...
    0.5 0.005 10 7; ...
    0.5 0.005 10 90];

%%  Set tank level and area
Area = 500; %m^2 surface area of resevoir
Base_elevation = sum(z);

%% Pump definition

pump_def_A = -2;
pump_def_B = -1;
pump_def_C = 20000;
eff_max = 0.75;

Q_max1 = roots([pump_def_A pump_def_B pump_def_C]);
Q_max1 = Q_max1(Q_max1 > 0);

electricity_price(1:24) = 0.24;
electricity_price(7:9) = 0.48;
electricity_price(18:21) = 0.48;
pump_schedule(1:24) = 1;
pump_schedule = [1 2 3 0 1 2 3 0 2 1 0 2 0 2 3 1 2 3 1 2 2 1 3 0 ]; 

save('Nodes_14_12_2013.mat');
