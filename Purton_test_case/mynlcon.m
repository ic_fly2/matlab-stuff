function c = mynlcon(x,r)
%Nonlinear callback with adjustable number of constraints

N = length(r);

%Only preallocate if not solving with a white-box optimization solver
if(isa(x,'double'))
  c = zeros(N,1);
end

%Fill in each constraint equation (slow, but easy to read)
for i = 1:N
    c(i) = x(i)*x(2*N+i) - r(i)*x(i)*x(N+i) + (1-r(i))*x(N+i)*x(2*N+i);
end