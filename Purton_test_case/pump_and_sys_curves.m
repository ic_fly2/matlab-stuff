
% from EPAnet
% clear all;
%% Pump and sys curves
%% pump
% pump_data =  importdata('E:\Phd\Hydraulics\EPAnet\Etaline_125-160.txt');%  [40.0000   33.5000;   90.0000   33.0000;  150.0000   31.0000;  190.0000   28.9000;  250.0000   24.4000;  300.0000   19.0000];
% pump_data = pump_data.data ;
% X = pump_data(:,1); %m3/0.5h !!!
% Y = pump_data(:,2); 
% K_P = polyfit(X,Y,2);

% values
% K_Pa = -0.0002;
% K_Pb = 0.0214;
% K_Pc = 32.9480;

%% system
% only one pipe (link 1) is relevant
% need to find the compunents of the system curve static and dynamic head
% loss: produce points for a polynomial and then fit tha data through

% % static
% elevation = 20; %m from model
% H_max = 2; %H_max
% H_min = 0; %H_max
% 
% %dynamic
% Q_test = 50:20:320; %m3/h
% Q_test = Q_test/(60*60); %m3/s !!!
% for i = 1:length(Q_test)
%     [friction re regime Head_loss ~]= DarcyWeisbachFactorCalc(0.5,Q_test(i),0.001,5000);
%     H(i) = Head_loss;
% end
% 
% delta_H_1 =  elevation+H_min+H;
% Q_test_half = Q_test*60*60;
% K_S = polyfit(Q_test_half,delta_H_1,2);
clear all;

pump_curves = [-50	0	326.88245	;...
               -75	0	390.323675	;...
               -100	0	453.7649	;...
               -125	0	517.206125	;...
               -150	0	580.64735	;...
               -175	0	644.088575	;...
               -200	0	707.5298	;...
               -225	0	770.971025	;...
               -250	0	834.41225	;...
               -275	0	897.853475	;...
               -300	0	961.2947	;...
               -325	0	1024.735925	;...
               -350	0	1088.17715	];


           
system_curves = [15.95195399	0	159.5195399;...
                 26.53347217	0	132.6673609;...
                 39.70105897	0	99.25264742;...
                 47.57016738	0	79.28361231;...
                 52.80320325	0	66.00400407;...
                 56.53472122	0	56.53472122;...
                 65.8403917     0	32.92019585;...
                 73.05538438	0	14.61107688;...
                  8.874040056	0	177.4808011;...
                  4.701717295	0	188.0686918;...
                  1.950503078	0	195.0503078];


q = 0:0.1:2;
for j = 1:length(pump_curves)
    h_p(j,:) = polyval(pump_curves(j,:),q');
    p = plot(q,h_p(j,:)); hold on;
end
for i = 1:length(system_curves)    
    h_s(i,:) = polyval(system_curves(i,:),q);
    s = plot(q,h_s(i,:),'r'); hold on;
end
axis([0 max(q) 0 max(max(h_p))+50]);
xlabel('Flow rate (m^3 /s)');ylabel('Head (m)')
legend([p s],'pumps','systems')
    %% linearisation stuff
% % total
% %min
% delta_H_1 =  elevation+H_min+H;
% Q_test_half = Q_test*60*60;
% K_S_min = polyfit(Q_test_half,delta_H_1,2);
% 
% %max
% delta_H_2 =  elevation+H_max+H;
% K_S_max = polyfit(Q_test_half,delta_H_2,2);
% 
% a_1 = K_P(1)-K_S_min(1);
% b_1 = K_P(2)-K_S_min(2);
% c_1 = K_P(3)-K_S_min(3);
% 
% a_2 = K_P(1)-K_S_max(1);
% b_2 = K_P(2)-K_S_max(2);
% c_2 = K_P(3)-K_S_max(3);
% 
% %% finding duty points
% % first roots
% solution_min_1 = -b_1 - (b_1^2 - 4*a_1*c_1)^0.5;
% Q_max = solution_min_1/(2*a_1);
% 
% solution_max_2 = -b_2 - (b_2^2 - 4*a_2*c_2)^0.5;
% Q_min = solution_max_2/(2*a_2);
% 
% Q_eval = linspace(Q_min,Q_max,20);
% H_P = polyval(K_P,Q_eval);
% [lambda1,S1] = polyfit(Q_eval,H_P,1);
% 
% H_S_min = polyval(K_S_min,Q_eval);
% [lambda2,S2] = polyfit(Q_eval,H_S_min,1);
% 
% % H_S_max= polyval(K_S_max,Q_eval); % not needed
% % [lambda3,S3] = polyfit(Q_eval,H_S_max,1);
% %% plotting for check
% % comment for normal operations
% Q_av = (Q_max + Q_min)/2;

