%% info
% test case curves for purton trunk mains. Curves fitted to estimated data
% by hand. System stats:
% 100m resevoir elevation
% 3 pumps at 1MW each 80% efficient
% system curves assumed:

% system = 0.00005.*x.^2 + 100; %flow in m3 per 1/2 hour intervall!!
% pump = -0.00003.*x.^2 + 180;

%% test case %enable only this section to run in optimsation_fx...
% 
% for i = 1:no_pumps
%     sys = [K_S(1) K_S(2) h(t)+elevation];
%     pump = [K_P(1)/(i^2) K_P(2)/i K_P(3)];
%     solution = roots(pump - sys);
%     Qp(t,i) = solution(max(imag(roots(pump - sys))==0)); % flow in m3/h!!!!
% end

%% system visualistaion
elevation = 29;
h_max = 2;
h_min = 0;

%% polynomials
pump = [-0.00003 0 180];
 sys1 = [0.00005 0   elevation+h_min]; % lower limit h_min
 sys2 = [0.00005 0   elevation+h_max]; % upper limit h_max
 sys3 = [0.00005 0   elevation+(h_max+h_min)/2 ];
%  power = [-0.06 220]; % efficiancy concerns

%% duty points
duty_pt_1 = max(roots(sys1-pump));
duty_pt_2 = max(roots(sys2-pump));
duty_pt_3 = max(roots(sys3-pump));

%% linearisation
% make mx +c type curves for system curve.
m1 = sys1(1)*duty_pt_1*2 + sys1(2);
m2 = sys2(1)*duty_pt_2*2 + sys2(2);
m3 = sys3(1)*duty_pt_3*2 + sys3(2);

m = [m1 m2 m3];
S1 = mean(m);
% S2 = elevation;
S2 = (sys1(1)*duty_pt_1^2 - sys1(2)+m1 - sys1(3))*-1;

lin = [S1  S2];

 


%% curves
q = 1:2000;
y1 = polyval(sys1,q);
y2 = polyval(sys2,q);
y3 = polyval(sys3,q);
z = polyval(pump,q);
x = polyval(power,q);
l = polyval(lin,q); 
	
plot(y1);hold on;plot(z,'c');plot(l,'r')

%% linearisation
Q1 = max(roots(pump - sys1));
Q2 = max(roots(pump - sys2));
Qlin = (Q1+Q2)/2;
Linear_pump = [pump(1)*2*Qlin  polyval(pump,Qlin)-pump(1)*2*Qlin*Qlin];
lin_pump = polyval(Linear_pump,Q2:Q1);
plot(Q2:Q1,lin_pump,'r')
Linear_sys = [sys(1)*2*Qlin  polyval(sys3,Qlin)-sys3(1)*2*Qlin*Qlin];
lin_sys = polyval(Linear_sys,Q2:Q1);
plot(Q2:Q1,lin_sys,'r')