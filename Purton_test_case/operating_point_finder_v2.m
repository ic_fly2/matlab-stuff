% system_curve_builder_v2;
% clf

%% load pump settings
if plot_on == 1
figure;
else
end
for k = 1:no_pumps
pump_constants(1:length(system_constants),4) = 0;
pump_constants(3,k) =  pump_def_A*(1/k)^2;
pump_constants(2,k) =  -pump_def_B/k;
pump_constants(1,k) =  pump_def_C;    
H_P_multi(k,:)= pump_constants(3,k)*(Q).^2 + pump_constants(2,k)*(Q) + pump_constants(1,k);

intersection_poly = system_constants - fliplr(pump_constants(:,k)');
r = roots(intersection_poly);

% find intersections
Q_operation(k) = max(r(imag(r) == 0)); % more robust version: r(abs(imag(r)) <= 0.01)
H_operation(k) = pump_constants(3,k)*Q_operation(k)^2 + pump_constants(2,k)*Q_operation(k) + pump_constants(1);

Q_max(k) = Q_max1*k;

if plot_on == 1
hold on
plot(Q,H_P_multi(k,:),'r',Q,H_loss,'b',Q_operation(k),H_operation(k),'ro', 'MarkerSize', 18)
axis([0 max(Q_operation)*1.2 0 max(H_operation)*1.2])
else
end
end
%% estimate efficiency
Q_eff = Q_max/2;
deviation = Q_operation - Q_eff;
eff = eff_max - abs(deviation./Q_max);

%% calculate head profile for pump on
for j = 1:length(Q_operation)
for i = 1:No_nodes+1 % loop over pipes
    if i == 1
        Q_main(i,j+1) = Q_operation(j) ;
        stag_point = 1;
        direction = 1; 
    elseif sum(q(1:i-1)) <  Q_operation(j)         
        Q_main(i,j+1)  = Q_operation(j) - sum(q(1:i-1));
        stag_point =  stag_point +1; % set this point as still fully supplied by pump
    else
        direction = -1;
        remainder_flow = abs(Q_operation(j) - sum(q(1:stag_point))); % flow the pump can't provide but needs to
        Q_main(i,j+1) = (remainder_flow + sum(q(stag_point:i-2)));
        % flow is in opposite direction
    end
    
    [f,~] = DarcyWeisbachFactorCalc( D(i), Q_main(i,j+1),e(i) ,L(i)) ; % find friction factor
    Head_loss(i,j+1)= (z(i)+ f * (L(i)/D(i))*Q_main(i,j+1)^2/(2*9.81*((D(i)/2)^2 * pi)^2))*direction;% find delta H due to friction and elevation change
%     Head_loss_total(i,j) = sum(Head_loss(1:i),j);
Q_main(i,j+1) = Q_main(i,j+1)*direction;
end
Head_loss_total(:,j+1) = cumsum(Head_loss(:,j+1),1);
Head(:,j+1) = H_operation(j) -  Head_loss_total(:,j+1) + Tank_elevation(t);
end

%%  calculate head profile for pump off
for i = 1:No_nodes+1
   Q_main(i,1) = sum(q(1:i-1));
   [f,~] = DarcyWeisbachFactorCalc( D(i), Q_main(i,1),e(i) ,L(i)) ;
   Head_loss(i,1)= (z(i)+ f * (L(i)/D(i))*Q_main(i,1)^2/(2*9.81*((D(i)/2)^2 * pi)^2))*-1;
   Q_main(i,1) = Q_main(i,1)*-1;
   if Q_main(i,1) == 0 % avoid summation errors
       Head_loss(i,1) = 0;
   end
       
   
end
% Head_loss(1,1) = 0; % is NaN as no flow but needs to be zero to allow summation
Head_loss_total(:,1) = cumsum(-Head_loss(:,1),1); % minus as flow is reversed
Head(:,1) = Head_loss_total(:,1) + Tank_elevation(t);
if plot_on == 1
figure;
plot(Head(:,1)) % plot the head loss accros the main
else
end
%% power calculation
% fluid % load fluid information
power = rho*9.81*Q_operation.*H_operation./eff;
power = [0 power];
% subtract power delivered to resevoir as this is energy stored for the
% future 
% tank_flow = Q_main(No_nodes+1,:);
% pwr_loss = power - rho*9.81*tank_flow*Tank_elevation(t);
% % clear Q_main

% plot(Q,H_loss,'g',Q,H_P_multi(2,:),'c',Q,H_P_multi(1,:),'b',Q_intersect,H_intersect,'ro', 'MarkerSize', 18)

% %% find operating point for different pump settings
% % find roots
% intersection_poly = fliplr(system_constants) - pump_constants;
% r = roots(fliplr(intersection_poly));
% 
% % find intersections
% Q_intersect = r(imag(r) == 0); % more robust version: r(abs(imag(r)) <= 0.01)
% H_intersect = pump_constants(3)*Q_intersect^2 + pump_constants(2)*Q_intersect + pump_constants(1);
% 
% 
% plot(Q,H_loss,'g',Q,H_P_multi(2,:),'c',Q,H_P_multi(1,:),'b',Q_intersect,H_intersect,'ro', 'MarkerSize', 18)
% 
% % for several pumps
