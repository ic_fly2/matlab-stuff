%system curve building
% establish the flows in the pipe indipendand of pump low then find head
% losses for a range of Qs and fit a curve

%sum losses for individual sections
% clear all;
%% parameters

% define flows
% q = [ 1 5 2 6 4 8 5 8]; % test wise fixed allocation of demands at sum nodes
for i = 1:No_nodes
q(i) = Node(t+1,2,i);
end



% Q = 1:2:sum(q)*2; 
Q = 1:2:200; 
% No_nodes = length(q); 
%define pipes

D = info_pipes(:,1);
e = info_pipes(:,2);
L = info_pipes(:,3);
z = info_pipes(:,4); % positive is up in pumping direction


Tank_elevation(1:N) = Base_elevation + resevoir_level(1:N)  ;


if length(D) == No_nodes+1 %catch errors in network design
else
    error('input dimension do not match, need one more pipe than nodes')
end


% add some zeros for speed
Flow = zeros(No_nodes+1,length(Q));
f = zeros(No_nodes+1,length(Q));
delta_H_N = zeros(No_nodes+1,length(Q));
%% loop
for i = 1:No_nodes+1 % loop over pipes
    for j = 1:length(Q) % for all flows
        if i == 1
        Flow(i,j) = Q(j)  ;
        else         
        Flow(i,j) = Q(j) - sum(q(1:i-1));
        end
    [f,~] = DarcyWeisbachFactorCalc( D(i),Flow(i,j),e(i) ,L(i)) ; % find friction factor
    delta_H_N(i,j)= z(i)+ sign(Flow(i,j))* f * (L(i)/D(i))*(Flow(i,j)^2)/(2*9.81*((D(i)/2)^2 * pi)^2);% find delta H due to friction and elevation change
    end
end
%% treat results to find revers and zero flows 
delta_H_N(isnan(delta_H_N)) = 0 ;
p = sign(Flow)==1; 
H_loss = sum(delta_H_N.*p,1) + Tank_elevation(t); % remove all flows with revers or zero flow as they are not fed with pump head
%% summarise results and find system curve
% sum head loss for each pump flow case
% delta_H = sum(delta_H_N,1);

system_constants = polyfit(Q,H_loss,3);

% evaluation of system curve
%  f = polyval(system_constants,Q);
%  plot(Q,H_loss,'o',Q,f,'-')
%%  deal with the revers flows 

% in new script
% %% find operating point for different pump settings
% % load pump curve
% % for one pump
% pump_constants(1:length(system_constants)) = 0;
% pump_constants(3) =  -2;
% pump_constants(2) =  -1;
% pump_constants(1) =  20000;
% H_P = pump_constants(3)*Q.^2 + pump_constants(2)*Q + pump_constants(1);
% % find intersection
% intersection_poly = fliplr(system_constants) - pump_constants;
% r = roots(fliplr(intersection_poly));
% Q_intersect = r(find(imag(r) == 0));
% H_intersect = pump_constants(3)*Q_intersect^2 + pump_constants(2)*Q_intersect + pump_constants(1);
% plot(Q,H_loss,'g',Q,H_P,'b',Q_intersect,H_intersect,'ro', 'MarkerSize', 18)
