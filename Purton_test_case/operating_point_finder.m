% system_curve_builder_v2;
% clf

%% load pump settings
% load pump curve
% for one pump
eff_max = 0.75;
pump_constants(1:length(system_constants)) = 0;
pump_constants(3) =  -2;
pump_constants(2) =  -1;
pump_constants(1) =  20000;
Q_max1 = roots(fliplr(pump_constants));
Q_max1 = Q_max1(Q_max1 > 0);
H_P= pump_constants(3)*Q.^2 + pump_constants(2)*Q + pump_constants(1);
% for multiple pumps
for k = 1:3
pump_constants(1:length(system_constants),6) = 0;
pump_constants(3,k) =  -2*(1/k)^2;
pump_constants(2,k) =  -1/k;
pump_constants(1,k) =  20000;    
H_P_multi(k,:)= pump_constants(3,k)*(Q).^2 + pump_constants(2,k)*(Q) + pump_constants(1,k);

intersection_poly = system_constants - fliplr(pump_constants(:,k)');
r = roots(intersection_poly);

% find intersections
Q_intersect(k) = max(r(imag(r) == 0)); % more robust version: r(abs(imag(r)) <= 0.01)
H_intersect(k) = pump_constants(3,k)*Q_intersect(k)^2 + pump_constants(2,k)*Q_intersect(k) + pump_constants(1);

Q_max(k) = Q_max1*k;

% hold on
% plot(Q,H_P_multi(k,:),'r',Q,H_loss,'b',Q_intersect(k),H_intersect(k),'ro', 'MarkerSize', 18)

end
%% estimate efficiency
Q_eff = Q_max/2;
deviation = Q_intersect - Q_eff;
eff = eff_max - abs(deviation./Q_max);

%% calculate head profile
for j = 1:length(Q_intersect)
for i = 1:No_nodes+1 % loop over pipes
    if i == 1
        Q_main(i,j) = Q_intersect(j) ;
    elseif sum(q(1:i-1)) <  Q_intersect(j)         
        Q_main(i,j)  = Q_intersect(j) - sum(q(1:i-1));
    end
    
    [f,~] = DarcyWeisbachFactorCalc( D(i), Q_main(i,j),e(i) ,L(i)) ; % find friction factor
    Head_loss(i,j)= z(i)+ f * (L(i)/D(i))*Q_main(i,j)^2/(2*9.81*((D(i)/2)^2 * pi)^2);% find delta H due to friction and elevation change
%     Head_loss_total(i,j) = sum(Head_loss(1:i),j);
end
Head_loss_total(:,j) = cumsum(Head_loss(:,j),1);
Head(:,j) = H_intersect(j) -  Head_loss_total(:,j) + Tank_elevation;

end

% plot(Head(:,1)) % plot the head loss accros the main

%% power calculation
fluid % load fluid information
power = rho*9.81*Q_intersect.*H_intersect./eff;

% subtract power delivered to resevoir as this is energy stored for the
% future 
tank_flow = Q_main(No_nodes+1,:);
pwr_loss = power - rho*9.81*tank_flow*Tank_elevation;


% plot(Q,H_loss,'g',Q,H_P_multi(2,:),'c',Q,H_P_multi(1,:),'b',Q_intersect,H_intersect,'ro', 'MarkerSize', 18)

% %% find operating point for different pump settings
% % find roots
% intersection_poly = fliplr(system_constants) - pump_constants;
% r = roots(fliplr(intersection_poly));
% 
% % find intersections
% Q_intersect = r(imag(r) == 0); % more robust version: r(abs(imag(r)) <= 0.01)
% H_intersect = pump_constants(3)*Q_intersect^2 + pump_constants(2)*Q_intersect + pump_constants(1);
% 
% 
% plot(Q,H_loss,'g',Q,H_P_multi(2,:),'c',Q,H_P_multi(1,:),'b',Q_intersect,H_intersect,'ro', 'MarkerSize', 18)
% 
% % for several pumps
