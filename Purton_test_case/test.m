%check valve concept test

clear all
N = 1000;
Dh = rand(N,1)*10-5;
q = rand(N,1)-0.5;

for i = 1:N
    if q(i) >= 0 
        plot(q(i),Dh(i),'r+'); hold on
    end
end
axis([-0.5 0.5 -5 5])