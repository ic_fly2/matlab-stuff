function [ Q] = q_quadratic_form(Kp,Ks,omega,h )
%UNTITLED Summary of this function goes here Kp is the pump vector, Ks the
%system vector, omega the speed as fraction of the speed for Kp and h the resevoir altitude
%   Detailed explanation goes here

% test
% Kp = [   -0.002   0   28];
% Ks = [0.000002392368680   0.000028961696068  19.999680417148980];;
% omega = 1;
% h = 8;

    a = Kp(1)/omega-Ks(1); 
    b = Kp(2)/omega-Ks(2);
    c = Kp(3)-Ks(3)-h;
    % solution
    q = roots([a b c]);
    
%     clean the solution
    if max(q) >= 0 && imag(max(q)) == 0
        Q = max(q) ;
    else 
        Q = NaN;
    end
end

