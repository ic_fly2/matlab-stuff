clear all
%% Problem of the form
% min qB + Pe*q/q_old
% s.t. mass and energy constraints
no_pumps = 1;
N = 5;
L = 10000;
Kc = 5

%% objective functions
q_est = 0.4+(rand(1,N)-0.5)/10 ;
c = [ 0.0001*ones(1,N)./q_est zeros(1,2*N)];

H = [eye(N) eye(N)*L  zeros(N); ...
    eye(N)*L zeros(N) zeros(N);...
    zeros(N) zeros(N) zeros(N)];
%% eq
%mass balance
% Aeq = [eye(N) zeros(N) (diag(linspace(-1,-1,N-1),-1)+eye(N))*Area];
beq = rand(1,N-1);

%energy balance
% Kk %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i = 1:no_pumps
ds = 2*K_S(1)*q_old + K_S(2); % derivative version
dp(i) = 2*K_P(1,i)*q_old + K_P(2,i);
K_k(i) = ds - dp(i);
end

no_pumps = 3;
K_k = [2 3 5];

c = ones(1,N)*K_k(1) ;
for i = 2:no_pumps
c = [c ones(1,N)*K_k(i)];
end
c = diag(c);

d = eye(N);
for i = 2:no_pumps
d = [d; eye(N)];
end
Aeq = [c c d; repmat(eye(N),1,no_pumps) zeros(N,N*no_pumps) diag(ones(1,N-1)*-1,-1) + diag(ones(1,N)) ];
% Aeq = [Aeq; Kc*eye(N) Kc*eye(N) eye(N)];
beq = [beq  rand(1,N)]; %% work on beq


%% ineq
A = Aeq(1,:);
Aeq(1,:) = [];
b = rand;

%% lb
lb = [zeros(1,no_pumps*2*N) zeros(1,N)];
ub = [ones(1,no_pumps*2*N)*10000000 ones(1,N)*5];

% optimisation
x = quadprog(H,c,A,b,Aeq,beq,lb,ub)
% [Y, fval, exitflag, output] = cplexqp(H,c',A,b,Aeq,beq',lb',ub');