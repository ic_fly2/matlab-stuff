%% linearisation test
%values
q_new = 0.8;
T_new = 1;

q = 0.440;
T = 0.0;
%% eq
u_1 = 0.5*q + 0.5*T;
u_2 = 0.5*q - 0.5*T;

du_1dq = 0.5*(q + T);
du_1dT = 0.5*(q + T);
du_2dq = 0.5*(q - T);
du_2dT = 0.5*(-q + T);

u1_est = du_1dq *q_new + du_1dT *T;
u2_est = du_2dq *q_new + du_2dT *T;

qT_est = (du_1dq -du_2dq)*q_new + (du_1dT-du_2dT)*T_new
qT = q_new*T_new