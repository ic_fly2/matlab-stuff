% quadratic constrained problems
clear all
%% parameters
N = 3; % number of intervals
t = 30; %length of one interval
h_t_min = 10; %minumum tank level
h_t_max = 22.8;
q_p =300; %pump flow
A_t =176.7; %resevoir area
h1 = 10; %initial level
q_p_min = 0;
q_p_max = 200;
% pipeline get average type from system curve builder
L = 2000;
D = 0.8;
A = 0.5;
e = 0.0005;

delta_h = q_p*0.06/A_t; %change in resevoir height for pump inflow

%% vectors
% electricity cost and water demand
Pe(1:12) = 28; Pe(13:20) = 85; Pe(21:34) = 48; Pe(35:40) =48; Pe(41:N) = 28; 
d = [60 58 58 57 56 57 58 60 60 61 62 70 80 90 110 107 105 104 103 102 100 99 105 98 90 89 87 86 85 84 83 82 81 82 82 83 84 84 75 70 74 73 72 71 65 62 60 53]; 
h = zeros(N,1);
q = zeros(N,1);
f = [Pe h']; % c^T
%% pump and system settings
P1 = -250;
P2 =55 ;
P3 = 500;

% system


for i = 1:N
S1(i) = DarcyWeisbachFactorCalc(D,d(i),e,L)*L/(D*2*9.81*A)*L ;% times extra stuff (check formula)
end
S2(1:N) = 0;
S3(1:N) = 0;
%% Q
for i = 1:N
    Q(:,:,i)= zeros(3*N);
    U_T = triu(ones(N,N),0); % change from triangular to identity
    Q(2*N+1:3*N,1:N,i) = U_T;
end

for i = N+1:2*N
    Q(:,:,i)= zeros(3*N);
    I = eye(N) * (P1 - S1(i-N));
    Q(2*N+1:3*N,2*N+1:3*N,i) = -I;
end

Q(:,:,2*N+1)= zeros(3*N); % sot semi definite yet (or positive for that matter)

%% A

A(1,1:3*N,1:2*N +1) = 0;

for i = N+1:2*N
    A(1,i,i) = 1; % s3 as height of tank
end

for i = 2*N+1:3*N
    A(1,i,i-N) = P2; % add second pump coefficient
end

A(1,2*N,2*N +1) = 1; %last hight is same as first hight

%% b
for i = 1:N
    b(i) = sum(d(1:i))*0.06/A_t - h1;
end

for i = N+1:2*N
    b(i) = -P3 + S3(i-N); b(i) = b(i)*-1; 
end
b(2*N+1) =0;
%% bounds
ub(1:N) = t;
ub(N+1:2*N) = h_t_max;
ub(2*N+1:3*N) = q_p_max;

lb(1:N) = 0;
lb(N+1:2*N) = h_t_min;
lb(2*N+1:3*N) = q_p_min;

lb = lb';
ub =ub';

%% solve
% use a cone solver or some ssemi definite programming to solve this. CPLEX should be able to do this

% for i = 1:2*N+1
%  assignin('base',['Q' num2str(i)],Q(:,:,i))
%  assignin('base',['A' num2str(i)],A(:,:,i))
%  assignin('base',['b' num2str(i)],b(i))
% end
% 
% cvx_begin
%     variable x(3*N)
%   0.5*x'*Q1*x >= b1
%   0.5* x'*Q2*x+A2'*x <= 0
%   0.5* x'*Q3*x+A3'*x <= 0
%   0.5* x'*Q4*x+A4'*x <= 0
%   0.5* x'*Q5*x+A5'*x <= 0
%   0.5* x'*Q6*x+A6'*x <= 0
%   0.5* x'*Q7*x+A7'*x <= 0
%     x >= 0
%     minimize(f'*x)
% cvx_end
   