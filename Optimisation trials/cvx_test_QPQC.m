close all; clear; clc
clear all;
%% QC problem test
N = 5;
%from excell quadratic
K_P = [-248.015873	0	248.015873];
K_S = [450.8566276	0	112.7141569];
% H = rand(n); H = H*H'; % make spsd
f(1:N) = 50;
f(N+1:2*N) = 0;
f(2*N+1:3*N) = -1;

% Q1 = rand(n); Q1 = Q1*Q1'; % make spsd
Q1 = [zeros(N) zeros(N) eye(N)*(K_S(1)-K_P(1)); ...
    zeros(N) zeros(N) zeros(N);...
    eye(N)*(K_S(1)-K_P(1)) zeros(N)  zeros(N)];
g1(3*N,1) = 0;
Q2 = rand(3*N); Q2 = Q2*Q2'; % make spsd
g2 = rand(3*N,1);
b1(1,3*N) = 10000 ;
b2 =  rand(3*N,1);
cvx_begin
    variable x(3*N)
    0.5*x'*Q1*x+g1'*x <= b1
    g2'*x == b2
    x >= 0
    minimize(f'*x)
cvx_end
