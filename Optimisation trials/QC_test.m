clear all;
%% QC problem test
N = 48;
%from excell quadratic
K_P = [-248.015873	0	248.015873];
K_S = [450.8566276	0	112.7141569];

%m3/s to l/s
% % K_P(1) = K_P(1)/(1000*1000);
% % K_P(2) = K_P(2)/(1000);
% % K_S(1) = K_S(1)/(1000*1000);
% % K_S(2) = K_S(2)/(1000);

% inequalty
% % Q = eye(N)*(K_S(1)-K_P(1));
% % l(1:N,1) = (K_S(2)-K_P(2));
% % r =  eye(N)*(K_P(3)-K_S(3));

% linear
K_PL = [-218.2539683	296.031746];
K_SL = [396.7538323	25.4283138];
K_SL(1) = K_SL(1)/(1000);
K_PL(1) = K_PL(1)/(1000);

Aeq = eye(N)*(K_PL(1)-K_SL(1) );
beq(1:N,1) = -(K_PL(2)-K_SL(2) );


% objective funtcion
f(1:N,1) = -1;
H =[];


%bounds
lb(1:N,1) = 0;
ub = [];

problem.f = f;
problem.lb = lb;
problem.ub = ub;
problem.H =[]; %	Double matrix 	Matrix for objective function
problem.Aineq =[];	%Double matrix 	Linear inequality constraints
problem.bineq =[];	%Double column vector 	Righthand side for linear inequality constraints

% problem.Aeq =[];	%Double matrix 	Linear equality constraints
problem.Aeq = Aeq;

problem.beq = beq ;	%Double column vector 	Righthand side for linear equality constraints
problem.x0 =[];
%   	options

% quadratic inequalty for flow
% for i = 1:N
% problem.qc(i).a = (K_S(2)-K_P(2));
% problem.qc(i).rhs = (K_P(3)-K_S(3));
% problem.qc(i).Q = (K_S(1)-K_P(1));
% end
[x, fval, exitflag, output]= cplexqcp(problem)
% x = cplexqcp(H,f,H,H,H,H,H,H,H,lb,ub)
% x = cplexqcp(H,f,H,H,H,H,l,Q,r,lb,ub)

