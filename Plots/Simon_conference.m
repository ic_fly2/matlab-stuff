% simple plots
close all
load('Results_48_07')
load('Tariffs_n_emissions1')

day = 16; % 15\% difference!!!!
x1 = results{day,10}.schedule_tmp;
x2 = results{day,1}.schedule_tmp;

x1 = sum(x1,2);
x2 = sum(x2,2);
t = linspace(0,1,48);   
GHG = emissions(day,:);
Pe = tariff(day,:);
subplot(3,1,1)
h1 = plotyy(t,GHG,t,Pe);
title('Emissions and Day ahead prices')
datetick('x','HH:MM')
 h1(1).YLabel.String  = 'GHG emissions';
  h1(2).YLabel.String  ='� / MWh';



subplot(3,1,2)
h2 = bar(t,x1);
title('Cost optimised pump schedule')
axis([0 1 0 1])
datetick('x','HH:MM')
ylabel('Pump status')
set(gca,'YTick',[0 1])
set(gca,'YTickLabel',{'OFF' 'ON'})

subplot(3,1,3)
bar(t,x2)
title('GHG avoidance pump schedule')
axis([0 1 0 1])
datetick('x','HH:MM')
ylabel('Pump status')
xlabel('Time of day')
set(gca,'YTick',[0 1])
set(gca,'YTickLabel',{'OFF' 'ON'})



    
% for day = 1:20;
%     rate(day) = results{day,10}.GHG/results{day,1}.GHG;
% end
% 
% for i = 1: 10
%     GHG_tot(i) = results{1,i}.GHG;
%     fval_tot(i) = results{1,i}.fval;
% end
% 
% plot(fval_tot,GHG_tot,'ko')

