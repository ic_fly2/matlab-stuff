% DR_event_analysis
close all
clearvars
load('DR_event3')
norm_GHG = out_norm2_c.GHG;
norm_DR = out_norm2_c.cost;

out_30.cost(out_30.GHG == 0) = NaN;
out_30.GHG(out_30.GHG == 0) = NaN;
out_60.cost(out_60.GHG == 0) = NaN;
out_60.GHG(out_60.GHG == 0) = NaN;
out_90.cost(out_90.GHG == 0) = NaN;
out_90.GHG(out_90.GHG == 0) = NaN;
out_120.cost(out_120.GHG == 0) = NaN;
out_120.GHG(out_120.GHG == 0) = NaN;
out_150.cost(out_150.GHG == 0) = NaN;
out_150.GHG(out_150.GHG == 0) = NaN;

GHG{1} = out_30.GHG - out_norm_c.GHG;
DR{1}  = out_30.cost -out_norm_c.cost;

GHG{2} = out_60.GHG - out_norm_c.GHG;
DR{2}  = out_60.cost -out_norm_c.cost;

GHG{3} = out_90.GHG - out_norm_c.GHG;
DR{3}  = out_90.cost -out_norm_c.cost;

GHG{4} = out_120.GHG - out_norm_c.GHG;
DR{4}  = out_120.cost -out_norm_c.cost;

GHG{5} = out_150.GHG - out_norm_c.GHG;
DR{5}  = out_150.cost -out_norm_c.cost;

for i = 1:5
%     DR{i}(10:20)=[];
%     GHG{i}(10:20) =[];
    
    mean_cost(i) = nanmean(DR{i});
    median_cost(i) = nanmedian(DR{i});
    max_cost(i) = nanmax(DR{i});
    mean_GHG(i) = nanmean(GHG{i});
    max_GHG(i) = nanmax(GHG{i});
%     figure
hold on
    plot(DR{i},'+')
%     axis([0 48 0 20])
end


    

% energy = 0.5*59.44*[1:5]';
energy = 0.5*178*[1:5]';
table = [[30:30:150]' energy mean_cost'./energy max_cost'./energy  mean_GHG'./energy max_GHG'./energy];
disp(table)
array2latextable(table)
rev_min = energy*75/1000;
rev_max = energy*200/1000;
table2 = [rev_min rev_max  mean_cost'./rev_min mean_cost'./rev_max max_cost'./rev_min ];
disp(table2)
array2latextable(table2)

%% plots
% pick a schedule

settings.Time_shift = 27;
settings.mods.status = 'No';
sched_or=  out_norm2_c.sched{settings.Time_shift};
sched_mod  =  out_90.sched{settings.Time_shift};

% [H_save_or Q] = EPS_from_T0(sched_or, 'van_zyl_norm', 48,[0.8 0.5],settings);
% [H_save_mod] = EPS_from_T0(sched_mod, 'van_zyl_norm', 48,[0.8 0.5],settings);
% h_tank_mod = H_save_mod(:,7)+0.22;
% h_tank_or = H_save_or(:,7)+0.22;

[H_save_or Q] = EPS_from_T0(sched_or, 'Lecture_example_norm', 48,0.8,settings);
[H_save_mod] = EPS_from_T0(sched_mod, 'Lecture_example_norm', 48,0.8,settings);
h_tank_mod = H_save_mod(:,4)+0.22;
h_tank_or = H_save_or(:,4)+0.22;

% figure('Position',[100 100 340 340])
x = [0:0.0417/2:1]+0.0417*0.5*settings.Time_shift ;


figure('Position',[100 100 680 340])
s1 = subplot(2,1,1);
hold on
 area1 = area([min(x) min(x)+0.0417*1.5],[3 3]);
 area1.FaceColor = [0.8 0.8 0.8];
area1.EdgeColor = [0.8 0.8 0.8];

h_or = plot(x,h_tank_or,'--' );

h_mod = plot(x,h_tank_mod,'-' );
% load('flow_16')
flow_16 = Q(:,2);
h_min = flow_16*60*30*4/(pi*50^2);
h_m = plot(x,h_min);




h_leg1 = legend({'area1','h_or','h_mod','h_m'},{'DR Event','planned level','Event level','Minumum'});
h_leg1.Interpreter = 'Latex';
h_leg1.Orientation = 'horizontal';
 xlabel('Time','Interpreter','Latex' )
 ylabel('Tank level (m)','Interpreter','Latex' )
 grid minor
 datetick('x','HH:MM')
 axis([min(x) max(x) 0 3])
s2 = subplot(2,1,2);
    hold on


% equal_scheds = find((sched_mod == sched_or) == 1);
area2 = area([min(x)-0.007 min(x)+0.0417*1.25],[3 3]);
b1 =  bar(x,[sum(sched_mod,2) sum(sched_or,2)]);
datetick('x','HH:MM')

 area2.FaceColor = [0.8 0.8 0.8];
area2.EdgeColor = [0.8 0.8 0.8];
% alpha(0.5)
%  hold on; bar(sched_or,'b')
%  bar(equal_scheds,sched_mod(equal_scheds),'k')
%  bar(sched_mod,'r')
%  bar(equal_scheds,sched_mod(equal_scheds),'k')
 axis([min(x)-0.007 max(x)+0.007 0 3])
s1.Position = [0.1300 0.4838 0.7750 0.5];
s2.Position = [0.1300 0.1100 0.7750 0.2412];

 xlabel('Time','Interpreter','Latex' )
 ylabel('Pumps ON','Interpreter','Latex' )
 b1(1).BarWidth = 1.5;
 b1(2).BarWidth = 1.5;
hleg2 =  legend('Event','Mod.','orig');
hleg2.Orientation = 'horizontal';