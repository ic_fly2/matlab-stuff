close all;
clear all
colormap summer
% FFR
% first = load('Numbers_and_schedule_FFR.mat');
% second = load('Numbers_and_schedule_FFR_2.mat');
% third = load('Numbers_and_schedule_FFR_3.mat');
% fourth = load('Numbers_and_schedule_FFR_4.mat');
% GHG = [first.GHG; second.GHG; third.GHG; fourth.GHG];
% cost = [first.cost; second.cost; third.cost; fourth.cost];
% duration_record = [first.duration_record; second.duration_record; third.duration_record; fourth.duration_record];
%STOR
load('Numbers_and_schedule_STOR')
figure('Position',[100 100 350 250])
ghg_norm = GHG./duration_record/10;
ghg_norm = ghg_norm/(139/2*.7);
h1 = histogram(-ghg_norm,[0:10:500]);
h1.FaceColor = [  0    0.5000    0.4000];
% axis([0 500 0 140]); %STOR
axis([0 500 0 40]) % STOR
xlabel('GHG emissions $gCO_2/kWh$','Interpreter','Latex')
% xlabel('Cost','Interpreter','Latex')
ylabel('Frequency','Interpreter','Latex')


iters =  length(ghg_norm); 

mean_part = zeros(1,iters);
std_part = zeros(1,iters);
for i=1:iters
    mean_part(i) = mean(ghg_norm(1:i));
    std_part(i) = std(ghg_norm(1:i));
end



figure('Position',[100 100 350 250])
cost_norm = cost./duration_record;
cost_norm = cost_norm/(139/2);
% STOR ONLY
hold on;
% plot([7.5 7.5],[0 100],'k');
% plot([20 20],[0 50],'k'); 
h2 = histogram(-cost_norm,[0:1:50]);
h2.FaceColor = [  0    0.5000    0.4000];

% % plot mean vals
% str1 = ['Minimum compensation 7.5 p/kWh'];
% text(8,100,str1,'color','k','Interpreter','latex')
% str2 =['Maximum compensation'];
% text(21,50,str2,'color','k','Interpreter','latex') 
% str2a =['20 p/kWh'];
% text(21,30,str2a,'color','k','Interpreter','latex')

% plot([mean(-cost_norm) mean(-cost_norm)],[0 250],'r');
% str3 = ['Mean cost:  ' num2str(round(mean(-cost_norm),2)) ' p/kWh'];
% text(mean(-cost_norm)+1,250,str3,'color','r','Interpreter','latex')




% axis([0 50 0 280]) % STOR
axis([0 50 0 100]) % FFR
% xlabel('GHG emissions $gCO_2/kWh$','Interpreter','Latex')
xlabel('Cost $p/kWh$','Interpreter','Latex')
ylabel('Frequency','Interpreter','Latex')

mean_part_cost = zeros(1,iters);
std_part_cost = zeros(1,iters);
for i=1:iters
    mean_part_cost(i) = nanmean(cost_norm(1:i));
    std_part_cost(i) = nanstd(cost_norm(1:i));
end

figure('Position',[100 100 350 200])
% normalised
% plot(mean_part_cost/nanmean(cost_norm),'b');
% hold on
% plot(std_part_cost/nanstd(cost_norm),'b--');
plot(mean_part/nanmean(ghg_norm),'r');
hold on
plot(std_part/nanstd(ghg_norm),'r--');
% not normalised
% plot(mean_part_cost,'b');
% hold on
% plot(std_part_cost,'b--');
% plot(mean_part,'r');
% hold on
% plot(std_part,'r--');

hleg = legend('$\overline{cost}$','$\sigma$ cost','$\overline{GHG}$','$\sigma$ GHG');
hleg.Interpreter = 'Latex';
hleg.Location = 'SouthEast';
xlabel('Number of simulated events','Interpreter','Latex')
ylabel('Normalised values','Interpreter','Latex')

% GHG plot
load('D:\Phd\MATLAB\Phd\Kourken\Files\Files\Combo.mat','EI_2014')
load('DR_norm_schedule_window1')
figure('Position',[100 100 350 150])
av = zeros(48,1);
for i = 1:365
    hold on
    lh = plot(linspace(0,1,48),EI_2014{i});
    lh.Color = [0 0 0 0.1];
    
    av = av + EI_2014{i};

end
%    CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];
CO2_kWh_av = av/365;   
plot(linspace(0,1,48),CO2_kWh_av,'r','Linewidth',2);  
ylabel('$gCO_2e$/kWh','Interpreter','Latex')
xlabel('Time of Day','Interpreter','Latex')
datetick('x','HH:MM')
axis([0 1 -Inf Inf])

nanmean(ghg_norm)
nanmean(cost_norm)
prctile(cost_norm,5)
prctile(cost_norm,95)
prctile(ghg_norm,5)
