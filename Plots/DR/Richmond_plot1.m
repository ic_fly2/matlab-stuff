clear all;
close all;
% load('C:\Phd\MATLAB\Phd\Results\DR\STOR_flex_demand_factor_Richmond_skeleton_20150726T154038.mat')
% load('C:\PhD\MATLAB\PhD\Results\DR\STOR_flex_demand_factor_Richmond_skeleton_20150726T203644.mat')
% for    i = 1:length(out)
%     d_f(i) = out{i}.settings.DR.demand_factor;
%     DR(i) = out{i}.DR_D;
%     
% end
%  disp(DR)
 

figure('position',[100 100 340 340])
 d_F  = [ 1.2:0.1:1.7 ];
 optimal_DR = [0;   4.6473;11.5805 ; 6.2756 ; 21.1182 ;21.1182];
 
 GHG = [  1.774986364227564e+05;...
          1.777213088109457e+05;...
           1.890044980067707e+05;...
           1.923767681433935e+05;...
           2.173220138341659e+05; ...
            2.173220138341659e+05]/1000;
        
        
        

           
 optimal_DR_VSD = [0 ; 0;0 ;0; 12.8214; 13.7163]  ;      

    
  b1 =  bar([optimal_DR';optimal_DR_VSD']');
   hold on;
   [AX,H1,H2] = plotyy(0,0,[1:6],GHG);
   H1.Color = [0 0 0];
H2.Color = [0 0 0];
H1.Marker = 'o';
H1.LineStyle = 'none';
H2.Marker = 'o';
H2.LineStyle = 'none';
    AX(2).YColor = [0 0 0];
     AX(1).YColor = [0 0 0];
%      set(AX(1),'YLabel','Optimal level of DR power /kW ');
   AX(2).YLim = [0 250];
   AX(2).YTick = [0:50:250];
    colormap(gray)
xlabel('$d_s$ / $d_o$','Interpreter','Latex')
AX(2).YLabel.String = 'GHG emissions $kgCO_{2e}$';
AX(1).YLabel.String = 'Optimal level of DR power (kW)';
 
 AX(2).YLabel.Interpreter = 'Latex';
 AX(1).YLabel.Interpreter = 'Latex';
AX(1).YMinorGrid = 'On';
% set(gca,'XTickLabel','1.2' '1.4' '1.4' '1.5')
set(gca,'XTickLabel',{'1.2' '1.4' '1.4' '1.5' '1.6' '1.7' })

% legend({'Fixed Speed Pump','Variable speed Pump'},'Interpreter','latex')
legend({'Fixed Speed Pump','Variable speed Pump','GHG emmisions'},'Interpreter','latex','Location','NorthWest')
set(gca,'Position',[0.1300    0.1200    0.7750    0.78])