clearvars
figure('Position',[100 100 350 350])
% close all
% load('noflex_demand_1-14_price_strech_0-1_van_zyl_norm_15_09_2015')
window1 = load('window1_demand_1-14_price_strech_0-1_van_zyl_norm_15_09_2015');
window1_add = load('window1_demand_075-095_price_strech_0-1_van_zyl_norm_17_09_2015_additional');
window1_add2 = load('window1_demand_05-07_price_strech_0-1_van_zyl_norm_17_09_2015_additional2');
% windows = load('windows_demand_1-14_price_strech_0-1_van_zyl_norm_15_09_2015');
% noflex = load('noflex_demand2_1-14_price_strech_0-1_van_zyl_norm_15_09_2015');
% 
% %% read out
%  results = windows.results; 
%   results = noflex.results;
results = [ window1_add2.results; window1_add.results(1:4,:); window1.results];
[I J] = size(results);
for i = 1:I;
    for j = 1:J

       fval(i,j) =       results{i,j}.fval;
       GHG(i,j) =        results{i,j}.GHG; 
        gap(i,j) = results{i,j}.Remaining_gap;
      DR_D(i,j) =         results{i,j}.DR_D; 
      cost_pump(i,j) =    results{i,j}.cost_pump; 
      rev_DR(i,j) =       results{i,j}.rev_DR;
      
      if i == I
        P(j) = max(results{i,j}.def.settings.price)/min(results{i,j}.def.settings.price);
      end
    end
    dsdo(i) = results{i,j}.def.settings.DR.demand_factor;
end
% GHG contour plot provisional_contour_plot
GHG = GHG-852264.104;
GHG = GHG*365/(137*132*100); % in terms on MWh/MW/a and kW of pump 852264.104
h = contourf(P,dsdo,GHG);
colormap(summer);
 
% hatch infeasible region
for i = linspace(0,2,10)
    hold on;
    m = max(dsdo)/max(P);
    x = linspace(0,10)+i/m;
    y = m*x  + 1.4 - i;
    plot(x,y,'k','Linewidth',0.5)
end

axis([min(P) max(P) min(dsdo) max(dsdo)])
 ylabel('$\frac{d_s}{d_o}$','Interpreter','Latex','Fontsize',16,'Rotation',0)
xlabel('Price (max/min)','Interpreter','Latex')
 text(mean(P),1.5,['Infeasible region'],'Interpreter','Latex','Backgroundcolor',[1 1 1])
     colorbar


 
%% load cleaned data
load('DR_D_window1_clean2.mat')
% load('DR_D_windows_clean')
% load('DR_D_no_flex_clean')
%% contour /  mesh price vs demand plots
J = length(P);
I = length(dsdo);
% corrections due to remaining gap
% DR_D(1,3) = 132.2262;
% DR_D(4,9) = 132.2262;
% mesh(dsdo'*ones(1,J),ones(I,1)*P,DR_D/132.2262)
% contour(dsdo'*ones(1,J),ones(I,1)*P,DR_D/132.2262,2,'r')


% windows
%DR_D(1,[2 4 5]) = 132.2262;
%DR_D(2,8) = 132.2262;
% mod = [zeros(2,14); DR_D];
% dsdo_mod = [0.85 0.9 dsdo];
%mesh(dsdo_mod'*ones(1,J),ones(I+2,1)*P,DR_D/132.2262)
contourf(dsdo'*ones(1,J),ones(I,1)*P,DR_D/132.2262,2)
% contourf(dsdo_mod'*ones(1,J),ones(I+2,1)*P,mod/132.2262,2)
colormap(summer(3)); %summer only for noflex
cmap = [         0    0.5000    0.4000;...
    0.5000    0.7500    0.4000;...
    1.0000    1.0000    0.4000];
hold on
%xlabel('$d_s/d_o$','Interpreter','Latex');
xlabel('$\frac{d_s}{d_o}$','Interpreter','Latex','Fontsize',16)
ylabel('Price (max/min)','Interpreter','Latex')
% axis([min(dsdo_mod ) max(dsdo_mod ) min(P) max(P)])
axis([min(dsdo ) max(dsdo ) min(P) max(P)])
% Legend cheat
plot([1.4 1.4],[1 4.5],'Color','k','LineWidth',1);
h1 = area([0 0 0 0], [0 0 0 0],'Facecolor',cmap(1,:));
h2 = area([0 0 0 0], [0 0 0 0],'Facecolor',cmap(2,:));
h3 = area([0 0 0 0], [0 0 0 0],'Facecolor',cmap(3,:));
h4 = area([0 0 0 0], [0 0 0 0],'Facecolor',[1 1 1]);
 h_leg = legend([h1, h2, h3, h4],{'No pump','One pump','Two pumps','Infeasible'});
%h_leg = legend([h1, h2],{'No pump','One pump'}); %noflex scenario

% 
% % % 3D stuff
% zlabel('DR viable','Interpreter','Latex')
% axis([-Inf Inf -Inf Inf 0 2 ])
% grid minor

%% GHG stuff
% hold on
% plot(dsdo,mean(GHG,2),'ok')
% xlabel('$\frac{d_s}{d_o}$','Interpreter','Latex','Fontsize',16)
% ylabel('mean $gCO_{2e}$','Interpreter','Latex')



%% old stuff
%  load('D:\PhD\MATLAB\PhD\Results\DR\real_demand_feas_range_van_zyl20150816T231037.mat')
%  T = load('aditional_3d_data');
% % load('C:\PhD\MATLAB\PhD\Results\DR\real_demand_feas_range_Richmond_norm220150816T211018.mat')
% % plots
% dr_pwr = dr_lev;
%   for r = 1:10
%     for i = 1:10
%         for p = 1:10
%                    if  dr_lev(r,i,p) >  0;
%                       dr_lev(r,i,p) = 1;
%                    elseif isnan(dr_lev(r,i,p))
%                        dr_lev(r,i,p) = 0;
%                    end
%         end
%     end
%   end
%
%
%      y = ones(10,1)*[1:10]*10; %reward
%     x = 0.7+[1:10]'/10*ones(1,10); % demand
%     summed = zeros(10);
% for p = 1:10
%      hold on
% %     mesh(x,y,dr_lev(:,:,i)*i/5)
%     dr_lev(:,:,p)
%     tmp = dr_lev(:,:,p)*max(strech(EL.price{2},p/5))/min(strech(EL.price{2},p/5));
%     summed(tmp > summed) = tmp(tmp > summed);
% %     summed = dr_lev(:,:,i)*i/5;
% end
% summed(summed == 0) = NaN;
% mesh(x,y,summed)
% axis([40 80 0.8 1.7 0 Inf])
% close all
% figure('Position',[100 100 340 320])
% load('summed')
% load('infeas')
% h1 = mesh(x',y',summed);
% additional data
% clean summed as per extras
% hold on
% h3 = plot3(1.6*ones(1,10),10:10:100,10.24*ones(1,10),'b+'); % double capacity
% h2 = plot3(x',y',infeas,'b+'); %infeasable
% grid minor
% axis([ 1 1.7 10 100 0 Inf])
%  axis([10 100 0.8 1.7 -Inf Inf])
% ax1 = ylabel('Reward ($\pounds$/kW/a) ','Interpreter','Latex');
% ax1.Position = [0.8  50  -1.9923];

%  ax2 = xlabel('Demand factor $d_s/d_o$','Interpreter','Latex');
% ax2 = xlabel('$d_s/d_o$','Interpreter','Latex');

%  ax2.Position = [ 28    1.3   -0.6445];

% ax3 = zlabel('Price (max/min)','Interpreter','Latex');
% hleg = legend({'h1','h3','h2'},{'Viable region','Capacity limit','Infeasable'});
% hleg = legend({'h1','h2'},{'Viable region','Capacity limit'});

% hleg.Interpreter = 'Latex';
 EL_pricing;
for i = 1:10; r(i) = max(strech(EL.price{2},[i]/5))/min(strech(EL.price{2},[i]/5)); end
z = r'*ones(1,10);
figure('Position',[100 100 340 320])
% GHG_no = squeeze(load('GHG_van_zyl_no_DR','GHG'));
% GHG_no = load('GHG_demand_07_17_van_zyl');
% GHG_no = GHG_no.GHG; 
% cost_no = squeeze(load('cost_van_zyl_no_DR','cost'));
GHG_DR = squeeze(GHG(:,:,10)); GHG_DR(:,9) = T.GHG(:,9); %construct res
GHG_res = GHG_DR - GHG_no';
GHG_res(isnan(summed)) = 0;
 mesh(x',z,GHG_res*365/136/178/10) %per annum 136 kwH per kW commited
hg1 = plot(0.7+[4:8]/10,GHG_res(10,4:8)*365/136/178,'+k');
hold on
hg2 = plot(0.7+[9]/10,GHG_res(10,9)*365/136/356,'ok');
hg1 = plot(0.7+[4:8]/10,GHG_res(4:8)*365/136/178,'+k');
hold on
hg2 = plot(0.7+[9]/10,GHG_res(9)*365/136/356,'ok');
legend('DR from one pump','DR from two pumps')




