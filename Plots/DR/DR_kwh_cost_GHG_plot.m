% plot cost of DR as ratio of power
% example:
% pump_cost
close all
clear all
%load('D:\Phd\MATLAB\Phd\Results\DR\TEST_FFR_fix_price_fixed_DR_van_zyl_norm_20150721T135648.mat','out') % too large DR rev
% load('D:\Phd\MATLAB\Phd\Results\DR\TEST_FFR_VSD_price_fixed_DR_van_zyl_norm_20150721T120809.mat')
% load('D:\Phd\MATLAB\Phd\Results\DR\TEST_FFR_VSD_price_fixed_DR_van_zyl_norm_20150721T135618.mat')
% load('Fixed_DR_energy_results_case4Richmond_skeleton_20150721T191712.mat')
% real set:
% very low cost: load('D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case2van_zyl_norm_20150722T030626.mat')
% load('D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case2van_zyl_norm_20150721T225304.mat')
load('D:\Phd\MATLAB\Phd\Results\DR\real\Fixed_DR_energy_results_case2van_zyl_norm_20150722T030626.mat')

% flex


%for Lecture_example_flex_STOR_fixed_fixed_DR_ratio2
% name = 'C:\Phd\MATLAB\Phd\Results\DR\Flex_fixed_DR_caseLecture_example_norm_20150727T120856.mat'
% name = 'D:\Phd\MATLAB\Phd\Results\DR\Flex_fixed_DR_case_VSDLecture_example_norm_20150727T120913.mat'
% load(name)
figure('position',[100 100 340 340])
% pump_cost = [2000 2205 2430 2900 3400 6000];
% % DR
% cost_DR = [ 0     5    20   40   60   80  ];  
% % Total
% total = pump_cost - cost_DR;
% % GHG
% GHG = rand(1,6)*100;
% 

for i = 1:7
    pump_cost(i)  = out{1,i}.cost_pump;
    cost_DR(i)  = -out{1,i}.rev_DR/1000/365;
    GHG(i)  = out{1,i}.GHG;
    DR_ratio(i) = out{1,i}.DR_D;
end

% GHG = GHG - 1.2706e+06; % no DR schedule
GHG = GHG - out{1,1}.GHG;
kW_DR = out{1}.Power.total(1)*DR_ratio;
kWh_per_kW = [27.4 68.4  136.6667]/365; %7.5 

GHG_1 = GHG./kW_DR*kWh_per_kW(1);
GHG_2 = GHG./kW_DR*kWh_per_kW(2);
GHG_3 = GHG./kW_DR*kWh_per_kW(3);

GHG = GHG_3 ;


% cost = pump_cost - 2.2418e+04;
cost = pump_cost -min(pump_cost);
% cost = pump_cost -out{1,1}.cost_pump;
% cost_kWh = cost./kW_DR*kWh_per_kW(3)*100/1000;
cost_kWh = cost./kW_DR*kWh_per_kW(3)*100;
% x = [0:0.2:1];
hold on
% plot(x,cost_DR,'k*')
[AX, H1, H2] = plotyy(DR_ratio,cost_kWh,DR_ratio,GHG);

% AX(1).YLim = [0 1.1*max(pump_cost)];
% AX(2).YLim = [0 1.1*max(GHG)];


set(AX(1),'YMinorGrid','On')
set(AX(1),'YMinorTick','On')
H1.Color = [0 0 0];
H2.Color = [0 0 0];

set(AX(1),'YColor',[0 0 0])
set(AX(2),'YColor',[0 0 0])

H1.LineStyle = 'none';
H2.LineStyle = 'none';

% AX(1).YTick = [0::1.1*max(pump_cost)];
% AX(2).YTick = [0:20:140];

H1.Marker = '+';
H2.Marker = 'o';

xlabel('DR ratio','Interpreter','latex')
AX(1).YLabel.String = 'Cost p/kWh';
AX(1).YLabel.Interpreter = 'Latex';
AX(2).YLabel.String = 'GHG emissions $gCO_{2e}$ / kWh';
AX(2).YLabel.Interpreter = 'Latex';
set(AX(1),'Xtick',[0:0.2:1])

hleg = legend('Cost of energy','Emissions');
set(hleg,'Interpreter','Latex')
 set(gca,'Position',[0.18500 0.1100 0.65 0.8150])
 title('Flexible STOR')

