clear all
load('Questionaire4')

 Lecture_example_questionair_defenitions
 close all
% out{length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)} = [];
%% out_dr(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
for i1 = 1:length(reward_set)
    out_dr_reward(i1,:) = reshape(out_dr(i1,:,:,:,:,:),[],1)';
    dr_reward_per(i1) = length(find(out_dr_reward(i1,:) > 0))/...
        (length(find(out_dr_reward(i1,:) > 0))+length(find(out_dr_reward(i1,:) == 0)));
end
for i2 = 1:length(Length_set)
    out_dr_Length(i2,:) = reshape(out_dr(:,i2,:,:,:,:),[],1)';
    dr_Length_per(i2) = length(find(out_dr_Length(i2,:) > 0))/...
        (length(find(out_dr_Length(i2,:) > 0))+length(find(out_dr_Length(i2,:) == 0)));
end

for i3 = 1:length(Elev_set)    
    out_dr_Elev(i3,:) = reshape(out_dr(:,:,i3,:,:,:),[],1)';
     dr_Elev_per(i3) = length(find(out_dr_Elev(i3,:) > 0))/...
        (length(find(out_dr_Elev(i3,:) > 0))+length(find(out_dr_Elev(i3,:) == 0)));
end

for i4 = 1:length(Demand_set)
    out_dr_Demand(i4,:) = reshape(out_dr(:,:,:,i4,:,:),[],1)';
         dr_Demand_per(i4) = length(find(out_dr_Demand(i4,:) > 0))/...
        (length(find(out_dr_Demand(i4,:) > 0))+length(find(out_dr_Demand(i4,:) == 0)));
end

for i5 = 1:length(storage_set)    
    out_dr_storage(i5,:) = reshape(out_dr(:,:,:,:,i5,:),[],1)';
    dr_storage_per(i5) = length(find(out_dr_storage(i5,:) > 0))/...
        (length(find(out_dr_storage(i5,:) > 0))+length(find(out_dr_storage(i5,:) == 0)));
end
for i6 = 1:length(price_set)
    out_dr_price(i6,:) = reshape(out_dr(:,:,:,:,:,i6),[],1)';
    dr_price_per(i6) = length(find(out_dr_price(i6,:) > 0))/...
        (length(find(out_dr_price(i6,:) > 0))+length(find(out_dr_price(i6,:) == 0)));
end    

%% Elevation and Length to static vs dynamic head

HW_constant = 10.67/(100^1.85*0.55^4.87);
for i2 = 1:length(Length_set)
    for i3 = 1:length(Elev_set) 
        for i4 = 1:length(Demand_set)
            dr_ratio_per(i2,i3,i4) = length(find(out_dr(:,i2,i3,i4,:,:)> 0))/...
               ( length(find(out_dr(:,i2,i3,i4,:,:) > 0)) + length(find(out_dr(:,i2,i3,i4,:,:) == 0)));
           
           ratio(i2,i3,i4) = HW_constant*(Demand_set(i4)*189)^1.85 * Length_set(i2)*1500 /...
                               (20*Elev_set(i3));
        end
    end
end
            
ratio = reshape(ratio,[1 numel(ratio)]);
dr_ratio_per = reshape( dr_ratio_per,[1 numel( dr_ratio_per)]);
plot(ratio,dr_ratio_per,'+')

%% out_ghg(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;

for i1 = 1:length(reward_set)
    out_ghg_reward(i1,:) = reshape(out_ghg(i1,:,:,:,:,:),[],1)';
end
for i2 = 1:length(Length_set)
    out_ghg_Length(i2,:) = reshape(out_ghg(:,i2,:,:,:,:),[],1)';
end
for i3 = 1:length(Elev_set)    
    out_ghg_Elev(i3,:) = reshape(out_ghg(:,:,i3,:,:,:),[],1)';    
end

for i4 = 1:length(Demand_set)
    out_ghg_Demand(i4,:) = reshape(out_ghg(:,:,:,i4,:,:),[],1)';
end

for i5 = 1:length(storage_set)    
    out_ghg_storage(i5,:) = reshape(out_ghg(:,:,:,:,i5,:),[],1)';    
end
for i6 = 1:length(price_set)
    out_ghg_price(i6,:) = reshape(out_ghg(:,:,:,:,:,i6),[],1)';
end

% plot(storage_set,sparse(out_ghg_storage),'*')
p_ghg_stor = polyfit(storage_set,nanmean(out_ghg_storage'),1);
% p_ghg_Demand = polyfit(Demand_set,nanmean(out_ghg_Demand'),1);
plot(storage_set,nanmean(out_ghg_storage'),'*')
hold on
 hold on
plot(Length_set,nanmean(out_dr_Length'),'r*')
plot(storage_set,nanmean(out_dr_storage'),'b*')
plot(reward_set,nanmean(out_dr_reward'),'g*')
plot(Demand_set,nanmean(out_dr_Demand'),'k*')
plot(price_set,nanmean(out_dr_price'),'y*')

 hold on
%plot(nanmean(out_dr_Length'),'r*')
plot(storage_set/mean(storage_set),nanmean(out_dr_storage'),'b*')
plot(reward_set/mean(reward_set),nanmean(out_dr_reward'),'go')
plot(Demand_set/mean(Demand_set),nanmean(out_dr_Demand'),'k+')
plot(price_set/mean(price_set),nanmean(out_dr_price'),'r*')

figure('Position',[0 0 340 340])
subplot(3,1,1)


plot(Demand_set,dr_Demand_per*100,'+')
[p,S] = polyfit(Demand_set,dr_Demand_per*100,1);
disp('Demand')
disp(S.normr)
disp(p)
x = linspace(min(Demand_set),max(Demand_set));
hold on
plot(x,polyval(p,x),'--');
ylabel('% of viable DR')
xlabel('Demand factor')

subplot(3,1,2)
% corr = nanmean(out_dr_reward');
corr = [3.2531    3.2724   3.2980  3.3494 ]; %swaped position 3 and 4 as incorreclty summed :(
plot(reward_set,corr,'go')
hold on 
[p,S] = polyfit(reward_set,corr,2);
x = linspace(min(reward_set),max(reward_set));
disp('Reward')
disp(S.normr)
disp(p)
plot(x,polyval(p,x),'--')
%plot(reward_set,reward_set.^2*0.000000000020062-0.0000012362500000*reward_set+3.2714,'--')
%  [p,S] = polyfit(reward_set,corr,2)
% normr: 0.0043
xlabel('reward in �')
% plot(Demand_set/mean(Demand_set),nanmean(out_dr_Demand'),'k+')

subplot(3,1,3)
plot(price_set,nanmean(out_dr_price'),'r*')
hold on
% [p,S] = polyfit(price_set,nanmean(out_dr_price'),1)
% normr: 0.7651
xlabel('Ratio of max. and min. tariff price')

[p,S] = polyfit(price_set,nanmean(out_dr_price'),1);
 plot(price_set,polyval(p,price_set),'--')
disp('Price')
disp(S.normr)
disp(p)

figure('Position',[0 0 340 340])
subplot(3,1,1)
plot(storage_set,dr_storage_per*100,'b*')
hold on
corr = dr_storage_per*100; 
corr = corr(2:end); %skip first entry as it is zero
[p,S] = polyfit(storage_set(2:end),corr,1);
plot(storage_set(2:end),storage_set(2:end)*p(1)+p(2),'--')
ylabel('% of viable DR')
xlabel('Tank capacity in h')
disp('Storage')
disp(S.normr)
disp(p)

subplot(3,1,2)
plot(Length_set,dr_Length_per*100,'+')
[p,S] = polyfit(Length_set,dr_Length_per*100,2);
x = linspace(min(Length_set),max(Length_set));
hold on
plot(x,polyval(p,x),'--')
disp('Length')
disp(p)
disp(S.normr)
ylabel('% of viable DR')
xlabel('Length ratio')

subplot(3,1,3)
plot(Elev_set,dr_Elev_per*100,'+')
[p,S] = polyfit(Elev_set,dr_Elev_per*100,2);
x = linspace(min(Elev_set),max(Elev_set));
hold on
plot(x,polyval(p,x),'--')
disp('Elev')
disp(p)
disp(S.normr)
ylabel('% of viable DR')
xlabel('Elevation ratio')



% out_fval(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
% out_cost_pump(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;
% out_rev_dr(length(reward_set),length(Elev_set),length(Demand_set),length(Length_set),length(storage_set),length(price_set)) = 0;

