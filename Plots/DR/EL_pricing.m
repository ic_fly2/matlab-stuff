
Electricity_Price(1:11) = 28.59;
Electricity_Price(12:19) = 85.76;
Electricity_Price(20:33) = 48.49;
Electricity_Price(34:40) = 85.76;
Electricity_Price(41:44) = 48.49;
Electricity_Price(45:48) =  28.59;
Electricity_Price = Electricity_Price/5;

North_weekday(1:6)   = 6.2;
North_weekday(7:14)  = 6.0;
North_weekday(15:22) = 7.9;
North_weekday(23:30) = 8.5;
North_weekday(31:38) = 19.2;
North_weekday(39:46) = 8.5;
North_weekday(47:48) = 6.2;


North_weekend(1:6)   = 6.1;
North_weekend(7:14)  = 5.9;
North_weekend(15:28) = 7.5;
North_weekend(29:36) = 8.5;
North_weekend(37:45) = 7.2;
North_weekend(46:48) = 6.1;

EL.price{1} = Electricity_Price;
EL.price{2} = North_weekday;
EL.price{3} = North_weekend;


try
load('window_times')
end
%% carefull with plotting this is called in the results generation!!
% close all
% hold on;
% plot(Electricity_Price,'k+')
% plot(North_weekday,'k*')
% plot(North_weekend,'ko')

% figure('position',[0 0 340 340 ]);
% barplot = [Electricity_Price;North_weekday;North_weekend]';
% bar(barplot)
% x = [731204:0.0417/2:731205];
% subplot(3,1,1)
% bar(x,Electricity_Price)
% EL_pricing_sub_plot_config
% % plot(Electricity_Price,'k+')
% subplot(3,1,2)
% bar(x,North_weekday,'k')
% EL_pricing_sub_plot_config  
% % plot(North_weekday,'k*')
% subplot(3,1,3)
% bar(x,North_weekend)
% EL_pricing_sub_plot_config
% % plot(North_weekend,'ko')

% figure('position',[0 0 340 340 ]);
% % barplot = [Electricity_Price;North_weekday;North_weekend]';
% % bar(barplot)
% x = [731204:0.0417/2:731205];
% subplot(3,1,1)
% bar(x,Electricity_Price)
% EL_pricing_sub_plot_config
% % plot(Electricity_Price,'k+')
% subplot(3,1,2)
% bar(x,North_weekday)
% EL_pricing_sub_plot_config  
% % plot(North_weekday,'k*')
% subplot(3,1,3)
% bar(x,North_weekend)
% EL_pricing_sub_plot_config
% plot(North_weekend,'ko')
% subplot(4,1,4)

% figure('position',[0 0 340 240 ]);
% subplot(2,1,1)
% bar(x,North_weekday)
% EL_pricing_sub_plot_config
% 
% subplot(2,1,2)
% for i = 1:13; plot([times(i,1) times(i,2)],[i,i],'k'); hold on; end
% i = 14;  plot([7 13.5]/24,[i,i],'r','Linewidth',2);
% i = 14;  plot([16.5 21]/24,[i,i],'r','Linewidth',2);
% plot(0,0)
% set(gca,'YTickLabel',{'' '' ''})
% datetick('x','HH:MM')
% axis([-inf inf 0 20])
% ylabel('Windows','Interpreter','latex')
% ylabel('p/kWh','Interpreter','latex')
% xlabel('Time of Day','Interpreter','latex')
% 7.0000   13.5000   16.5000   21.0000 
CO2_kWh_av = [404 403 397 392 391 404 418 425 428 430 431 431 432 432 430 430 428 427 425 425 427 426 419 407];

% figure('position',[0 0 340 120 ]);
% %  x2 = [731204:0.0417:731205];   
%     time = linspace(0,1,length(CO2_kWh_av));
%         plot(time,CO2_kWh_av,'k')
%     hold on
%     plot(time,ones(1,24)*375,'k:')
%     plot(time,ones(1,24)*400,'k:')
%     plot(time,ones(1,24)*425,'k:')
% datetick('x','HH:MM')
% ylabel('$gCO_{2e}$ / kWh','Interpreter','latex')
% xlabel('Time of Day','Interpreter','latex')
% axis([-Inf Inf 350 450])  

STOR_util = [200 235];
STOR_av = [2];

rev = STOR_util*25*82/60 + 365*12* STOR_av;


