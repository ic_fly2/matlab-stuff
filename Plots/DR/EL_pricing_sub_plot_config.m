colormap('gray');
datetick('x','HH:MM')
axis([-Inf Inf 0 20])   
ylabel('pence / kWh','Interpreter','latex')
xlabel('Time of Day','Interpreter','latex')
