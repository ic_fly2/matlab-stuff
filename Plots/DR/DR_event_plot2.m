% DR event model
clearvars
% settings:
EL_pricing

def.N =48;
def.approx_number = 5;
def.vmax_min =6;
def.M  = 999;
def.settings.time_limit  = 600;
def.Desired_ratio = NaN;
def.asymmetry = 'on';

def.def = 'van_zyl_norm';
% def.def =  'Richmond_skeleton';
% def.def =  'Lecture_example_norm';
def.settings.price =   EL.price{2}; 
def.settings.DR.status = 'On';
def.settings.DR.flex = 'No';

def.settings.DR.desired_ratio = NaN;
def.settings.DR.rest_period = 240; %minutes
def.settings.DR.reward = 60000;
def.settings.DR.desired_share = 0.3333;
def.settings.DR.demand_factor = 1.1;
%def.settings.DR.recovery = 240;
%def.settings.mods.Length_factor = 1;
% 
% 
% def.settings.mods.status = 'No';
% 

% def.settings.mod_schedule.status = 'No';
% def.settings.mod_schedule.sched = [0 0 0];
% def.settings.mod_schedule.index = 1;

def.settings.GHG = CO2_kWh_av ;


def.settings.Time_shift = 1;
duration = 2; %check N
% normal DR:

def.settings.mod_schedule.status = 'No';
out_norm = experiment(def);
out_norm_c.GHG = out_norm.GHG;
out_norm_c.cost = out_norm.cost_pump;
out_norm_c.fval = out_norm.fval;
out_norm_c.sched = out_norm.schedule_tmp;

def.settings.h0 = out_norm.h0;
% event 
def.settings.mod_schedule.status = 'Yes';
def.settings.mod_schedule.index = 1;

schedule = DR_schedule_modder(out_norm.schedule_tmp,duration,1);
def.settings.mod_schedule.sched =  schedule(1:duration,:);
out = experiment(def);
out.GHG   = out.GHG;
out.cost  = out.cost_pump;
out.fval  = out.fval;
out.sched = out.schedule_tmp;
    