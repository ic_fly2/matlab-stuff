clearvars
% close all
 load('D:\PhD\MATLAB\PhD\Results\DR\real_demand_feas_range_van_zyl20150816T231037.mat')
%  T = load('aditional_3d_data');
% % load('C:\PhD\MATLAB\PhD\Results\DR\real_demand_feas_range_Richmond_norm220150816T211018.mat')
% % plots
% dr_pwr = dr_lev;
%   for r = 1:10
%     for i = 1:10
%         for p = 1:10
%                    if  dr_lev(r,i,p) >  0;
%                       dr_lev(r,i,p) = 1;
%                    elseif isnan(dr_lev(r,i,p))
%                        dr_lev(r,i,p) = 0;
%                    end
%         end
%     end
%   end
%
%
y = ones(10,1)*[1:10]*10; %reward
x = 0.7+[1:10]'/10*ones(1,10); % demand
%     summed = zeros(10);
% for p = 1:10
%      hold on
% %     mesh(x,y,dr_lev(:,:,i)*i/5)
%     dr_lev(:,:,p)
%     tmp = dr_lev(:,:,p)*max(strech(EL.price{2},p/5))/min(strech(EL.price{2},p/5));
%     summed(tmp > summed) = tmp(tmp > summed);
% %     summed = dr_lev(:,:,i)*i/5;
% end
% summed(summed == 0) = NaN;
% mesh(x,y,summed)
% axis([40 80 0.8 1.7 0 Inf])
close all
figure('Position',[100 100 340 320])
load('summed')
% load('infeas')
h1 = mesh(x',y',summed);
% additional data
% clean summed as per extras
hold on
% h3 = plot3(1.6*ones(1,10),10:10:100,10.24*ones(1,10),'b+'); % double capacity
% h2 = plot3(x',y',infeas,'b+'); %infeasable
grid minor
axis([ 1 1.7 10 100 0 Inf])
%  axis([10 100 0.8 1.7 -Inf Inf])
ax1 = ylabel('Reward ($\pounds$/kW/a) ','Interpreter','Latex');
ax1.Position = [0.8  50  -1.9923];

%  ax2 = xlabel('Demand factor $d_s/d_o$','Interpreter','Latex');
ax2 = xlabel('$d_s/d_o$','Interpreter','Latex');

%  ax2.Position = [ 28    1.3   -0.6445];

ax3 = zlabel('Price (max/min)','Interpreter','Latex');
% hleg = legend({'h1','h3','h2'},{'Viable region','Capacity limit','Infeasable'});
hleg = legend({'h1','h2'},{'Viable region','Capacity limit'});

hleg.Interpreter = 'Latex';

for i = 1:10; r(i) = max(strech(EL.price{2},[i]/5))/min(strech(EL.price{2},[i]/5)); end
z = r'*ones(1,10);
figure('Position',[100 100 340 320])
GHG_no = squeeze(load('GHG_van_zyl_no_DR','GHG'));
GHG_no = load('GHG_demand_07_17_van_zyl');
GHG_no = GHG_no.GHG; 
cost_no = squeeze(load('cost_van_zyl_no_DR','cost'));
GHG_DR = squeeze(GHG(:,:,10)); GHG_DR(:,9) = T.GHG(:,9); %construct res
GHG_res = GHG_DR - GHG_no';
GHG_res(isnan(summed)) = 0;
 mesh(x',z,GHG_res*365/136/178/10) %per annum 136 kwH per kW commited
% hg1 = plot(0.7+[4:8]/10,GHG_res(10,4:8)*365/136/178,'+k');
% hold on
% hg2 = plot(0.7+[9]/10,GHG_res(10,9)*365/136/356,'ok');
% hg1 = plot(0.7+[4:8]/10,GHG_res(4:8)*365/136/178,'+k');
% hold on
% hg2 = plot(0.7+[9]/10,GHG_res(9)*365/136/356,'ok');
% legend('DR from one pump','DR from two pumps')
zlabel('GHG emissions (g/kWh)','Interpreter','Latex')
xlabel('$d_s/d_o$','Interpreter','Latex');
ylabel('Price (max/min)','Interpreter','Latex')
axis([1 1.65 -Inf Inf -Inf Inf ])
grid minor


