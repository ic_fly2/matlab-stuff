 close all

% load('D:\Phd\MATLAB\Phd\Results\DR\TEST_FFR_VSD_demand_N=1220150720T165057.mat', 'out2')
% load('D:\Phd\MATLAB\Phd\Results\DR\TEST_FFR_VSD_price_fixed_DR_van_zyl_norm_20150721T182448.mat')
% load('STOR_flex_demand_factor_Lecture_example_norm_20150725T161418.mat')
% load('STOR_flex_demand_factor_van_zyl_norm_20150725T174806.mat')
figure('Position',[100 100 340 340])
for x = 1:10
    for y = 1:7
        reward(x,y) = out{x,y}.settings.DR.reward;
        %price_strech(x,y) = max(out{x,y}.settings.price)/min(out{x,y}.settings.price);
        demand_factor(x,y) =out{x,y}.settings.DR.demand_factor;
        DR_D(x,y) = out{x,y}.DR_D;
        cost(x,y) = out{x,y}.fval;
    end
end

h1 = surf(reward,demand_factor,DR_D);
set(h1,'facecolor','none');
xlabel('Reward $/MW/a')
% xlabel('Price factor')
ylabel('Demand factor')
zlabel('DR Power kW')
% colormap(gray)