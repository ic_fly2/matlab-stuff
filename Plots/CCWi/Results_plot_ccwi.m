 clear all;
load('Richmond_results_CCWI_compiled')
close all;
h1 = figure('position',[100 100 700 300]);
subplot(1,2,1)
h_time = bar(time_taken);
axis([0.5 8.5 0 1000])
title('Computational time','Fontsize',fontsize,'Interpreter','Latex')

% h_leg = legend('N = 6','N = 12','N = 24','N = 48','Fontsize',fontsize,'Interpreter','Latex');
set(gca,'FontSize',fontsize)
set(gca,'YScale','log')
set(gca,'Position',[0.0800 0.1200 0.3347 0.8150])
% set(gca,'XAxisLocation','Bottom')

% set(h_leg,'Location','SouthWest','Fontsize',fontsize,'Interpreter','Latex')
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Time taken (s)','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')
colormap('gray');

subplot(1,2,2)
h_cost = bar(fval);
axis([0.5 12 0 20000])

title('Computated operating cost','Fontsize',fontsize,'Interpreter','Latex')
colormap('gray');
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Operating Cost in \textit{\textsterling}','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')
set(gca,'Position',[0.5003 0.120  0.46 0.8150])

% h_time = legend('N = 6','N = 12','N = 24','N = 48');

% make legend:
% hleg = legend('N = 6','N = 12','N = 24');
% hleg = legend('N = 6','N = 12','N = 24','N = 48');


% set(h_leg,'Location','NorthEast','Fontsize',fontsize,'Interpreter','Latex')
% xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
% ylabel('Time taken (s)','Fontsize',fontsize,'Interpreter','Latex')
%  set(gca,'yMinorGrid','on')

legend('N = 6','N = 12','N = 24','N = 48','Fontsize',fontsize,'Interpreter','Latex');
 
%  %% No solution label Richmond

  str1 = 'No solutions in time limit';
  text(1,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
  text(2,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
  text(3,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
%str2 = % '$*$' '$\dagger$' $\ddagger$' 