clear all; close all;
 fontsize = 10;
% %% Pipe plot
D = 0.9;
e = 0.001;
L = 1000;
vmax = 2;
pmax = 10;
plotting = 'on';

qmax = vmax*pi*(D^2)/4; % max flow

q = linspace(0,qmax*2,200);

for i = 1:length(q)
     [ ~,~,~,H(i),K1(i),n(i) ] = DarcyWeisbachFactorCalc( D,q(i),e,L,'DW') ;
end
H(1) = 0;
Delta_H =  0.01*max(H);
H_plus  = H + Delta_H;
H_minus = H;
H_minus(H_minus <= 0) = 0;
%% crude guessing method (bisection method) (optimisation not analytical)
k = 1;
m = 0.1;
c_temp = 0;
Q_lim_index(1) = 1; 

while q(Q_lim_index(k)) <= qmax
    while m(k)*(q-q(Q_lim_index(k)))+c_temp(k) <= H_plus
        m(k) = m(k)+0.01;
    end
    Q_lim_index(k+1) = find( m(k)*(q-q(Q_lim_index(k)))+c_temp(k) - H_minus > eps, 1, 'last' );
    m(k+1) = m(k) + m(k)*0.3;
    c_temp(k+1) = H_minus(Q_lim_index(k+1));
    k = k+1;
end
C(1) = 0;
for i = 1:k-1 
    Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
    H_lim(i) = Q_lim(i)*m(i) + C(i);
    C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
end

% shift qlim along
Q_lim(length(Q_lim)+1) = 0;
Q_lim =  circshift(Q_lim',1)'; % first value
% Q_lim(length(Q_lim)+1) = ; % last value (max)
if pmax < length(m)
    error('Too many segments, increase the tolerarance')
end

% %% plot stuff

        fig2 = figure('Position',[0 0 340 300]);
% %     hatching
%     hold on;
% x_beg = linspace(-max(H)*1.1,max(H)*1.1,40);
% m_diag = max(H)/qmax;
% for i = 1:length(x_beg)
%     x_diag = 0:100;
%     y_diag = -x_diag*m_diag+x_beg(i);
%     plot(x_diag,y_diag,'k','Linewidth',0.5);
% end
% area(q,H,'FaceColor','w','Linewidth',0.001)

    hold on
    h1 = plot(q,H,'k','LineWidth',1.5); %quadratic approximation
    h1a =plot(-q,-H,'k','LineWidth',1.5);
%     h2 = plot(q,H_minus,'k:'); %Allowed error
%     h2 = plot(-q,-H_minus,'k:'); 
    h2 = plot(q,H_plus,'k:');
    h2a = plot(-q,-H_plus,'k:');
    for i = 1:k-1
        x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
        h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'k--','LineWidth',1.1);
        h3a = plot(-x(i,:),-x(i,:)*m(i) -C(i),'k--','LineWidth',1.1);
    end
    
%     for i = 1:k-1
%         x(i,:) = linspace(0,pi*0.25*vmax*D^2,100);
%         plot_x = x(i,:);
%         plot_y = x(i,:)*m(i) + C(i);
%         h4 = plot(plot_x,plot_y,'k--','LineWidth',1); %convex constrained set
%         for j = 1:30
%             x_start = linspace(0,1.5,30);
%             plot_x_diag = linspace(x_start(j),2,100);
%             plot_y_diag = -(1.2/4.5)*plot_x_diag +x_start(j)*m(i) + C(i);
%             h5 = plot(plot_x_diag, plot_y_diag,'k','LineWidth',1);
%         end
%     end
    plot([-2 2],[0 0],'k-','LineWidth',0.5);
    plot([0 0],[-5 5],'k-','LineWidth',0.5);
    
%     hleg = legend([h1 h2 h4],{'Quadratic approximation',['Allowed error ' num2str(Delta_H) ' m'],'Convex constrained set' } );
    hleg = legend([h1 h3 h2],{'Quadratic approx.','Piecewise approx.', 'Max approx. Error'},'Fontsize',fontsize,'Location','SouthEast');
    axis([ -pi*0.25*vmax*D^2  pi*0.25*vmax*D^2 -H(100) H(100)])
%     axis([0 pi*0.25*vmax*D^2 0 H(100)])
    ylabel('Head loss [$m$]','Fontsize',1*fontsize,'Interpreter','LaTex');
    xlabel('Volumetric flow [$m^3/s$]','Fontsize',1*fontsize,'Interpreter','LaTex')
    set(hleg,'Location','SouthEast','Fontsize',1*fontsize,'Interpreter','LaTex')
    set(gca,'FontSize',1*fontsize)
    set(gca,'position',[0.14    0.1467    0.78    0.7783])
    hold on;
    plot([0 0 0 0 0 0 0 0 0  -1 -0.5 1 0.5],[-4:4 0 0 0 0],'k+')
    

%    saveas(gcf,'E:/Phd/My written stuff/Figures/pipe_curve4_fig.eps')