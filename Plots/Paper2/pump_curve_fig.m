%% pump plot (from the function)
clear all; close all;
fontsize = 10;
a = -2.0833e+05;% -1; 
b =  -583.3333;%0;
c = 38;% 25;

qmax = max(roots([a b c]));
q = linspace(0,qmax,200);
for i = 1:length(q)
    H = polyval([a b c],q);
end
H = H(H >= 0);
q = q(1:length(H));
Delta_H =  0.005*max(H);
H_plus  = H + Delta_H;
H_minus = H - Delta_H;

power = 3*q.^2+2*q+750;

k = 1;
m = 1;
c_temp = H_plus(1);
Q_lim_index(1) = 1;
C(1) = H_plus(1);
m(1) = b;
while Q_lim_index(k) < length(q)
    
    while  polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= H_minus(Q_lim_index:end)
        m(k) = m(k)-0.01;
        %         c_temp(k) = H_plus(Q_lim_index(k)) - q(Q_lim_index(k))*m(k);
    end
    Q_lim_index(k+1) = find( H_plus - polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= 0, 1, 'last' );
    m(k+1) = m(k) + m(k)*0.01;
    c_temp(k+1) = H_plus(Q_lim_index(k+1));%- q(Q_lim_index(k+1))*m(k+1);
    k = k+1;
    
end

for i = 1:k-1
    Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
    H_lim(i) = Q_lim(i)*m(i) + C(i);
    C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
end

C(end) = [];
m(end) = [];




% plot stuff
figure('position',[100 100 400 300]) %400 px wide
hold on
[AX,h11,h22] = plotyy(q,H,q,power,'plot');
% make the plotyy right
 set(AX(1),'YColor', [0 0 0]); set(AX(2),'YColor', [0 0 0]);
 set(AX(2),'YTick',[500:100:1000]);set(AX(2),'YLim',[500 1000]);
 set(h11,'LineStyle','--','LineWidth',2,'Color',[0 0 0])
 set(h22,'LineStyle','-.','LineWidth',1,'Color',[0 0 0])
% hatching
x_beg = linspace(-max(H)*1.3,max(H)*1.3,40);
m_diag = max(H)/qmax;
for i = 1:length(x_beg)
    x_diag = 0:100;
    y_diag = x_diag*m_diag+x_beg(i);
    plot(AX(1),x_diag,y_diag,'Color',[0.7 0.7 0.7],'Linewidth',0.05);
end
area(AX(1),q,H,'FaceColor','w','Linewidth',0.001)

% characteristic curve


h1 = plot(AX(1),q,H,'k--','LineWidth',3);
% piecwise linear
for i = 1:k-2
    x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
    x(i,:) = linspace(0,qmax*1.1,100);
    h3 = plot(AX(1),x(i,:),x(i,:)*m(i) + C(i),'k:','LineWidth',1);
end

% simple
x = 2:0.1:3;
y = ones(1,length(x))*18.7;
hold on;
h4 = plot(AX(1),x,y,'k','LineWidth',1);
% 
hleg = legend([h1 h3 h4 h22],{'Characteristic Curve','Bounds of convex set','Fixed head','Power'} );
%     axis([0 qmax 0 H(end)])
%ylabel('Head /m','Fontsize',fontsize);
 ylabel(AX(1),'Head /m','Fontsize',fontsize);
ylabel(AX(2),'Power /kW','Fontsize',fontsize);
xlabel('Volumeflow /m^3','Fontsize',fontsize)
set(hleg,'Location','SouthWest','Fontsize',fontsize)
set(AX(2),'Position',[0.13 0.15 0.715 0.815])
set(gca,'FontSize',fontsize)

% save as pump_curve_fig4.eps
% 
% % saveas(gcf,'D:/Phd/My written stuff/Figures/pump_curve_fig.eps')
% 
