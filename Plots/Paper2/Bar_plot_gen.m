%% produce graphics
clear all; close all;
fontsize = 10;

h1 = figure('position',[100 100 350 300]);
% load('Results_Richmond_skeleton_times_and_fval_run1')

% load('Results3Richmond_skeleton_pump_station1_times_and_fval_run1')
% load('Results3Richmond_times_and_fval_run1')
% load('Results3van_zyl_times_and_fval_run1')
% load('Results5_Richmond_skeleton_original_times_and_fval_run1')

% load('Results5_Richmond_skeleton_original_times_and_fval_run1')
%% richmond original
% T5 = load('Results5_Richmond_skeleton_original_times_and_fval_run1');
% T10 = load('Results10_original_appox_7.mat');
% T10_2 = load('Results10_original_approx5');
% T9 = load('Results9_Richmond_skeleton_original_times_and_fval_run1');
% 
% T99= load('Results99Richmond_skeleton_original_times_and_fval_run1');
% %fval
% T9.fval{7} =  T10.fval;
% T9.fval{8} =  NaN;
% fval = cell2mat([T5.fval T9.fval; T99.fval]);
% 
% fval = [ones(3,4)*NaN; fval];
% % fval(7,4) = 13319.87
% 
% %time taken
% T9.time_taken{7} =  T10.time_taken;
% 
% T9.time_taken{5} =  T10_2.time_taken;
% T9.time_taken{8} =  600;
% %[T5.time_taken T9.time_taken]
% 
% time_taken = cell2mat([T5.time_taken T9.time_taken; T99.time_taken]);
% 
% time_taken = [ones(3,4)*600; time_taken];
% % time_taken(7,4) = 81.27;
% 
% %remaining Gap
% T9.Remaining_gap{8} =  Inf;
% T9.Remaining_gap{5} =  T10_2.Remaining_gap;
% T9.Remaining_gap{7} =  T10.Remaining_gap;
% Remaining_gap = cell2mat([T5.Remaining_gap T9.Remaining_gap; T99.Remaining_gap]);
% Remaining_gap = [ones(3,4)*Inf; Remaining_gap];
% Remaining_gap(7,4) = 0.07;

% save('Richmond_results_compiled')


% save('Richmond_results_compiled')




%% detect gaps and prepare labeling

%% Richmond pump station
%load('Results3Richmond_skeleton_pump_station1_times_and_fval_run1')
%load('Results3Richmond_times_and_fval_run1')
%load('Results3van_zyl_times_and_fval_run1')
%load('Results5_Richmond_skeleton_original_times_and_fval_run1')
% load('Results5_Richmond_skeleton_original_times_and_fval_run1')
T5 = load('Results5Richmond_skeleton_pump_station1_times_and_fval_run1');
% T10 = load('Results10_pump_station1_appox_7');
% T10_2 = load('Results10_pump_station1_approx5');
T9 = load('Results9_Richmond_skeleton_pump_station1_times_and_fval_run1');
%fval

% T9.fval{7} =  T10.fval;
% T9.fval{8} =  NaN;
fval = cell2mat([T5.fval(:,1:3) T9.fval]);
fval = [ones(3,4)*NaN; fval];
% fval(7,4) = 13319.87

%time taken
% T9.time_taken{7} =  T10.time_taken;

% T9.time_taken{5} =  T10_2.time_taken;
% T9.time_taken{8} =  600;
%[T5.time_taken T9.time_taken]
time_taken = cell2mat([T5.time_taken(:,1:3) T9.time_taken]);
time_taken = [ones(3,4)*600; time_taken];
% time_taken(7,4) = 81.27;

%remaining Gap
% T9.Remaining_gap{8} =  Inf;
% T9.Remaining_gap{5} =  T10_2.Remaining_gap;
% T9.Remaining_gap{7} =  T10.Remaining_gap;
Remaining_gap = cell2mat([T5.Remaining_gap(:,1:3) T9.Remaining_gap]);
Remaining_gap = [ones(3,4)*Inf; Remaining_gap];
% Remaining_gap(7,4) = 0.07;
% save('Richmond_pump_station_results_compiled')

 %% Van zyl results
% T2 = load('Results2van_zyl_times_and_fval_run1.mat');
% T99 = load('Results99van_zyl_times_and_fval_run1.mat');
% time_taken = cell2mat([T2.time_taken(1:8,:); T99.time_taken] );
% fval = cell2mat([T2.fval(1:8,:);T99.fval]);
% Remaining_gap = cell2mat([T2.Remaining_gap(1:8,:);T99.Remaining_gap]);
% load('Richmond_results_CCWI_compiled')

h1 = figure('position',[100 100 700 300]);
subplot(1,2,1)
h_time = bar(time_taken);
title('Computational time','Fontsize',fontsize,'Interpreter','Latex')

h_leg = legend('N = 6','N = 12','N = 24','N = 48');
set(gca,'FontSize',fontsize)
set(gca,'YScale','log')
% set(gca,'XAxisLocation','Bottom')
%axis([0.4 8.6 0.1 1000]);
set(h_leg,'Location','SouthWest','Fontsize',fontsize,'Interpreter','Latex')
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Time taken (s)','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')
colormap('gray');

subplot(1,2,2)
h_cost = bar(fval);

title('Computed operating cost','Fontsize',fontsize,'Interpreter','Latex')

colormap('gray');
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Operating Cost in \textit{\textsterling}','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')

% h_time = legend('N = 6','N = 12','N = 24','N = 48');

% make legend:
% hleg = legend('N = 6','N = 12','N = 24');
% hleg = legend('N = 6','N = 12','N = 24','N = 48');


set(h_leg,'Location','SouthWest','Fontsize',fontsize,'Interpreter','Latex')
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Time taken (s)','Fontsize',fontsize,'Interpreter','Latex')
 set(gca,'yMinorGrid','on')

 
%  %% No solution label Richmond
subplot(1,2,2)
  str1 = 'No solutions in time limit';
  text(1,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
  text(2,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
  text(3,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
%str2 = % '$*$' '$\dagger$' $\ddagger$' 
%%% locations at (X-1).65, (X-1).9  X.1  X.275      
%text(5.4,700,str2,'Fontsize',fontsize,'Interpreter','Latex')

% set(h1,'Position',[813 291 350 400])
% set(gca,'Position',[0.1506 0.1363 0.7544 0.8]) %adjust sixes as needed: [x,y, width, height]



%% plot fval
figure('position',[0 0 500 350]);
h_cost = bar(fval);
colormap('gray');
 axis([0.4 12.6 0 Inf]);
% h_time = legend('N = 6','N = 12','N = 24','N = 48');
h_leg = legend('N = 6','N = 12','N = 24','N = 48');
set(gca,'FontSize',fontsize)
% set(gca,'YScale','log')
% set(gca,'XAxisLocation','Bottom')
% make legend:
% hleg = legend('N = 6','N = 12','N = 24');
% hleg = legend('N = 6','N = 12','N = 24','N = 48');

set(h_leg,'Location','SouthWest','Fontsize',fontsize,'Interpreter','Latex')
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Operating Cost  ($\mathsterling$)','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')
 
%  %% No solution label Richmond

%  str1 = 'No solution';
%  text(1,0.12,str1,'Fontsize',fontsize,'Rotation',90)
%str2 = % '$*$' '$\dagger$' $\ddagger$' 
%%% locations at (X-1).65, (X-1).9  X.1  X.275      
%text(5.4,700,str2,'Fontsize',fontsize,'Interpreter','Latex')

% set(h1,'Position',[813 291 350 400])
% set(gca,'Position',[0.1506 0.1363 0.7544 0.8]) %adjust sixes as needed: [x,y, width, height]
 
%% tabulated stuff

% OF_times = time_taken([1,7,9:12],:); % approximations 
% array2latextable(OF_times,'s')

