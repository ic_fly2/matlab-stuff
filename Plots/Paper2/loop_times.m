list = {'loop2','loop1','loop0'};
approx_number = 10;
list = {'van_zyl_D20','van_zyl_D60','van_zyl_D100','van_zyl_D140','van_zyl_D180','van_zyl_D220','van_zyl_D260','van_zyl_D300','van_zyl_D340','van_zyl_D380'};
N_set = [6 12 24 48];
for approx_number = [1,4,7]
    file_name = ['results_from_runs_approx_' num2str(approx_number)];
    for i_def = 1:length(list)
        def = list{i_def};        
        for i_N = 1:4
            N = N_set(i_N);
            [time_taken{i_def,i_N}, fval{i_def,i_N}, diff_H{i_def,i_N}, diff_Q{i_def,i_N},diff_Q_per{i_def,i_N} ,diff_H_per{i_def,i_N} Remaining_gap{i_def,i_N}  x{i_def,i_N} ] = experiment(def,N,approx_number,6);
            save(file_name)
        end
    end
    
    
end