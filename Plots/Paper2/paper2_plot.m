% % % make pretty plots for the paper
% % 
% %% pump plot (from the function)
% clear all; close all;
% fontsize = 10;
% a = -1;
% b = 0;
% c = 25;
% 
% qmax = max(roots([a b c]));
% q = linspace(0,qmax,200);
% for i = 1:length(q)
%     H = polyval([a b c],q);
% end
% H = H(H >= 0);
% q = q(1:length(H));
% Delta_H =  0.005*max(H);
% H_plus  = H + Delta_H;
% H_minus = H - Delta_H;
% 
% power = 3*q.^2+2*q+750;
% 
% k = 1;
% m = 1;
% c_temp = H_plus(1);
% Q_lim_index(1) = 1;
% C(1) = H_plus(1);
% m(1) = b;
% while Q_lim_index(k) < length(q)
%     
%     while  polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= H_minus(Q_lim_index:end)
%         m(k) = m(k)-0.01;
%         %         c_temp(k) = H_plus(Q_lim_index(k)) - q(Q_lim_index(k))*m(k);
%     end
%     Q_lim_index(k+1) = find( H_plus - polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= 0, 1, 'last' );
%     m(k+1) = m(k) + m(k)*0.01;
%     c_temp(k+1) = H_plus(Q_lim_index(k+1));%- q(Q_lim_index(k+1))*m(k+1);
%     k = k+1;
%     
% end
% 
% for i = 1:k-1
%     Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
%     H_lim(i) = Q_lim(i)*m(i) + C(i);
%     C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
% end
% 
% C(end) = [];
% m(end) = [];
% 
% 
% 
% 
% % plot stuff
% figure('position',[100 100 400 300]) %400 px wide
% hold on
% [AX,h11,h22] = plotyy(q,H,q,power,'plot');
% % make the plotyy right
%  set(AX(1),'YColor', [0 0 0]); set(AX(2),'YColor', [0 0 0]);
%  set(AX(2),'YTick',[500:100:1000]);set(AX(2),'YLim',[500 1000]);
%  set(h11,'LineStyle','--','LineWidth',2,'Color',[0 0 0])
%  set(h22,'LineStyle','-.','LineWidth',1,'Color',[0 0 0])
% % hatching
% x_beg = linspace(-max(H)*1.3,max(H)*1.3,40);
% m_diag = max(H)/qmax;
% for i = 1:length(x_beg)
%     x_diag = 0:100;
%     y_diag = x_diag*m_diag+x_beg(i);
%     plot(AX(1),x_diag,y_diag,'Color',[0.7 0.7 0.7],'Linewidth',0.05);
% end
% area(AX(1),q,H,'FaceColor','w','Linewidth',0.001)
% 
% % characteristic curve
% 
% 
% h1 = plot(AX(1),q,H,'k--','LineWidth',3);
% % piecwise linear
% for i = 1:k-2
%     x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
%     x(i,:) = linspace(0,qmax*1.1,100);
%     h3 = plot(AX(1),x(i,:),x(i,:)*m(i) + C(i),'k:','LineWidth',1);
% end
% 
% % simple
% x = 2:0.1:3;
% y = ones(1,length(x))*18.7;
% hold on;
% h4 = plot(AX(1),x,y,'k','LineWidth',1);
% % 
% hleg = legend([h1 h3 h4 h22],{'Characteristic Curve','Bounds of convex set','Fixed head','Power'} );
% %     axis([0 qmax 0 H(end)])
% %ylabel('Head /m','Fontsize',fontsize);
%  ylabel(AX(1),'Head /m','Fontsize',fontsize);
% ylabel(AX(2),'Power /kW','Fontsize',fontsize);
% xlabel('Volumeflow /m^3','Fontsize',fontsize)
% set(hleg,'Location','SouthWest','Fontsize',fontsize)
% set(AX(2),'Position',[0.13 0.15 0.715 0.815])
% set(gca,'FontSize',fontsize)

% save as pump_curve_fig4.eps
%set(AX(1),'Color',[1 1 1])
% 
% % saveas(gcf,'D:/Phd/My written stuff/Figures/pump_curve_fig.eps')
% 
% Pipe plot
% close all; clear all;
% D = 0.9;
% e = 0.001;
% L = 1000;
% vmax = 2;
% pmax = 10;
% plotting = 'on';
% 
% qmax = vmax*pi*(D^2)/4; % max flow
% 
% q = linspace(0,qmax*2,200);
% 
% for i = 1:length(q)
%      [ ~,~,~,H(i),K1(i),n(i) ] = DarcyWeisbachFactorCalc( D,q(i),e,L,'DW') ;
% end
% H(1) = 0;
% Delta_H =  0.001*max(H);
% H_plus  = H + Delta_H;
% H_minus = H - Delta_H;
% H_minus(H_minus <= 0) = 0;
% %% crude guessing method (bisection method) (optimisation not analytical)
% k = 1;
% m = 0.1;
% c_temp = 0;
% Q_lim_index(1) = 1; 
% 
% while q(Q_lim_index(k)) <= qmax
%     while m(k)*(q-q(Q_lim_index(k)))+c_temp(k) <= H_plus
%         m(k) = m(k)+0.01;
%     end
%     Q_lim_index(k+1) = find( m(k)*(q-q(Q_lim_index(k)))+c_temp(k) - H_minus > eps, 1, 'last' );
%     m(k+1) = m(k) + m(k)*0.3;
%     c_temp(k+1) = H_minus(Q_lim_index(k+1));
%     k = k+1;
% end
% C(1) = 0;
% for i = 1:k-1 
%     Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
%     H_lim(i) = Q_lim(i)*m(i) + C(i);
%     C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
% end
% 
% % shit qlim along
% Q_lim(length(Q_lim)+1) = 0;
% Q_lim =  circshift(Q_lim',1)'; % first value
% % Q_lim(length(Q_lim)+1) = ; % last value (max)
% if pmax < length(m)
%     error('Too many segments, increase the tolerarance')
% end

%% plot stuff

%         fig2 = figure;
% %     hatching
%     hold on;
% x_beg = linspace(-max(H)*1.1,max(H)*1.1,40);
% m_diag = max(H)/qmax;
% for i = 1:length(x_beg)
%     x_diag = 0:100;
%     y_diag = -x_diag*m_diag+x_beg(i);
%     plot(x_diag,y_diag,'k','Linewidth',0.5);
% end
% area(q,H,'FaceColor','w','Linewidth',0.001)
% 
%     hold on
%     h1 = plot(q,H,':k','LineWidth',2); %quadratic approximation
%     h2 = plot(q,H_minus,'k:'); %Allowed error
%     plot(q,H_plus,'k:')
%     for i = 1:k-1
%         x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
%         h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'k--','LineWidth',2);
%     end
%     
%     for i = 1:k-1
%         x(i,:) = linspace(0,pi*0.25*vmax*D^2,100);
%         plot_x = x(i,:);
%         plot_y = x(i,:)*m(i) + C(i);
%         h4 = plot(plot_x,plot_y,'k--','LineWidth',1); %convex constrained set
%         for j = 1:30
%             x_start = linspace(0,1.5,30);
%             plot_x_diag = linspace(x_start(j),2,100);
%             plot_y_diag = -(1.2/4.5)*plot_x_diag +x_start(j)*m(i) + C(i);
%             h5 = plot(plot_x_diag, plot_y_diag,'k','LineWidth',1);
%         end
%     end
%     
%     hleg = legend([h1 h2 h4],{'Quadratic approximation',['Allowed error ' num2str(Delta_H) ' m'],'Convex constrained set' } );
%     axis([0 pi*0.25*vmax*D^2 0 H(100)])
%     ylabel('Head loss /m','Fontsize',1.7*fontsize);
%     xlabel('Volumeflow /m^3','Fontsize',1.7*fontsize)
%     set(hleg,'Location','NorthWest','Fontsize',1.7*fontsize)
%     set(gca,'FontSize',1.7*fontsize)

% % saveas(gcf,'E:/Phd/My written stuff/Figures/pipe_curve1_fig.eps')
% 
% %% continous plot
% for i = 1:length(q)
%      [ ~,~,~,H(i),K1(i),n(i) ] = DarcyWeisbachFactorCalc( D,q(i),e,L,'DW') ;
% end
% H(1) = 0;
% Delta_H =  0.01*max(H);
% H_plus  = H + Delta_H;
% H_minus = H - Delta_H;
% H_minus(H_minus <= 0) = 0;
% %% crude guessing method (bisection method) (optimisation not analytical)
% k = 1;
% m = 0.1;
% c_temp = 0;
% Q_lim_index(1) = 1; 
% 
% while q(Q_lim_index(k)) <= qmax
%     while m(k)*(q-q(Q_lim_index(k)))+c_temp(k) <= H_plus
%         m(k) = m(k)+0.01;
%     end
%     Q_lim_index(k+1) = find( m(k)*(q-q(Q_lim_index(k)))+c_temp(k) - H_minus > eps, 1, 'last' );
%     m(k+1) = m(k) + m(k)*0.3;
%     c_temp(k+1) = H_minus(Q_lim_index(k+1));
%     k = k+1;
% end
% C(1) = 0;
% for i = 1:k-1 
%     Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
%     H_lim(i) = Q_lim(i)*m(i) + C(i);
%     C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
% end
% 
% % shit qlim along
% Q_lim(length(Q_lim)+1) = 0;
% Q_lim =  circshift(Q_lim',1)'; % first value
% % Q_lim(length(Q_lim)+1) = ; % last value (max)
% if pmax < length(m)
%     error('Too many segments, increase the tolerarance')
% end
% 
% % polynomial approximations
% P2 = polyfit(q,H,2)    ;
% q_all = [-fliplr(q) q(2:end)] ;
% H_all = [-fliplr(H) H(2:end)] ;
% 
% % allowed error
% 
%  h2 = plot(q,H_plus,'k:'); %Allowed error
%       plot(q,H_plus-2*H_plus(1),'k:')
%       plot(-q,-H_plus+2*H_plus(1),'k:')
%       plot(-q,-H_plus,'k:')
% 
% 
% 
% % figure;
% hold on;
% h5 =  plot(q_all,H_all,'k-','Linewidth',1); % real thing
% for i = 1:k-1
%     x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
%     h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'k--','LineWidth',2);
%     h4 = plot(-x(i,:),-x(i,:)*m(i) - C(i),'k--','LineWidth',2);
% end
% % modify q_all and H_all
% H_all(abs(q_all)>= qmax) = [];
% q_all(abs(q_all)>= qmax) = [];
% P3 = polyfit(q_all,H_all,3);
% h6 = plot(q_all,polyval(P3,q_all),'k-','LineWidth',2);
% h7 = plot(zeros(1,length(H_all)),H_all,'k',q_all,zeros(1,length(q_all)),'k');
% % hleg = legend([h5 h3 h6],{'D-W Solution','Piecewise approximation','Cubic approximation' } );
% hleg = legend([h1 h2 h4 h5 h3 h6],{'Quadratic approximation',['Allowed error ' num2str(round(Delta_H,2)) ' m'],'Convex constrained set' 'D-W Solution','Piecewise approximation','Cubic approximation' } );
% 
%     ylabel('Head loss /m','Fontsize',fontsize);
%     xlabel('Volumeflow /m^3','Fontsize',fontsize)
%     set(hleg,'Location','NorthWest','Fontsize',fontsize)
%     set(gca,'FontSize',fontsize)
%    axis([(-pi*0.25*vmax*D^2)/2 pi*0.25*vmax*D^2 -H(100)/2 H(100)])
% %    set(fig2, 'units', 'centimeters', 'pos', [0 0 15 15])
% 
% % saveas(gcf,'E:/Phd/My written stuff/Figures/pipe_curve2_fig.eps')

%% random data box plot
% X =  rand(100,7);
% X(:,1) = X(:,1)*0.2; 
% set = 2; X(:,set) = X(:,set)*0.25;
% set = 3; X(:,set) = X(:,set)*0.22;
% set = 4; X(:,set) = X(:,set)*0.27;
% set = 5; X(:,set) = X(:,set)*0.3;
% set = 6; X(:,set) = X(:,set)*0.35;
% set = 7; X(:,set) = X(:,set)*0.4;

% cd C:\Phd\MATLAB\Phd\Results\Approx_comparission
% def = 'van_zyl';
% N_set = [6 12 24 48];
% rep = 1;
% approx_number = 1;
% for i = 1:length(N_set)
%     N = N_set(i);
% pump_approx = 'Pump_quad';
% pipe_approx = 'pipe_quad';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
%  % quad + piece (are intigers or NLP more expensive)
% approx_number = 2;
% pump_approx = 'Pump_quad';
% pipe_approx = 'pipe_piece';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
% 
%  % convex + piece (How much cheaper is LC vs QC)
% approx_number = 3;
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_piece';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
% 
% 
%  % convex (MIQP fast and exact method?)
% approx_number = 4; 
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_convex';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%       load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
% 
% result.approx(approx_number).d = deviance;
%  % convex (MILP faster or slower thatr MIQP)
% approx_number = 5;
% pump_approx = 'Pump_convex';
% pipe_approx = 'pipe_convex';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
%  % Simplified pump (Can the pump be simplified even further)
% approx_number = 6;
% pump_approx = 'Pump_simple';
% pipe_approx = 'pipe_piece';
% result.approx(approx_number).d = deviance;
%  % simplified pump (Can pump and pipe be simplified to death)
% approx_number = 7;
% pump_approx = 'Pump_simple';
% pipe_approx = 'pipe_convex';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
%  % simplified pump (Can pump and pipe be simplified to death)
% approx_number = 8;
% pump_approx = 'Pump_quad_ineq'; 
% pipe_approx = 'pipe_quad_ineq';
% load(['deviance_'       def '_approx_' num2str(approx_number) '_N_' num2str(N) '_rep_' num2str(rep)]);
%         result.approx(approx_number).d = deviance;
% 
%         all = [];
%         for i = 1:8
%             part = result.approx(i).d;
%             part(length(part)+1:31) = NaN;
%             all = [all part];
%         end
%         all(:,9) = all(:,8);
%         all(:,4:8) = all(:,3:7);
%         all(:,3) = all(:,9) ;
%         all(:,9) = [];
%         boxplot(abs(all))
%         
%          axis([0.5 8.5 0 100])
% %         deviance = deviance.*(abs(deviance) <= 100); % "cheat" throw out redicoulous values
% %         deviance(  deviance == 0) =  [];
% %         boxplot(abs(deviance))
% %        
% 
% 
% xlabel('Approximation Method')
% ylabel('Deviation in Percent')
% ylabel('Deviation of hydraulic result (%)')
% xlabel('Approximation method')
% figure;
% end
