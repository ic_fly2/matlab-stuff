%% produce graphics
% collect all differences in flow speed
clear all; close all;
fontsize = 10;
h1 = figure('position',[100 100 350 300]); %400 px wide
% load(file_name)
cost = rand(3,10);
cost = [cost; zeros(1,length(cost))];
% time_taken = rand(1,10);
%% detect gaps and prepare labeling

%% time taken

% time_taken = cell2mat(time_taken);
h1 = bar(cost);
colormap('gray');
% axis([0.4 9.6 0.1 1000]);
% h_time = legend('N = 6','N = 12','N = 24','N = 48');
axis([0.5 4.8 0 1])
h2 = legend('1a','1b','1c','7a','7b','7c','8a','8b','8c','Sim');
set(gca,'XTickLabel', ['N = 12'; 'N = 24'; 'N = 48'; '      '])

set(gca,'FontSize',fontsize)
set(gca,'XAxisLocation','Bottom')
% hleg = legend('N = 6','N = 12','N = 24');
% h1 = legend('N = 6','N = 12','N = 24');
set(h2,'Location','SouthEast','Fontsize',fontsize)
xlabel('Time Steps')
ylabel('Cost in �')
 set(gca,'yMinorGrid','on')

 

