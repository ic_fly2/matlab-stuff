R = load('ResultsRichmond_skeleton_modified_times_and_fval_run_proper_06_19_15_23_27');
V = load('Resultsvan_zyl_times_and_fval_run_proper_06_19_15_18_34');

Diff_percentage_R = cell2mat(R.fval_diff_per([1 12 7 9 10 11],:));     
Diff_percentage_V = cell2mat(V.fval_diff_per([1 12 7 9 10 11],:));

min(Diff_percentage_V')'
min(Diff_percentage_R')'

nanmean(Diff_percentage_V,2)
nanmean(Diff_percentage_R,2)

OF_times= cell2mat(time_taken([1 12 7 9 10 11],:));     
array2latextable(OF_times,'s')