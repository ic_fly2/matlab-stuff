close all;
clear all;
% load('ResultsRichmond_skeleton_modified_times_and_fval_run_proper_06_19_15_23_27')
% fval = cell2mat(fval(1:8,:));
% fval(1,1) = fval(2,1); 

load('Resultsvan_zyl_times_and_fval_run_proper_06_19_15_18_34') % good stuff!!
fval = cell2mat(fval(1:8,:));
fval(1,1) = fval(2,1);
fval(8,:) = fval(8,:)/.75; % forgot the efficiency
fval(:,1) = fval(2,1);

% fval = cell2mat(Remaining_gap)*100;


h2 = figure('position',[100 100 290 290]);
h_cost = bar(fval);
colormap('gray');
fontsize= 12;
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
%ylabel('Operating Cost  [k\textit{\textsterling}]','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Optimality bound  [\%]','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on')


set(gca,'XTickLabel',{'1a' '2' '3' '4' '5' '6' '7a' '8'})
set(gca,'YTickLabel',{'0' '50' '100' '150' '200' }) %van zyl
% set(gca,'YTickLabel',{'0' '2' '4' '6' '8' '10'  '12' '14' '15'}) %Richmond

% legend stuff
hold on
n6 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'MarkerSize',10);
n12 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.3333 0.3333 0.3333],'MarkerSize',10);
n24 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.6667 0.6667 0.6667],'MarkerSize',10);
n48 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[1 1 1],'MarkerSize',10);
n6 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'MarkerSize',10);
n12 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.3333 0.3333 0.3333],'MarkerSize',10);
n24 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.6667 0.6667 0.6667],'MarkerSize',10);
n48 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[1 1 1],'MarkerSize',10);
% h_leg = legend([n6,n12,n24,n48 ],{'N=6','N=12','N=24','N=48'});
% set(h_leg, 'Position',[-0.0484    0.9251    1.0264    0.0787],'Orientation','horizontal')
% set(h_leg,'Fontsize',fontsize,'Interpreter','Latex')
% set(h_leg,'Box','off')
axis([0.4 8.6 0 max(max(fval))*1.1])
% axis([0.4 8.6 0 100])
%% No solution label Richmond
% str1 = 'No solutions in time limit';
% text(1.1,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% text(2.1,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% % text(3,200,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% str2 = 'Infeasible';
% text(8,200,str2,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')

% str1 = '$\infty$';
% text(1.15,5,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% text(2.15,5,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% text(3.18,5,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% str1 = 'No solutions in time limit';
% text(1.05,5,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')
% text(2.05,5,str1,'Fontsize',fontsize,'Rotation',90,'Interpreter','Latex')


