%% pump plot (from the function)
clear all; close all;
% fontsize = 10;
% a = -2.0833e+05;% -1; 
% b =  -583.3333;%0;
% c = 38;% 25;
% 
% qmax = max(roots([a b c]));
% q = linspace(0,qmax,200);
% for i = 1:length(q)
%     H = polyval([a b c],q);
% end
% H = H(H >= 0);
% q = q(1:length(H));
% Delta_H =  0.005*max(H);
% H_plus  = H + Delta_H;
% H_minus = H - Delta_H;
% 
% %power = 3*q.^2+2*q+750;
% 
% k = 1;
% m = 1;
% c_temp = H_plus(1);
% Q_lim_index(1) = 1;
% C(1) = H_plus(1);
% m(1) = b;
% while Q_lim_index(k) < length(q)
%     
%     while  polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= H_minus(Q_lim_index:end)
%         m(k) = m(k)-0.01;
%         %         c_temp(k) = H_plus(Q_lim_index(k)) - q(Q_lim_index(k))*m(k);
%     end
%     Q_lim_index(k+1) = find( H_plus - polyval([m(k) c_temp(k)],q-q(Q_lim_index(k))) >= 0, 1, 'last' );
%     m(k+1) = m(k) + m(k)*0.01;
%     c_temp(k+1) = H_plus(Q_lim_index(k+1));%- q(Q_lim_index(k+1))*m(k+1);
%     k = k+1;
%     
% end
% 
% for i = 1:k-1
%     Q_lim(i) =  (c_temp(i+1) - m(i+1)*q(Q_lim_index(i+1)) - c_temp(i) + m(i)*q(Q_lim_index(i)))/(m(i)-m(i+1));
%     H_lim(i) = Q_lim(i)*m(i) + C(i);
%     C(i+1) = H_lim(i) - Q_lim(i)*m(i+1);
% end
% 
% C(end) = [];
% m(end) = [];
% save('Pump_plot_data')
load('Pump_plot_data')
% plot stuff
q = q*10;
Q_lim = Q_lim *10;
qmax = qmax*10;
 m = m/10;
figure('position',[100 100 340 300]) %400 px wide
hold on

% power
q_power = linspace(0,0.122,4);
power = [10 11 14 20];
p_power = polyfit(q_power,power,4);


[AX h11 h22] = plotyy(q,H,q_power,power);
ylabel(AX(1),'Pressure Head [m]','Fontsize',fontsize,'Interpreter','Latex');
ylabel(AX(2),'Power [kW]','Fontsize',fontsize,'Interpreter','Latex');
xlabel(AX(1),'Volumetric flow rate [$m^3/s$]','Fontsize',fontsize,'Interpreter','Latex')
set(AX(1), 'Position',[0.12 0.14 0.75 0.78])
 % fix position
% h11 = plot(q,H,'LineStyle','-.','LineWidth',1,'Color',[0 0 0]);
% make the plotyy right
  set(AX(1),'YColor', [0 0 0]); set(AX(2),'YColor', [0 0 0]);
  set(h11,'LineStyle','--','LineWidth',2,'Color',[0 0 0])
  set(h22,'LineStyle','-.','LineWidth',1,'Color',[0 0 0])
  
  %set(AX(2),'YTick',[500:100:1000]);set(AX(2),'YLim',[0 50]);
  set(AX(1),'YLim',[0 60]);
  set(AX(2),'YLim',[0 60]);
  set(AX(1),'YTick',[0:10:40])
  
  
% hatching
x_beg = linspace(-max(H)*1.5,max(H)*1.5,40);
m_diag = max(H)/qmax;
for i = 1:length(x_beg)
    x_diag = linspace(0,max(q*1.3));
    y_diag = x_diag*m_diag+x_beg(i);
    hold on;
    plot(x_diag,y_diag,'Color',[0.7 0.7 0.7],'Linewidth',0.05);
end
area(q,H,'FaceColor','w','LineStyle','-','LineWidth',1);
%plot([0 0],[0 50],'k-','LineWidth',1 ); % fix axis
% characteristic curve

h0 = plot(q,H,'k','LineWidth',0.5); % Charcurve
h1 = plot(q,H,'k--','LineWidth',2); % quad
% piecwise linear
for i = 1:k-2
    x(i,:) = linspace(q(Q_lim_index(i)),q(Q_lim_index(i+1)+1),100);
    x(i,:) = linspace(0,qmax*1.1,100);
    h3 = plot(x(i,:),x(i,:)*m(i) + C(i),'k:','LineWidth',1);
end

% simple
x = 0.055:0.01:0.065;
y = ones(1,length(x))*27;
hold on;
h4 = plot(x,y,'k','LineWidth',2);
% 

hleg = legend([h0 h1 h3 h4 h22],...
    {'Characteristic curve','Quadratic approx.',...
    'Bounds of convex set \hspace{1mm}','Fixed head approx.','Power consumption'},...
    'Fontsize',fontsize,'Interpreter','Latex');



% ylabel('Pressure Head [m]','Fontsize',fontsize,'Interpreter','Latex');
% xlabel('Volumeflow [$m^3/s$]','Fontsize',fontsize,'Interpreter','Latex')
set(hleg,'Location','NorthEast')
% set(gca,'FontSize',fontsize)