clear all
%  load('ResultsRichmond_skeleton_modified_times_and_fval_run_proper_06_19_15_23_27')
load('Resultsvan_zyl_times_and_fval_run_proper_06_19_15_18_34') % good stuff!!
 time_taken = cell2mat(time_taken(1:8,:));


close all
h1 = figure('position',[100 100 290 290]);
%  subplot(1,2,1)
h_time = bar(time_taken);
% title('Computational time','Fontsize',fontsize,'Interpreter','Latex')
hold on;
fontsize = 12;
% h_leg = legend('N = 6','N = 12','N = 24','N = 48');
set(gca,'FontSize',fontsize)
set(gca,'YScale','log')
% set(gca,'XAxisLocation','Bottom')
% axis([0.4 8.6 0.1 1000]);
% set(h_leg,'Location','SouthEast','Fontsize',fontsize,'Interpreter','Latex') 
% set(h_leg, 'Position',[ 0.0029 0.800 0.9293 0.0733],'Orientation','horizontal')
xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
ylabel('Time taken (s)','Fontsize',fontsize,'Interpreter','Latex')
set(gca,'yMinorGrid','on','XTicklabel',{'1a','2','3','4','5','6','7a'})
set(gca,'XTickLabel',{'1a' '2' '3' '4' '5' '6' '7a' '8'})
colormap('gray');
n6 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0 0 0],'MarkerSize',10);
n12 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.3333 0.3333 0.3333],'MarkerSize',10);
n24 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[0.6667 0.6667 0.6667],'MarkerSize',10);
n48 = plot(-1,-1,'s','MarkerEdgeColor','k','MarkerFaceColor',[1 1 1],'MarkerSize',10);
% h_leg = legend([n6,n12,n24,n48 ],{'N=6','N=12','N=24','N=48'});
% set(h_leg, 'Position',[-0.0484    0.9251    1.0264    0.0787],'Orientation','horizontal')
% set(h_leg,'Fontsize',fontsize,'Interpreter','Latex')
% set(h_leg,'Box','off')
axis([0.4 8.6 0 1000])


% OF_times= cell2mat(time_taken([1 12 7 9 10 11],:));     
% array2latextable(OF_times,'s')    