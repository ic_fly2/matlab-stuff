T0 = load('Resultsloop0_times_and_fval_run_proper_06_20_15_16_47');
T1 = load('Resultsloop1_times_and_fval_run_proper_06_20_15_17_22');
T2 = load('Resultsloop2_times_and_fval_run_proper_06_20_15_18_17');

T0_1 = load('Resultsloop0_times_and_fval_run_proper_06_22_15_14_27');
T1_1 = load('Resultsloop1_times_and_fval_run_proper_06_22_15_14_27');
T2_1 = load('Resultsloop2_times_and_fval_run_proper_06_22_15_14_37');

time_takenAN1(1,:) = cell2mat(T0.time_taken(1,:));
time_takenAN1(2,:) = cell2mat(T1.time_taken(1,:));
time_takenAN1(3,:) = cell2mat(T2.time_taken(1,:));

gapAN1(1,:) = cell2mat(T0.Remaining_gap(1,:));
gapAN1(2,:) = cell2mat(T1.Remaining_gap(1,:));
gapAN1(3,:) = cell2mat(T2.Remaining_gap(1,:));

disp('AN1a')
disp(time_takenAN1')
disp(gapAN1')


time_takenAN7(1,:) = cell2mat(T0.time_taken(7,:));
time_takenAN7(2,:) = cell2mat(T1.time_taken(7,:));
time_takenAN7(3,:) = cell2mat(T2.time_taken(7,:));

gapAN7(1,:) = cell2mat(T0.Remaining_gap(7,:));
gapAN7(2,:) = cell2mat(T1.Remaining_gap(7,:));
gapAN7(3,:) = cell2mat(T2.Remaining_gap(7,:));

disp('AN7a')
disp(time_takenAN7')
disp(gapAN7')




time_takenAN8(1,:) = cell2mat(T0_1.time_taken(8,:));
time_takenAN8(2,:) = cell2mat(T1_1.time_taken(8,:));
time_takenAN8(3,:) = cell2mat(T2_1.time_taken(8,:));

gapAN8(1,:) = cell2mat(T0_1.Remaining_gap(8,:));
gapAN8(2,:) = cell2mat(T1_1.Remaining_gap(8,:));
gapAN8(3,:) = cell2mat(T2_1.Remaining_gap(8,:));

disp('AN8')
disp(time_takenAN8')
disp(gapAN8')

array2latextable(round(time_takenAN8',2),' s')


