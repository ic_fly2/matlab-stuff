clear all
close all
% load('Results2van_zyl_times_and_fval_run1.mat')
load('Resultsvan_zyl_times_and_fval_run_proper_06_19_15_18_34')

% load('Resultsvan_zyl_times_and_fval_run_proper_06_19_15_18_34')
% load('Results3Richmond_skeleton_pump_station1_times_and_fval_run1')

%bar plot: for CCWI
for i = 1:8
    diff_Q_comp = vertcat(diff_Q{i,:});
    Err(i) = nanmean(nanmean(abs(diff_Q_comp)));
end
 figure;  hold on; plot(Err,'+r')
 
 
 load('ResultsRichmond_skeleton_modified_times_and_fval_run_proper_06_19_15_23_27')

 for i = 1:8
    diff_Q_comp = vertcat(diff_Q{i,:});
    Err(i) = nanmean(nanmean(abs(diff_Q_comp)));
end
%  for i = 1:8
%     Err(i) = mean(mean(abs(diff_Q{i,1})));
% end
hold on; plot(Err,'+b')
% array2latextable(round(Err*100,2), '\%')
% % bar(Err)
% h1 = figure('position',[200  200 340 250]); fontsize = 10;hold on;
% % for paper:
% for i = 1:8
%     Diff_Q(i,:) = reshape(abs(diff_Q{i,1}),[1 numel(diff_Q{i,1})]);
% end
% 
% hb = boxplot(Diff_Q');
% 
% xlabel('Approximation Number','Fontsize',fontsize,'Interpreter','Latex')
% ylabel('Flow rate m$^3$/s','Fontsize',fontsize,'Interpreter','Latex')
% 
% h_err = plot([0:9],ones(1,10)*0.02,'k--'); % Van Zyl 10% error in demand estimate!
% h_dummy1 = plot(-5,2,'r+');
% h_dummy2 = plot([-5:-4],[2 2],'r-');
% axis([0.5 8.5 0 0.13]);
% hleg = legend([h_err h_dummy1 h_dummy2],{'Error in demand estimate','Outlier','Median value'},'Location','Northwest');
% set(hleg,'Interpreter','Latex')