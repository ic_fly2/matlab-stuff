clear all;
%% definition of numbers
N = 48; % number of intervals
t = 30; %length of one interval
h_t_min = 10; %minumum tank level
h_t_max = 22.8;
Q_p =85; %pump flow
A_t =176.7; %resevoir area
h1 = 10; %initial level

delta_h = Q_p*0.06/A_t; %change in resevoir height for pump inflow


%% vectors
Pe(1:12) = 28; Pe(13:20) = 85; Pe(21:34) = 48; Pe(35:40) =48; Pe(41:N) = 28; 
d_t = [60 58 58 57 56 57 58 60 60 61 62 70 80 90 110 107 105 104 103 102 100 99 105 98 90 89 87 86 85 84 83 82 81 82 82 83 84 84 75 70 74 73 72 71 65 62 60 53]; 

%bounds
ub(1:N) = t;
ub(N+1:2*N) = h_t_max;
lb(1:N) = 0;
lb(N+1:2*N) = h_t_min;

lb = lb';
ub =ub';

Aeq(1:N,1:N) = delta_h;
Aeq = tril(Aeq,0);
Aeq(1:N,N+1:N*2) = eye(N)*-1; 
Aeq(N+1,:) = 0;
Aeq(N+1,N*2) =1;


beq(1:N+1) = h1; 
for k = 1:N
    beq(k) = sum(d_t(1:k))*0.06/A_t - h1;
end

beq = beq';

h = zeros(N,1); 
f = [Pe h']

 A = Aeq; b = beq;
%% optimize
X = linprog(f,A,b,Aeq,beq,lb,ub)
